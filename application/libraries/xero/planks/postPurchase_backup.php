<?php
$this->reInitialize();
$brightpearlconfig = $this->ci->account1Config;
$brightpearlconfig = reset($brightpearlconfig);
foreach($this->accountDetails as $account2Id => $accountDetails){	
	$nominalMappings		= array();
	$logs					= array();
	$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1CustomFieldValue' => '','account2Id' => $account2Id))->result_array();
	foreach($nominalMappingTemps as $nominalMappingTemp){
		if(@!$nominalMappings[$nominalMappingTemp['account1NominalId']]){
			if(!$nominalMappingTemp['account1ChannelId']){
				$nominalMappings[$nominalMappingTemp['account1NominalId']] = $nominalMappingTemp;
			}
		}
		if($nominalMappingTemp['account1ChannelId']){
			$nominalMappings[$nominalMappingTemp['account1NominalId']]['channel'][strtolower($nominalMappingTemp['account1ChannelId'])] = $nominalMappingTemp;
		}
	}
	$config	= $this->accountConfig[$account2Id];
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	$datas = $this->ci->db->where_in('status',array('0'))->get_where('purchase_order',array('account2Id' => $account2Id))->result_array();
	if(!$datas){continue;}		
	$customerMappings = array();
	$customerMappingsTemps = $this->ci->db->select('customerId,createdCustomerId,email')->get_where('customers',array('account2Id' => $account2Id))->result_array();
	foreach($customerMappingsTemps as $customerMappingsTemp){
		$customerMappings[$customerMappingsTemp['customerId']] = $customerMappingsTemp;
	}
	$productMappings = array();
	$allSalesItemProductIdsTemps = $this->ci->db->select('productId')->get_where('purchase_item',array('status' => '0','productId > ' => '1004'))->result_array();
	if($allSalesItemProductIdsTemps){
		$allSalesItemProductIds = array_column($allSalesItemProductIdsTemps,'productId'); 
		if($allSalesItemProductIds){
			$this->postProducts($allSalesItemProductIds);
		}
	}	
	$allSalesCustomerTemps = $this->ci->db->select('customerId')->get_where('purchase_order',array('status' => '0','customerId <>' => ''))->result_array();
	if($allSalesCustomerTemps){
		$allSalesCustomer = array_column($allSalesCustomerTemps,'customerId'); 
		if($allSalesCustomer){
			$this->postCustomers($allSalesCustomer);
		}
	}
	$productMappingsTemps = $this->ci->db->select('productId,createdProductId,name,params,sku')->get_where('products',array('account2Id' => $account2Id))->result_array();
	foreach($productMappingsTemps as $productMappingsTemp){
		$productMappings[$productMappingsTemp['productId']] = $productMappingsTemp;
	}
	$taxMappings = array();
	$taxMappingsTemps = $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id,'orderType <> ' => '1'))->result_array(); 
	if($taxMappingsTemps){
		foreach($taxMappingsTemps as $taxMappingsTemp){
			$taxMappings[$taxMappingsTemp['account1TaxId']] = $taxMappingsTemp;
		}
	}
	$channelMappingsTemps = $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	if($channelMappingsTemps){
		foreach($channelMappingsTemps as $channelMappingsTemp){
			if(!isset($channelMappings[$channelMappingsTemp['account1ChannelId']])){
				$channelMappings[$channelMappingsTemp['account1ChannelId']] = $channelMappingsTemp;
			}
			if($channelMappingsTemp['account1WarehouseId']){
				$channelMappings[$channelMappingsTemp['account1ChannelId']]['warehouseDetails'][$channelMappingsTemp['account1WarehouseId']] = $channelMappingsTemp;
			}
		}
	}
	if($brightpearlconfig['AvalaraTaxRateEnabled'] == 1){
		$TaxData				= array();
		$TaxRateZipCodeDatas	= array();
		$TaxRateZipCode			= array();
		$updateOrder			= 100;
		$TaxRateZipCode			= $this->ci->db->get_where('avalara_taxRates',array('account2Id' => $account2Id))->result_array();
		if($TaxRateZipCode){
			foreach($TaxRateZipCode as $TaxRateZipCodeData){
				$TaxRateZipCodeDatas[$TaxRateZipCodeData['TaxName']]	= $TaxRateZipCodeData;
			}
		}
		
		$url					= '2.0/TaxRates';
		$this->initializeConfig($account2Id, 'GET', $url);
		$results				= $this->getCurl($url, 'GET', '', 'json', $account2Id)[$account2Id];
		
		if($results['TaxRates']['TaxRate']){
			foreach($results['TaxRates']['TaxRate'] as $TaxRateData){
				if($TaxRateData['Status'] == 'ACTIVE'){
					if(!$TaxRateZipCodeDatas[$TaxRateData['Name']]){
						$TaxData[]	= array(
							'account1Id'	=>	$accountDetails['account1Id'],
							'account2Id'	=>	$account2Id,
							'TaxName'		=>	$TaxRateData['Name'],
							'TaxType'		=>	$TaxRateData['TaxType'],
							'TaxCode'		=>	$TaxRateData['ReportTaxType'],
							'TaxRate'		=>	$TaxRateData['EffectiveRate'],
						);
					}
					else{
						continue;
					}
				}
				else{
					continue;
				}
			}
		}
		if($TaxData){
			$TaxInsertDataBatch	= array_chunk($TaxData,$updateOrder,true);
			foreach($TaxInsertDataBatch as $TaxData){
				$this->ci->db->insert_batch('avalara_taxRates', $TaxData);
			}
		}
	}
	if($datas){				
		foreach($datas as $orderDatas){
			if($orderDatas['createOrderId']){continue;}
			$config1 = $this->ci->account1Config[$orderDatas['account1Id']];
			$orderId = $orderDatas['orderId'];
			$rowDatas = json_decode($orderDatas['rowData'],true);
			$channelId = $rowDatas['assignment']['current']['channelId'];
			$createdRowData = json_decode($orderDatas['createdRowData'],true);
			$billAddress 	=  $rowDatas['parties']['billing'];
			$shipAddress 	=  $rowDatas['parties']['delivery'];
			$orderCustomer 	=  $rowDatas['parties']['supplier'];
			if(@!$customerMappings[$orderCustomer['contactId']]['createdCustomerId']){
				continue;
			}
			$missingSkus = array();$InvoiceLineAdd = array();$invoiceLineCount = 0;$productCreateIds = array();
			$TaxType				= '';
			$isAvalaraTaxApplicable	= 0;
			$XeroTaxType			= '';
			$TaxComponents			= array();
			$InsertTaxResponse		= array();
			$InsertZipCodeInfo		= array();
			$TaxRatefromAvalara		= array();
			$PostalCode				= $rowDatas['parties']['delivery']['postalCode'];
			$CountryIsoCode			= $rowDatas['parties']['delivery']['countryIsoCode'];
			foreach($rowDatas['orderRows'] as $rowId => $orderRows){
				if($brightpearlconfig['AvalaraTaxRateEnabled'] == 1){
					if($orderRows['productId'] > 1001){
						if(($orderRows['rowValue']['taxCode'] == '-')&&($orderRows['rowValue']['rowTax']['value'] > 0)){
							$isAvalaraTaxApplicable	= 1;
						}
					}
				}
			}
			if($isAvalaraTaxApplicable == 1){
				$TaxRateZipCode	= $this->ci->db->get_where('avalara_zipcode_taxinfo',array('zipcode' => $PostalCode))->row_array();
				if($TaxRateZipCode){
					$TaxName	= $TaxRateZipCode['TaxName'];
				}
				if($TaxName){
					$TaxTypeData	= $this->ci->db->get_where('avalara_taxRates',array('TaxName' => $TaxName))->row_array();
					if($TaxTypeData){
						$TaxType	= $TaxTypeData['TaxType'];
					}
				}
				if(!$TaxType){
					$TaxRatefromAvalara	= $this->fetchAvalaraTaxRate($CountryIsoCode, $PostalCode);
				}
			}
			if($TaxRatefromAvalara){
				$TaxTypeData	= $this->ci->db->get_where('avalara_taxRates',array('account2Id' => $account2Id))->result_array();
				$XeroTaxName	= array();
				$AvalaraTaxRate	= $TaxRatefromAvalara['totalRate']*100;
				$XeroTaxName	= $TaxRatefromAvalara['rates'][0]['name']; 
				$XeroTaxName	= explode(" ",$XeroTaxName);
				$XeroTaxName	= $XeroTaxName[0].' Tax '.$AvalaraTaxRate.'%';
				
				foreach($TaxTypeData as $TaxTypeDatass){
					if($TaxTypeDatass['TaxName'] == $XeroTaxName){
						$TaxType	= $TaxTypeDatass['TaxType'];
						break;
					}
				}
				if($TaxType){
					$InsertZipCodeInfo	= array(
						'zipcode'	=> $PostalCode,
						'TaxName'	=> $XeroTaxName,
						'params'	=> json_encode($TaxRatefromAvalara),
					);
					$this->ci->db->insert('avalara_zipcode_taxinfo', $InsertZipCodeInfo);
				}
				else{
					if($TaxRatefromAvalara['rates']){
						foreach($TaxRatefromAvalara['rates'] as $rate){
							$TaxComponents[]	= array(
								"Name"				=> $rate['name'],
								"Rate"				=> ($rate['rate']*100),
								"IsCompound"		=> false,
								"IsNonRecoverable"	=> false,
							);
						}
					}
					$TaxRequest	= array(
						'Name'			=> $XeroTaxName,
						'Rate'			=> $AvalaraTaxRate,
						'Status'		=> 'ACTIVE',
						'TaxComponents'	=> $TaxComponents,
					);
					$InsertZipCodeInfo	= array(
						'zipcode'	=> $PostalCode,
						'TaxName'	=> $XeroTaxName,
						'params'	=> json_encode($TaxRatefromAvalara),
					);
					$TaxRateUrl		= '2.0/TaxRates';
					$this->initializeConfig($account2Id, 'POST', $TaxRateUrl);
					$TaxResponse	= $this->getCurl($TaxRateUrl, 'POST', json_encode($TaxRequest), 'json', $account2Id)[$account2Id];
					if(strtolower($TaxResponse['Status']) == 'ok'){
						$CreatedData	= array(
							'Request'		=>	$TaxRequest,
							'Response'		=>	$TaxResponse,
						);
						$InsertTaxResponse	= array(
							'account1Id'		=>	$accountDetails['account1Id'],
							'account2Id'		=>	$account2Id,
							'TaxName'			=>	$TaxResponse['TaxRates']['TaxRate']['Name'],
							'TaxType'			=>	$TaxResponse['TaxRates']['TaxRate']['TaxType'],
							'TaxCode'			=>  '',
							'TaxRate'			=>	$TaxResponse['TaxRates']['TaxRate']['EffectiveRate'],
						);
						$this->ci->db->insert('avalara_zipcode_taxinfo', $InsertZipCodeInfo);
						$this->ci->db->insert('avalara_taxRates', $InsertTaxResponse);
						$TaxType	= $TaxResponse['TaxRates']['TaxRate']['TaxType'];
					}
				}
			}
			foreach($rowDatas['orderRows'] as $rowId => $orderRows){
				if($orderRows['productId'] > 1001){
					$productId = $orderRows['productId'];
					if(@!$productMappings[$productId]['createdProductId']){
						$missingSkus[] = $orderRows['productSku'];
						continue;
					} 
					$ItemRefName = $productMappings[$productId]['sku'];
					$ItemRefValue = $productMappings[$productId]['sku'];
				}
				else{
					if($orderRows['rowValue']['rowNet']['value'] >= 0){
						$ItemRefValue = $config['genericSku'];
						$ItemRefName  = $orderRows['productName'];
						if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){ 
							$ItemRefValue = $config['shippingItem'];
							$ItemRefName  = 'Shipping';
						}
					}
					else if($orderRows['rowValue']['rowNet']['value'] < 0){
						$ItemRefValue = $config['discountItem'];
						$ItemRefName  = 'Discount Item';
					}
					else{
						continue; 
					}
				}
				$productId = $orderRows['productId'];
				$params = @$productMappings[$productId]['params'];
				$params = json_decode($params,true);				
				$taxId = $config['salesNoTaxCode'];
				if(@$taxMappings[$orderRows['rowValue']['taxClassId']]){
					$taxId = $taxMappings[$orderRows['rowValue']['taxClassId']]['account2TaxId'];
				}
				$AssetAccountRef   = $config['AssetAccountRef'];					
				$ExpenseAccountRef = $config['ExpenseAccountRef'];
				if($config['InventoryManagementEnabled'] == true){
					$AssetAccountRef = $config['InventoryManagementProductPurchaseNominalCode'];
					$ExpenseAccountRef = $config['InventoryManagementProductPurchaseNominalCode'];
					if(($params['id']) && (!$params['stock']['stockTracked'])){
						$AssetAccountRef = $config['ExpenseAccountRef'];
						$ExpenseAccountRef = $config['ExpenseAccountRef'];
					}
				}
				
				if(@isset($nominalMappings[$orderRows['nominalCode']]['account2NominalId'])){
					$ExpenseAccountRef = $nominalMappings[$orderRows['nominalCode']]['account2NominalId'];
					$nominalMapped = $nominalMappings[$orderRows['nominalCode']];
					if($channelId){
						if(isset($nominalMapped['channel'])){
							if(isset($nominalMapped['channel'][strtolower($channelId)])){
								$ExpenseAccountRef = $nominalMapped['channel'][strtolower($channelId)]['account2NominalId'];
							}
						}
					}
				}
				if(@isset($nominalMappings[$orderRows['nominalCode']]['account2NominalId'])){
					$AssetAccountRef = $nominalMappings[$orderRows['nominalCode']]['account2NominalId'];
					$nominalMapped = $nominalMappings[$orderRows['nominalCode']];
					if($channelId){
						if(isset($nominalMapped['channel'])){
							if(isset($nominalMapped['channel'][strtolower($channelId)])){
								$AssetAccountRef = $nominalMapped['channel'][strtolower($channelId)]['account2NominalId'];
							}
						}
					}
				}		
				$AccountCode	= $AssetAccountRef;
				if($params){
					$params	= json_decode($params,true);
					if(($params['id']) && (!$params['stock']['stockTracked'])){
						$AccountCode	= $ExpenseAccountRef;
					}
				}
				
				$UnitAmount	= 0.00;
				if($orderRows['rowValue']['rowNet']['value'] > 0){
					$UnitAmount	= $orderRows['rowValue']['rowNet']['value'] / $orderRows['quantity']['magnitude'];
				}
				$InvoiceLineAdd[$invoiceLineCount] = array(
					//'ItemCode'	=> $orderRows['productSku'],
					'ItemCode' 		=> $ItemRefValue, 
					'Description' 	=> $orderRows['productName'],
					'Quantity' 		=> (int)$orderRows['quantity']['magnitude'],
					'UnitAmount' 	=> sprintf("%.4f",$UnitAmount),
					'TaxType' 		=> $taxId,
					'AccountCode' 	=> $AccountCode,
					'TaxAmount' 	=> sprintf("%.4f",$orderRows['rowValue']['rowTax']['value']),
					'LineAmount' 	=> sprintf("%.4f",$orderRows['rowValue']['rowNet']['value']),
				);
				if(($orderRows['rowValue']['taxCode'] == '-') && ($orderRows['rowValue']['rowTax']['value'] > 0)){
					if($TaxType){
						$InvoiceLineAdd[$invoiceLineCount]['TaxType']	= $TaxType;
					}
				}
				$invoiceLineCount++;
			}
			if($missingSkus){
				$missingSkus = array_unique($missingSkus);
				$this->ci->db->update('purchase_order',array('message' => 'Missing sku : '.implode(",",$missingSkus)),array('orderId' => $orderId));
				continue;
			}
			$DeliveryDate	= date('Y-m-d');$taxDate = date('Y-m-d');
			if(@$rowDatas['delivery']['deliveryDate']){
				$DeliveryDate	= date('Y-m-d',strtotime($rowDatas['delivery']['deliveryDate']));
			}
			if(@$rowDatas['invoices']['0']['invoiceReference']){
				$taxDate	= date('Y-m-d',strtotime($rowDatas['invoices']['0']['taxDate']));
			}
			$dueDate	= date('Y-m-d');
			if(@$rowDatas['invoices']['0']['dueDate']){
				$dueDate	= date('Y-m-d',strtotime($rowDatas['invoices']['0']['dueDate']));
			}
			$invoiceRef	= $orderId;
			if(@$rowDatas['invoices']['0']['invoiceReference']){
				$invoiceRef	= @$rowDatas['invoices']['0']['invoiceReference'];
			}
			$request = array(
				'Type' 			=> 'ACCPAY', // ACCREC - Create purchase order, ACCPAY - Create bill
				'Contact' 		=> array(
					'ContactID' => $customerMappings[$orderCustomer['contactId']]['createdCustomerId'],
					'Addresses' => array(
						array(
							'AddressType' 	=> 'DELIVERY',
							'AddressLine1' 	=> @$shipAddress['addressLine1'],
							'AddressLine2' 	=> @$shipAddress['addressLine2'],
							'City' 			=> @$shipAddress['addressLine3'],
							'Region' 		=> @$shipAddress['addressLine4'],
							'PostalCode'	=> @$shipAddress['postalCode'],
							'Country' 		=> @$shipAddress['countryIsoCode'],
						),
					),
				),
				'InvoiceNumber'			=> $invoiceRef,
				'Reference'				=> $orderId,
				'Status'				=> 'AUTHORISED', 
				'CurrencyCode'			=> $rowDatas['currency']['orderCurrencyCode'],			
				'CurrencyRate'			=> ($rowDatas['currency']['exchangeRate'])?($rowDatas['currency']['exchangeRate']):1,		
				'DeliveryAddress'		=> implode(",",array_filter(array_unique($shipAddress))),	 					
				'Telephone'				=> $shipAddress['currency']['telephone'],						
				'Date'					=> @$taxDate,
				'dueDate'				=> @$dueDate,
				'LineItems' 			=> $InvoiceLineAdd
			);
			if($config['defaultCurrency'] != $config1['currencyCode']){
				unset($request['CurrencyRate']);
			}
			$url		= '2.0/Invoices';
			$this->initializeConfig($account2Id, 'POST', $url);
			$results	= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
			$createdRowData					= array(
				'Request data : '	=> $request,
				'Response data : '	=> $results,
			);
			$logs[$orderDatas['orderId']]	= array(
				'Request data : '	=> $request,
				'Response data : '	=> $results,
			);
			$this->ci->db->update('purchase_order',array('createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
			if((strtolower($results['Status']) == 'ok') && (isset($results['Invoices']['Invoice']['InvoiceID']))){
				$this->ci->db->update('purchase_order',array('status' => '1','invoiceRef' => $invoiceRef,'createOrderId' => $results['Invoices']['Invoice']['InvoiceID']),array('id' => $orderDatas['id']));
			}
		}
	}
	if($logs){
		foreach($logs as $orderId => $log){
			$path	= FCPATH.'logs'.DIRECTORY_SEPARATOR . $accountDetails['account1Id'].DIRECTORY_SEPARATOR . 'po'.DIRECTORY_SEPARATOR;
			if(!is_dir($path)){
				mkdir($path,0777,true);
				chmod(dirname($path), 0777); 
			}
			file_put_contents($path. $orderId.'.logs',"\n\n BP Log Added On : ".date('c')." \n". json_encode($log),FILE_APPEND);
		}
	}
}
$this->fetchPurchasePayment();
?>