<?php 
$this->reInitialize(); 
$brightpearlconfig	= $this->ci->account1Config;
$brightpearlconfig	= reset($brightpearlconfig);
foreach($this->accountDetails as $account2Id => $accountDetails){
	$logs					= array();
	$nominalMappings		= array();
	$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1CustomFieldValue' => '','account2Id' => $account2Id))->result_array();
	foreach($nominalMappingTemps as $nominalMappingTemp){
		if(@!$nominalMappings[$nominalMappingTemp['account1NominalId']]){
			if(!$nominalMappingTemp['account1ChannelId']){
				$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
		}
		if($nominalMappingTemp['account1ChannelId']){
			$nominalMappings[$nominalMappingTemp['account1NominalId']]['channel'][strtolower($nominalMappingTemp['account1ChannelId'])]	= $nominalMappingTemp;
		}
	}
	$config	= $this->accountConfig[$account2Id];
	$nominalCodeForShipping	= explode(",",$config['nominalCodeForShipping']); 
	if($brightpearlconfig['UnInvoicingEnabled'] == 1){
		if($orgObjectId){
			$orderQuery	= ' AND orderId in ('.$orgObjectId.')';
		}
		$dataQuery	= 'SELECT * FROM sales_order where (status = 0 OR uninvoiced = 1)';
		if($orderQuery){
			$dataQuery	.=  $orderQuery ;
		}
		$dataQuery	.= 'AND account2Id ='. $account2Id;
		$datas		= $this->ci->db->query($dataQuery)->result_array();
	}
	else{
		if($orgObjectId){
			$this->ci->db->where_in('orderId',$orgObjectId);
		}
		$datas	= $this->ci->db->where_in('status',array('0'))->get_where('sales_order',array('account2Id' => $account2Id))->result_array();	
	}
	$allSalesItemProductIdsTemps	= $this->ci->db->select('productId')->get_where('sales_item',array('status' => '0','productId > ' => '1001'))->result_array();
	if($allSalesItemProductIdsTemps){
		$allSalesItemProductIds	= array_column($allSalesItemProductIdsTemps,'productId'); 
		if($allSalesItemProductIds){
			$this->postProducts($allSalesItemProductIds);
		} 
	}
	$allSalesCustomerTemps	= $this->ci->db->select('customerId')->get_where('sales_order',array('status' => '0','customerId <>' => ''))->result_array();
	if($allSalesCustomerTemps){
		$allSalesCustomer	= array_column($allSalesCustomerTemps,'customerId'); 
		if($allSalesCustomer){
			$this->postCustomers($allSalesCustomer); 
		}
	}
	$productMappings		= array();
	$productMappingsTemps	= $this->ci->db->select('productId,createdProductId,name,sku')->get_where('products',array('account2Id' => $account2Id))->result_array();
	foreach($productMappingsTemps as $productMappingsTemp){
		$productMappings[$productMappingsTemp['productId']]		= $productMappingsTemp;
	}	
	$customerMappings		= array();
	$customerMappingsTemps	= $this->ci->db->select('customerId,createdCustomerId,email')->get_where('customers',array('account2Id' => $account2Id))->result_array();
	foreach($customerMappingsTemps as $customerMappingsTemp){
		$customerMappings[$customerMappingsTemp['customerId']]	= $customerMappingsTemp;
	}
	$taxMappings			= array();
	$taxMappingsTemps		= $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id,'orderType <>' => '2'))->result_array();
	if($taxMappingsTemps){ 
		foreach($taxMappingsTemps as $taxMappingsTemp){
			$taxMappings[$taxMappingsTemp['account1TaxId']]		= $taxMappingsTemp;
		}
	}
	$channelMappings		= array();
	$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	if($channelMappingsTemps){
		foreach($channelMappingsTemps as $channelMappingsTemp){
			if(!isset($channelMappings[$channelMappingsTemp['account1ChannelId']])){
				$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
			}
			if($channelMappingsTemp['account1WarehouseId']){
				$channelMappings[$channelMappingsTemp['account1ChannelId']]['warehouseDetails'][$channelMappingsTemp['account1WarehouseId']]	= $channelMappingsTemp;
			}
		}
	}
	if($brightpearlconfig['AvalaraTaxRateEnabled'] == 1){
		$TaxData				= array();
		$TaxRateZipCodeDatas	= array();
		$TaxRateZipCode			= array();
		$updateOrder			= 100;
		$TaxRateZipCode			= $this->ci->db->get_where('avalara_taxRates',array('account2Id' => $account2Id))->result_array();
		if($TaxRateZipCode){
			foreach($TaxRateZipCode as $TaxRateZipCodeData){
				$TaxRateZipCodeDatas[$TaxRateZipCodeData['TaxName']]	= $TaxRateZipCodeData;
			}
		}
		$url					= '2.0/TaxRates';
		$this->initializeConfig($account2Id, 'GET', $url);
		$results				= $this->getCurl($url, 'GET', '', 'json', $account2Id)[$account2Id];
		if($results['TaxRates']['TaxRate']){
			foreach($results['TaxRates']['TaxRate'] as $TaxRateData){
				if($TaxRateData['Status'] == 'ACTIVE'){
					if(!$TaxRateZipCodeDatas[$TaxRateData['Name']]){
						$TaxData[]	= array(
							'account1Id'	=>	$accountDetails['account1Id'],
							'account2Id'	=>	$account2Id,
							'TaxName'		=>	$TaxRateData['Name'],
							'TaxType'		=>	$TaxRateData['TaxType'],
							'TaxCode'		=>	$TaxRateData['ReportTaxType'],
							'TaxRate'		=>	$TaxRateData['EffectiveRate'],
						);
					}
					else{
						continue;
					}
				}
				else{
					continue;
				}
			}
		}
		if($TaxData){
			$TaxInsertDataBatch	= array_chunk($TaxData,$updateOrder,true);
			foreach($TaxInsertDataBatch as $TaxData){
				$this->ci->db->insert_batch('avalara_taxRates', $TaxData);
			}
		}
	}
	if($datas){				
		foreach($datas as $orderDatas){
			// UNINVOICING CODE STARTS //// UNINVOICING CODE STARTS //// UNINVOICING CODE STARTS //// UNINVOICING CODE STARTS //
			if($orderDatas['uninvoiced'] == 1){
				if(!$orderDatas['paymentDetails']){
					$uninvoiceCount	= $orderDatas['uninvoiceCount'];
					$Request		= array(
						"InvoiceID"		=>	$orderDatas['createOrderId'],
						"Status"		=>	"VOIDED",
					);
					$url			= '2.0/Invoices';
					$this->initializeConfig($account2Id, 'POST', $url);
					$results		= $this->getCurl($url, 'POST', json_encode($Request), 'json', $account2Id)[$account2Id];
					$createdRowData	= array(
						'Request data 	: ' => $Request,
						'Response data 	: ' => $results,
					);
					$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
					if((strtolower($results['Status']) == 'ok') && (isset($results['Invoices']['Invoice']['InvoiceID']))){
						$uninvoiceCount++;
						$this->ci->db->update('sales_order',array('status' => '0','uninvoiced' => '2','createOrderId' => '', 'invoiceRef' => '', 'uninvoiceCount' => $uninvoiceCount),array('id' => $orderDatas['id']));
					}
				}
 				else if($orderDatas['paymentDetails']){
					$uninvoiceCount	= $orderDatas['uninvoiceCount'];
					$payment_datas	= json_decode($orderDatas['paymentDetails'],true);
					foreach($payment_datas as $paymemt_key => $payment_data){
						if(strlen($paymemt_key) > 20){
							$paykeys[]	= $paymemt_key;
							unset($payment_datas[$paymemt_key]);
						}
					}
					foreach($paykeys as $DeletePaymentID){
						$DeletePaymentRequest	= array(
							"PaymentID"				=>	$DeletePaymentID,
							"Status"				=>	"DELETED",
						);
						$url			= '2.0/Payments';
						$this->initializeConfig($account2Id, 'POST', $url);
						$DeleteResponse	= $this->getCurl($url, 'POST', json_encode($DeletePaymentRequest), 'json', $account2Id)[$account2Id];
					}
					$Request	= array(
						"InvoiceID"	=>	$orderDatas['createOrderId'],
						"Status"	=>	"VOIDED",
					);
					$url			= '2.0/Invoices';
					$this->initializeConfig($account2Id, 'POST', $url);$this->initializeConfig($account2Id, 'POST', $url);
					$results		= $this->getCurl($url, 'POST', json_encode($Request), 'json', $account2Id)[$account2Id];
					$createdRowData	= array(
						'Request data 	: '	=> $Request,
						'Response data 	: ' => $results,
					);
					$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
					if((strtolower($results['Status']) == 'ok') && (isset($results['Invoices']['Invoice']['InvoiceID']))){
						$uninvoiceCount++;
						$this->ci->db->update('sales_order',array('status' => '0','createOrderId' => '','uninvoiced' => '2', 'isPaymentCreated' => '0', 'sendPaymentTo' => '', 'paymentDetails' => '', 'invoiceRef' => '','uninvoiceCount' => $uninvoiceCount),array('id' => $orderDatas['id']));
					}
				}
				continue;
			}
			// UNINVOICING CODE ENDS //// UNINVOICING CODE ENDS //// UNINVOICING CODE ENDS //// UNINVOICING CODE ENDS //
			if($orderDatas['createOrderId']){
				continue;
			}				
			$config1		= $this->ci->account1Config[$orderDatas['account1Id']];
			$orderId		= $orderDatas['orderId'];
			$rowDatas		= json_decode($orderDatas['rowData'],true);
			$channelId		= $rowDatas['assignment']['current']['channelId'];
			$warehouseId	= $rowDatas['warehouseId'];
			$createdRowData	= json_decode($orderDatas['createdRowData'],true);
			$billAddress 	= $rowDatas['parties']['billing'];
			$shipAddress 	= $rowDatas['parties']['delivery'];
			$orderCustomer 	= $rowDatas['parties']['customer'];
			$uninvoiceCount = $orderDatas['uninvoiceCount'];
			if(@!$customerMappings[$orderCustomer['contactId']]['createdCustomerId']){
				continue;
			}
			$missingSkus					= array();$invoiceLineCount			= 0;$itemDiscountTax	= 0;
			$productCreateIds				= array();$totalItemDiscount		= 0;$orderTaxId			= '';
			$InvoiceLineAdd					= array();$isDiscountCouponAdded	= 0;$orderTaxAmount		= 0;
			$productUpdateNominalCodeInfos	= array();$isDiscountCouponTax		= 0;$discountCouponAmt	= 0;
			$linNumber	
			
			$TaxType				= '';
			$isAvalaraTaxApplicable	= 0;
			$XeroTaxType			= '';
			$TaxComponents			= array();
			$InsertTaxResponse		= array();
			$InsertZipCodeInfo		= array();
			$TaxRatefromAvalara		= array();
			$PostalCode				= $rowDatas['parties']['delivery']['postalCode'];
			$CountryIsoCode			= $rowDatas['parties']['delivery']['countryIsoCode']; 
			
			foreach($rowDatas['orderRows'] as $rowId => $orderRows){
				if($orderRows['productId'] >= 1001){
					if(substr_count(strtolower($orderRows['productName']),'coupon')){
						if($orderRows['nominalCode'] == $config['nominalCodeForDiscount']){
							$isDiscountCouponAdded	= 1;
						}
					}
				}
				if($brightpearlconfig['AvalaraTaxRateEnabled'] == 1){
					if($orderRows['productId'] > 1001){
						if(($orderRows['rowValue']['taxCode'] == '-') && ($orderRows['rowValue']['rowTax']['value'] > 0)){
							$isAvalaraTaxApplicable	= 1;
						}
					}
				}
			}
			if($isAvalaraTaxApplicable == 1){
				$TaxRateZipCode	= $this->ci->db->get_where('avalara_zipcode_taxinfo',array('zipcode' => $PostalCode))->row_array();
				if($TaxRateZipCode){
					$TaxName	= $TaxRateZipCode['TaxName'];
				}
				if($TaxName){
					$TaxTypeData	= $this->ci->db->get_where('avalara_taxRates',array('TaxName' => $TaxName))->row_array();
					if($TaxTypeData){
						$TaxType	= $TaxTypeData['TaxType'];
					}
				}
				if(!$TaxType){
					$TaxRatefromAvalara	= $this->fetchAvalaraTaxRate($CountryIsoCode, $PostalCode);
				}
			}
			if($TaxRatefromAvalara){
				$TaxTypeData	= $this->ci->db->get_where('avalara_taxRates',array('account2Id' => $account2Id))->result_array();
				$XeroTaxName	= array();
				$AvalaraTaxRate	= sprintf("%.4f",($TaxRatefromAvalara['totalRate']*100));
				$XeroTaxName	= $TaxRatefromAvalara['rates'][0]['name']; 
				$XeroTaxName	= explode(" ",$XeroTaxName);
				$XeroTaxName	= $XeroTaxName[0].' Tax '.$AvalaraTaxRate.'%';
				$avTaxPartName 	= strtolower($XeroTaxName[0]).' ';				
				foreach($TaxTypeData as $TaxTypeDatass){
					$xeroSaveTaxRate = sprintf("%.4f",$$TaxTypeDatass['TaxRate']);
					if((substr_count(strtolower($TaxTypeDatass['TaxName']),$avTaxPartName)) && ($xeroSaveTaxRate == $AvalaraTaxRate)){
						$TaxType	= $TaxTypeDatass['TaxType'];
						break;						
					}					
				}
				if($TaxType){
					$InsertZipCodeInfo	= array(
						'zipcode'	=> $PostalCode,
						'TaxName'	=> $XeroTaxName,
						'params'	=> json_encode($TaxRatefromAvalara),
					);
					$this->ci->db->insert('avalara_zipcode_taxinfo', $InsertZipCodeInfo);
				}
				else{
					if($TaxRatefromAvalara['rates']){
						$tempAvalaraTaxes = array();
						foreach($TaxRatefromAvalara['rates'] as $tempAvalaraTaxesDatas){
							$taxUniqueName = strtolower($tempAvalaraTaxesDatas['name']);							
							if(isset($tempAvalaraTaxes[strtolower($tempAvalaraTaxesDatas['name'])][strtolower($tempAvalaraTaxesDatas['type'])])){
								$tempAvalaraTaxes[strtolower($tempAvalaraTaxesDatas['name'])][strtolower($tempAvalaraTaxesDatas['type'])]['rate'] += $tempAvalaraTaxesDatas['rate'];
							}
							else{
								$tempAvalaraTaxes[strtolower($tempAvalaraTaxesDatas['name'])][strtolower($tempAvalaraTaxesDatas['type'])] = $tempAvalaraTaxesDatas;
							}
						}
						foreach($tempAvalaraTaxes as $tempAvalaraTaxe){
							$avalaraTypeCount = count($tempAvalaraTaxe);
							foreach($tempAvalaraTaxe as $tempAvalaraTax){
								$name = $tempAvalaraTax['name'];
								if($avalaraTypeCount > 1){
									$name = $tempAvalaraTax['name'] .' ' .$tempAvalaraTax['type'];
								}
								$TaxComponents[]	= array(
									"Name"				=> $name,
									"Rate"				=> ($tempAvalaraTax['rate']*100),
									"IsCompound"		=> false,
									"IsNonRecoverable"	=> false,
								);
							}
						}
					}
					$TaxRequest	= array(
						'Name'			=> $XeroTaxName,
						'Rate'			=> $AvalaraTaxRate,
						'Status'		=> 'ACTIVE',
						'TaxComponents'	=> $TaxComponents,
					);
					$InsertZipCodeInfo	= array(
						'zipcode'	=> $PostalCode,
						'TaxName'	=> $XeroTaxName,
						'params'	=> json_encode($TaxRatefromAvalara),
					);
					$TaxRateUrl		= '2.0/TaxRates';
					$this->initializeConfig($account2Id, 'POST', $TaxRateUrl);
					$TaxResponse	= $this->getCurl($TaxRateUrl, 'POST', json_encode($TaxRequest), 'json', $account2Id)[$account2Id];
					if(strtolower($TaxResponse['Status']) == 'ok'){
						$CreatedData	= array(
							'Request'		=>	$TaxRequest,
							'Response'		=>	$TaxResponse,
						);
						$InsertTaxResponse	= array(
							'account1Id'		=>	$accountDetails['account1Id'],
							'account2Id'		=>	$account2Id,
							'TaxName'			=>	$TaxResponse['TaxRates']['TaxRate']['Name'],
							'TaxType'			=>	$TaxResponse['TaxRates']['TaxRate']['TaxType'],
							'TaxCode'			=>  '',
							'TaxRate'			=>	$TaxResponse['TaxRates']['TaxRate']['EffectiveRate'],
						);
						$this->ci->db->insert('avalara_zipcode_taxinfo', $InsertZipCodeInfo);
						$this->ci->db->insert('avalara_taxRates', $InsertTaxResponse);
						$TaxType	= $TaxResponse['TaxRates']['TaxRate']['TaxType'];
					}
					else{
						$avalaraTaxError = 1;
					}
				}
			}
			if(($avalaraTaxError) && ($isAvalaraTaxApplicable)){
				$this->ci->db->update('sales_order',array('message' => json_encode($TaxResponse)),array('orderId' => $orderId));
				continue;
			}
			$itemtaxAbleLine	= $config['orderLineTaxCode'];$discountTaxAbleLine = $config['orderLineTaxCode'];
			foreach($rowDatas['orderRows'] as $rowId => $orderRows){
				$LineTaxId		= '';
				if(@$taxMappings[$orderRows['rowValue']['taxClassId']]){
					$LineTaxId	= $taxMappings[$orderRows['rowValue']['taxClassId']]['account2TaxId'];
					$orderTaxId	= $taxMappings[$orderRows['rowValue']['taxClassId']]['account2TaxId'];
				}						
				if($orderRows['productId'] > 1001){
					$productId	= $orderRows['productId'];
					if(@!$productMappings[$productId]['createdProductId']){
						$missingSkus[]	= $orderRows['productSku'];
						continue;
					} 
					$ItemRefName	= $productMappings[$productId]['sku'];
					$ItemRefValue	= $orderRows['productSku'];
				}
				else{
					if($orderRows['rowValue']['rowNet']['value'] > 0){
						$ItemRefValue	= $config['genericSku'];
						$ItemRefName	= $orderRows['productName'];
						if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){ 
							$ItemRefValue	= $config['shippingItem'];
							$ItemRefName	= 'Shipping';
						}
					}
					else if($orderRows['rowValue']['rowNet']['value'] < 0){
						$ItemRefValue	= $config['discountItem'];
						$ItemRefName	= 'Discount Item';
					}
					else{
						continue; 
					}
				}
				$price	= $orderRows['rowValue']['rowNet']['value'] + $orderRows['rowValue']['rowTax']['value'];
				if($LineTaxId){
					$price			=  $orderRows['rowValue']['rowNet']['value']; 
					$orderTaxAmount	+= $orderRows['rowValue']['rowTax']['value'];
				}
				$originalPrice	= $price;
				if($orderRows['discountPercentage'] > 0){
					$discountPercentage	= 100 - $orderRows['discountPercentage'];
					if($discountPercentage == 0){
						$originalPrice	= $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
					}
					else{
						$originalPrice	= round((($price * 100) / ( 100 - $orderRows['discountPercentage'])),2);
					}
					$tempTaxAmt	= $originalPrice - $price;
					if($tempTaxAmt > 0){
						$totalItemDiscount	= $tempTaxAmt;						
						$itemDiscountTax	= 1; 
						$itemtaxAbleLine	= $LineTaxId;
					}
				}	
				else if($isDiscountCouponAdded){
					$originalPriceWithTax	= $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
					$originalPrice			= $originalPriceWithTax - $orderRows['rowValue']['rowTax']['value'];
					$discountCouponAmtTemp	= ($originalPrice - $price);
					if($discountCouponAmtTemp > 0){
						$discountCouponAmt		+= $discountCouponAmtTemp;						
						$isDiscountCouponTax	=  1;
						$discountTaxAbleLine	=  $LineTaxId;
					}
				}
				$IncomeAccountRef	= $config['IncomeAccountRef'];
				if(@isset($nominalMappings[$orderRows['nominalCode']])){
					if(@$nominalMappings[$orderRows['nominalCode']]['account2NominalId']){
						$IncomeAccountRef	= $nominalMappings[$orderRows['nominalCode']]['account2NominalId'];
					}
					$nominalMapped	= $nominalMappings[$orderRows['nominalCode']];
					if($channelId){
						if(isset($nominalMapped['channel'])){
							if(isset($nominalMapped['channel'][strtolower($channelId)])){
								if($orderRows['productId'] > 1001){
									$productUpdateNominalCodeInfos[$productId]['org']	= $IncomeAccountRef;
								}
								$IncomeAccountRef	= $nominalMapped['channel'][strtolower($channelId)]['account2NominalId'];
								if($orderRows['productId'] > 1001){
									$productUpdateNominalCodeInfos[$productId]['upd']	= $IncomeAccountRef;
								}
							}
						}
					}
				}
				$UnitAmount	= 0.00;
				if($originalPrice > 0){
					$UnitAmount	= $originalPrice / $orderRows['quantity']['magnitude'];
				}
				if(is_nan($originalPrice)){
					$originalPrice	= 0.00;
				}
				$InvoiceLineAdd[$invoiceLineCount]	= array(
					'ItemCode' 		=> $ItemRefValue, 
					'Description'	=> $orderRows['productName'],
					'Quantity' 		=> (int)$orderRows['quantity']['magnitude'],
					'UnitAmount' 	=> sprintf("%.4f",$UnitAmount),
					'TaxType' 		=> $LineTaxId,
					'AccountCode' 	=> $IncomeAccountRef,
					'TaxAmount' 	=> $orderRows['rowValue']['rowTax']['value'],
					//'LineAmount' 	=> $originalPrice,	
				);	
				/* 
				if($orderRows['quantity']['magnitude'] <= 1){
					$InvoiceLineAdd[$invoiceLineCount]['UnitAmount']	= $InvoiceLineAdd[$invoiceLineCount]['LineAmount'];
				} */
				if(!$LineTaxId){
					unset($InvoiceLineAdd[$invoiceLineCount]['TaxType']);
				}
				if(($orderRows['rowValue']['taxCode'] == '-') && ($orderRows['rowValue']['rowTax']['value'] > 0)){
					if($TaxType){
						$InvoiceLineAdd[$invoiceLineCount]['TaxType']	= $TaxType;
					}
				}
				if($orderRows['discountPercentage'] > 0){
				/*  $InvoiceLineAdd[$invoiceLineCount]['LineAmount']	= $orderRows['rowValue']['rowNet']['value']; */ 
					$InvoiceLineAdd[$invoiceLineCount]['DiscountRate']	= $orderRows['discountPercentage'];
				}					
				if(isset($channelMappings[$channelId])){
					$trackingDetails	= $channelMappings[$channelId]['account2ChannelId'];
					if(@$channelMappings[$channelId]['warehouseDetails'][$warehouseId]){
						$trackingDetails	= $channelMappings[$channelId]['warehouseDetails'][$warehouseId]['account2ChannelId'];
					}
					if($trackingDetails){
						$trackingDetails	= explode("~=",$trackingDetails);
						$InvoiceLineAdd[$invoiceLineCount]['Tracking']	= array(array('Name' => $trackingDetails['0'],'Option' => $trackingDetails['1']));
					}
				}
				$invoiceLineCount++;
			}
			if($missingSkus){
				$missingSkus	= array_unique($missingSkus);
				$this->ci->db->update('sales_order',array('message' => 'Missing sku : '.implode(",",$missingSkus)),array('orderId' => $orderId));
				continue;
			}
			$dueDate	= date('Y-m-d');$taxDate = date('Y-m-d');
			if(@$rowDatas['invoices']['0']['dueDate']){
				$dueDate	= gmdate('Y-m-d',strtotime($rowDatas['invoices']['0']['dueDate']));
			}
			if(@$rowDatas['invoices']['0']['taxDate']){
				$taxDate	= date('Y-m-d',strtotime($rowDatas['invoices']['0']['taxDate']));
			}
			$invoiceRef	= $orderId;
			if(@$rowDatas['invoices']['0']['invoiceReference']){
				$invoiceRef	= @$rowDatas['invoices']['0']['invoiceReference'];
			}
			if($discountCouponAmt){
				$InvoiceLineAdd[$invoiceLineCount]	= array(
					'ItemCode' 		=> $config['couponItem'],
					'Description' 	=> 'Coupon Discount',
					'Quantity' 		=> 1,
					'UnitAmount' 	=> (-1) * sprintf("%.2f",$discountCouponAmt),
					'LineAmount' 	=> (-1) * sprintf("%.2f",$discountCouponAmt),
					'TaxAmount' 	=> 0.00,
				);
				if($isDiscountCouponTax){
					$InvoiceLineAdd[$invoiceLineCount]['TaxType']	= array( 'ListID' => $discountTaxAbleLine );
				}
				else{
					$InvoiceLineAdd[$invoiceLineCount]['TaxType']	= array( 'ListID' => $config['orderLineNoTaxCode'] );
				}
				if(isset($channelMappings[$channelId])){
					$trackingDetails	= $channelMappings[$channelId]['account2ChannelId'];
					if(@$channelMappings[$channelId]['warehouseDetails'][$warehouseId]){
						$trackingDetails	= $channelMappings[$channelId]['warehouseDetails'][$warehouseId]['account2ChannelId'];;
					}
					if($trackingDetails){
						$trackingDetails	= explode("~=",$trackingDetails);
						$InvoiceLineAdd[$invoiceLineCount]['Tracking']	= array(array('Name' => $trackingDetails['0'],'Option' => $trackingDetails['1']));
					}
				}
				$invoiceLineCount++;
			}		
			if(($uninvoiceCount == 0) || ($uninvoiceCount == '')){
				$Reference	= $invoiceRef;
			}
			if($uninvoiceCount > 0)
				$Reference	= $invoiceRef.'/New-0'.$uninvoiceCount;
			}
			$request	= array(
				'Type' 			=> 'ACCREC',
				'Contact' 		=> array(
					'ContactID'		=> $customerMappings[$orderCustomer['contactId']]['createdCustomerId'],
					'Addresses' 	=> array(
						array(
							'AddressType' 	=> 'DELIVERY',
							'AddressLine1' 	=> @$shipAddress['addressLine1'],
							'AddressLine2' 	=> @$shipAddress['addressLine2'],
							'City' 			=> @$shipAddress['addressLine3'],
							'Region' 		=> @$shipAddress['addressLine4'],
							'PostalCode'	=> @$shipAddress['postalCode'],
							'Country' 		=> @$shipAddress['countryIsoCode'],
						),
					),
				),
				'InvoiceNumber'	=> $Reference, 
				'Reference'		=> $orderId, 
				'Status'		=> 'AUTHORISED',
				'CurrencyCode'	=> $rowDatas['currency']['orderCurrencyCode'],						
				'CurrencyRate'	=> ($rowDatas['currency']['exchangeRate'])?($rowDatas['currency']['exchangeRate']):1,						
				'Date'			=> @$taxDate,
				'DueDate'		=> @$dueDate,
				'LineItems' 	=> $InvoiceLineAdd
			);
			if($config['defaultCurrency'] != $config1['currencyCode']){
				unset($request['CurrencyRate']);
			}
			$url			= '2.0/Invoices';
			$this->initializeConfig($account2Id, 'POST', $url);
			$results		= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
			$createdRowData	= array(
				'Request data : '	=> $request,
				'Response data : '	=> $results,
			);
			$logs[$orderDatas['orderId']]	= array(
				'Request data : '	=> $request,
				'Response data : '	=> $results,
			);
			$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
			if((strtolower($results['Status']) == 'ok') && (isset($results['Invoices']['Invoice']['InvoiceID']))){	
				$this->ci->db->update('sales_order',array('status' => '1','invoiceRef' => $invoiceRef,'createOrderId' => $results['Invoices']['Invoice']['InvoiceID']),array('id' => $orderDatas['id']));
			}
		}
	}
	if($logs){
		foreach($logs as $orderId => $log){
			$path	= FCPATH.'logs'.DIRECTORY_SEPARATOR . $accountDetails['account1Id'].DIRECTORY_SEPARATOR . 'so'.DIRECTORY_SEPARATOR;
			if(!is_dir($path)){
				mkdir($path,0777,true);
				chmod(dirname($path), 0777); 
			}
			file_put_contents($path. $orderId.'.logs',"\n\n BP Log Added On : ".date('c')." \n". json_encode($log),FILE_APPEND);
		}
	}
}
$this->postSalesPayment($orgOrderId);	
$this->fetchSalesPayment();		
?>