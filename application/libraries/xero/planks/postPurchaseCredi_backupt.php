<?php
$this->reInitialize();
foreach($this->accountDetails as $account2Id => $accountDetails){	
	$nominalMappings = array();$logs = array();
	$nominalMappingTemps = $this->ci->db->get_where('mapping_nominal',array('account1CustomFieldValue' => '','account2Id' => $account2Id))->result_array();
	foreach($nominalMappingTemps as $nominalMappingTemp){
		if(@!$nominalMappings[$nominalMappingTemp['account1NominalId']]){
			if(!$nominalMappingTemp['account1ChannelId']){
				$nominalMappings[$nominalMappingTemp['account1NominalId']] = $nominalMappingTemp;
			}
		}
		if($nominalMappingTemp['account1ChannelId']){
			$nominalMappings[$nominalMappingTemp['account1NominalId']]['channel'][strtolower($nominalMappingTemp['account1ChannelId'])] = $nominalMappingTemp;
		}
	}
	$config = $this->accountConfig[$account2Id];
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	$datas = $this->ci->db->where_in('status',array('0'))->get_where('purchase_credit_order',array('account2Id' => $account2Id))->result_array(); 			
	$customerMappings = array();
	$customerMappingsTemps = $this->ci->db->select('customerId,createdCustomerId,email')->get_where('customers',array('account2Id' => $account2Id))->result_array();
	foreach($customerMappingsTemps as $customerMappingsTemp){
		$customerMappings[$customerMappingsTemp['customerId']] = $customerMappingsTemp;
	}
	$productMappings = array();
	$productMappingsTemps = $this->ci->db->select('productId,createdProductId,name,sku')->get_where('products',array('account2Id' => $account2Id))->result_array();
	foreach($productMappingsTemps as $productMappingsTemp){
		$productMappings[$productMappingsTemp['productId']] = $productMappingsTemp;
	}
	$taxMappings = array();
	$taxMappingsTemps = $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id,'orderType <> ' => '1'))->result_array();
	if($taxMappingsTemps){
		foreach($taxMappingsTemps as $taxMappingsTemp){
			$taxMappings[$taxMappingsTemp['account1TaxId']] = $taxMappingsTemp;
		}
	}	
	$channelMappingsTemps = $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	if($channelMappingsTemps){
		foreach($channelMappingsTemps as $channelMappingsTemp){
			if(!isset($channelMappings[$channelMappingsTemp['account1ChannelId']])){
				$channelMappings[$channelMappingsTemp['account1ChannelId']] = $channelMappingsTemp;
			}
			if($channelMappingsTemp['account1WarehouseId']){
				$channelMappings[$channelMappingsTemp['account1ChannelId']]['warehouseDetails'][$channelMappingsTemp['account1WarehouseId']] = $channelMappingsTemp;
			}
		}
	}
	if($datas){				
		foreach($datas as $orderDatas){
			if($orderDatas['createOrderId']){continue;}
			$config1 = $this->ci->account1Config[$orderDatas['account1Id']];
			$orderId = $orderDatas['orderId'];
			$rowDatas = json_decode($orderDatas['rowData'],true);
			$channelId = $rowDatas['assignment']['current']['channelId'];
			$billAddress 	=  $rowDatas['parties']['billing'];
			$shipAddress 	=  $rowDatas['parties']['delivery'];
			$orderCustomer 	=  $rowDatas['parties']['supplier'];
			if(@!$customerMappings[$orderCustomer['contactId']]['createdCustomerId']){
				continue;
			}
			$missingSkus = array();$InvoiceLineAdd = array();$invoiceLineCount = 0;
			foreach($rowDatas['orderRows'] as $rowId => $orderRows){
				if($orderRows['productId'] > 1004){
					$productId = $orderRows['productId'];
					if(@!$productMappings[$productId]['createdProductId']){
						$missingSkus[] = $orderRows['productSku'];
						continue;
					}
					$taxId = $config['salesNoTaxCode'];
					if(@$taxMappings[$orderRows['rowValue']['taxClassId']]){
						$taxId = $taxMappings[$orderRows['rowValue']['taxClassId']]['account2TaxId'];
					}
					$ExpenseAccountRef = $config['ExpenseAccountRef'];						
					if(@isset($nominalMappings[$orderRows['nominalCode']]['account2NominalId'])){
						$ExpenseAccountRef = $nominalMappings[$orderRows['nominalCode']]['account2NominalId'];
						$nominalMapped = $nominalMappings[$orderRows['nominalCode']];
						if($channelId){
							if(isset($nominalMapped['channel'])){
								if(isset($nominalMapped['channel'][strtolower($channelId)])){
									$ExpenseAccountRef = $nominalMapped['channel'][strtolower($channelId)]['account2NominalId'];
								}
							}
						}
					}
					$UnitAmount = 0.00;
					if($orderRows['rowValue']['rowNet']['value'] > 0){
						$UnitAmount = $orderRows['rowValue']['rowNet']['value'] / $orderRows['quantity']['magnitude'];
					}
					$InvoiceLineAdd[$invoiceLineCount] = array(
						'ItemCode' 		=> $orderRows['productSku'],
						'ItemCode' 		=> $orderRows['productSku'],
						'Description' 	=> $orderRows['productName'],
						'Quantity' 		=> (int)$orderRows['quantity']['magnitude'],
						'UnitAmount' 	=> sprintf("%.4f",$UnitAmount),
						'TaxType' 		=> $taxId,
						'AccountCode' 	=> $ExpenseAccountRef, 
						'TaxAmount' 	=> sprintf("%.4f",$orderRows['rowValue']['rowTax']['value']),
						'LineAmount' 	=> sprintf("%.4f",$orderRows['rowValue']['rowNet']['value']),
					);
					$invoiceLineCount++;
				}
			}
			if($missingSkus){
				$missingSkus = array_unique($missingSkus);
				$this->ci->db->update('purchase_order',array('message' => 'Missing sku : '.implode(",",$missingSkus)),array
				('orderId' => $orderId));
				continue;
			}
			$dueDate = date('Y-m-d');$ShipDate = '';
			if(@$rowDatas['invoices']['0']['dueDate']){
				$dueDate = date('Y-m-d',strtotime($rowDatas['invoices']['0']['dueDate']));
			}
			if(@$rowDatas['delivery']['deliveryDate']){
				$ShipDate = date('Y-m-d',strtotime($rowDatas['delivery']['deliveryDate']));
			}
			$invoiceRef = $orderId;
			if(@$rowDatas['invoices']['0']['invoiceReference']){
				$invoiceRef = @$rowDatas['invoices']['0']['invoiceReference'];
			}
			$request = array(
				'Type' 			=> 'ACCPAYCREDIT',
				'Contact' 		=> array(
					'ContactID' => $customerMappings[$orderCustomer['contactId']]['createdCustomerId'],
					'Addresses' => array(
						array(
							'AddressType' 	=> 'DELIVERY',
							'AddressLine1' 	=> @$shipAddress['addressLine1'],
							'AddressLine2' 	=> @$shipAddress['addressLine2'],
							'City' 			=> @$shipAddress['addressLine3'],
							'Region' 		=> @$shipAddress['addressLine4'],
							'PostalCode'	=> @$shipAddress['postalCode'],
							'Country' 		=> @$shipAddress['countryIsoCode'],
						),
					),
				),
				'CreditNoteNumber' 		=> $invoiceRef,
				'Reference' 			=> $orderId,
				'Status'				=> 'AUTHORISED',
				'CurrencyCode'			=> $rowDatas['currency']['orderCurrencyCode'],		
				'CurrencyRate'	=> ($rowDatas['currency']['exchangeRate'])?($rowDatas['currency']['exchangeRate']):1,
				'Date'					=> @$taxDate,
				'LineItems' 			=> $InvoiceLineAdd
			);
			$url = '2.0/CreditNotes';
			if($config['defaultCurrency'] != $config1['currencyCode']){
				unset($request['CurrencyRate']);
			}
			$this->initializeConfig($account2Id, 'POST', $url);
			$results = $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
			$createdRowData = array(
				'Request data : ' => $request,
				'Response data : ' => $results,
			);
			$logs[$orderDatas['orderId']] = array(
				'Request data : ' => $request,
				'Response data : ' => $results,
			);
			$this->ci->db->update('purchase_credit_order',array('createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
			if((strtolower($results['Status']) == 'ok') && (isset($results['CreditNotes']['CreditNote']['CreditNoteID']))){	
				$this->ci->db->update('purchase_credit_order',array('status' => '1','invoiceRef' => $invoiceRef,'createOrderId' => $results['CreditNotes']['CreditNote']['CreditNoteID']),array('id' => $orderDatas['id']));
			}
		}				
	}
	if($logs){
		foreach($logs as $orderId => $log){
			$path = FCPATH.'logs'.DIRECTORY_SEPARATOR . $accountDetails['account1Id'].DIRECTORY_SEPARATOR . 'pc'.DIRECTORY_SEPARATOR;
			if(!is_dir($path)) { mkdir($path,0777,true);chmod(dirname($path), 0777); }
			file_put_contents($path. $orderId.'.logs',"\n\n BP Log Added On : ".date('c')." \n". json_encode($log),FILE_APPEND);
		}
	}
}
$this->fetchPurchaseCreditPayment();

?>