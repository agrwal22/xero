<?php
$this->reInitialize();
$enableAvalaraTax           = $this->ci->globalConfig['enableAvalaraTax'];
$enableGiftCard             = $this->ci->globalConfig['enableGiftCard'];
$enableAmazonFees           = $this->ci->globalConfig['enableAmazonFees'];
$enableInventoryTransfer    = $this->ci->globalConfig['enableInventoryTransfer'];
$enableInventoryManagement	= $this->ci->globalConfig['enableInventoryManagement'];
foreach($this->accountDetails as $account2Id => $accountDetails){
	if($accountDetails['OAuthVersion'] == '2'){
		$this->refreshToken($account2Id);
	}
	$config = $this->accountConfig[$account2Id];
	if($orgObjectId){
		$this->ci->db->where_in('productId',$orgObjectId);
	}
	$datas = $this->ci->db->where_in('status',array('0','2'))->get_where('products',array('account2Id' => $account2Id))->result_array();
	
	$nominalMappings		= array();
	$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1ChannelId' => '','account2Id' => $account2Id))->result_array();
	foreach($nominalMappingTemps as $nominalMappingTemp){
		if(!isset($nominalMappings[$nominalMappingTemp['account1NominalId']])){
			$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
		}
		if($nominalMappingTemp['account1CustomFieldValue']){
			$nominalMappings[$nominalMappingTemp['account1NominalId']]['productCat'][strtolower($nominalMappingTemp['account1CustomFieldValue'])]	= $nominalMappingTemp;
		}
	}
	
	$taxType			= array('0' => 'all','1' => 'sales','2' => 'purchase');
	$taxMapppings		= array();
	$taxMappingTemps	= $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id))->result_array();
	foreach($taxMappingTemps as $taxMappingTemp){
		$orderType	= (int)$taxMappingTemp['orderType'];
		$taxMapppings[$taxMappingTemp['account1TaxId']][$orderType]	= $taxMappingTemp;
	}
	
	if($datas){				
		$datass	= array_chunk($datas,5);
		foreach($datass as $datas){
			$pid				= array_column($datas, 'productId');
			$productPriceLists	= $this->ci->{$this->ci->globalConfig['fetchProduct']}->getProductPriceList($pid);	
			$request			= array();
			$requestCount		= 0;
			$requestLogs		= array();
			foreach($datas as $productDatas){
				$config1			= $this->ci->account1Config[$productDatas['account1Id']];
				$productId			= $productDatas['productId'];
				$productPriceList	= @$productPriceLists[$productId];
				$SalesPrice			= 0.00;
				$PurchaseCost		= 0.00;
				if(@$productPriceList[$config['productRetailPriceList']] > 0){
					$SalesPrice		= $productPriceList[$config['productRetailPriceList']]; 
				}
				if(@$productPriceList[$config['productCostPriceList']] > 0){
					$PurchaseCost	= $productPriceList[$config['productCostPriceList']];
				}
				$description	= strip_tags($productDatas['description']);
				if(!$description){
					$description	= $productDatas['name'];
				}
				$params					= json_decode($productDatas['params'],true);
				$AssetAccountRef 		= $config['AssetAccountRef'];
				$COGSAccountRef 		= $config['COGSAccountRef'];
				$ExpenseAccountRef 		= $config['ExpenseAccountRef'];
				$IncomeAccountRef 		= $config['IncomeAccountRef'];
				if(isset($nominalMappings[$params['nominalCodeSales']]['account2NominalId'])){
					$nominalMapped		= $nominalMappings[$params['nominalCodeSales']];
					$IncomeAccountRef	= $nominalMapped['account2NominalId'];
					if(isset($nominalMapped['productCat'])){
						if(isset($nominalMapped['productCat'][strtolower($params['customFields'][$config1['productCatCustomField']])])){
							$IncomeAccountRef	= $nominalMapped['productCat'][strtolower($params['customFields'][$config1['productCatCustomField']])]['account2NominalId'];
						}
					}				
				}
				if(isset($nominalMappings[$params['nominalCodePurchases']]['account2NominalId'])){
					$nominalMapped		= $nominalMappings[$params['nominalCodePurchases']];
					$ExpenseAccountRef	= $nominalMapped['account2NominalId'];
					if(isset($nominalMapped['productCat'])){
						if(isset($nominalMapped['productCat'][strtolower($params['customFields'][$config1['productCatCustomField']])])){
							$ExpenseAccountRef	= $nominalMapped['productCat'][strtolower($params['customFields'][$config1['productCatCustomField']])]['account2NominalId'];
						}
					}
					$COGSAccountRef	= $ExpenseAccountRef;
				}					
				if(isset($nominalMappings[$params['nominalCodeStock']]['account2NominalId'])){
					$nominalMapped		= $nominalMappings[$params['nominalCodeStock']];
					$AssetAccountRef	= $nominalMapped['account2NominalId'];
					if(isset($nominalMapped['productCat'])){
						if(isset($nominalMapped['productCat'][strtolower($params['customFields'][$config1['productCatCustomField']])])){
							$AssetAccountRef	= $nominalMapped['productCat'][strtolower($params['customFields'][$config1['productCatCustomField']])]['account2NominalId'];
						}
					}				
				}
				if(!$productDatas['sku']){
					continue;
				}
				$request['items'][$requestCount]	= array(
					'Code' 					    => $productDatas['sku'],
					'Name' 					    => substr($productDatas['name'],0,50),
					'Description' 			    => $description,
					'PurchaseDescription' 	    => $description,
					'IsSold' 				    => true,
					'IsPurchased' 			    => true,
					'PurchaseDetails' 		    => array(
						'UnitPrice' 				=> $PurchaseCost,
						'COGSAccountCode' 			=> $COGSAccountRef, 
						'AccountCode' 				=> $ExpenseAccountRef,
					),
					'SalesDetails' 			    => array(
						'UnitPrice' 				=> $SalesPrice,
						'AccountCode' 				=> $IncomeAccountRef,
					),
					'InventoryAssetAccountCode'	=> $AssetAccountRef
				);
				if($productDatas['createdProductId']){
					$request['items'][$requestCount]['ItemID']	= $productDatas['createdProductId'];
				}
				$bpTaxId	= $params['financialDetails']['taxCode']['id'];
				if(isset($taxMapppings[$bpTaxId])){
					$taxMappping	= $taxMapppings[$bpTaxId];
					if(!isset($taxMappping['1'])){$taxMappping['1']					= $taxMappping['0'];}
					if(!isset($taxMappping['2'])){$taxMappping['2']					= $taxMappping['0'];}
					$request['items'][$requestCount]['PurchaseDetails']['TaxType']	= $taxMappping['2']['account2TaxId'];
					$request['items'][$requestCount]['SalesDetails']['TaxType']		= $taxMappping['1']['account2TaxId'];
				}
				else{
					if($params['financialDetails']['taxable']){
						$request['items'][$requestCount]['SalesDetails']['TaxType']	= $config['TaxCode'];
					}
				}
				if($params['stock']['stockTracked']){
					unset($request['items'][$requestCount]['PurchaseDetails']['AccountCode']);
					$request['items'][$requestCount]['IsTrackedAsInventory']	= true;
				}
				else{
					unset($request['items'][$requestCount]['PurchaseDetails']['COGSAccountCode']);
					unset($request['items'][$requestCount]['SalesDetails']['COGSAccountCode']);
					unset($request['items'][$requestCount]['InventoryAssetAccountCode']); 
					$request['items'][$requestCount]['IsTrackedAsInventory']	= false;
				}				
				if($enableInventoryManagement){
					$InventMgt_SellNominalCode		=	'';
					$InventMgt_PurchaseNominalCode	=	'';
					
					if(isset($nominalMappings[$params['nominalCodeSales']]['account2NominalId'])){
						$nominalMapped				= $nominalMappings[$params['nominalCodeSales']];
						$InventMgt_SellNominalCode	= $nominalMapped['account2NominalId'];
						if(isset($nominalMapped['productCat'])){
							if(isset($nominalMapped['productCat'][strtolower($params['customFields'][$config1['productCatCustomField']])])){
								$InventMgt_SellNominalCode	= $nominalMapped['productCat'][strtolower($params['customFields'][$config1['productCatCustomField']])]['account2NominalId'];
							}
						}				
					}
					if(isset($nominalMappings[$params['nominalCodePurchases']]['account2NominalId'])){
						$nominalMapped					= $nominalMappings[$params['nominalCodePurchases']];
						$InventMgt_PurchaseNominalCode	= $nominalMapped['account2NominalId'];
						if(isset($nominalMapped['productCat'])){
							if(isset($nominalMapped['productCat'][strtolower($params['customFields'][$config1['productCatCustomField']])])){
								$InventMgt_PurchaseNominalCode	= $nominalMapped['productCat'][strtolower($params['customFields'][$config1['productCatCustomField']])]['account2NominalId'];
							}
						}
					}
					
					if(!$InventMgt_SellNominalCode){
						$InventMgt_SellNominalCode 		= $config['InventoryManagementProductSellNominalCode'];
					}
					if(!$InventMgt_PurchaseNominalCode){
						$InventMgt_PurchaseNominalCode	= $config['InventoryManagementProductPurchaseNominalCode'];
					}
					
					if($config['InventoryManagementEnabled'] == true){
						if($params['stock']['stockTracked']){
							unset($request['items'][$requestCount]['PurchaseDetails']['COGSAccountCode']);
							unset($request['items'][$requestCount]['SalesDetails']['COGSAccountCode']);
							unset($request['items'][$requestCount]['InventoryAssetAccountCode']); 
							$request['items'][$requestCount]['IsTrackedAsInventory']			= false;
							$request['items'][$requestCount]['SalesDetails']['AccountCode'] 	= $InventMgt_SellNominalCode;
							$request['items'][$requestCount]['PurchaseDetails']['AccountCode']	= $InventMgt_PurchaseNominalCode;
						}
					}
				}
				$requestLogs[strtolower($productDatas['sku'])]['Request Data'] 				= $request['items'][$requestCount];
				$requestLogs[strtolower($productDatas['sku'])]['Request Data']['productId']	= $productDatas['productId'];
				$requestCount++;
			}
			if($request){
				$url		= '2.0/Items/';
				$this->initializeConfig($account2Id, 'POST', $url);
				$results	= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
				$ceatedParams	= array(
					'Request data	: ' => $request,
					'Response data	: ' => $results,
				);
				$productIds	= array_column($datas,'productId');
				if($productIds){
					$this->ci->db->where_in('productId',$productIds)->update('products',array('ceatedParams' => json_encode($ceatedParams)),array('account2Id' => $account2Id));
					if((strtolower($results['Status']) == 'ok') && (isset($results['Items']['Item']))){
						$xeroItems	= $results['Items']['Item'];
						if(!isset($xeroItems['0'])){
							$xeroItems	= array($xeroItems);
						}
						foreach($xeroItems as $xeroItem){
							$this->ci->db->update('products',array('status' => '1','createdProductId' => $xeroItem['ItemID']),array('sku' => $xeroItem['Code'],'account2Id' => $account2Id));
							$requestLogs[strtolower($xeroItem['Code'])]['Response Data']	= $xeroItem;
						}
					}
				}
			}
			if($requestLogs){
				foreach($requestLogs as $requestLog){ 
					$productId	= $requestLog['Request Data']['productId'];
					$path		= FCPATH.'logs'.DIRECTORY_SEPARATOR . $accountDetails['account1Id'].DIRECTORY_SEPARATOR . 'products'.DIRECTORY_SEPARATOR;
					if(!is_dir($path)){
						mkdir($path,0777,true);
						chmod(dirname($path), 0777);
					}
					file_put_contents($path. $productId.'.logs',"\n\n Xero Log Added On : ".date('c')." \n". json_encode($requestLog),FILE_APPEND);
				}
			}
		}
	}
}
?>