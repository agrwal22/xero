<?php
$this->reInitialize();
$batchUpdates	= array(); 		
foreach($this->accountDetails as $account2Id => $accountDetails){
	if($accountDetails['OAuthVersion'] == '2'){
		$this->refreshToken($account2Id);
	}
	$logs	= array();
	$config	= $this->accountConfig[$account2Id];
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	else{
		$this->ci->db->group_start()->where('status', '0')->or_group_start()->where('paymentCreated', '0')->group_end()->group_end();
	}
	$journalDatasTemps	= $this->ci->db->get_where('amazon_ledger',array('account2Id' => $account2Id))->result_array();
	$journalDatas		= array();
	foreach($journalDatasTemps as $journalDatasTemp){
		if(isset($journalDatas[$journalDatasTemp['orderId']][$journalDatasTemp['journalTypeCode']])){
			$journalDatas[$journalDatasTemp['orderId']][$journalDatasTemp['journalTypeCode']]['amount']	+= $journalDatasTemp['amount'];
		}
		else{
			$journalDatas[$journalDatasTemp['orderId']][$journalDatasTemp['journalTypeCode']]	= $journalDatasTemp;
		}
	}
	$orderIds	= array_keys($journalDatas);
	$orderIds	= array_filter($orderIds);
	
	$customerMappings		= array();
	$customerMappingsTemps	= $this->ci->db->select('customerId,createdCustomerId,email')->get_where('customers',array('account2Id' => $account2Id))->result_array();
	foreach($customerMappingsTemps as $customerMappingsTemp){
		$customerMappings[$customerMappingsTemp['customerId']]	= $customerMappingsTemp;
	}
	if($orderIds){
		$logs				= array();
		$salesOrderDatas	= $this->ci->db->where_in('orderId', $orderIds)->get_where('sales_order',array('status >' => 0,'account2Id' => $account2Id))->result_array();
		$allSalesData		= array();
		foreach($salesOrderDatas as $salesOrderData){
			if(!$salesOrderData['createOrderId']){
				continue;
			}
			$allSalesData[$salesOrderData['orderId']]	= $salesOrderData;
		}
		foreach($journalDatas as $jID => $journalDatasss){
			foreach($journalDatasss as $Jtype => $journalData){
				$createdRowData	= json_decode($orderDatas['createdParams'],true);
				if(@$createdRowData['lastTry']){
					if($createdRowData['lastTry'] > strtotime('-3 hour')){
						continue;
					}
				}
				$SalesOrderoID	= $journalData['orderId'];
				$JournalId		= $journalData['journalsId'];
				$journalParam	= json_decode($journalData['params'],true);
				if(!$allSalesData[$SalesOrderoID]){
					continue;
				}
				if($journalData['createdJournalsId'] AND $journalData['paymentCreated']){
					continue;
				}
				if($journalData['createdJournalsId'] AND !$journalData['paymentCreated']){
					$createdRowData	= json_decode($journalData['createdParams'],true);
					$PaymentRequest	= array();
					$PaymentRequest	= array(
						'Invoice'		=> array(
							'InvoiceID'		=> $journalData['createdJournalsId'],
						),
						'Account'		=> array(
							'Code'				=> $config['amazonfeePaymentMethodRef'],
						),
						'Date'			=> $journalParam['taxDate'],
						'Reference'		=> $journalParam['description'],
						'CurrencyRate'	=> ($journalParam['exchangeRate'])?($journalParam['exchangeRate']):1,
						'Amount'		=> sprintf("%.4f",$journalData['amount']),
					);
					$BillPaymentUrl	= '2.0/Payments';
					$this->initializeConfig($account2Id, 'PUT', $url);
					$Paymentresults	= $this->getCurl($BillPaymentUrl, 'PUT', json_encode($PaymentRequest), 'json', $account2Id)[$account2Id];
					$createdRowData['Bill Payment Request (Date : '.date('Y-m-d-H-i-s').')']	= $PaymentRequest;
					$createdRowData['Bill Payment Response (Date : '.date('Y-m-d-H-i-s').')']	= $Paymentresults;
					$logs[$JournalId]['Bill Payment Request Data']	= $PaymentRequest;
					$logs[$JournalId]['Bill Payment Response Data']	= $Paymentresults;
					$this->ci->db->update('amazon_ledger',array('createdParams' => json_encode($createdRowData)),array('journalsId' => $JournalId));
					if((strtolower($Paymentresults['Status']) == 'ok') && (isset($Paymentresults['Payments']['Payment']['PaymentID']))){
						$this->ci->db->update('amazon_ledger',array('paymentCreated' => '1'),array('journalsId' => $JournalId));
						continue;
					}
					else{
						continue;
					}
					continue;
				}
				$orderRowData			= json_decode($allSalesData[$SalesOrderoID]['rowData'],true);
				$orderCreatedRowData	= json_decode($allSalesData[$SalesOrderoID]['createdRowData'],true);
				$orderPaymentData		= json_decode($allSalesData[$SalesOrderoID]['paymentDetails'],true);
				$PaymentFound			= 0;
				foreach($orderPaymentData as $pKey => $orderPaymentDatas){
					if(strlen($pKey) > 20){
						if($orderPaymentDatas['amount'] > 0){
							$PaymentFound	= 1;
							break;
						}
					}
				}
				if(!$PaymentFound){
					continue;
				}
				$request		= array();
				$BillLineCount	= 0;
				$BillLineItem[$BillLineCount]	= array(
					'Description' 	=> substr($journalParam['description'],0,4000),
					'Quantity' 		=> 1,
					'UnitAmount' 	=> sprintf("%.4f",$journalData['amount']),
					'AccountCode'	=> $config['amazonfeeAccountRef'],
				);
				$request	= array(
					'Type' 				=> 'ACCPAY',   //use ACCPAY to create Bill in Xero
					'Contact' 			=> array(
						'ContactID'			=> $config['amazondefaultSupplier'],
					),
					'LineItems' 		=> $BillLineItem,
					'Date'				=> $journalParam['taxDate'],
					'DueDate'			=> $journalParam['taxDate'],
					'InvoiceNumber'		=> $JournalId,
					'Reference'			=> $SalesOrderoID,
					'CurrencyCode'		=> $orderRowData['currency']['orderCurrencyCode'],			
					'CurrencyRate'		=> ($journalParam['exchangeRate'])?($journalParam['exchangeRate']):1,
					'Status'			=> 'AUTHORISED', 
				);
				$url			= '2.0/Invoices?unitdp=4';
				$this->initializeConfig($account2Id, 'POST', $url);
				$results		= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
				$createdRowData	= array(
					'Request data	: '	=> $request,
					'Response data	: '	=> $results,
				);
				$logs[$JournalId]	= $createdRowData;
				$this->ci->db->update('amazon_ledger',array('createdParams' => json_encode($createdRowData)),array('journalsId' => $JournalId));
				if((strtolower($results['Status']) == 'ok') && (isset($results['Invoices']['Invoice']['InvoiceID']))){
					$this->ci->db->where(array('journalsId' => $JournalId))->update('amazon_ledger',array('status' => '1','createdJournalsId' => $results['Invoices']['Invoice']['InvoiceID'],'createdParams' => json_encode($createdRowData)));
					
					$PaymentRequest		= array();
					$PaymentRequest		= array(
						'Invoice'		=> array(
							'InvoiceID'		=> $results['Invoices']['Invoice']['InvoiceID'],
						),
						'Account'		=> array(
							'Code'			=> $config['amazonfeePaymentMethodRef'],
						),
						'Date'			=> $journalParam['taxDate'],
						'Reference'		=> $journalParam['description'],
						'CurrencyRate'	=> ($journalParam['exchangeRate'])?($journalParam['exchangeRate']):1,
						'Amount'		=> sprintf("%.4f",$journalData['amount']),
					);
					$BillPaymentUrl	= '2.0/Payments';
					$this->initializeConfig($account2Id, 'PUT', $url);
					$Paymentresults	= $this->getCurl($BillPaymentUrl, 'PUT', json_encode($PaymentRequest), 'json', $account2Id)[$account2Id];
					$createdRowData['Bill Payment Request (Date : '.date('Y-m-d-H-i-s').')']	= $PaymentRequest;
					$createdRowData['Bill Payment Response (Date : '.date('Y-m-d-H-i-s').')']	= $Paymentresults;
					$logs[$JournalId]['Bill Payment Request Data']	= $PaymentRequest;
					$logs[$JournalId]['Bill Payment Response Data']	= $Paymentresults;
					$this->ci->db->update('amazon_ledger',array('createdParams' => json_encode($createdRowData)),array('journalsId' => $JournalId));
					if((strtolower($Paymentresults['Status']) == 'ok') && (isset($Paymentresults['Payments']['Payment']['PaymentID']))){
						$this->ci->db->update('amazon_ledger',array('paymentCreated' => '1'),array('journalsId' => $JournalId));
					}
				}
				else{
					$createdRowData['lastTry']	= strtotime('now');
					$this->ci->db->update('amazon_ledger',array('createdParams' => json_encode($createdRowData)),array('journalsId' => $JournalId));
				}
			}
		}
	}
	if($logs){
		foreach($logs as $JournalId => $log){
			$path	= FCPATH.'logs'.DIRECTORY_SEPARATOR . $accountDetails['account1Id'].DIRECTORY_SEPARATOR . 'journal'.DIRECTORY_SEPARATOR;
			if(!is_dir($path)){
				mkdir($path,0777,true);
				chmod(dirname($path), 0777); 
			}
			file_put_contents($path. $JournalId.'.logs',"\n\n Xero Log Added On : ".date('c')." \n". json_encode($log),FILE_APPEND);
		}
	}
}
?>