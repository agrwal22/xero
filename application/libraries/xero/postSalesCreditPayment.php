<?php
$this->reInitialize();
foreach($this->accountDetails as $account2Id => $accountDetails){
	if($accountDetails['OAuthVersion'] == '2'){
		$this->refreshToken($account2Id);
	}
	$brightpearlconfig	= $this->ci->account1Config[$accountDetails['account1Id']];
	$config				= $this->accountConfig[$account2Id];
	$logs				= array();
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	$datas	= $this->ci->db->get_where('sales_credit_order',array(/* 'sendPaymentTo' => 'xero','isPaymentCreated' => '0', */'status >=' => '1','account2Id' => $account2Id))->result_array();
	$paymentMappings		= array();
	$paymentMappingsTemps	= $this->ci->db->get_where('mapping_payment',array('account2Id' => $account2Id))->result_array();
	foreach($paymentMappingsTemps as $paymentMappingsTemp){
		$paymentMappings[$paymentMappingsTemp['account1PaymentId']]	= $paymentMappingsTemp;
	}
	if($datas){
		$journalIds	= array();
		foreach($datas as $orderDatas){
			$paymentDetails	= json_decode($orderDatas['paymentDetails'],true);
			if($paymentDetails){
				foreach($paymentDetails as $paymentKey => $paymentDetail){
					if($paymentDetail['sendPaymentTo'] == 'xero'){
						if($paymentDetail['status'] == '0'){
							if($paymentKey){
								$journalIds[]	= $paymentKey;
							}
						}								
					}
				}
			}
		}
		$journalIds		= array_unique($journalIds);
		$journalDatas	= $this->ci->brightpearl->fetchJournalByIds($journalIds);
		foreach($datas as $orderDatas){
			$orderId	= $orderDatas['orderId'];
			$account1Id	= '';
			$rowDatas	= json_decode($orderDatas['rowData'],true);					
			if(@!$orderDatas['createOrderId']){
				continue;
			}
			$createdRowData	= json_decode($orderDatas['createdRowData'],true);	
			$paymentDetails	= json_decode($orderDatas['paymentDetails'],true);
			$reference		= array();
			$amount			= 0;
			$paidAmount		= 0;
			$paymentMethod	= $orderDatas['paymentMethod'];
			$totalReceivedPaidAmount	= 0;
			
			/* PAYMENTREVERSAL CODE STARTS */
			if($brightpearlconfig['PaymentReversalEnabled']){
				foreach($paymentDetails as $reversekey => $paymentDetail){
					if(($paymentDetail['amount'] < 0) AND ($paymentDetail['status'] == 0) AND ($paymentDetail['paymentType'] == 'RECEIPT') AND ($paymentDetail['paymentInitiatedIn'] == 'brightpearl')){
						$brightpearlreversepaymentkey[$reversekey] = $paymentDetail;
					}
				}
				$updateArray		= array();
				$error_reported		= 0;
				$copypaymentdetails	= $paymentDetails;
				if($brightpearlreversepaymentkey){
					foreach($brightpearlreversepaymentkey as $reversepaymentkey => $brightpearlreversepaymentkeyss){
						foreach($copypaymentdetails as $newkeys => $paymentDetail){
							if(strlen($newkeys) > 20){
								if($paymentDetail['status'] == 1){
									if(($paymentDetail['paymentMethod'] == $brightpearlreversepaymentkeyss['paymentMethod']) AND (($paymentDetail['amount'] + $brightpearlreversepaymentkeyss['amount']) == 0)){
										$xeroReverseKeys	= $newkeys;
										if($xeroReverseKeys){
											$DeletePaymentRequest	= array(
												"PaymentID"				=>	$xeroReverseKeys,
												"Status"				=>	"DELETED",
											);
											$DeleteUrl				= '2.0/Payments';
											$this->initializeConfig($account2Id, 'POST', $DeleteUrl);
											$DeleteResponse			= $this->getCurl($DeleteUrl, 'POST', json_encode($DeletePaymentRequest), 'json', $account2Id)[$account2Id];
											if(strtolower($DeleteResponse['Status'])=='ok'){
												$paymentDetails[$newkeys]['PaymentDelete']		= 'yes';
												$paymentDetails[$newkeys]['reverseby']			= 'brightpearl';
												$paymentDetails[$reversepaymentkey]['status']	= '1';
												$paymentDetails[uniqid()]						= $DeleteResponse;
												
												unset($copypaymentdetails[$newkeys]);
												unset($brightpearlreversepaymentkey[$reversepaymentkey]);
											}
											else{
												$error_reported	= 1;
												continue;
											}
										}
									}
								}
							}
						}
					}
					if($error_reported == 0){
						$updateArray['paymentDetails']	= json_encode($paymentDetails);
						$updateArray['status']			= 1;
						$updateArray['sendPaymentTo']	= '';
						$this->ci->db->where(array('orderId' => $orderId))->update('sales_credit_order',$updateArray);
					}
				}
				$paymentDetails		= array();
				$updatedPaymentdata	= $this->ci->db->select('paymentDetails')->get_where('sales_credit_order',array('orderId' => $orderId))->row_array();
				$paymentDetails		= json_decode($updatedPaymentdata['paymentDetails'],true);
			}
			/* PAYMENTREVERSAL CODE ENDS */
			
			foreach($paymentDetails as $paymentKey => $paymentDetail){
				if($paymentDetail['sendPaymentTo'] == 'xero'){
					$totalReceivedPaidAmount	+= $paymentDetail['amount'];
					if($paymentDetail['status'] == '0' AND $paymentDetail['amount'] > 0){
						if(@$paymentDetail['lastTry']){
							if($paymentDetail['lastTry'] > strtotime('-6 hour')){
								continue;
							}
						}
						$amount	= $paymentDetail['amount'];
						if(isset($journalDatas[$paymentKey])){
							$reference		= $journalDatas[$paymentKey]['description'];
							$CurrencyRate	= $journalDatas[$paymentKey]['exchangeRate'];
						}
						if($paymentDetail['paymentMethod']){
							$paymentMethod	= $paymentDetail['paymentMethod'];
						}
						$accountCode	= $config['IncomeAccountRef'];
						if($paymentMethod){
							if(isset($paymentMappings[$paymentMethod])){
								$accountCode	= $paymentMappings[$paymentMethod]['account2PaymentId'];
							}
						}
						if(@$paymentDetail['paymentDate']){
							$paymentDate	= $paymentDetail['paymentDate'];
							$BPDateOffset	= (int)substr($paymentDate,23,3);
							$xeroOffset		= 0;
							$diff			= 0;
							$diff			= $BPDateOffset - $xeroOffset;
							$date			= new DateTime($paymentDate);
							$BPTimeZone		= 'GMT';
							$date->setTimezone(new DateTimeZone($BPTimeZone));
							if($diff > 0){
								$diff			.= ' hour';
								$date->modify($diff);
							}
							$paymentDate	= $date->format('Y-m-d');
						}
						else{
							$paymentDate	= date('c');
						}
						$request	= array( 
							'CreditNote'	=> array('CreditNoteID' => $orderDatas['createOrderId']),
							'Account'		=> array('Code' => $accountCode),
							'Date'			=> $paymentDate,
							'Reference'		=> $reference,
							'CurrencyRate'	=> $CurrencyRate,
							'Amount'		=> $amount,
						);
						if(!$request['CurrencyRate']){
							unset($request['CurrencyRate']);
						}
						if(!$request['Reference']){
							unset($request['Reference']);
						}
						$url		= '2.0/Payments';
						$this->initializeConfig($account2Id, 'PUT', $url);
						$results	= $this->getCurl($url, 'PUT', json_encode($request), 'json', $account2Id)[$account2Id];
						$createdRowData['Create payment request date : '.date('Y-m-d-H-i-s')]	= $request;
						$createdRowData['Create payment response date: '.date('Y-m-d-H-i-s')]	= $results;
						$logs[$orderDatas['orderId']]	= array(
							'Request data 	: '	=> $request,
							'Response data 	: '	=> $results,
						);			
						$this->ci->db->update('sales_credit_order',array('createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
						if((strtolower($results['Status']) == 'ok') && (isset($results['Payments']['Payment']['PaymentID']))){
							$updateArray	= array();
							$paymentDetails[$paymentKey]['status']							= '1';
							$paymentDetails[$paymentKey]['AmountCreditedIn']				= 'Xero';
							$paymentDetails[$paymentKey]['XeroPaymentID']					= $results['Payments']['Payment']['PaymentID'];
							
							$paymentDetails[$results['Payments']['Payment']['PaymentID']]	= array(
								'amount'				=> $amount,
								'parentpaymentID'		=> $paymentKey, 
								'status' 				=> 1,
								'AmountCreditedIn'		=> 'xero',
								'DeletedonBrightpearl'	=> 'NO',
								'paymentMethod'			=> $paymentDetail['paymentMethod'],
							
							);
							
							$xeroAmount	= $createdRowData['Response data : ']['CreditNotes']['CreditNote']['Total'];							
							if($totalReceivedPaidAmount >= $xeroAmount){
								$updateArray	= array(
									'isPaymentCreated'	=> '1',
									'status' 			=> '3',
								);
							}
							$updateArray['paymentDetails'] = json_encode($paymentDetails);
							$this->ci->db->where(array('orderId' => $orderDatas['orderId']))->update('sales_credit_order',$updateArray);   
						}
						else{
							$updateArray	= array();
							$paymentDetails[$paymentKey]['lastTry']	= strtotime('now');
							$updateArray['paymentDetails']			= json_encode($paymentDetails);
							$this->ci->db->where(array('orderId' => $orderDatas['orderId']))->update('sales_credit_order',$updateArray);
						}
					}
				}
			}
			
		}				
	}
	if($logs){
		foreach($logs as $orderId => $log){
			$path	= FCPATH.'logs'.DIRECTORY_SEPARATOR . $accountDetails['account1Id'].DIRECTORY_SEPARATOR . 'sc'.DIRECTORY_SEPARATOR;
			if(!is_dir($path)){
				mkdir($path,0777,true);
				chmod(dirname($path), 0777);
			}
			file_put_contents($path. $orderId.'.logs',"\n\n BP Log Added On : ".date('c')." \n". json_encode($log),FILE_APPEND);
		}
	}
}
?>