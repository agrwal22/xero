<?php
$this->reInitialize();
$enableAvalaraTax           = $this->ci->globalConfig['enableAvalaraTax'];
$enableGiftCard             = $this->ci->globalConfig['enableGiftCard'];
$enableAmazonFees           = $this->ci->globalConfig['enableAmazonFees'];
$enableInventoryTransfer    = $this->ci->globalConfig['enableInventoryTransfer'];
$enableInventoryManagement	= $this->ci->globalConfig['enableInventoryManagement'];
foreach($this->accountDetails as $account2Id => $accountDetails){
	if($accountDetails['OAuthVersion'] == '2'){
		$this->refreshToken($account2Id);
	}
	$config	= $this->accountConfig[$account2Id];$logs = array();
	 if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	$datas	= $this->ci->db->order_by('orderId','desc')->get_where('sales_order',array('isPaymentCreated' => '0','status >=' => '1','account2Id' => $account2Id))->result_array();
	$paymentMappings		= array();
	$paymentMappingsTemps	= $this->ci->db->get_where('mapping_payment',array('account2Id' => $account2Id))->result_array();
	foreach($paymentMappingsTemps as $paymentMappingsTemp){
		$paymentMappings[$paymentMappingsTemp['account1PaymentId']]	= $paymentMappingsTemp;
	}
	if($datas){
		$journalIds	= array();
		$tempOrderId = array();
		foreach($datas as $orderDatas){
			if(@!$orderDatas['createOrderId']){
				continue;
			}
			$paymentDetails	= json_decode($orderDatas['paymentDetails'],true);
			if($paymentDetails){
				foreach($paymentDetails as $paymentKey => $paymentDetail){
					if($paymentDetail['sendPaymentTo'] == 'xero'){
						if($paymentDetail['status'] == '0'){
							$tempOrderId[$orderDatas['orderId']] = $paymentDetails;
							if($paymentKey){
								$journalIds[]	= $paymentDetail['journalId'];
								$journalIds[]	= $paymentKey;
							}
						}								
					}
				}
			}
		}
		$journalIds		= array_filter($journalIds);
		$journalIds		= array_unique($journalIds);
		//sort($tempOrderId);
		sort($journalIds);	
		$journalDatas	= $this->ci->brightpearl->fetchJournalByIds($journalIds);
		foreach($datas as $orderDatas){
			if(@!$orderDatas['createOrderId']){
				continue;
			}
			$account1Id			= $orderDatas['account1Id'];
			$config1			= $this->ci->account1Config[$account1Id];
			$giftCardPayment	= $config1['giftCardPayment'];
			$orderId			= $orderDatas['orderId'];
			$rowDatas			= json_decode($orderDatas['rowData'],true);
			$parentOrderId = $rowDatas['parentOrderId'];
			$parentPaymentInfos = array();
			if(@!$orderDatas['createOrderId']){
				continue;
			}	
			$createdRowData	= json_decode($orderDatas['createdRowData'],true);	
			$paymentDetails	= json_decode($orderDatas['paymentDetails'],true);
			if(!$paymentDetails){ continue; }				
			$reference		= '';
			$paymentMethod	= $orderDatas['paymentMethod'];
			$amount			= 0;
			$paidAmount		= 0;
			$giftAmount		= 0;
			$CurrencyRate	= 1;
			$totalReceivedPaidAmount	= 0;
							// PAYMENT REVERSAL CODE STARTS //// PAYMENT REVERSAL CODE STARTS //// PAYMENT REVERSAL CODE STARTS //
			$PaidPayment				= array();
			$ReversePayment 			= array();
			$updateArray				= array();
			$deletedKey					= array();
			$xeroReversePaymentIDs		= array();
			$adjustmentPaymentAmount	= 0;
			$totalPaymentSentAmount		= 0;
			$mainPaymentFound = 0;
			foreach($paymentDetails as $key => $PaymentDetail){
				if(($PaymentDetail['status'] == 0)){
					if($PaymentDetail['paymentType'] == 'RECEIPT'){
						$mainPaymentFound = 1;
					}
					
				}
			}
			$adjPayType = array('ADJUSTMENT','OTHER');
			$adjutmentUsedPaymentId = array();
			foreach($paymentDetails as $key => $PaymentDetail){
				if(@$PaymentDetail['sendPaymentTo'] == 'xero'){
					//if(($PaymentDetail['status'] == 0) || (($mainPaymentFound) && ($PaymentDetail['paymentMethod'] == 'ADJUSTMENT'))){
					if(($PaymentDetail['status'] == 0)){
						if($PaymentDetail['paymentType'] == 'PAYMENT'){
							if($mainPaymentFound){
								if($PaymentDetail['paymentMethod'] == 'ADJUSTMENT'){
									$adjustmentPaymentAmount	+= abs($PaymentDetail['amount']);
								}
								else if($PaymentDetail['paymentMethod'] == 'OTHER'){
									$adjustmentPaymentAmount	+= abs($PaymentDetail['amount']);
								}
								else{
									$ReversePayment[$key]	= $PaymentDetail;
								}
							}	
							else{
								if(!in_array($PaymentDetail['paymentMethod'],$adjPayType)){
									$ReversePayment[$key]	= $PaymentDetail;
								}
							}
						}
						else if($PaymentDetail['paymentType'] == 'RECEIPT'){
							$updateParentPayment	= 0;$usedupdateParentPayment = 0;
							if($PaymentDetail['paymentMethod'] == 'ADJUSTMENT'){
								$updateParentPayment	= 1;
							}
							else if($PaymentDetail['paymentMethod'] == 'OTHER'){
								$updateParentPayment	= 1;
							}
							if($updateParentPayment){
								if(!$parentPaymentInfos){
									if($parentOrderId){
										$parentPaymentInfosTemps	= $this->ci->db->get_where('sales_order',array('orderId' => $parentOrderId))->row_array();
										if($parentPaymentInfosTemps){
											$parentPaymentInfos	= json_decode($parentPaymentInfosTemps['paymentDetails'],true);
										}
										if($parentPaymentInfos){
											foreach($parentPaymentInfos as $parentPaymentInfo){
												if($parentPaymentInfo['paymentType'] == 'RECEIPT'){
													if(($parentPaymentInfo['paymentMethod'] != 'ADJUSTMENT') && ($parentPaymentInfo['paymentMethod'] != 'OTHER')){
														$PaymentDetail['paymentMethod']	= $parentPaymentInfo['paymentMethod'];
														$usedupdateParentPayment = 1;
													}
												}
											}
										}
									}
								}
							}
							if($PaymentDetail['paymentMethod'] == 'ADJUSTMENT'){
								if($usedupdateParentPayment){
									$totalPaymentSentAmount	+= abs($PaymentDetail['amount']);
									$PaidPayment[$key]		= $PaymentDetail;
								}
								else{
									$adjutmentUsedPaymentId[$key] = $PaymentDetail;
								}
							}
							else{
								$totalPaymentSentAmount	+= abs($PaymentDetail['amount']);
								$PaidPayment[$key]		= $PaymentDetail;
							}
						}						
					}
				}
			}
			if(($totalPaymentSentAmount > 0) && ($totalPaymentSentAmount < $adjustmentPaymentAmount)){
				$remainingAmount	= $adjustmentPaymentAmount - $totalPaymentSentAmount;
				$sentPaymentDetails	= array();
				foreach($paymentDetails as $key => $PaymentDetail){
					if(@$PaymentDetail['sendPaymentTo'] == 'xero'){
						if($PaymentDetail['status'] == 1){							
							if($PaymentDetail['paymentType'] == 'RECEIPT'){
								$tempPaidAmount				= abs($PaymentDetail['amount']);
								$remainingAmount			= $remainingAmount - $tempPaidAmount;
								$sentPaymentDetails[$key]	= $PaymentDetail;
								if($remainingAmount <= 0){
									break;
								}
							}						
						}
					}
				} 
				if($sentPaymentDetails){
					$isDeleted	= 0;
					foreach($sentPaymentDetails as $key => $sentPaymentDetail){
						foreach($paymentDetails as $subkey => $PaymentDetail){
							if($key == $PaymentDetail['ParentOrderId']){
								if(strtolower($PaymentDetail['PaymentDelete']) != 'yes'){
									$DeletePaymentRequest	= array(
										"PaymentID"	=>	$subkey,
										"Status"	=>	"DELETED",
									);
									$DeleteUrl		= '2.0/Payments';
									$this->initializeConfig($account2Id, 'POST', $DeleteUrl);
									$DeleteResponse	= $this->getCurl($DeleteUrl, 'POST', json_encode($DeletePaymentRequest), 'json', $account2Id)[$account2Id];
									if(strtolower($DeleteResponse['Status'])=='ok'){
										$isDeleted	= 1;
										$paymentDetails[$subkey]['PaymentDelete']	= 'yes';
										$paymentDetails[$subkey]['reverseby']		= 'brightpearl';
										$paymentDetails[$key]['status']				= '0';
										$updateArray['paymentDetails']				= json_encode($paymentDetails);
										$this->ci->db->where(array('orderId' => $orderDatas['orderId']))->update('sales_order',$updateArray);
									}
								}
							}
						}
					}
					if($isDeleted){
						continue;
					}
				}
			}
			$orgAdjustmentPaymentAmount	= $adjustmentPaymentAmount;
			foreach($ReversePayment as $key => $pData){
				foreach($PaidPayment as $key2 => $rData){
					if(($pData['amount']+$rData['amount']) == 0){
						$deletedKey	= array($key,$key2);
						unset($ReversePayment[$key]);
						unset($PaidPayment[$key2]);
						break;
					}
				}
			}
			if($deletedKey){
				foreach($paymentDetails as $updatekey => $paymentDetailsdata){
					if(in_array($updatekey, $deletedKey)){
						$paymentDetails[$updatekey]['status']	= 1; 
					}
				}
			}
			$reverseAppliedIds = array();
			if($ReversePayment){
				foreach($ReversePayment as $key => $Details){
					foreach($paymentDetails as $key2 => $PaymentDetail){
						if((strlen($key2) > 20) && (!@$paymentDetails[$key2]['ReversePayId'])){
							if(($PaymentDetail['amount']+$Details['amount'] == 0) && ($PaymentDetail['paymentMethod'] == $Details['paymentMethod'])){
								$xeroReversePaymentIDs[]				= $key2;
								$reverseAppliedIds[] = $key;
								$reverseAppliedIds[] = $key2;								
								$paymentDetails[$key2]['ReversePayId']	= $key;
								unset($ReversePayment[$key]);
								break;
							}
							else{
								if($adjutmentUsedPaymentId){
									foreach($adjutmentUsedPaymentId as $adjcheckKey => $adjutmentUsedPaymentIdDatas){
										$adjPayment = $adjutmentUsedPaymentIdDatas['amount'];
										if($adjPayment < 0){
											$adjPayment = (-1) * $adjPayment;
										}
										$PaymentDetailamount = $PaymentDetail['amount'];
										$Detailsamount = $Details['amount'];
										$tempSumCheck = round($Detailsamount + $PaymentDetailamount,3);
										if(($tempSumCheck + $adjPayment == 0) && ($PaymentDetail['paymentMethod'] == $Details['paymentMethod'])){
											$xeroReversePaymentIDs[]				= $key2;
											$reverseAppliedIds[] = $key;
											$reverseAppliedIds[] = $key2;
											$reverseAppliedIds[] = $adjcheckKey;
											$paymentDetails[$key2]['ReversePayId']	= $key;
											unset($ReversePayment[$key]);
											unset($adjutmentUsedPaymentId[$adjcheckKey]);
											break;
										}
									}
								}
							}
						}
					}
				}
			}
			if($xeroReversePaymentIDs){
				foreach($xeroReversePaymentIDs as $xeroReversePaymentID){
					$DeletePaymentRequest	= array(
						"PaymentID"	=>	$xeroReversePaymentID,
						"Status"	=>	"DELETED",
					);
					$DeleteUrl		= '2.0/Payments';
					$this->initializeConfig($account2Id, 'POST', $DeleteUrl);
					$DeleteResponse	= $this->getCurl($DeleteUrl, 'POST', json_encode($DeletePaymentRequest), 'json', $account2Id)[$account2Id];
					if(strtolower($DeleteResponse['Status'])=='ok'){
						$paymentDetails[$xeroReversePaymentID]['PaymentDelete']	= 'yes';
						$paymentDetails[$xeroReversePaymentID]['reverseby']		= 'brightpearl';
						if($reverseAppliedIds){
							foreach($reverseAppliedIds as $reverseAppliedId){
								$paymentDetails[$reverseAppliedId]['status'] = 1;
							}
						}
					}
				}
			}
			$updateArray['paymentDetails']	= json_encode($paymentDetails);
			$this->ci->db->where(array('orderId' => $orderDatas['orderId']))->update('sales_order',$updateArray);
				// PAYMENT REVERSAL CODE ENDS //// PAYMENT REVERSAL CODE ENDS //// PAYMENT REVERSAL CODE ENDS //
			$isPaymentPosted = 0;
			foreach($PaidPayment as $paymentKey => $paymentDetail){
				if($paymentDetail['sendPaymentTo'] == 'xero'){
					$totalReceivedPaidAmount	+= $paymentDetail['amount'];
					if(($paymentDetail['status'] == '0') && ($paymentDetail['amount'] > 0)){
						if(@$paymentDetail['lastTry']){
							if($paymentDetail['lastTry'] > strtotime('-6 hour')){
								continue;
							}
						}
						$isPostPayment = 1;
						if($enableGiftCard){
							if($paymentDetail['paymentMethod'] == $giftCardPayment){
								$isPostPayment = 0;
								$giftAmount		= $paymentDetail['amount'];
								$suburl			= '2.0/Invoices/'.$orderDatas['createOrderId'];
								$this->initializeConfig($account1Id, 'GET', $suburl);
								$qboOrderInfos	= $this->getCurl($suburl, 'get', '', 'json', $account1Id)[$account1Id];
								if(!$qboOrderInfos['Invoices']['Invoice']['LineItems']['LineItem']['0']){
									$qboOrderInfos['Invoices']['Invoice']['LineItems']['LineItem']	= array($qboOrderInfos['Invoices']['Invoice']['LineItems']['LineItem']);
								}
								$qboOrderInfos['Invoices']['Invoice']['LineItems']['LineItem'][count($qboOrderInfos['Invoices']['Invoice']['LineItems']['LineItem'])]	= array(
									'ItemCode' 		=> $config['giftCardItem'],
									'Description'	=> 'Gift Card',
									'Quantity' 		=> 1,
									'UnitAmount' 	=> (-1) * sprintf("%.2f",$giftAmount),
									'LineAmount' 	=> (-1) * sprintf("%.2f",$giftAmount),
									'TaxAmount' 	=> 0.00,
								);
								$rqInvoice							= $qboOrderInfos['Invoices']['Invoice'];
								$rqInvoice['Contact']['Addresses']	= $rqInvoice['Contact']['Addresses']['Address'];
								$rqInvoice['Contact']['Phones']		= $rqInvoice['Contact']['Phones']['Phone'];
								$rqInvoice['LineItems']				= $rqInvoice['LineItems']['LineItem'];
								$suburl								= '2.0/Invoices/';
								$this->initializeConfig($account1Id, 'POST', $suburl);
								$updateResults	= @$this->getCurl($suburl, 'POST', json_encode($rqInvoice), 'json',$account2Id)[$account2Id];
								$createdRowData['Gift card request date : '.date('Y-m-d-H-i-s')]	= $request;
								$createdRowData['Gift card response date: '.date('Y-m-d-H-i-s')]	= $updateResults;
								$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
								if((strtolower($updateResults['Status']) == 'ok') && (isset($updateResults['Invoices']['Invoice']['InvoiceID']))){	
									$updateArray	= array();
									if($paymentDetail['sendPaymentTo'] == 'xero'){
										if($paymentDetail['paymentMethod'] == $giftCardPayment){
											$paymentDetails[$paymentKey]['status']	= '1';
										}
									}
									$paymentDetails[$results['Payments']['Payment']['PaymentID']]	= array(
										"amount" 		=> '0.00',
										"ParentOrderId"	=>	$paymentKey,
										'status' 		=> '1',
									);
									$xeroAmount	= $createdRowData['Response data : ']['Invoices']['Invoice']['Total'];
									if($totalReceivedPaidAmount >= $xeroAmount){
										$updateArray	= array(
											'isPaymentCreated'	=> '1',
											'status' 			=> '3',
										);
									}
									$updateArray['paymentDetails']	= json_encode($paymentDetails);
									$this->ci->db->where(array('orderId' => $orderDatas['orderId']))->update('sales_order',$updateArray);
								}
							}
						}
						if($isPostPayment){					
							$amount	= $paymentDetail['amount'];
							if($adjustmentPaymentAmount){
								if($amount > $adjustmentPaymentAmount){
									$amount	= $amount - $adjustmentPaymentAmount;
									$adjustmentPaymentAmount	= 0;
								}
							}
							if($amount == 0){
								if($paymentDetail['sendPaymentTo'] == 'xero'){
									if($paymentDetail['paymentMethod'] != $giftCardPayment){
										$paymentDetails[$paymentKey]['status']	= '1';
									}
								}
								$updateArray['paymentDetails']	= json_encode($paymentDetails);
								$this->ci->db->where(array('orderId' => $orderDatas['orderId']))->update('sales_order',$updateArray);
							}
							if($amount <= 0){
								continue;
							}
							$foundJournalData	= 0;
							if($paymentDetail['journalId']){
								if(isset($journalDatas[$paymentDetail['journalId']])){
									@$reference			= $journalDatas[$paymentDetail['journalId']]['description'];
									$CurrencyRate		= $journalDatas[$paymentDetail['journalId']]['exchangeRate'];
									$foundJournalData	= 1;
								}
							}
							if(!$foundJournalData){
								if(isset($journalDatas[$paymentKey])){
									@$reference		= $journalDatas[$paymentKey]['description'];
									$CurrencyRate	= $journalDatas[$paymentKey]['exchangeRate'];
								}
							}
							$accountCode	= $config['IncomeAccountRef'];
							if($paymentDetail['paymentMethod']){
								$paymentMethod	= $paymentDetail['paymentMethod'];
							}
							if($paymentMethod){
								if(isset($paymentMappings[$paymentMethod])){
									$accountCode	= $paymentMappings[$paymentMethod]['account2PaymentId'];
								}
							}
							if(@$paymentDetail['paymentDate']){
								$paymentDate	= $paymentDetail['paymentDate'];
								$BPDateOffset	= (int)substr($paymentDate,23,3);
								$xeroOffset		= 0;
								$diff			= 0;
								$diff			= $BPDateOffset - $xeroOffset;
								$date			= new DateTime($paymentDate);
								$BPTimeZone		= 'GMT';
								$date->setTimezone(new DateTimeZone($BPTimeZone));
								if($diff > 0){
									$diff			.= ' hour';
									$date->modify($diff);
								}
								$paymentDate	= $date->format('Y-m-d');
							}
							else{
								$paymentDate	= date('c');
							}
							
							$request	= array(
								'Invoice'		=> array('InvoiceID' => $orderDatas['createOrderId']),
								'Account'		=> array('Code' => $accountCode),
								'Date'			=> $paymentDate,
								'CurrencyRate'	=> $CurrencyRate,
								'Reference'		=> $reference,
								'Amount'		=> $amount,
							);
							if(!$request['Reference']){
								unset($request['Reference']);
							}
							$url		= '2.0/Payments';
							$this->initializeConfig($account2Id, 'PUT', $url);
							$results	= $this->getCurl($url, 'PUT', json_encode($request), 'json', $account2Id)[$account2Id];
							$createdRowData['Create xero payment request date']		= $request;
							$createdRowData['Create xero payment response date']	= $results;
							$logs[$orderDatas['orderId']]	= array(
								'Request data 	: '	=> $request,
								'Response data	: '	=> $results,
							);
							$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
							if((strtolower($results['Status']) == 'ok') && (isset($results['Payments']['Payment']['PaymentID']))){
								$isPaymentPosted = 1;
								$updateArray	= array();
								if($paymentDetail['sendPaymentTo'] == 'xero'){
									if($paymentDetail['paymentMethod'] != $giftCardPayment){
										$paymentDetails[$paymentKey]['status']	= '1';
									}
								}
								$paymentDetails[$results['Payments']['Payment']['PaymentID']]	= array(
									"amount" 				=> $amount,
									"ParentOrderId"			=> $paymentKey,
									'status'				=> '1',
									'AmountCreditedIn'		=> 'xero',
									'DeletedonBrightpearl'	=> 'NO',
									'paymentMethod'			=> $paymentDetail['paymentMethod'],					
								);
								$xeroAmount	= $createdRowData['Response data : ']['Invoices']['Invoice']['Total'];
								if($totalReceivedPaidAmount >= $xeroAmount){
									$updateArray	= array(
										'isPaymentCreated'	=> '1',
										'status' 			=> '3',
									);
								}
								$updateArray['paymentDetails']	= json_encode($paymentDetails);
								$this->ci->db->where(array('orderId' => $orderDatas['orderId']))->update('sales_order',$updateArray);
							}
							else{
								$updateArray	= array();
								$paymentDetails[$paymentKey]['lastTry']	= strtotime('now');
								$updateArray['paymentDetails']			= json_encode($paymentDetails);
								$this->ci->db->where(array('orderId' => $orderDatas['orderId']))->update('sales_order',$updateArray);
							}
						}
					}
				} 
			}
			if($isPaymentPosted){
				if($orgAdjustmentPaymentAmount){
					if($adjustmentPaymentAmount <= 0){
						foreach($paymentDetails as $key => $PaymentDetail){
							if(@$PaymentDetail['sendPaymentTo'] == 'xero'){
								if($PaymentDetail['status'] == 0){
									if($PaymentDetail['paymentType'] == 'PAYMENT'){
										if($PaymentDetail['paymentMethod'] == 'ADJUSTMENT'){
											$paymentDetails[$key]['status'] = 1;
										}
										else if($PaymentDetail['paymentMethod'] == 'OTHER'){
											$paymentDetails[$key]['status'] = 1;
										}
									}													
								}
							}
						}
						$updateArray['paymentDetails']	= json_encode($paymentDetails);
						$this->ci->db->where(array('orderId' => $orderDatas['orderId']))->update('sales_order',$updateArray);
					}
					else{
						$adjustmentDiff = $orgAdjustmentPaymentAmount - $adjustmentPaymentAmount;
						foreach($paymentDetails as $key => $PaymentDetail){
							if(@$PaymentDetail['sendPaymentTo'] == 'xero'){
								if($PaymentDetail['status'] == 0){
									if($PaymentDetail['paymentType'] == 'PAYMENT'){
										if($PaymentDetail['paymentMethod'] == 'ADJUSTMENT'){
											$amoumt = abs($PaymentDetail['amount']);
											if($adjustmentDiff > $amoumt){
												$adjustmentDiff = $adjustmentDiff - $amoumt;
												$paymentDetails[$key]['status'] = 1;
											}
											else{											
												$paymentDetails[$key]['amount'] = $amoumt - $adjustmentDiff;
												$adjustmentDiff = 0;
											}
										}
										else if($PaymentDetail['paymentMethod'] == 'OTHER'){
											if($adjustmentDiff > $amoumt){
												$adjustmentDiff = $adjustmentDiff - $amoumt;
												$paymentDetails[$key]['status'] = 1;
											}
											else{											
												$paymentDetails[$key]['amount'] = $amoumt - $adjustmentDiff;
												$adjustmentDiff = 0;
											}
										}
									}
									if($adjustmentDiff <= 0){
										break;
									}
								}
							}
						}
						$updateArray['paymentDetails']	= json_encode($paymentDetails);
						$this->ci->db->where(array('orderId' => $orderDatas['orderId']))->update('sales_order',$updateArray);
					}
				}
			}
		}
	}
	if($logs){
		foreach($logs as $orderId => $log){
			$path = FCPATH.'logs'.DIRECTORY_SEPARATOR . $accountDetails['account1Id'].DIRECTORY_SEPARATOR . 'sc'.DIRECTORY_SEPARATOR;
			if(!is_dir($path)){
				mkdir($path,0777,true);
				chmod(dirname($path), 0777);
			}
			file_put_contents($path. $orderId.'.logs',"\n\n BP Log Added On : ".date('c')." \n". json_encode($log),FILE_APPEND);
		}
	}
}
?>