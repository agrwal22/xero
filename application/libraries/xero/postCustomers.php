<?php
$this->reInitialize();
$countryLists		= array();
$countryListTemps	= json_decode(file_get_contents(FCPATH . 'application' . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'country' . DIRECTORY_SEPARATOR . 'countries.json'),true);
foreach($countryListTemps as $countryListTemp){
	$countryLists[strtolower($countryListTemp['alpha-3'])]	= $countryListTemp;
}
$currencyDetails	= $this->ci->brightpearl->getAllCurrency(); 
foreach($this->accountDetails as $account2Id => $accountDetails){
	if($accountDetails['OAuthVersion'] == '2'){
		$this->refreshToken($account2Id);
	}
	$config		= $this->accountConfig[$account2Id];
	if($orgObjectId){
		$this->ci->db->where_in('customerId',$orgObjectId);
	}
	$datas				= $this->ci->db->where_in('status',array('0','2'))->get_where('customers',array('account2Id' => $account2Id))->result_array();
	
	$taxMapppings		= array();
	$taxMappingTemps	= $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id))->result_array();
	$taxType			= array('0' => 'all','1' => 'sales','2' => 'purchase');
	foreach($taxMappingTemps as $taxMappingTemp){
		$orderType = (int)$taxMappingTemp['orderType'];
		$taxMapppings[$taxMappingTemp['account1TaxId']][$orderType] = $taxMappingTemp;
	}
	
	if($datas){
		$datass	= array_chunk($datas,1);
		foreach($datass as $datas){				
			$requests		= array();
			$requestCount	= 0;
			$requestLogs	= array();
			foreach($datas as $customerDatas){	
				$request		= array();
				$account1Id		= $customerDatas['account1Id'];
				$currencyDetail	= $currencyDetails[$account1Id];
				$config1		= $this->ci->account1Config[$customerDatas['account1Id']];
				$companyName	= $customerDatas['company'];
				$ContactPersons	= array();
				/* if($config['BrightpearlAnonymousCustomer']){
					if($companyName == $config['BrightpearlAnonymousCustomer']){
						$this->ci->db->update('customers',array('status' => '1','createdCustomerId' => $config['XeroAnonymousCustomer']),array('customerId' => $customerDatas['customerId'],'account2Id' => $account2Id));					
						continue;
					}
				} */
				if($companyName){
					$customerCount			= 0;
					$ContactPersonsTemps	= $this->ci->db->get_where('customers',array('account2Id' => $account2Id, 'company' => $companyName))->result_array();
					foreach($ContactPersonsTemps as $ContactPersonsTemp){
						$ContactPersonsTempRowData	= json_decode($ContactPersonsTemp['params'],true);
						if(!$ContactPersonsTempRowData['isPrimaryContact']){
							$customerCount++;
							if($customerCount < 6){
								$ContactPersons[]	= array(
									'FirstName' 		=> $ContactPersonsTemp['fname'],
									'LastName' 			=> $ContactPersonsTemp['lname'],
									'EmailAddress' 		=> $ContactPersonsTemp['email'],
									'IncludeInEmails'	=> false,
								);
							}
						}
						else{
							$customerDatas = $ContactPersonsTemp;
						}
					}
				}
				$customerId		= $customerDatas['customerId'];
				$rowDatas		= json_decode($customerDatas['params'],true);
				$name           = $customerDatas['fname'].' '.$customerDatas['lname'];
				$billAddress    = $rowDatas['postalAddresses'][$rowDatas['postAddressIds']['BIL']];
				$shipAddress    = $rowDatas['postalAddresses'][$rowDatas['postAddressIds']['DEL']];
				$billCountry    = $billAddress['countryIsoCode'];
				$shipCountry    = $shipAddress['countryIsoCode'];
				if(($billCountry) && (isset($countryLists[strtolower($billCountry)]))){
					$billCountry	= $countryLists[strtolower($billCountry)]['name'];
				}
				if(($shipCountry) && (isset($countryLists[strtolower($shipCountry)]))){
					$shipCountry	= $countryLists[strtolower($shipCountry)]['name'];
				}
				$currencyCode	= $config1['currencyCode'];
				if(isset($rowDatas['financialDetails']['currencyId'])){
					if(isset($currencyDetail[$rowDatas['financialDetails']['currencyId']])){
						$currencyCode	= $currencyDetail[$rowDatas['financialDetails']['currencyId']]['code'];
					}
				}
				$request	= array(
					'ContactNumber' 	=> $customerDatas['customerId'],
					'Name' 				=> ($customerDatas['company'])?($customerDatas['company']):$name,
					'FirstName' 		=> $customerDatas['fname'], 
					'LastName' 			=> $customerDatas['lname'],
					'Addresses'			=> array(
						array(
							'AddressType' 	=> 'STREET',
							'AddressLine1' 	=> @$billAddress['addressLine1'],
							'AddressLine2' 	=> @$billAddress['addressLine2'],
							'City' 			=> @$billAddress['addressLine3'],
							'Region' 		=> @$billAddress['addressLine4'],
							'PostalCode'	=> @$billAddress['postalCode'],
							'Country' 		=> @$billCountry,
						),
						array(
							'AddressType' 	=> 'POBOX',
							'AddressLine1' 	=> @$shipAddress['addressLine1'],
							'AddressLine2' 	=> @$shipAddress['addressLine2'],
							'City' 			=> @$shipAddress['addressLine3'],
							'Region' 		=> @$shipAddress['addressLine4'],
							'PostalCode'	=> @$shipAddress['postalCode'],
							'Country' 		=> @$shipCountry,
						),
						
					),
					'Phones'			=> array(
						array(
							'PhoneType' 	=> 'DEFAULT',
							'PhoneNumber' 	=> $customerDatas['phone'], 
						),
					),
					'IsCustomer'		=> ($customerDatas['isSupplier'])?false:true,
					'IsSupplier'		=> ($customerDatas['isSupplier'])?true:false,
					'EmailAddress'		=> $customerDatas['email'],
					'DefaultCurrency'	=> $currencyCode,
				);
				$bpTaxId	= $rowDatas['financialDetails']['taxCodeId'];
				if(isset($taxMapppings[$bpTaxId])){
					$taxMappping	= $taxMapppings[$bpTaxId];
					if(!isset($taxMappping['1'])){$taxMappping['1']	= $taxMappping['0'];}
					if(!isset($taxMappping['2'])){$taxMappping['2']	= $taxMappping['0'];}
					$request['AccountsReceivableTaxType']			= $taxMappping['1']['account2TaxId'];
					$request['AccountsPayableTaxType']				= $taxMappping['2']['account2TaxId'];
				}
				if($ContactPersons){
					$request['ContactPersons']	= $ContactPersons;
				}
				if(!$customerDatas['isSupplier']){
					$customerTypeValues	= @$rowDatas['customFields'][$config1['customerTypeCustomField']];
					if(isset($customerTypeMappings[strtolower($customerTypeValues)])){
						$customerType	= $customerTypeMappings[strtolower($customerTypeValues)]['account2customertypeId'];
						if($customerType){
							$request['CustomerTypeRef']	= array('value' => $customerType);
						}
					}
					if(!$rowDatas['isPrimaryContact']){
						$bpCompanyName	= $rowDatas['organisation']['name'];
						if($bpCompanyName){
							$isPrimaryCompanySent	= 0;
							$bpCompanyDatas			= $this->ci->db->get_where('customers',array('createdCustomerId <> ' => '','company' => $bpCompanyName))->result_array();
							$bpCompanyDatasInfo		= array();
							foreach($bpCompanyDatas as $bpCompanyData){
								$bpCompanyDataRowData	= json_decode($bpCompanyData['params'],true);
								if($bpCompanyDataRowData['isPrimaryContact']){
									$isPrimaryCompanySent	= 1;
									$bpCompanyDatasInfo		= $bpCompanyData;
									break;
								}
							}
							if($isPrimaryCompanySent){
								$request['Job']			= true;
								$request['ParentRef']	= array('value' => $bpCompanyDatasInfo['createdCustomerId']);
							}
							else{
								$this->ci->db->update('customers',array('message' => 'Primary customer not sent yet'),array('id' => $customerDatas['id']));
							}
						}
					}
				}
				if(@$rowDatas['communication']['telephones']['MOB']){
					$request['Phones'][]	= array(
						'PhoneType' 	=> 'MOBILE',
						'PhoneNumber' 	=> $rowDatas['communication']['telephones']['MOB'], 
					);
				}
				$requests['Contacts'][$requestCount]	= $request;
				$requestLogs[strtolower($request['ContactNumber'])]['Request Data']	= $request;
				$requestCount++;
			}
			if($requests){
				$customerIds	= array_column($datas,'customerId');
				if($customerIds){
					$url			= '2.0/Contacts';
					$this->initializeConfig($account2Id, 'POST', $url);
					$results		= $this->getCurl($url, 'POST', json_encode($requests), 'json', $account2Id)[$account2Id];
					$ceatedParams	= array(
						'Request data	: ' => $requests,
						'Response data	: ' => $results,
					);
					$this->ci->db->where_in('customerId',$customerIds)->update('customers',array('ceatedParams' => json_encode($ceatedParams)),array('account2Id' => $account2Id));
					if((strtolower($results['Status']) == 'ok') && (isset($results['Contacts']['Contact']))){
						$xeroContacts	= $results['Contacts']['Contact'];
						if(!isset($xeroContacts['0'])){
							$xeroContacts	= array($xeroContacts);
						}
						foreach($xeroContacts as $xeroContact){
							$requestLogs[strtolower($xeroContact['ContactNumber'])]['Response Data']	= $request;
							$this->ci->db->update('customers',array('status' => '1','createdCustomerId' => $xeroContact['ContactID']),array('customerId' => $xeroContact['ContactNumber'],'account2Id' => $account2Id));
							if($companyName){
								$this->ci->db->update('customers',array('status' => '1','createdCustomerId' => $xeroContact['ContactID']),array('company' => $xeroContact['Name'],'account2Id' => $account2Id));
							}
						}
					}
				}
			}
			if($requestLogs){
				foreach($requestLogs as $customerId => $requestLog){ 
					$path	= FCPATH.'logs'.DIRECTORY_SEPARATOR . $accountDetails['account1Id'].DIRECTORY_SEPARATOR . 'customers'.DIRECTORY_SEPARATOR;
					if(!is_dir($path)){
						mkdir($path,0777,true);
						chmod(dirname($path), 0777);
					}
					file_put_contents($path. $customerId.'.logs',"\n\n Xero Log Added Log On : ".date('c')." \n". json_encode($requestLog),FILE_APPEND);  
				}
			}					
		}
	}
}
?>