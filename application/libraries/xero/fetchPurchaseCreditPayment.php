<?php
$this->reInitialize();
$enableAvalaraTax           = $this->ci->globalConfig['enableAvalaraTax'];
$enableGiftCard             = $this->ci->globalConfig['enableGiftCard'];
$enableAmazonFees           = $this->ci->globalConfig['enableAmazonFees'];
$enableInventoryTransfer    = $this->ci->globalConfig['enableInventoryTransfer'];
$enableInventoryManagement	= $this->ci->globalConfig['enableInventoryManagement'];
$batchUpdates = array(); 		
foreach($this->accountDetails as $account2Id => $accountDetails){		
	$basedOn	= 'createOrderId';
	$config		= $this->accountConfig[$account2Id];
	
	$pendingPayments	  = array();
	$pendingPaymentsTemps = $this->ci->db->select('orderId,createOrderId,isPaymentCreated,sendPaymentTo,paymentDetails,'.$basedOn)->get_where('purchase_credit_order',array(/* 'isPaymentCreated' => '0', */'createOrderId <>' => ''))->result_array();
	foreach($pendingPaymentsTemps as $pendingPaymentsTemp){
		if($pendingPaymentsTemp[$basedOn]){
			$pendingPayments[$pendingPaymentsTemp[$basedOn]] = $pendingPaymentsTemp;
		}
	}
	
	$url	   = '2.0/Payment';
	$subParams = array('where' => 'PaymentType="APCREDITPAYMENT"');
	$this->initializeConfig($account2Id, 'GET', $url,$subParams);			
	$url	   = '2.0/Payment?where='.urlencode('PaymentType="APCREDITPAYMENT"');
	$this->headers['If-Modified-Since'] = gmdate('c',strtotime('-1 days'));
	$results   = $this->getCurl($url, 'GET','', 'json', $account2Id)[$account2Id];
	if($results){
		$PaymentsRes	= $results['Payments']['Payment'];
		if(!isset($PaymentsRes['0'])){
			$PaymentsRes	= array($PaymentsRes);
		}
		foreach($PaymentsRes as $Payments){
			$Invoices	= $Payments['Invoice']; 
			if(!isset($Invoices['0'])){
				$Invoices	= array($Invoices);
			}
			foreach($Invoices as $Invoice){
				$inoviceId	= @$Invoice['InvoiceID'];	
				if(!$inoviceId){
					continue;
				}	
				$paymentDetails	= array();
				if(!isset($pendingPayments[$inoviceId])){
					continue;
				}
				if(!isset($batchUpdates[$inoviceId]['paymentDetails'])){
					$paymentDetails	= @json_decode($pendingPayments[$inoviceId]['paymentDetails'],true);
				}
				else{
					$paymentDetails = $batchUpdates[$inoviceId]['paymentDetails'];
				}
				if(isset($paymentDetails[$Payments['PaymentID']]) && ($Payments['Status'] == 'AUTHORISED')){
					continue;
				}
				if(isset($paymentDetails[$Payments['PaymentID']])){
					if($Payments['Status'] == 'DELETED'){
						if(strtolower($paymentDetails[$Payments['PaymentID']]['DeletedonBrightpearl']) == 'yes'){
							continue;
						}
						if(strtolower($paymentDetails[$Payments['PaymentID']]['DeletedonBrightpearl']) == 'no'){
							$brightpearlpaykey	= $paymentDetails[$Payments['PaymentID']]['brightpearlPayID'];
							@$paymentDetails[$Payments['PaymentID']]	= array(
								'amount'				=> $Payments['Amount'],
								'sendPaymentTo'			=> 'brightpearl',
								'status'				=> '0',
								'Reference' 			=> $Payments['Reference'],
								'currency' 				=> $Payments['Invoice']['CurrencyCode'],
								'CurrencyRate' 			=> $Payments['CurrencyRate'],
								'PaymentStatus'			=> $Payments['Status'],
								'paymentMethod' 		=> $Payments['Account']['Code'],
								'paymentDate' 			=> $Payments['Date'],
								'DeletedonBrightpearl'	=> 'NO',
								'paymentInitiateIn'		=> 'xero',
								'IsReversal'			=> 1,
								'brightpearlPayID'		=> $brightpearlpaykey,
							);
						}					
					}
				}
				else{
					@$paymentDetails[$Payments['PaymentID']]	= array(
						'amount' 				=> $Payments['Amount'],
						'sendPaymentTo'			=> 'brightpearl',
						'status' 				=> '0',
						'Reference' 			=> $Payments['Reference'],
						'currency' 				=> $Payments['Invoice']['CurrencyCode'],
						'CurrencyRate' 			=> $Payments['CurrencyRate'],
						'PaymentStatus'			=> $Payments['Status'],
						'paymentMethod' 		=> $Payments['Account']['Code'],
						'paymentDate' 			=> $Payments['Date'],
						'DeletedonBrightpearl'	=> 'NO',
						'paymentInitiateIn'		=> 'xero',
						'IsReversal'			=> 0,
					);
				}
				$batchUpdates[$inoviceId] 					= array(
					'paymentDetails'	=> $paymentDetails,
					$basedOn 			=> $inoviceId,
					'sendPaymentTo' 	=> 'brightpearl',
				);
			}
		}
	}
}
if($batchUpdates){
	foreach($batchUpdates as $key => $batchUpdate){
		$batchUpdates[$key]['paymentDetails'] = json_encode($batchUpdate['paymentDetails']);
	} 
	$batchUpdates = array_chunk($batchUpdates,200);
	foreach($batchUpdates as $batchUpdate){
		if($batchUpdate){
			$this->ci->db->update_batch('purchase_credit_order',$batchUpdate,$basedOn); 
		}
	}
}
?>