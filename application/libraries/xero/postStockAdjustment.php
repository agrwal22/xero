<?php
$this->reInitialize();
$enableAvalaraTax           = $this->ci->globalConfig['enableAvalaraTax'];
$enableGiftCard             = $this->ci->globalConfig['enableGiftCard'];
$enableAmazonFees           = $this->ci->globalConfig['enableAmazonFees'];
$enableInventoryTransfer    = $this->ci->globalConfig['enableInventoryTransfer'];
$enableInventoryManagement	= $this->ci->globalConfig['enableInventoryManagement'];
foreach($this->accountDetails as $account2Id => $accountDetails){
	if($accountDetails['OAuthVersion'] == '2'){
		$this->refreshToken($account2Id);
	}
	$config	= $this->accountConfig[$account2Id];
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}		
	$datasTemps	= $this->ci->db->where_in('status',array('0'))->get_where('stock_adjustment',array('account2Id' => $account2Id))->result_array(); 
	if(!$datasTemps){
		$this->ci->db->where_in('ActivityId',$orgObjectId);
		
		$datasTemps	= $this->ci->db->where_in('status',array('0'))->get_where('stock_adjustment',array('account2Id' => $account2Id))->result_array(); 
	}
	
	$productMappings		= array();
	$productMappingsTemps	= $this->ci->db->select('productId,sku,name,createdProductId')->get_where('products',array('account2Id' => $account2Id))->result_array();
	foreach($productMappingsTemps as $productMappingsTemp){
		$productMappings[$productMappingsTemp['productId']]	= $productMappingsTemp;
	}	
	$productId	= array_column($datasTemps,'productId');
	if($productId){
		$this->postProducts($productId);
	} 
	$this->ci->db->reset_query();
	$datass		= array();
	
	foreach($datasTemps as $datasTemp){
		if($datasTemp['GoodNotetype'] == 'GO'){
			$datass[$datasTemp['ActivityId']][]	= $datasTemp;
		}
		if($datasTemp['GoodNotetype'] == 'SC'){
			$datass[$datasTemp['orderId']][]	= $datasTemp;
		}
	}
	
	foreach($datass as $datas){
		$positiveStockRequest	= array();
		$negativeStockRequest	= array();
		$positiveOrderIds		= array();
		$negativeOrderIds		= array(); 
		foreach($datas as $data){
			$createdRowData	= json_decode($orderDatas['createdRowData'],true);
			if(@$createdRowData['lastTry']){
				if($createdRowData['lastTry'] > strtotime('-3 hour')){
					continue;
				}
			}
			$config1		= $this->ci->account1Config[$data['account1Id']];
			$stockAdjustmentAccountRef	= $config['stockAdjustmentAccountRef'];
			if($data['accountCode']){
				$stockAdjustmentAccountRef	= $data['accountCode']; 
			}
			$LineItem	= array(
				'ItemCode' 		=> $productMappings[$data['productId']]['sku'],
				'Description'	=> $productMappings[$data['productId']]['name'],
				'Quantity' 		=> (int)$data['qty'],
				'TaxType' 		=> $config['salesNoTaxCode'],
				'UnitAmount' 	=> abs(round($data['price'],3,PHP_ROUND_HALF_UP)),
				'LineAmount' 	=> abs(round((round($data['price'],3,PHP_ROUND_HALF_UP) * $data['qty']),2,PHP_ROUND_HALF_UP)),
			);
			$xeroItemInfos = array();
			$totalPrice = 0;
			if($LineItem['Quantity'] < 0){
				$xeroProductId	= $productMappings[$data['productId']]['createdProductId'];
				if($xeroProductId){
					$url		= '2.0/Items/'.$xeroProductId;
					$this->initializeConfig($account2Id, 'GET', $url);
					$results	= $this->getCurl($url, 'GET', '', 'json', $account2Id)[$account2Id]['Items']['Item'];
					if($results){
						$bpAdjustedQty	= abs($LineItem['Quantity']);
						$remainingQty	= $bpAdjustedQty - $results['QuantityOnHand'];
						if($remainingQty == 0){
							if($results['TotalCostPool'] > 0){
								$LineItem['LineAmount']	= $results['TotalCostPool'];
								$LineItem['UnitAmount']	= abs(round(($LineItem['LineAmount'] / $data['qty']),4,PHP_ROUND_HALF_UP));
							}
							else{
								$LineItem['LineAmount'] = 0;
								$LineItem['UnitAmount'] = 0;
							}
						}
					}
				}
				$LineItem['Quantity']	= (-1) * $LineItem['Quantity'];
			}
			if($data['GoodNotetype'] ==  'GO'){
				if($data['qty'] > 0){
					$LineItem['TaxType']				= $config['PurchaseNoTaxCode'];
					$LineItem['AccountCode']			= $config['InventoryTransferPurchaseAccountRef'];
					$positiveStockRequest[]				= $LineItem;
					$positiveOrderIds[$data['orderId']]	= $data['orderId'];
				}
				else{
					$LineItem['TaxType']				= $config['salesNoTaxCode'];
					$LineItem['AccountCode']			= $config['InventoryTransferSalesAccountRef'];
					$negativeStockRequest[]				= $LineItem;
					$negativeOrderIds[$data['orderId']] = $data['orderId'];
					if($data['price'] > 0){ 
						$LineItem['Quantity']	= (-1) * $LineItem['Quantity'];
					}
				}
			}
			if($data['GoodNotetype'] ==  'SC'){
				if($data['qty'] > 0){
					$LineItem['TaxType']				= $config['PurchaseNoTaxCode'];
					$positiveStockRequest[]				= $LineItem;
					$positiveOrderIds[$data['orderId']]	= $data['orderId'];
					if($data['price'] > 0){
						$stockLine = array(
							'Description' 	=> 'Inventory Adjustment',
							'Quantity' 		=> 1,
							'LineAmount'	=> '-'.abs(round(($LineItem['UnitAmount'] * $data['qty']),4,PHP_ROUND_HALF_UP)),
							'TaxType' 		=> $config['PurchaseNoTaxCode'],
							'AccountCode' 	=> $stockAdjustmentAccountRef,
						);
						$positiveStockRequest[]	= $stockLine;
					}
				}
				else{
					$LineItem['TaxType']				= $config['PurchaseNoTaxCode'];
					$negativeStockRequest[]				= $LineItem;
					$negativeOrderIds[$data['orderId']] = $data['orderId'];
					if($data['price'] > 0){ 
						$LineItem['Quantity']	= (-1) * $LineItem['Quantity'];
						$stockLine = array(
							'Description' 	=> 'Inventory Adjustment',
							'Quantity' 		=> 1,
							'LineAmount'	=> '-'.abs(round(($LineItem['UnitAmount'] * $data['qty']),4,PHP_ROUND_HALF_UP)),
							'TaxType' 		=> $config['PurchaseNoTaxCode'],
							'AccountCode' 	=> $stockAdjustmentAccountRef,
						);
						$negativeStockRequest[]	= $stockLine;
					}
				}	
			}
		}
		if($positiveStockRequest){
			$customer = 'Inventory Adjustments';
			if($data['GoodNotetype'] ==  'GO'){
	 			$customer = $config['InterCoSupplier'];
			}
			
			$url		= '2.0/Invoices';
			$request	= array(
				'Type'			=> 'ACCPAY',
				'Contact'		=> array(
					'Name'	=> $customer,
				),
				'Date'			=> date('Y-m-d',strtotime($data['created'])),
				'DueDate'		=> date('Y-m-d',strtotime($data['created'])),
				'CurrencyCode'	=> $config1['currencyCode'],	
				'Status'		=> 'AUTHORISED',					
				'LineItems'		=> $positiveStockRequest
			);	
			$this->initializeConfig($account2Id, 'POST', $url);
			$results		= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
			$createdRowData	= array(
				'Request data	: ' => $request,
				'Response data	: ' => $results,
			);
			$this->ci->db->where_in('orderId',array_values($positiveOrderIds))->where(array('account2Id' => $account2Id))->update('stock_adjustment',array('createdRowData' => json_encode($createdRowData)));
			if((strtolower($results['Status']) == 'ok') && (isset($results['Invoices']['Invoice']['InvoiceID']))){	
				$this->ci->db->where_in('orderId',array_values($positiveOrderIds))->where(array('account2Id' => $account2Id))->update('stock_adjustment',array('status' => '1'));
			}
			else{
				$createdRowData['lastTry']	= strtotime('now');
				$this->ci->db->where_in('orderId',array_values($positiveOrderIds))->where(array('account2Id' => $account2Id))->update('stock_adjustment',array('createdRowData' => json_encode($createdRowData)));
			}
		} 
		if($negativeStockRequest){
			$url		= '2.0/CreditNotes';
			$request	= array(
				'Type' 			=> 'ACCPAYCREDIT',
				'Contact' 		=> array(
					'Name'			=> 'Inventory Adjustments',
				),
				'Date'			=> date('Y-m-d',strtotime($data['created'])),
				'DueDate'		=> date('Y-m-d',strtotime($data['created'])),
				'CurrencyCode'	=> $config1['currencyCode'],	
				'Status'		=> 'AUTHORISED',					
				'LineItems'		=> $negativeStockRequest
			);
			if($data['GoodNotetype'] ==  'GO'){
				$url		= '2.0/Invoices';
				$request	= array(
					'Type' 			=> 'ACCREC',
					'Contact' 		=> array(
						'Name'			=> $config['InterCoCustomer'],
					),
					'Date'			=> date('Y-m-d',strtotime($data['created'])),
					'DueDate'		=> date('Y-m-d',strtotime($data['created'])),
					'CurrencyCode'	=> $config1['currencyCode'],	
					'Status'		=> 'AUTHORISED',					
					'LineItems'		=> $negativeStockRequest
				);
	 		}
			$this->initializeConfig($account2Id, 'POST', $url);
			$results		= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
			$createdRowData	= array(
				'Request data	: ' => $request,
				'Response data	: ' => $results,
			);
			$this->ci->db->where_in('orderId',array_values($negativeOrderIds))->where(array('account2Id' => $account2Id))->update('stock_adjustment',array('createdRowData' => json_encode($createdRowData)));
			if((strtolower($results['Status']) == 'ok') && (isset($results['CreditNotes']['CreditNote']['CreditNoteID']))){
				$this->ci->db->where_in('orderId',array_values($negativeOrderIds))->where(array('account2Id' => $account2Id))->update('stock_adjustment',array('status' => '1'));
			}
			elseif((strtolower($results['Status']) == 'ok') && (isset($results['Invoices']['Invoice']['InvoiceID']))){
				$this->ci->db->where_in('orderId',array_values($negativeOrderIds))->where(array('account2Id' => $account2Id))->update('stock_adjustment',array('status' => '1'));
			}
			else{
				$createdRowData['lastTry']	= strtotime('now');
				$this->ci->db->where_in('orderId',array_values($negativeOrderIds))->where(array('account2Id' => $account2Id))->update('stock_adjustment',array('createdRowData' => json_encode($createdRowData)));
			}
		}
	}
}
?>