<?php
	$AllTimeZones	= array("PACIFICSTANDARDTIME(MEXICO)" => "America/Tijuana","SAWESTERNSTANDARDTIME" => "America/Guyana","MAGADANSTANDARDTIME" => "Asia/Magadan","VLADIVOSTOKSTANDARDTIME" => "Asia/Vladivostok","CENTRALPACIFICSTANDARDTIME" => "Pacific/Noumea","RUSSIATIMEZONE11" => "Asia/Anadyr","KAMCHATKASTANDARDTIME" => "Asia/Kamchatka","MAURITIUSSTANDARDTIME" => "Indian/Mauritius","NCENTRALASIASTANDARDTIME" => "Asia/Novosibirsk","NORTHASIASTANDARDTIME" => "Asia/Krasnoyarsk","NORTHASIAEASTSTANDARDTIME" => "Asia/Irkutsk","YAKUTSKSTANDARDTIME" => "Asia/Yakutsk","MOROCCOSTANDARDTIME" => "Africa/Casablanca","UTC" => "UTC","GMTSTANDARDTIME" => "Europe/London","GREENWICHSTANDARDTIME" => "Africa/Monrovia","WEUROPESTANDARDTIME" => "Europe/Amsterdam","CENTRALEUROPESTANDARDTIME" => "Europe/Belgrade","ROMANCESTANDARDTIME" => "Europe/Brussels","CENTRALEUROPEANSTANDARDTIME" => "Europe/Sarajevo","WCENTRALAFRICASTANDARDTIME" => "Africa/Lagos","NAMIBIASTANDARDTIME" => "Africa/Windhoek","JORDANSTANDARDTIME" => "Asia/Amman","GTBSTANDARDTIME" => "Europe/Athens","MIDDLEEASTSTANDARDTIME" => "Asia/Beirut","EGYPTSTANDARDTIME" => "Africa/Cairo","SYRIASTANDARDTIME" => "Asia/Damascus","SOUTHAFRICASTANDARDTIME" => "Africa/Harare","FLESTANDARDTIME" => "Europe/Helsinki","TURKEYSTANDARDTIME" => "Europe/Istanbul","ISRAELSTANDARDTIME" => "Asia/Jerusalem","KALININGRADSTANDARDTIME" => "Europe/Kaliningrad","LIBYASTANDARDTIME" => "Africa/Tripoli","ARABICSTANDARDTIME" => "Asia/Baghdad","ARABSTANDARDTIME" => "Asia/Kuwait","BELARUSSTANDARDTIME" => "Europe/Minsk","RUSSIANSTANDARDTIME" => "Europe/Moscow","EAFRICASTANDARDTIME" => "Africa/Nairobi","IRANSTANDARDTIME" => "Asia/Tehran","ARABIANSTANDARDTIME" => "Asia/Muscat","AZERBAIJANSTANDARDTIME" => "Asia/Baku","RUSSIATIMEZONE3" => "Europe/Samara","GEORGIANSTANDARDTIME" => "Asia/Tbilisi","CAUCASUSSTANDARDTIME" => "Asia/Yerevan","AFGHANISTANSTANDARDTIME" => "Asia/Kabul","WESTASIASTANDARDTIME" => "Asia/Tashkent","PAKISTANSTANDARDTIME" => "Asia/Karachi","INDIASTANDARDTIME" => "Asia/Calcutta","SRILANKASTANDARDTIME" => "Asia/Calcutta","NEPALSTANDARDTIME" => "Asia/Katmandu","CENTRALASIASTANDARDTIME" => "Asia/Dhaka","BANGLADESHSTANDARDTIME" => "Asia/Dhaka","MYANMARSTANDARDTIME" => "Asia/Rangoon","SEASIASTANDARDTIME" =>	"Asia/Bangkok","CHINASTANDARDTIME" => "Asia/Hong_Kong","SINGAPORESTANDARDTIME" => "Asia/Kuala_Lumpur","WAUSTRALIASTANDARDTIME" => "Australia/Perth","TAIPEISTANDARDTIME" => "Asia/Taipei","ULAANBAATARSTANDARDTIME" => "Asia/Ulaanbaatar","TOKYOSTANDARDTIME" => "Asia/Tokyo","KOREASTANDARDTIME" => "Asia/Seoul","CENAUSTRALIASTANDARDTIME" => "Australia/Adelaide","AUSCENTRALSTANDARDTIME" => "Australia/Darwin","EAUSTRALIASTANDARDTIME" =>	"Australia/Brisbane","AUSEASTERNSTANDARDTIME" =>	"Australia/Canberra","WESTPACIFICSTANDARDTIME" => "Pacific/Guam","TASMANIASTANDARDTIME" =>	"Australia/Hobart","NEWZEALANDSTANDARDTIME" =>	"Pacific/Auckland","FIJISTANDARDTIME" => "Pacific/Fiji","TONGASTANDARDTIME" => "Pacific/Tongatapu","SAMOASTANDARDTIME" => "Pacific/Samoa","LINEISLANDSSTANDARDTIME" => "Australia/Adelaide","AZORESSTANDARDTIME" => "Atlantic/Azores","CAPEVERDESTANDARDTIME" => "Atlantic/Cape_Verde","MIDATLANTICSTANDARDTIME" => "America/Noronha","ESOUTHAMERICASTANDARDTIME" =>	"America/Sao_Paulo","ARGENTINASTANDARDTIME" =>	"America/Argentina/Buenos_Aires","SAEASTERNSTANDARDTIME" =>	"America/Cayenne","GREENLANDSTANDARDTIME" => "America/Godthab","MONTEVIDEOSTANDARDTIME" =>	"America/Montevideo","BAHIASTANDARDTIME" =>	"America/El_Salvador","NEWFOUNDLANDSTANDARDTIME" =>	"Canada/Newfoundland","PARAGUAYSTANDARDTIME" =>	"America/Asuncion","ATLANTICSTANDARDTIME" => "Canada/Atlantic","CENTRALBRAZILIANSTANDARDTIME" => "America/Cuiaba","PACIFICSASTANDARDTIME" => "America/Santiago","VENEZUELASTANDARDTIME" =>	"America/Caracas","SAPACIFICSTANDARDTIME" => "America/Bogota","EASTERNSTANDARDTIME" => "US/Eastern","USEASTERNSTANDARDTIME" =>	"US/East-Indiana","CENTRALAMERICASTANDARDTIME" =>	"America/Managua","CENTRALSTANDARDTIME" => "US/Central","CENTRALSTANDARDTIME(MEXICO)" => "America/Mexico_City","CANADACENTRALSTANDARDTIME" =>	"Canada/Saskatchewan","USMOUNTAINSTANDARDTIME" => "US/Arizona","MOUNTAINSTANDARDTIME(MEXICO)" => "America/Chihuahua","MOUNTAINSTANDARDTIME" => "US/Mountain","PACIFICSTANDARDTIME" => "America/Los_Angeles","ALASKANSTANDARDTIME" => "US/Alaska","HAWAIIANSTANDARDTIME" => "Pacific/Honolulu","UTC11" => "Pacific/Midway");
	
	$xeroTimeFormat	= array("MOROCCOSTANDARDTIME" => "00","UTC" => "00","GMTSTANDARDTIME" => "00","GREENWICHSTANDARDTIME" => "00","WEUROPESTANDARDTIME" => "+01","CENTRALEUROPESTANDARDTIME" => "+01","ROMANCESTANDARDTIME" => "+01","CENTRALEUROPEANSTANDARDTIME" => "+01","WCENTRALAFRICASTANDARDTIME" => "+01","NAMIBIASTANDARDTIME" => "+01","JORDANSTANDARDTIME" => "+02","GTBSTANDARDTIME" => "+02","MIDDLEEASTSTANDARDTIME" => "+02","EGYPTSTANDARDTIME" => "+02","SYRIASTANDARDTIME" => "+02","EEUROPESTANDARDTIME" => "+02","SOUTHAFRICASTANDARDTIME" => "+02","FLESTANDARDTIME" => "+02","TURKEYSTANDARDTIME" => "+02","ISRAELSTANDARDTIME" => "+02","KALININGRADSTANDARDTIME" => "+02","LIBYASTANDARDTIME" => "+02","ARABICSTANDARDTIME" => "+03","ARABSTANDARDTIME" => "+03","BELARUSSTANDARDTIME" => "+03","RUSSIANSTANDARDTIME" => "+03","EAFRICASTANDARDTIME" => "+03","IRANSTANDARDTIME" => "+03","ARABIANSTANDARDTIME" => "+04","AZERBAIJANSTANDARDTIME" => "+04","RUSSIATIMEZONE3" => "+04","MAURITIUSSTANDARDTIME" => "+04","GEORGIANSTANDARDTIME" => "+04","CAUCASUSSTANDARDTIME" => "+04","AFGHANISTANSTANDARDTIME" => "+04","WESTASIASTANDARDTIME" => "+05","EKATERINBURGSTANDARDTIME" => "+05","PAKISTANSTANDARDTIME" => "+05","INDIASTANDARDTIME" => "+05","SRILANKASTANDARDTIME" => "+05","NEPALSTANDARDTIME" => "+05","CENTRALASIASTANDARDTIME" => "+06","BANGLADESHSTANDARDTIME" => "+06","NCENTRALASIASTANDARDTIME" => "+06","MYANMARSTANDARDTIME" => "+06","SEASIASTANDARDTIME" => "+07","NORTHASIASTANDARDTIME" => "+07","CHINASTANDARDTIME" => "+08","NORTHASIAEASTSTANDARDTIME" => "+08","SINGAPORESTANDARDTIME" => "+08","WAUSTRALIASTANDARDTIME" => "+08","TAIPEISTANDARDTIME" => "+08","ULAANBAATARSTANDARDTIME" => "+08","TOKYOSTANDARDTIME" => "+09","KOREASTANDARDTIME" => "+09","YAKUTSKSTANDARDTIME" => "+09","CENAUSTRALIASTANDARDTIME" => "+09","AUSCENTRALSTANDARDTIME" => "+09","EAUSTRALIASTANDARDTIME" => "+10","AUSEASTERNSTANDARDTIME" => "+10","WESTPACIFICSTANDARDTIME" => "+10","TASMANIASTANDARDTIME" => "+10","MAGADANSTANDARDTIME" => "+10","VLADIVOSTOKSTANDARDTIME" => "+10","RUSSIATIMEZONE10" => "+11","CENTRALPACIFICSTANDARDTIME" => "+11","RUSSIATIMEZONE11" => "+12","NEWZEALANDSTANDARDTIME" => "+12","UTC+12" => "+12","FIJISTANDARDTIME" => "+12","KAMCHATKASTANDARDTIME" => "+12","TONGASTANDARDTIME" => "+13","SAMOASTANDARDTIME" => "+13","LINEISLANDSSTANDARDTIME" => "+14","AZORESSTANDARDTIME" => "-01","CAPEVERDESTANDARDTIME" => "-01","UTC02" => "-02","MIDATLANTICSTANDARDTIME" => "-02","ESOUTHAMERICASTANDARDTIME" => "-03","ARGENTINASTANDARDTIME" => "-03","SAEASTERNSTANDARDTIME" => "-03","GREENLANDSTANDARDTIME" => "-03","MONTEVIDEOSTANDARDTIME" => "-03","BAHIASTANDARDTIME" => "-03","NEWFOUNDLANDSTANDARDTIME" => "-03","PARAGUAYSTANDARDTIME" => "-04","ATLANTICSTANDARDTIME" => "-04","CENTRALBRAZILIANSTANDARDTIME" => "-04","SAWESTERNSTANDARDTIME" => "-04","PACIFICSASTANDARDTIME" => "-04","VENEZUELASTANDARDTIME" => "-04","SAPACIFICSTANDARDTIME" => "-05","EASTERNSTANDARDTIME" => "-05","USEASTERNSTANDARDTIME" => "-05","CENTRALAMERICASTANDARDTIME" => "-06","CENTRALSTANDARDTIME" => "-06","CENTRALSTANDARDTIME(MEXICO)" => "-06","CANADACENTRALSTANDARDTIME" => "-06","USMOUNTAINSTANDARDTIME" => "-07","MOUNTAINSTANDARDTIME(MEXICO)" => "-07","MOUNTAINSTANDARDTIME" => "-07","PACIFICSTANDARDTIME(MEXICO)" => "-08","PACIFICSTANDARDTIME" => "-08","ALASKANSTANDARDTIME" => "-09","HAWAIIANSTANDARDTIME" => "-10","UTC11" => "-11","DATELINESTANDARDTIME" => "-12");

$this->reInitialize(); 
$enableAvalaraTax           = $this->ci->globalConfig['enableAvalaraTax'];
$enableGiftCard             = $this->ci->globalConfig['enableGiftCard'];
$enableAmazonFees           = $this->ci->globalConfig['enableAmazonFees'];
$enableInventoryTransfer    = $this->ci->globalConfig['enableInventoryTransfer'];
$enableInventoryManagement	= $this->ci->globalConfig['enableInventoryManagement'];
foreach($this->accountDetails as $account2Id => $accountDetails){
	if($accountDetails['OAuthVersion'] == '2'){
		$this->refreshToken($account2Id);
	}
	$logs	= array();
	$config	= $this->accountConfig[$account2Id];
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	else{
		$this->ci->db->group_start()->where('status', '0')->or_group_start()->where('uninvoiced', '1')->group_end()->group_end();
	}
	$datas	= $this->ci->db->get_where('sales_order',array('account2Id' => $account2Id))->result_array();
	
	$allSalesItemProductIdsTemps	= $this->ci->db->select('productId')->get_where('sales_item',array('status' => '0','productId > ' => '1001'))->result_array();
	if($allSalesItemProductIdsTemps){
		$allSalesItemProductIds	= array_column($allSalesItemProductIdsTemps,'productId'); 
		if($allSalesItemProductIds){
			$this->postProducts($allSalesItemProductIds);
		} 
	}
	
	$genericcustomerMappings		= array();
	$genericcustomerMappingsTemps	= $this->ci->db->get_where('mapping_genericcustomer',array('account2Id' => $account2Id))->result_array();
	foreach($genericcustomerMappingsTemps as $genericcustomerMappingsTemp){
		$genericcustomerMappings[$genericcustomerMappingsTemp['account1ChannelId']] = $genericcustomerMappingsTemp;
	}
	
	$this->ci->db->reset_query();
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	$allSalesCustomerTemps	= $this->ci->db->select('customerId,rowData')->get_where('sales_order',array('status' => '0','customerId <>' => ''))->result_array();
	$allSalesCustomer		= array();
	if($allSalesCustomerTemps){
		foreach($allSalesCustomerTemps as $allSalesCustomerTemp){
			$tempRowData	= json_decode($allSalesCustomerTemp['rowData'],true);
			$channelId		= $tempRowData['assignment']['current']['channelId'];
			if($channelId){
				$genericcustomerMapping	= @$genericcustomerMappings[$channelId];	
				if(!$genericcustomerMapping){
					$allSalesCustomer[] = $allSalesCustomerTemp['customerId'];
				}
			}
			else{
				$allSalesCustomer[] = $allSalesCustomerTemp['customerId'];
			}
		}
		$allSalesCustomer = array_unique($allSalesCustomer);		
		if($allSalesCustomer){
			$this->postCustomers($allSalesCustomer); 
		}
	}
	
	$nominalMappings		= array();
	$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1CustomFieldValue' => '','account2Id' => $account2Id))->result_array();
	foreach($nominalMappingTemps as $nominalMappingTemp){
		if(@!$nominalMappings[$nominalMappingTemp['account1NominalId']]){
			if(!$nominalMappingTemp['account1ChannelId']){
				$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
		}
		if($nominalMappingTemp['account1ChannelId']){
			$nominalMappings[$nominalMappingTemp['account1NominalId']]['channel'][strtolower($nominalMappingTemp['account1ChannelId'])]	= $nominalMappingTemp;
		}
	}
	$nominalCodeForShipping	= explode(",",$config['nominalCodeForShipping']);
	$nominalCodeForGiftCard	= explode(",",$config['nominalCodeForGiftCard']);
	
	
	$productMappings		= array();
	$productMappingsTemps	= $this->ci->db->select('productId,createdProductId,name,sku')->get_where('products',array('account2Id' => $account2Id))->result_array();
	foreach($productMappingsTemps as $productMappingsTemp){
		$productMappings[$productMappingsTemp['productId']]		= $productMappingsTemp;
	}	
	
	$customerMappings		= array();
	$customerMappingsTemps	= $this->ci->db->get_where('customers',array('account2Id' => $account2Id))->result_array();
	foreach($customerMappingsTemps as $customerMappingsTemp){
		$customerMappings[$customerMappingsTemp['customerId']]	= $customerMappingsTemp;
	}
	
	$taxMappings			= array();
	$taxMappingsTemps		= $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id,'orderType <>' => '2'))->result_array();
	if($taxMappingsTemps){ 
		foreach($taxMappingsTemps as $taxMappingsTemp){
			$taxMappings[$taxMappingsTemp['account1TaxId']]		= $taxMappingsTemp;
		}
	}
	
	$channelMappings		= array();
	$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	if($channelMappingsTemps){
		foreach($channelMappingsTemps as $channelMappingsTemp){
			if(!isset($channelMappings[$channelMappingsTemp['account1ChannelId']])){
				$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
			}
			if($channelMappingsTemp['account1WarehouseId']){
				$channelMappings[$channelMappingsTemp['account1ChannelId']]['warehouseDetails'][$channelMappingsTemp['account1WarehouseId']]	= $channelMappingsTemp;
			}
		}
	}
	
	if($enableAvalaraTax){
		$TaxData				= array();
		$TaxRateZipCodeDatas	= array();
		$TaxRateZipCode			= array();
		$updateOrder			= 100;
		$TaxRateZipCode			= $this->ci->db->get_where('avalara_taxRates',array('account2Id' => $account2Id))->result_array();
		if($TaxRateZipCode){
			foreach($TaxRateZipCode as $TaxRateZipCodeData){
				$TaxRateZipCodeDatas[$TaxRateZipCodeData['TaxName']]	= $TaxRateZipCodeData;
			}
		}
		$url					= '2.0/TaxRates';
		$this->initializeConfig($account2Id, 'GET', $url);
		$results				= $this->getCurl($url, 'GET', '', 'json', $account2Id)[$account2Id];
		if($results['TaxRates']['TaxRate']){
			foreach($results['TaxRates']['TaxRate'] as $TaxRateData){
				if($TaxRateData['Status'] == 'ACTIVE'){
					if(!$TaxRateZipCodeDatas[$TaxRateData['Name']]){
						$TaxData[]	= array(
							'account1Id'	=>	$accountDetails['account1Id'],
							'account2Id'	=>	$account2Id,
							'TaxName'		=>	$TaxRateData['Name'],
							'TaxType'		=>	$TaxRateData['TaxType'],
							'TaxCode'		=>	$TaxRateData['ReportTaxType'],
							'TaxRate'		=>	$TaxRateData['EffectiveRate'],
						);
					}
					else{
						continue;
					}
				}
				else{
					continue;
				}
			}
		}
		if($TaxData){
			$TaxInsertDataBatch	= array_chunk($TaxData,$updateOrder,true);
			foreach($TaxInsertDataBatch as $TaxData){
				$this->ci->db->insert_batch('avalara_taxRates', $TaxData);
			}
		}
	}
	if($datas){			
		foreach($datas as $orderDatas){
			$config1	= $this->ci->account1Config[$orderDatas['account1Id']];
			// UNINVOICING CODE STARTS //// UNINVOICING CODE STARTS //// UNINVOICING CODE STARTS //// UNINVOICING CODE STARTS //
			if($config1['UnInvoicingEnabled']){
				if($orderDatas['uninvoiced'] == 1){
					if($orderDatas['createOrderId']){
						if(!$orderDatas['paymentDetails']){
							$uninvoiceCount	= $orderDatas['uninvoiceCount'];
							$Request		= array(
								"InvoiceID"		=>	$orderDatas['createOrderId'],
								"Status"		=>	"VOIDED",
							);
							$url			= '2.0/Invoices';
							$this->initializeConfig($account2Id, 'POST', $url);
							$results		= $this->getCurl($url, 'POST', json_encode($Request), 'json', $account2Id)[$account2Id];
							$createdRowData	= array(
								'Request data 	: ' => $Request,
								'Response data 	: ' => $results,
							);
							$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
							if((strtolower($results['Status']) == 'ok') && (isset($results['Invoices']['Invoice']['InvoiceID']))){
								$uninvoiceCount++;
								$this->ci->db->update('sales_order',array('status' => '0','uninvoiced' => '2','createOrderId' => '', 'invoiceRef' => '', 'uninvoiceCount' => $uninvoiceCount),array('id' => $orderDatas['id']));
							}
						}
						else if($orderDatas['paymentDetails']){
							$uninvoiceCount	= $orderDatas['uninvoiceCount'];
							$payment_datas	= json_decode($orderDatas['paymentDetails'],true);
							foreach($payment_datas as $paymemt_key => $payment_data){
								if(strlen($paymemt_key) > 20){
									$paykeys[]	= $paymemt_key;
									unset($payment_datas[$paymemt_key]);
								}
							}
							foreach($paykeys as $DeletePaymentID){
								$DeletePaymentRequest	= array(
									"PaymentID"				=>	$DeletePaymentID,
									"Status"				=>	"DELETED",
								);
								$url			= '2.0/Payments';
								$this->initializeConfig($account2Id, 'POST', $url);
								$DeleteResponse	= $this->getCurl($url, 'POST', json_encode($DeletePaymentRequest), 'json', $account2Id)[$account2Id];
							}
							$Request	= array(
								"InvoiceID"	=>	$orderDatas['createOrderId'],
								"Status"	=>	"VOIDED",
							);
							$url			= '2.0/Invoices';
							$this->initializeConfig($account2Id, 'POST', $url);
							$results		= $this->getCurl($url, 'POST', json_encode($Request), 'json', $account2Id)[$account2Id];
							$createdRowData	= array(
								'Request data 	: '	=> $Request,
								'Response data 	: ' => $results,
							);
							$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
							if((strtolower($results['Status']) == 'ok') && (isset($results['Invoices']['Invoice']['InvoiceID']))){
								$uninvoiceCount++;
								$this->ci->db->update('sales_order',array('status' => '0','createOrderId' => '','uninvoiced' => '2', 'isPaymentCreated' => '0', 'sendPaymentTo' => '', 'paymentDetails' => '', 'uninvoiceCount' => $uninvoiceCount),array('id' => $orderDatas['id']));
							}
						}
						continue;
					}
				}
			}
			// UNINVOICING CODE ENDS //// UNINVOICING CODE ENDS //// UNINVOICING CODE ENDS //// UNINVOICING CODE ENDS //
			if($orderDatas['createOrderId']){
				continue;
			}
			if(!$orderDatas['invoiced']){
				continue;
			}
			
			//CODE TO CHECK DROPSHIP PO'S ARE SENT OR NOT, IF NOT THEN SKIP THAT SALES ORDER
			$orderId	= $orderDatas['orderId'];
			if($orderDatas['IsDropShip']){
				$DropShipPODetail	= $this->ci->db->get_where('purchase_order',array('LinkedWithSO' => $orderId,'createOrderId' => NULL))->result_array();
				if($DropShipPODetail){
					$this->ci->db->update('sales_order',array('message' => 'DropShipPO is not Sent Yet'),array('orderId' => $orderId));
					continue;
				}
			}
			
			$XeroCustomerID			= '';
			$rowDatas		        = json_decode($orderDatas['rowData'],true);
			$createdRowData	        = json_decode($orderDatas['createdRowData'],true);
			$channelId		        = $rowDatas['assignment']['current']['channelId'];
			$warehouseId	        = $rowDatas['warehouseId'];
			$billAddress 	        = $rowDatas['parties']['billing'];
			$shipAddress 	        = $rowDatas['parties']['delivery'];
			$orderCustomer 	        = $rowDatas['parties']['customer'];
			$uninvoiceCount         = $orderDatas['uninvoiceCount'];
			$CustomerCompanyName	= $customerMappings[$orderCustomer['contactId']]['company'];
			$XeroCustomerID			= $customerMappings[$orderCustomer['contactId']]['createdCustomerId'];
			$genericcustomerMapping	= @$genericcustomerMappings[$channelId];
			if($genericcustomerMapping){
				if(@strlen($genericcustomerMapping['account2ChannelId']) > 1){
					$XeroCustomerID	= $genericcustomerMapping['account2ChannelId'];
				}
			}
			elseif(@!$customerMappings[$orderCustomer['contactId']]['createdCustomerId']){
				$CreatedCustomerData = $this->ci->db->get_where('customers',array('account2Id' => $account2Id, 'company' => $CustomerCompanyName))->result_array();
				if($CreatedCustomerData){
					if((count($CreatedCustomerData)) <  5){
						continue;
					}
					else{
						foreach($CreatedCustomerData as $CreatedCustomerDatas){
							$NewParam = json_decode($CreatedCustomerDatas['params'],true);
							if($NewParam['isPrimaryContact'] == true){
								$XeroCustomerID = $CreatedCustomerDatas['createdCustomerId'];
								break;
							}
						}
					} 
				}
			}
			if(!$XeroCustomerID){
				continue;
			}
			if(@$createdRowData['lastTry']){
				if($createdRowData['lastTry'] > strtotime('-3 hour')){
					continue;
				}
			}
			$BrightpearlTotalAmount	        = $rowDatas['totalValue']['total'];
			$missingSkus			        = array();
			$productCreateIds	            = array();
			$InvoiceLineAdd					= array();
			$productUpdateNominalCodeInfos	= array();
			$TaxComponents                  = array();
			$InsertTaxResponse              = array();
			$InsertZipCodeInfo              = array();
			$TaxRatefromAvalara	            = array();
			$TaxTypeData                    = array();
			$taxDatas						= array();
			$avaZipDatas					= array();
			$invoiceLineCount               = 0;
			$totalItemDiscount              = 0;
			$isDiscountCouponAdded	        = 0;
			$isDiscountCouponTax		    = 0;
			$discountCouponAmt	            = 0;
			$isAvalaraTaxApplicable         = 0;
			$avalaraTaxError                = 0;
			$this->countRequest             = 0;
			$orderTaxId                     = '';
			$XeroTaxType					= ''; 
			$xeroInvoiceRefrence	        = '';
			$TaxType                        = '';
			$TaxName				        = '';
			$couponItemLineID				= '';
			$shippingLineID					= '';
			$PostalCode				        = $rowDatas['parties']['delivery']['postalCode'];
			$CountryIsoCode			        = $rowDatas['parties']['delivery']['countryIsoCode'];			
			foreach($rowDatas['orderRows'] as $rowId => $orderRows){
				if($orderRows['productId'] >= 1000){
					if(substr_count(strtolower($orderRows['productName']),'coupon')){
						if($orderRows['nominalCode'] == $config['nominalCodeForDiscount']){
							$isDiscountCouponAdded	= 1;
							$couponItemLineID		= $rowId;
						}
					}
					if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){ 
						$shippingLineID			= $rowId;
					}
				}
				if($enableAvalaraTax){
					if($orderRows['productId'] > 1001){
						if(($orderRows['rowValue']['taxCode'] == '-') && ($orderRows['rowValue']['rowTax']['value'] > 0)){
							if(strtolower($CountryIsoCode) == 'us'){
								$isAvalaraTaxApplicable	= 1;
							}
						}
					}
				}
			}
			if($isAvalaraTaxApplicable == 1){
				$TaxRateZipCode	= $this->ci->db->get_where('avalara_zipcode_taxinfo',array('zipcode' => $PostalCode))->row_array();
				if($TaxRateZipCode){
					$TaxName	= $TaxRateZipCode['TaxName'];
				}
				if($TaxName){
					$TaxTypeData	= $this->ci->db->get_where('avalara_taxRates',array('TaxName' => $TaxName,'account2Id' => $account2Id))->row_array();
					if($TaxTypeData){
						$TaxType	= $TaxTypeData['TaxType'];
					}
				}
				if(!$TaxType){
					$TaxRatefromAvalara	= $this->fetchAvalaraTaxRate($CountryIsoCode, $PostalCode);
					if($TaxRatefromAvalara['error']){
						$createdRowData	= array(
							'Avalara Response	: '	=> $TaxRatefromAvalara,
						);
						$this->ci->db->update('sales_order',array('message' => $TaxRatefromAvalara['error']['message'],'createdRowData' => json_encode($createdRowData)),array('orderId' => $orderId,'account2Id' => $account2Id));
						continue;
					}
				}
			}
			if($TaxRatefromAvalara){
				$TaxTypeData	= $this->ci->db->get_where('avalara_taxRates',array('account2Id' => $account2Id))->result_array();
				$XeroTaxName	= array();
				$AvalaraTaxRate	= sprintf("%.4f",($TaxRatefromAvalara['totalRate']*100));
				$XeroTaxName	= $TaxRatefromAvalara['rates'][0]['name']; 
				$XeroTaxNames	= explode(" ",$XeroTaxName);
				$XeroTaxName	= $XeroTaxNames[0].' Tax '.$AvalaraTaxRate.'%';
				$avTaxPartName	= strtolower($XeroTaxNames[0]).' ';		
				foreach($TaxTypeData as $TaxTypeDatass){
					$xeroSaveTaxRate	= sprintf("%.4f",$TaxTypeDatass['TaxRate']);
					if((substr_count(strtolower($TaxTypeDatass['TaxName']),$avTaxPartName)) && ($xeroSaveTaxRate == $AvalaraTaxRate)){
						$TaxType	= $TaxTypeDatass['TaxType'];
						break;						
					}					
				}
				if($TaxType){
					$InsertZipCodeInfo	= array(
						'zipcode'	=> $PostalCode,
						'TaxName'	=> $XeroTaxName,
						'params'	=> json_encode($TaxRatefromAvalara),
					);
					$this->ci->db->insert('avalara_zipcode_taxinfo', $InsertZipCodeInfo);
				}
				else{
					if($TaxRatefromAvalara['rates']){
						$tempAvalaraTaxes	= array();
						foreach($TaxRatefromAvalara['rates'] as $tempAvalaraTaxesDatas){
							$taxUniqueName	= strtolower($tempAvalaraTaxesDatas['name']);							
							if(isset($tempAvalaraTaxes[strtolower($tempAvalaraTaxesDatas['name'])][strtolower($tempAvalaraTaxesDatas['type'])])){
								$tempAvalaraTaxes[strtolower($tempAvalaraTaxesDatas['name'])][strtolower($tempAvalaraTaxesDatas['type'])]['rate'] += $tempAvalaraTaxesDatas['rate'];
							}
							else{
								$tempAvalaraTaxes[strtolower($tempAvalaraTaxesDatas['name'])][strtolower($tempAvalaraTaxesDatas['type'])] = $tempAvalaraTaxesDatas;
							}
						}
						foreach($tempAvalaraTaxes as $tempAvalaraTaxe){
							$avalaraTypeCount	= count($tempAvalaraTaxe);
							foreach($tempAvalaraTaxe as $tempAvalaraTax){
								$name	= $tempAvalaraTax['name'];
								if($avalaraTypeCount > 1){
									$name	= $tempAvalaraTax['name'] .' ' .$tempAvalaraTax['type'];
								}
								$TaxComponents[]	= array(
									"Name"				=> $name,
									"Rate"				=> ($tempAvalaraTax['rate']*100),
									"IsCompound"		=> false,
									"IsNonRecoverable"	=> false,
								);
							}
						}
					}
					$TaxRequest	= array(
						'Name'			=> $XeroTaxName,
						'Rate'			=> $AvalaraTaxRate,
						'Status'		=> 'ACTIVE',
						'TaxComponents'	=> $TaxComponents,
					);
					$InsertZipCodeInfo	= array(
						'zipcode'	=> $PostalCode,
						'TaxName'	=> $XeroTaxName,
						'params'	=> json_encode($TaxRatefromAvalara),
					);
					$TaxRateUrl		= '2.0/TaxRates';
					$this->initializeConfig($account2Id, 'POST', $TaxRateUrl);
					$TaxResponse	= $this->getCurl($TaxRateUrl, 'POST', json_encode($TaxRequest), 'json', $account2Id)[$account2Id];
					if(strtolower($TaxResponse['Status']) == 'ok'){
						$CreatedData	= array(
							'Request'		=>	$TaxRequest,
							'Response'		=>	$TaxResponse,
						);
						$InsertTaxResponse	= array(
							'account1Id'		=>	$accountDetails['account1Id'],
							'account2Id'		=>	$account2Id,
							'TaxName'			=>	$TaxResponse['TaxRates']['TaxRate']['Name'],
							'TaxType'			=>	$TaxResponse['TaxRates']['TaxRate']['TaxType'],
							'TaxCode'			=>  '',
							'TaxRate'			=>	$TaxResponse['TaxRates']['TaxRate']['EffectiveRate'],
						);
						$this->ci->db->insert('avalara_zipcode_taxinfo', $InsertZipCodeInfo);
						$this->ci->db->insert('avalara_taxRates', $InsertTaxResponse);
						$TaxType	= $TaxResponse['TaxRates']['TaxRate']['TaxType'];
					}
					else{
						$avalaraTaxError = 1;
					}
				}
			}
			if(($avalaraTaxError) && ($isAvalaraTaxApplicable)){
				$this->ci->db->update('sales_order',array('message' => json_encode($TaxResponse)),array('orderId' => $orderId));
				continue;
			}
			if(($isAvalaraTaxApplicable) && ($TaxType)){
				$taxDatas		= $this->ci->db->get_where('avalara_taxRates',array('TaxType' => $TaxType,'account2Id' => $account2Id))->row_array();
				$avaZipDatas	= $this->ci->db->get_where('avalara_zipcode_taxinfo',array('zipcode' => $PostalCode))->row_array();
			}
			if($isAvalaraTaxApplicable){
				if(!$avaZipDatas){
					continue;
				}
			}
			$itemtaxAbleLine		= $config['orderLineTaxCode'];
			$discountTaxAbleLine	= $config['orderLineTaxCode'];
			foreach($rowDatas['orderRows'] as $rowId => $orderRows){
				if($rowId == $couponItemLineID){
					continue;
				}
				$LineTaxId	= $config['salesNoTaxCode'];										
				$productId	= $orderRows['productId'];
				if($productId > 1001){
					if(@!$productMappings[$productId]['createdProductId']){
						$missingSkus[]	= $orderRows['productSku'];
						continue;
					} 
					$ItemRefName	= $productMappings[$productId]['sku'];
					$ItemRefValue	= $productMappings[$productId]['sku'];
				}
				else{
					if($orderRows['rowValue']['rowNet']['value'] >= 0){
						$ItemRefValue	= $config['genericSku'];
						$ItemRefName	= $orderRows['productName'];
						if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){ 
							$ItemRefValue	= $config['shippingItem'];
							$ItemRefName	= 'Shipping';
						}
						if(in_array($orderRows['nominalCode'],$nominalCodeForGiftCard)){
							$ItemRefValue	= $config['giftCardItem'];
							$ItemRefName	= $orderRows['productName'];
						}
					} 
					else if($orderRows['rowValue']['rowNet']['value'] < 0){
						$ItemRefValue	= $config['discountItem'];
						$ItemRefName	= 'Discount Item';
						if(in_array($orderRows['nominalCode'],$nominalCodeForGiftCard)){
							$ItemRefValue	= $config['giftCardItem'];
							$ItemRefName	= $orderRows['productName'];
						}
					}
				}
				// CALCULATE THE TAX BASED ON AVALARA RATE AND APPLY TAX AMOUNT BASEDN ON CALCULATED TAX
				$taxAmount	= $orderRows['rowValue']['rowTax']['value'];
				if(@$taxMappings[$orderRows['rowValue']['taxClassId']]){
					$LineTaxId	= $taxMappings[$orderRows['rowValue']['taxClassId']]['account2TaxId'];
					$orderTaxId	= $taxMappings[$orderRows['rowValue']['taxClassId']]['account2TaxId'];
				}
				else if($taxAmount > 0){
					$LineTaxId	= $config['TaxCode'];
				}
				$itemtaxAbleLine	= $LineTaxId;				
				$price				= $orderRows['rowValue']['rowNet']['value']; 				
				$originalPrice		= $price;
				$discountPercentage	= 0;
				if($orderRows['discountPercentage'] > 0){
					$discountTaxAbleLine	= $LineTaxId;
					$originalPrice			= $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
					$discountPercentage		= $orderRows['discountPercentage'];
					if($discountPercentage == 100){
							$totalItemDiscount	+= $originalPrice;		
							
					}
					else{
						$discountCouponAmtTemp	=  ($originalPrice * $orderRows['discountPercentage']) / 100;
						if($discountCouponAmtTemp > 0){
							$totalItemDiscount	+= $discountCouponAmtTemp;
						}
					}
					$discountPercentage	= 0;
				}
				else if($isDiscountCouponAdded){
					$originalPriceWithTax	= $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
					$discountCouponAmtTemp	= ($originalPriceWithTax - $price);
					if($discountCouponAmtTemp > 0){
						$discountCouponAmt += $discountCouponAmtTemp;						
						$isDiscountCouponTax = 1;
						$discountTaxAbleLine = $LineTaxId;
					}
				}
				$IncomeAccountRef	= $config['IncomeAccountRef'];
				if(@isset($nominalMappings[$orderRows['nominalCode']])){
					if(@$nominalMappings[$orderRows['nominalCode']]['account2NominalId']){
						$IncomeAccountRef	= $nominalMappings[$orderRows['nominalCode']]['account2NominalId'];
					}
					$nominalMapped	= $nominalMappings[$orderRows['nominalCode']];
					if($channelId){
						if(isset($nominalMapped['channel'])){
							if(isset($nominalMapped['channel'][strtolower($channelId)])){
								if($orderRows['productId'] > 1001){
									$productUpdateNominalCodeInfos[$productId]['org']	= $IncomeAccountRef;
								}
								$IncomeAccountRef	= $nominalMapped['channel'][strtolower($channelId)]['account2NominalId'];
								if($orderRows['productId'] > 1001){
									$productUpdateNominalCodeInfos[$productId]['upd']	= $IncomeAccountRef;
								}
							}
						}
					}
				}				
				$UnitAmount	= 0.00;
				if($originalPrice != 0){
					$UnitAmount	= $originalPrice / $orderRows['quantity']['magnitude'];
				}
				if($isDiscountCouponAdded){
					if($rowId != $shippingLineID){
						$UnitAmount	= $orderRows['productPrice']['value'];
					}
				}
				$InvoiceLineAdd[$invoiceLineCount]	= array(
					'ItemCode' 		=> $ItemRefValue, 
					'Description'	=> $orderRows['productName'],
					'Quantity' 		=> (int)$orderRows['quantity']['magnitude'],
					'UnitAmount' 	=> sprintf("%.4f",$UnitAmount),
					'TaxType' 		=> $LineTaxId,
					'AccountCode' 	=> $IncomeAccountRef,
					'TaxAmount' 	=> $taxAmount,
				);				
				if(!$LineTaxId){
					unset($InvoiceLineAdd[$invoiceLineCount]['TaxType']);
				}
				//IF ORDER IS APPLICABLE FOR AVALARA TAX WE ARE OVERRIDING THE TAX CODE				
				if(($orderRows['rowValue']['taxCode'] == '-') && ($orderRows['rowValue']['rowTax']['value'] > 0)){
					if($TaxType){
						$InvoiceLineAdd[$invoiceLineCount]['TaxType']	= $TaxType;
						if($taxDatas['TaxRate']){
							unset($InvoiceLineAdd[$invoiceLineCount]['TaxAmount']);
						}
					}
				}
				
				
				// ADDED THE ORDER TRACKING INFO
				if(isset($channelMappings[$channelId])){
					$trackingDetails	= $channelMappings[$channelId]['account2ChannelId'];
					if(@$channelMappings[$channelId]['warehouseDetails'][$warehouseId]){
						$trackingDetails	= $channelMappings[$channelId]['warehouseDetails'][$warehouseId]['account2ChannelId'];
					}
					if($trackingDetails){
						$trackingDetails	= explode("~=",$trackingDetails);
						$InvoiceLineAdd[$invoiceLineCount]['Tracking']	= array(array('Name' => $trackingDetails['0'],'Option' => $trackingDetails['1']));
					}
				}
				
				//CHANGE THE TAX TYPE AS NONE IF TAX NEED TO SEND AS LINE ITEM
				if($config['SendTaxAsLineItem']){
					if(($rowDatas['totalValue']['taxAmount']) > 0){
						$InvoiceLineAdd[$invoiceLineCount]['TaxAmount'] = 0.00;
						/* $InvoiceLineAdd[$invoiceLineCount]['TaxType']	= $config['salesNoTaxCode'];
						unset($InvoiceLineAdd[$invoiceLineCount]['TaxAmount']); */
					}
				}
				$invoiceLineCount++;
			}
			if($missingSkus){
				$missingSkus	= array_unique($missingSkus);
				$this->ci->db->update('sales_order',array('message' => 'Missing sku : '.implode(",",$missingSkus)),array('orderId' => $orderId));
				continue;
			}
			if($discountCouponAmt){
				$InvoiceLineAdd[$invoiceLineCount]	= array(
					'ItemCode' 		=> $config['couponItem'],
					'Description' 	=> 'Coupon Discount',
					'Quantity' 		=> 1,
					'UnitAmount' 	=> (-1) * sprintf("%.4f",$discountCouponAmt),
					'TaxAmount' 	=> 0.00,
				);
				if($TaxType){
					$InvoiceLineAdd[$invoiceLineCount]['TaxType']		= $TaxType; 
					unset($InvoiceLineAdd[$invoiceLineCount]['TaxAmount']);
				}
				else{
					if($discountTaxAbleLine){
						$InvoiceLineAdd[$invoiceLineCount]['TaxType']	= $discountTaxAbleLine;
					}
					else{
						$InvoiceLineAdd[$invoiceLineCount]['TaxType']	= $config['salesNoTaxCode'];
					}
				}
				if(isset($channelMappings[$channelId])){
					$trackingDetails	= $channelMappings[$channelId]['account2ChannelId'];
					if(@$channelMappings[$channelId]['warehouseDetails'][$warehouseId]){
						$trackingDetails	= $channelMappings[$channelId]['warehouseDetails'][$warehouseId]['account2ChannelId'];;
					}
					if($trackingDetails){
						$trackingDetails	= explode("~=",$trackingDetails);
						$InvoiceLineAdd[$invoiceLineCount]['Tracking']	= array(array('Name' => $trackingDetails['0'],'Option' => $trackingDetails['1']));
					}
				}
				//CHANGE THE TAX TYPE AS NONE IF TAX NEED TO SEND AS LINE ITEM
				if($config['SendTaxAsLineItem']){
					if(($rowDatas['totalValue']['taxAmount']) > 0){
						$InvoiceLineAdd[$invoiceLineCount]['TaxAmount']		= 0.00;
						/* $InvoiceLineAdd[$invoiceLineCount]['TaxType']	= $config['salesNoTaxCode'];
						unset($InvoiceLineAdd[$invoiceLineCount]['TaxAmount']); */
					}
				}
				$invoiceLineCount++;
			}
			if($totalItemDiscount){
				$InvoiceLineAdd[$invoiceLineCount]	= array(
					'ItemCode' 		=> $config['discountItem'],
					'Description' 	=> 'Item Discount',
					'Quantity' 		=> 1,
					'UnitAmount' 	=> (-1) * sprintf("%.4f",$totalItemDiscount),
					'TaxAmount' 	=> 0.00,
				);
				if($TaxType){
					$InvoiceLineAdd[$invoiceLineCount]['TaxType']	= $TaxType; 
					unset($InvoiceLineAdd[$invoiceLineCount]['TaxAmount']);
				}
				else {
					if($discountTaxAbleLine){
						$InvoiceLineAdd[$invoiceLineCount]['TaxType']	= $discountTaxAbleLine; 
					}
					else{
						$InvoiceLineAdd[$invoiceLineCount]['TaxType']	= $config['salesNoTaxCode'];
					}
				}
				if(isset($channelMappings[$channelId])){
					$trackingDetails	= $channelMappings[$channelId]['account2ChannelId'];
					if(@$channelMappings[$channelId]['warehouseDetails'][$warehouseId]){
						$trackingDetails	= $channelMappings[$channelId]['warehouseDetails'][$warehouseId]['account2ChannelId'];;
					}
					if($trackingDetails){
						$trackingDetails	= explode("~=",$trackingDetails);
						$InvoiceLineAdd[$invoiceLineCount]['Tracking']	= array(array('Name' => $trackingDetails['0'],'Option' => $trackingDetails['1']));
					}
				}
				//CHANGE THE TAX TYPE AS NONE IF TAX NEED TO SEND AS LINE ITEM
				if($config['SendTaxAsLineItem']){
					if(($rowDatas['totalValue']['taxAmount']) > 0){
						$InvoiceLineAdd[$invoiceLineCount]['TaxAmount']		= 0.00;
						/* $InvoiceLineAdd[$invoiceLineCount]['TaxType']	= $config['salesNoTaxCode'];
						unset($InvoiceLineAdd[$invoiceLineCount]['TaxAmount']); */
					}
				}
				$invoiceLineCount++;
			}
			if($config['SendTaxAsLineItem']){
				if(($rowDatas['totalValue']['taxAmount']) > 0){
					$InvoiceLineAdd[$invoiceLineCount]	= array(
						'Description' 	=> 'Total Tax Amount',
						'Quantity' 		=> 1,
						'UnitAmount' 	=> sprintf("%.4f",$rowDatas['totalValue']['taxAmount']),
						'AccountCode' 	=> $config['TaxItemLineNominal'],
						'TaxType'		=> $config['salesNoTaxCode'],
					);
					$invoiceLineCount++;
				}
			}
			
			$dueDate	= date('c');
			$taxDate	= date('c');
			if(@$rowDatas['invoices']['0']['dueDate']){
				$dueDate	= $rowDatas['invoices']['0']['dueDate'];
			}
			if(@$rowDatas['invoices']['0']['taxDate']){
				$taxDate	= $rowDatas['invoices']['0']['taxDate'];				
			}
			
			
			//taxdate chanages
			$BPDateOffset	= (int)substr($taxDate,23,3);
			$xeroOffset		= 0;
			$diff			= $BPDateOffset - $xeroOffset;
			$date1			= new DateTime($dueDate);
			$date			= new DateTime($taxDate);
			$BPTimeZone		= 'GMT';
			$date1->setTimezone(new DateTimeZone($BPTimeZone));
			$date->setTimezone(new DateTimeZone($BPTimeZone));
			if($diff > 0){
				$diff			.= ' hour';
				$date->modify($diff);
				$date1->modify($diff);
			}
			$taxDate		= $date->format('Y-m-d');
			$dueDate		= $date1->format('Y-m-d');
			
			$invoiceRef	= $orderId;
			if(@$rowDatas['invoices']['0']['invoiceReference']){
				$invoiceRef	= @$rowDatas['invoices']['0']['invoiceReference'];
			}					
			if(($uninvoiceCount == 0) || ($uninvoiceCount == '')){
				$InvoiceNumber	= $invoiceRef;
			}
			if($uninvoiceCount > 0){
				$InvoiceNumber	= $invoiceRef.'/New-0'.$uninvoiceCount;
			}
			if($config['TakeAsRefrence'] == 'BrightpearlCustomerReference'){
				$xeroInvoiceRefrence = $rowDatas['reference'];
			}
			else{
				$xeroInvoiceRefrence = $orderId;
			}
			if(!$xeroInvoiceRefrence){
				$xeroInvoiceRefrence = $orderId;
			}			
			$request	= array(
				'Type' 			=> 'ACCREC',
				'Contact' 		=> array(
					'ContactID'		=> $XeroCustomerID,
					'Addresses' 	=> array(
						array(
							'AddressType' 	=> 'DELIVERY',
							'AddressLine1' 	=> @$shipAddress['addressLine1'],
							'AddressLine2' 	=> @$shipAddress['addressLine2'],
							'City' 			=> @$shipAddress['addressLine3'],
							'Region' 		=> @$shipAddress['addressLine4'],
							'PostalCode'	=> @$shipAddress['postalCode'],
							'Country' 		=> @$shipAddress['countryIsoCode'],
						),
					),
				),
				'InvoiceNumber'	=> $InvoiceNumber, 
				'Reference'		=> $xeroInvoiceRefrence, 
				'Status'		=> 'AUTHORISED',
				'CurrencyCode'	=> $rowDatas['currency']['orderCurrencyCode'],						
				'CurrencyRate'	=> ($rowDatas['currency']['exchangeRate'])?($rowDatas['currency']['exchangeRate']):1,						
				'Date'			=> @$taxDate,
				'DueDate'		=> @$dueDate,
				'LineItems' 	=> $InvoiceLineAdd,
			);
			if($config['defaultCurrency'] != $config1['currencyCode']){
				unset($request['CurrencyRate']);
			}
			$url	= '2.0/Invoices?unitdp=4';
			if($accountDetails['OAuthVersion'] == 1){
				$url	= '2.0/Invoices';
			}
			if(!$accountDetails['OAuthVersion']){
				$url	= '2.0/Invoices';
			}
			$this->initializeConfig($account2Id, 'POST', $url);
			$results		= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
			$createdRowData	= array(
				'Request data	: '	=> $request,
				'Response data	: '	=> $results,
			);
			$logs[$orderId]	= $createdRowData;
			$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
			if((strtolower($results['Status']) == 'ok') && (isset($results['Invoices']['Invoice']['InvoiceID']))){
				$this->ci->db->update('sales_order',array('status' => '1','message' => '','invoiced' => '0','invoiceRef' => $invoiceRef,'createOrderId' => $results['Invoices']['Invoice']['InvoiceID']),array('id' => $orderDatas['id']));				
				
				$XeroTotalAmount	= $results['Invoices']['Invoice']['Total'];
				$NetRoundOff		= $BrightpearlTotalAmount - $XeroTotalAmount;
				$RoundOffCheck		= abs($NetRoundOff);
				$RoundOffApplicable = 0;
				if($RoundOffCheck != 0){
					if($RoundOffCheck < 0.9){
						$RoundOffApplicable = 1;
					}
					if($RoundOffApplicable){
						$InvoiceLineAdd[$invoiceLineCount] = array(
							'ItemCode'			=> $config['roundOffItem'],
							'Description' 		=> $config['roundOffItem'],
							'Quantity' 			=> 1,
							'UnitAmount' 		=> sprintf("%.4f",$NetRoundOff),
							'TaxType' 			=> $config['salesNoTaxCode'],							
						);	
						$request['LineItems']	= $InvoiceLineAdd;
						$url		= '2.0/Invoices?unitdp=4';
						if($accountDetails['OAuthVersion'] == 1){
							$url	= '2.0/Invoices';
						}
						if(!$accountDetails['OAuthVersion']){
							$url	= '2.0/Invoices';
						}
						$this->initializeConfig($account2Id, 'POST', $url);
						$results2	= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
						$createdRowData2		= array(
							'Request data	: '		=> $request,
							'Response data	: '		=> $results2,
						);
						if((strtolower($results2['Status']) == 'ok') && (isset($results2['Invoices']['Invoice']['InvoiceID']))){
							$this->ci->db->update('sales_order',array('status' => '1','invoiced' => '0','invoiceRef' => $invoiceRef,'createOrderId' => $results2['Invoices']['Invoice']['InvoiceID'], 'createdRowData' => json_encode($createdRowData2)),array('id' => $orderDatas['id']));
						}
						else{
							$this->ci->db->update('sales_order',array('message' => 'Unable to add Rounding Item'),array('id' => $orderDatas['id']));
						}
					}
				}
			}
			else{
				$createdRowData['lastTry'] = strtotime('now');
				$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
			}
		}	
	}
	if($logs){
		foreach($logs as $orderId => $log){
			$path	= FCPATH.'logs'.DIRECTORY_SEPARATOR . $accountDetails['account1Id'].DIRECTORY_SEPARATOR . 'so'.DIRECTORY_SEPARATOR;
			if(!is_dir($path)){
				mkdir($path,0777,true);
				chmod(dirname($path), 0777); 
			}
			file_put_contents($path. $orderId.'.logs',"\n\n BP Log Added On : ".date('c')." \n". json_encode($log),FILE_APPEND);
		}
	}
}
$this->postSalesPayment($orgObjectId);	
$this->fetchSalesPayment();		
?>