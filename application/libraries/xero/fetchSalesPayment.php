<?php
$this->reInitialize();
$enableAvalaraTax           = $this->ci->globalConfig['enableAvalaraTax'];
$enableGiftCard             = $this->ci->globalConfig['enableGiftCard'];
$enableAmazonFees           = $this->ci->globalConfig['enableAmazonFees'];
$enableInventoryTransfer    = $this->ci->globalConfig['enableInventoryTransfer'];
$enableInventoryManagement	= $this->ci->globalConfig['enableInventoryManagement'];
$batchUpdates		        = array();
$creditBatchUpdates	        = array();
$basedOn			        = 'createOrderId';
foreach($this->accountDetails as $account2Id => $accountDetails){	 		
	$config		= $this->accountConfig[$account2Id];	
	
	$pendingPayments		= array();
	$pendingPaymentsTemps	= $this->ci->db->select('orderId,createOrderId,isPaymentCreated,sendPaymentTo,paymentDetails')->get_where('sales_order',array(/* 'isPaymentCreated' => '0' ,*/'createOrderId <>' => ''))->result_array();
	foreach($pendingPaymentsTemps as $pendingPaymentsTemp){
		$pendingPayments[$pendingPaymentsTemp['createOrderId']]	= $pendingPaymentsTemp;
	}
	
	$url		= '2.0/Payment';
	$subParams	= array('where' => 'PaymentType="ACCRECPAYMENT"');
	$this->initializeConfig($account2Id, 'GET', $url,$subParams);			
	$url		= '2.0/Payment?where='.urlencode('PaymentType="ACCRECPAYMENT"');
	$this->headers['If-Modified-Since'] = gmdate('c',strtotime('-3 days'));
	$results	= $this->getCurl($url, 'GET','', 'json', $account2Id)[$account2Id];
	if($results){
		$PaymentsRes	= $results['Payments']['Payment'];
		if(!isset($PaymentsRes['0'])){
			$PaymentsRes	= array($PaymentsRes);
		}
		foreach($PaymentsRes as $Payments){
			$Invoices	= $Payments['Invoice'];
			$reference	= @explode(",",$Payments['Reference']);
			if(!isset($Invoices['0'])){
				$Invoices	= array($Invoices);
			}
			foreach($Invoices as $Invoice){
				$inoviceId	= @$Invoice['InvoiceID'];
				if(!$inoviceId){
					continue;
				}
				if(!isset($pendingPayments[$inoviceId])){
					continue;
				}
				$paymentDetails	= array();         
				if(!isset($batchUpdates[$inoviceId]['paymentDetails'])){
					$paymentDetails	= @json_decode($pendingPayments[$inoviceId]['paymentDetails'],true);
				}
				else{
					$paymentDetails = $batchUpdates[$inoviceId]['paymentDetails'];
				}
				if(isset($paymentDetails[$Payments['PaymentID']]) && ($Payments['Status'] == 'AUTHORISED')){
					continue;
				}
				if(isset($paymentDetails[$Payments['PaymentID']])){
					if($Payments['Status'] == 'DELETED'){
						if(strtolower($paymentDetails[$Payments['PaymentID']]['DeletedonBrightpearl']) == 'yes'){
							continue;
						}
						if(strtolower(@$paymentDetails[$Payments['PaymentID']]['reverseby']) == 'brightpearl'){
							continue;
						}
						if(strtolower($paymentDetails[$Payments['PaymentID']]['DeletedonBrightpearl']) == 'no'){
							$brightpearlRefrenceID	= $paymentDetails[$Payments['PaymentID']]['BrightpearlPayID'];
							@$paymentDetails[$Payments['PaymentID']]	= array(
								'amount'				=> $Payments['Amount'],
								'sendPaymentTo'			=> 'brightpearl',
								'Reference' 			=> $Payments['Reference'],
								'currency' 				=> $Payments['Invoice']['CurrencyCode'],
								'CurrencyRate' 			=> $Payments['CurrencyRate'],
								'paymentMethod' 		=> $Payments['Account']['Code'],
								'paymentDate' 			=> $Payments['Date'],
								'PaymentStatus'			=> $Payments['Status'],
								'DeletedonBrightpearl'	=> 'NO',
								'status'				=> '0',
								'IsReversal'			=> 1,
								'BrightpearlPayID'		=> $brightpearlRefrenceID,
							);
						}
					}
				}
				else{
					@$paymentDetails[$Payments['PaymentID']]	= array(
						'amount'				=> $Payments['Amount'],
						'sendPaymentTo'			=> 'brightpearl',
						'status'				=> '0',
						'Reference' 			=> $Payments['Reference'],
						'currency' 				=> $Payments['Invoice']['CurrencyCode'],
						'CurrencyRate' 			=> $Payments['CurrencyRate'],
						'paymentMethod' 		=> $Payments['Account']['Code'],
						'paymentDate' 			=> $Payments['Date'],
						'PaymentStatus'			=> $Payments['Status'],
						'DeletedonBrightpearl'	=> 'NO',
						'IsReversal'			=> 0,
					);
				}
				$batchUpdates[$inoviceId]					= array(
					'paymentDetails'		=> $paymentDetails,
					'createOrderId'			=> $inoviceId,
					'sendPaymentTo'			=> 'brightpearl',
				);
			}
		}
	}
	/* // STARTING CODE TO GET CREDIT APPLIED PAYMENTDETAILS */
	$creditPendingPayments	= array();
	$pendingPaymentsTemps	= $this->ci->db->select('orderId,createOrderId,isPaymentCreated,sendPaymentTo,paymentDetails,'.$basedOn)->get_where('sales_credit_order',array('isPaymentCreated' => '0','createOrderId <>' => ''))->result_array();
	foreach($pendingPaymentsTemps as $pendingPaymentsTemp){
		if($pendingPaymentsTemp[$basedOn]){
			$creditPendingPayments[$pendingPaymentsTemp[$basedOn]]	= $pendingPaymentsTemp;
		}
	}
	$url			= '2.0/CreditNotes';
	$subParams		= array('where' => 'Status="PAID"');
	$this->initializeConfig($account2Id, 'GET', $url,$subParams);			
	$url			= '2.0/CreditNotes?where='.urlencode('Status="PAID"');
	$this->headers['If-Modified-Since'] = gmdate('c',strtotime('-1 days'));
	$results		= $this->getCurl($url, 'GET','', 'json', $account2Id)[$account2Id];
	$CreditNotes	= $results['CreditNotes']['CreditNote'];
	if(!$CreditNotes['0']){
		$CreditNotes	= array($CreditNotes);
	}
	foreach($CreditNotes as $CreditNote){
		if($CreditNote['Type'] == 'ACCPAYCREDIT'){
			continue;
		}
		$allocations	= $CreditNote['Allocations']['Allocation'];
		if(!$allocations['0']){ 
			$allocations	= array($allocations);
		}
		foreach($allocations as $Invoice){
			$inoviceId	= @$Invoice['Invoice']['InvoiceID'];	
			if(!$inoviceId){
				continue;
			}
			if(!isset($pendingPayments[$inoviceId])){
				continue;
			}
			$paymentDetails	= array();
			if(!isset($batchUpdates[$inoviceId]['paymentDetails'])){
				$paymentDetails	= @json_decode($pendingPayments[$inoviceId]['paymentDetails'],true);
			}
			else{
				$paymentDetails = $batchUpdates[$inoviceId]['paymentDetails'];
			}
			$Payments	= $CreditNote;
			if(isset($paymentDetails[$Payments['CreditNoteID']])){
				continue;
			}
			@$paymentDetails[$Payments['CreditNoteID']]	= array(
				'amount' 			=> $Invoice['AppliedAmount'],
				'sendPaymentTo' 	=> 'brightpearl',
				'status' 			=> '0',
				'Reference' 		=> 'Applied amount',
				'currency' 			=> $Payments['CurrencyCode'],
				'CurrencyRate'		=> $Payments['CurrencyRate'],
				'isAppliedPayment'	=> '1',
				'IsReversal'		=> 0,
				'paymentDate' 		=> $Payments['Date'],
			);
			$batchUpdates[$inoviceId]					= array(
				'paymentDetails' 	=> $paymentDetails,
				$basedOn 			=> $inoviceId,
				'sendPaymentTo' 	=> 'brightpearl',
			);
		}
		/* // ASSING APPLIED PAYMENT TO CREDIT */
		foreach($allocations as $Invoice){
			$inoviceId	= @$CreditNote['CreditNoteID'];		
			if(!$inoviceId){
				continue;
			}
			if(!isset($creditPendingPayments[$inoviceId])){
				continue;
			}
			$paymentDetails	= array();
			if(!isset($creditBatchUpdates[$inoviceId]['paymentDetails'])){
				$paymentDetails	= @json_decode($creditPendingPayments[$inoviceId]['paymentDetails'],true);
			}
			else{
				$paymentDetails	= $creditBatchUpdates[$inoviceId]['paymentDetails'];
			}
			$Payments	= $CreditNote;
			if(isset($paymentDetails[$Invoice['Invoice']['InvoiceID']])){
				continue;
			}
			@$paymentDetails[$Invoice['Invoice']['InvoiceID']]	= array(
				'amount' 			=> $Invoice['AppliedAmount'],
				'sendPaymentTo'		=> 'brightpearl',
				'status' 			=> '0',
				'Reference' 		=> 'Applied amount',
				'currency' 			=> $Payments['CurrencyCode'],
				'CurrencyRate' 		=> $Payments['CurrencyRate'],
				'isAppliedPayment'	=> '1',
				'IsReversal'		=> 0,
				'paymentDate' 		=> $Payments['Date'],
			);
			$creditBatchUpdates[$inoviceId]						= array(
				'paymentDetails' 	=> $paymentDetails,
				$basedOn 			=> $inoviceId,
				'sendPaymentTo' 	=> 'brightpearl',
			);
		}
	}
	 
	/* STARTING CODE TO GET PREPAYMENT APPLIED PAYMENTDETAILS */
	$url		= '2.0/Prepayments';
	$subParams	= array('where' => 'Status="AUTHORISED"');
	$this->initializeConfig($account2Id, 'GET', $url);			
	$url		= '2.0/Prepayments?where='.urlencode('Status="AUTHORISED"');
	$url		= '2.0/Prepayments';
	$this->headers['If-Modified-Since'] = gmdate('c',strtotime('-3 days'));
	$results		= $this->getCurl($url, 'GET','', 'json', $account2Id)[$account2Id];
	$CreditNotes	= $results['Prepayments']['Prepayment'];
	if(!$CreditNotes['0']){
		$CreditNotes	= array($CreditNotes);
	}
	foreach($CreditNotes as $CreditNote){
		if($CreditNote['Type'] != 'RECEIVE-PREPAYMENT'){
			continue;
		}
		$allocations = $CreditNote['Allocations']['Allocation'];
		if(!$allocations){
			continue;
		}
		if(!$allocations['0']){
			$allocations	= array($allocations);
		}
		foreach($allocations as $Invoice){
			$inoviceId	= @$Invoice['Invoice']['InvoiceID'];
			if(!$inoviceId){
				continue;
			}
			if(!isset($pendingPayments[$inoviceId])){ 
				continue;
			}
			$paymentDetails	= array();
			if(!isset($batchUpdates[$inoviceId]['paymentDetails'])){
				$paymentDetails	= @json_decode($pendingPayments[$inoviceId]['paymentDetails'],true);
			}
			else{
				$paymentDetails = $batchUpdates[$inoviceId]['paymentDetails'];
			}
			$Payments = $CreditNote;
			if(isset($paymentDetails[$Payments['PrepaymentID']])){
				continue;
			}
			@$paymentDetails[$Payments['PrepaymentID']]	= array(
				'amount' 			=> $Invoice['AppliedAmount'],
				'sendPaymentTo' 	=> 'brightpearl',
				'status' 			=> '0',
				'Reference' 		=> 'Prepayment',
				'currency' 			=> $Payments['CurrencyCode'],
				'CurrencyRate' 		=> $Payments['CurrencyRate'],
				'isPrePayment'		=> '1',
				'paymentDate' 		=> $Payments['Date'],
			);
			$batchUpdates[$inoviceId]					= array(
				'paymentDetails' 	=> $paymentDetails,
				 $basedOn 			=> $inoviceId,
				'sendPaymentTo' 	=> 'brightpearl',
			);
		}
		/* ASSING APPLIED PAYMENT TO CREDIT */
	}
	/* CODE STARTS FOR FETCHING OVERPAYMENTS */
	$url		= '2.0/Overpayments';
	$subParams	= array('where' => 'Status="AUTHORISED"');
	$this->initializeConfig($account2Id, 'GET', $url);			
	$url		= '2.0/Overpayments?where='.urlencode('Status="AUTHORISED"');
	$url		= '2.0/Overpayments';
	$this->headers['If-Modified-Since']	= gmdate('c',strtotime('-3 days'));
	$results			= $this->getCurl($url, 'GET','', 'json', $account2Id)[$account2Id];
	$OverPaymentDatas	= $results['Overpayments']['Overpayment'];
	if(!$OverPaymentDatas['0']){
		$OverPaymentDatas	= array($OverPaymentDatas);
	}
	foreach($OverPaymentDatas as $OverPaymentData){
		if($OverPaymentData['Type'] != 'RECEIVE-OVERPAYMENT'){
			continue;
		}
		$allocations	= $OverPaymentData['Allocations']['Allocation'];
		if(!$allocations){
			continue;
		}
		if(!$allocations['0']){
			$allocations	= array($allocations);
		}
		foreach($allocations as $Invoice){
			$inoviceId	= @$Invoice['Invoice']['InvoiceID'];
			if(!$inoviceId){
				continue;
			}						
			if(!isset($pendingPayments[$inoviceId])){
				continue;
			}
			$paymentDetails	= array();
			if(!isset($batchUpdates[$inoviceId]['paymentDetails'])){
				$paymentDetails	= @json_decode($pendingPayments[$inoviceId]['paymentDetails'],true);
			}
			else{
				$paymentDetails = $batchUpdates[$inoviceId]['paymentDetails'];
			}
			$Payments	= $OverPaymentData;
			if(isset($paymentDetails[$Payments['OverpaymentID']])){
				continue;
			}
			@$paymentDetails[$Payments['OverpaymentID']]	= array(
				'amount' 			=> $Invoice['AppliedAmount'],
				'sendPaymentTo'		=> 'brightpearl',
				'status' 			=> '0',
				'Reference' 		=> 'OverPayment',
				'currency' 			=> $Payments['CurrencyCode'],
				'CurrencyRate' 		=> $Payments['CurrencyRate'],
				'isOverPayment'		=> '1',
				'paymentDate'		=> $Payments['Date'],
			);
			$batchUpdates[$inoviceId]	= array(
				'paymentDetails'	=> $paymentDetails,
				 $basedOn 			=> $inoviceId,
				'sendPaymentTo' 	=> 'brightpearl',
			);
		}
	}
}

if($batchUpdates){
	foreach($batchUpdates as $key => $batchUpdate){
		$batchUpdates[$key]['paymentDetails']	= json_encode($batchUpdate['paymentDetails']);
	} 
	$batchUpdates	= array_chunk($batchUpdates,200);
	foreach($batchUpdates as $batchUpdate){
		if($batchUpdate){
			$this->ci->db->update_batch('sales_order',$batchUpdate,'createOrderId');
		}
	}
}

if($creditBatchUpdates){
	foreach($creditBatchUpdates as $key => $batchUpdate){
		$creditBatchUpdates[$key]['paymentDetails']	= json_encode($batchUpdate['paymentDetails']);
		
	} 
	$creditBatchUpdates	= array_chunk($creditBatchUpdates,200);
	foreach($creditBatchUpdates as $batchUpdate){
		if($batchUpdate){
			$this->ci->db->update_batch('sales_credit_order',$batchUpdate,$basedOn);
		}
	}
}
?>