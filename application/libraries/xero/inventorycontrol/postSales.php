<?php
$this->reInitialize();
foreach($this->accountDetails as $account2Id => $accountDetails){	
	$nominalMappings = array();$logs = array();
	$nominalMappingTemps = $this->ci->db->get_where('mapping_nominal',array('account1CustomFieldValue' => '','account2Id' => $account2Id))->result_array();
	foreach($nominalMappingTemps as $nominalMappingTemp){
		if(@!$nominalMappings[$nominalMappingTemp['account1NominalId']]){
			if(!$nominalMappingTemp['account1ChannelId']){
				$nominalMappings[$nominalMappingTemp['account1NominalId']] = $nominalMappingTemp;
			}
		}
		if($nominalMappingTemp['account1ChannelId']){
			$nominalMappings[$nominalMappingTemp['account1NominalId']]['channel'][strtolower($nominalMappingTemp['account1ChannelId'])] = $nominalMappingTemp;
		}
	}
	$config = $this->accountConfig[$account2Id];
	$nominalCodeForShipping = explode(",",$config['nominalCodeForShipping']);
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	$datas = $this->ci->db->where_in('status',array('0'))->get_where('sales_order',array('account2Id' => $account2Id))->result_array(); 
	$customerMappings = array();
	$customerMappingsTemps = $this->ci->db->select('customerId,createdCustomerId,email')->get_where('customers',array('account2Id' => $account2Id))->result_array();
	foreach($customerMappingsTemps as $customerMappingsTemp){
		$customerMappings[$customerMappingsTemp['customerId']] = $customerMappingsTemp;
	}
	$productMappings = array();
	$allSalesItemProductIdsTemps = $this->ci->db->select('productId')->get_where('sales_item',array('status' => '0','productId > ' => '1001'))->result_array();
	if($allSalesItemProductIdsTemps){
		$allSalesItemProductIds = array_column($allSalesItemProductIdsTemps,'productId'); 
		if($allSalesItemProductIds){
			$this->postProducts($allSalesItemProductIds);
		}
	}
	$allSalesCustomerTemps = $this->ci->db->select('customerId')->get_where('sales_order',array('status' => '0','customerId <>' => ''))->result_array();
	if($allSalesCustomerTemps){
		$allSalesCustomer = array_column($allSalesCustomerTemps,'customerId'); 
		if($allSalesCustomer){
			$this->postCustomers($allSalesCustomer); 
		}
	}
	$productMappingsTemps = $this->ci->db->select('productId,createdProductId,name,sku')->get_where('products',array('account2Id' => $account2Id))->result_array();
	foreach($productMappingsTemps as $productMappingsTemp){
		$productMappings[$productMappingsTemp['productId']] = $productMappingsTemp;
	}
	$taxMappings = array();
	$taxMappingsTemps = $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id,'orderType <>' => '2'))->result_array();
	if($taxMappingsTemps){ 
		foreach($taxMappingsTemps as $taxMappingsTemp){
			$taxMappings[$taxMappingsTemp['account1TaxId']] = $taxMappingsTemp;
		}
	}
	$channelMappings = array();
	$channelMappingsTemps = $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	if($channelMappingsTemps){
		foreach($channelMappingsTemps as $channelMappingsTemp){
			$channelMappings[$channelMappingsTemp['account1ChannelId']] = $channelMappingsTemp;
		}
	}
	if($datas){				
		foreach($datas as $orderDatas){
			if($orderDatas['createOrderId']){continue;}					
			$config1 = $this->ci->account1Config[$orderDatas['account1Id']];
			$orderId = $orderDatas['orderId'];
			$rowDatas = json_decode($orderDatas['rowData'],true);
			$channelId = $rowDatas['assignment']['current']['channelId'];
			$createdRowData = json_decode($orderDatas['createdRowData'],true);
			$billAddress 	=  $rowDatas['parties']['billing'];
			$shipAddress 	=  $rowDatas['parties']['delivery'];
			$orderCustomer 	=  $rowDatas['parties']['customer'];
			
			if(@!$customerMappings[$orderCustomer['contactId']]['createdCustomerId']){
				continue;
			}
			$missingSkus = array();$InvoiceLineAdd = array();$invoiceLineCount = 0;$productCreateIds = array();$linNumber = 1;$totalItemDiscount = 0;$itemDiscountTax = 0;
			$productUpdateNominalCodeInfos = array();$orderTaxId = '';$orderTaxAmount = 0;$discountCouponAmt = 0;$isDiscountCouponAdded = 0;$isDiscountCouponTax = 0;
			foreach($rowDatas['orderRows'] as $rowId => $orderRows){
				if($orderRows['productId'] <= 1001){
					if(substr_count(strtolower($orderRows['productName']),'coupon')){
						if($orderRows['nominalCode'] == $config['nominalCodeForDiscount']){
							$isDiscountCouponAdded = 1;
						}
					}
				}
			}
			$itemtaxAbleLine = $config['orderLineTaxCode'];$discountTaxAbleLine = $config['orderLineTaxCode'];
			foreach($rowDatas['orderRows'] as $rowId => $orderRows){
				$LineTaxId = '';
				if(@$taxMappings[$orderRows['rowValue']['taxClassId']]){
					$LineTaxId = $taxMappings[$orderRows['rowValue']['taxClassId']]['account2TaxId'];
					$orderTaxId = $taxMappings[$orderRows['rowValue']['taxClassId']]['account2TaxId'];
				}						
				if($orderRows['productId'] > 1001){
					$productId = $orderRows['productId'];
					if(@!$productMappings[$productId]['createdProductId']){
						$missingSkus[] = $orderRows['productSku'];
						continue;
					}
					$ItemRefName = $productMappings[$productId]['sku'];
					$ItemRefValue = $productMappings[$productId]['sku'];
				}
				else{
					if($orderRows['rowValue']['rowNet']['value'] > 0){
						$ItemRefValue = $config['genericSku'];
						$ItemRefName  = $orderRows['productName'];
						if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){ 
							$ItemRefValue = $config['shippingItem'];
							$ItemRefName  = 'Shipping';
						}
					}
					else if($orderRows['rowValue']['rowNet']['value'] < 0){
						$ItemRefValue = $config['discountItem'];
						$ItemRefName  = 'Discount Item';
					}
					else{
						continue;
					}
				}
				$price = $orderRows['rowValue']['rowNet']['value'] + $orderRows['rowValue']['rowTax']['value'];
				if($LineTaxId){
					$price = $orderRows['rowValue']['rowNet']['value']; 
					$orderTaxAmount += $orderRows['rowValue']['rowTax']['value'];
				}
				$originalPrice = $price;
				if($orderRows['discountPercentage'] > 0){
					$discountPercentage = 100 - $orderRows['discountPercentage'];
					if($discountPercentage == 0){
						$originalPrice = $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
					}
					else{
						$originalPrice = round((($price * 100) / ( 100 - $orderRows['discountPercentage'])),2);
					}
					$tempTaxAmt = $originalPrice - $price;
					if($tempTaxAmt > 0){
						$totalItemDiscount += $tempTaxAmt;						
						$itemDiscountTax = 1; 
						$itemtaxAbleLine = $LineTaxId;
					}
				}	
				else if($isDiscountCouponAdded){
					$originalPriceWithTax = $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
					$originalPrice = $originalPriceWithTax - $orderRows['rowValue']['rowTax']['value'];
					$discountCouponAmtTemp = ($originalPrice - $price);
					if($discountCouponAmtTemp > 0){
						$discountCouponAmt += $discountCouponAmtTemp;						
						$isDiscountCouponTax = 1;
						$discountTaxAbleLine = $LineTaxId;
					}
				}
				$IncomeAccountRef 	= $config['IncomeAccountRef'];					
				if(@isset($nominalMappings[$orderRows['nominalCode']])){
					if(@$nominalMappings[$orderRows['nominalCode']]['account2NominalId']){
						$IncomeAccountRef = $nominalMappings[$orderRows['nominalCode']]['account2NominalId'];
					}
					$nominalMapped = $nominalMappings[$orderRows['nominalCode']];
					if($channelId){
						if(isset($nominalMapped['channel'])){
							if(isset($nominalMapped['channel'][strtolower($channelId)])){
								if($orderRows['productId'] > 1001){
									$productUpdateNominalCodeInfos[$productId]['org'] = $IncomeAccountRef;
								}
								$IncomeAccountRef = $nominalMapped['channel'][strtolower($channelId)]['account2NominalId'];
								if($orderRows['productId'] > 1001){
									$productUpdateNominalCodeInfos[$productId]['upd'] = $IncomeAccountRef;
								}
							}
						}
					}
				}
				$UnitAmount = 0.00;
				if($originalPrice > 0){
					$UnitAmount = $originalPrice / $orderRows['quantity']['magnitude'];
				}
				if(is_nan($originalPrice)){
					$originalPrice = 0.00;
				}
				$InvoiceLineAdd[$invoiceLineCount] = array( 
					'ItemCode' 		=> $ItemRefValue, 
					'Description' 	=> $orderRows['productName'],
					'Quantity' 		=> (int)$orderRows['quantity']['magnitude'],
					'UnitAmount' 	=> sprintf("%.4f",$UnitAmount),
					'TaxType' 		=> $LineTaxId,
					'AccountCode' 	=> $IncomeAccountRef,
					'TaxAmount' 	=> sprintf("%.4f",$orderRows['rowValue']['rowTax']['value']),
					'LineAmount' 	=> sprintf("%.4f",$originalPrice),	
				);		
				if($orderRows['quantity']['magnitude'] <= 1){
					$InvoiceLineAdd[$invoiceLineCount]['UnitAmount'] = $InvoiceLineAdd[$invoiceLineCount]['LineAmount'];
				}
				if(!$LineTaxId){
					unset($InvoiceLineAdd[$invoiceLineCount]['TaxType']);
				}				
				if(isset($channelMappings[$channelId])){
					$trackingDetails = $channelMappings[$channelId]['account2ChannelId'];
					if(@$channelMappings[$channelId]['warehouseDetails'][$warehouseId]){
						$trackingDetails = $channelMappings[$channelId]['warehouseDetails'][$warehouseId]['account2ChannelId'];
					}
					if($trackingDetails){
						$trackingDetails = explode("~=",$trackingDetails);
						$InvoiceLineAdd[$invoiceLineCount]['Tracking'] = array(array('Name' => $trackingDetails['0'],'Option' => $trackingDetails['1']));
					}
				}
				$invoiceLineCount++;
			}
			if($missingSkus){
				$missingSkus = array_unique($missingSkus);
				$this->ci->db->update('sales_order',array('message' => 'Missing sku : '.implode(",",$missingSkus)),array
				('orderId' => $orderId));
				continue;
			}
			$dueDate = date('Y-m-d');$taxDate = date('Y-m-d');
			if(@$rowDatas['invoices']['0']['dueDate']){
				$dueDate = gmdate('Y-m-d',strtotime($rowDatas['invoices']['0']['dueDate']));
			}
			if(@$rowDatas['invoices']['0']['taxDate']){
				$taxDate = date('Y-m-d',strtotime($rowDatas['invoices']['0']['taxDate']));
			}
			$invoiceRef = $orderId;
			if(@$rowDatas['invoices']['0']['invoiceReference']){
				$invoiceRef = @$rowDatas['invoices']['0']['invoiceReference'];
			}
			
			if($totalItemDiscount){
				$InvoiceLineAdd[$invoiceLineCount] = array(
					'ItemCode' 		=> $config['discountItem'],
					'Description' 	=> 'Item Discount',
					'Quantity' 		=> 1,
					'UnitAmount' 	=> (-1) * sprintf("%.2f",$totalItemDiscount),
					'LineAmount' 	=>(-1) * sprintf("%.2f",$totalItemDiscount),
					'TaxAmount' 	=> 0.00,							
				);
				if($itemDiscountTax){
					$InvoiceLineAdd[$invoiceLineCount]['TaxType']		= $itemtaxAbleLine;
				}
				else{
					$InvoiceLineAdd[$invoiceLineCount]['TaxType']		= $config['salesNoTaxCode'];
				}
				if(isset($channelMappings[$channelId])){
					$trackingDetails = $channelMappings[$channelId]['account2ChannelId'];
					$trackingDetails = explode("~=",$trackingDetails);
					$InvoiceLineAdd[$invoiceLineCount]['Tracking'] = array(array('Name' => $trackingDetails['0'],'Option' => $trackingDetails['1']));
				}
				$invoiceLineCount++; 
			}
			if($discountCouponAmt){
				$InvoiceLineAdd[$invoiceLineCount] = array(
					'ItemCode' 		=> $config['couponItem'],
					'Description' 	=> 'Coupon Discount',
					'Quantity' 		=> 1,
					'UnitAmount' 	=> (-1) * sprintf("%.2f",$discountCouponAmt),
					'LineAmount' 	=>(-1) * sprintf("%.2f",$discountCouponAmt),
					'TaxAmount' 	=> 0.00,
				);
				if($isDiscountCouponTax){
					$InvoiceLineAdd[$invoiceLineCount]['TaxType']		= array( 'ListID' => $discountTaxAbleLine );
				}
				else{
					$InvoiceLineAdd[$invoiceLineCount]['TaxType']		= array( 'ListID' => $config['orderLineNoTaxCode'] );
				}
				if(isset($channelMappings[$channelId])){
					$trackingDetails = $channelMappings[$channelId]['account2ChannelId'];
					$trackingDetails = explode("~=",$trackingDetails);
					$InvoiceLineAdd[$invoiceLineCount]['Tracking'] = array(array('Name' => $trackingDetails['0'],'Option' => $trackingDetails['1']));
				}
				$invoiceLineCount++;
			}					
			$request = array(
				'Type' 			=> 'ACCREC',
				'Contact' 		=> array(
					'ContactID' => $customerMappings[$orderCustomer['contactId']]['createdCustomerId'],
					'Addresses' => array(
						array(
							'AddressType' 	=> 'DELIVERY',
							'AddressLine1' 	=> @$shipAddress['addressLine1'],
							'AddressLine2' 	=> @$shipAddress['addressLine2'],
							'City' 			=> @$shipAddress['addressLine3'],
							'Region' 		=> @$shipAddress['addressLine4'],
							'PostalCode'	=> @$shipAddress['postalCode'],
							'Country' 		=> @$shipAddress['countryIsoCode'],
						),
					),
				),
				'InvoiceNumber'	=> $invoiceRef,
				'Reference'		=> $orderId,
				'Status'		=> 'AUTHORISED',
				'CurrencyCode'	=> $rowDatas['currency']['orderCurrencyCode'],						
				'CurrencyRate'	=> ($rowDatas['currency']['exchangeRate'])?($rowDatas['currency']['exchangeRate']):1,						
				'Date'			=> @$taxDate,
				'DueDate'		=> @$dueDate,
				'LineItems' 	=> $InvoiceLineAdd
			);
			$url = '2.0/Invoices';
			$this->initializeConfig($account2Id, 'POST', $url);
			$results = $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
			$createdRowData = array(
				'Request data : ' => $request,
				'Response data : ' => $results,
			);
			$logs[$orderDatas['orderId']] = array(
				'Request data : ' => $request,
				'Response data : ' => $results,
			);
			$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
			if((strtolower($results['Status']) == 'ok') && (isset($results['Invoices']['Invoice']['InvoiceID']))){	
				$this->ci->db->update('sales_order',array('status' => '1','invoiceRef' => $invoiceRef,'createOrderId' => $results['Invoices']['Invoice']['InvoiceID']),array('id' => $orderDatas['id']));
			}
		}
	}
	if($logs){
		foreach($logs as $orderId => $log){
			$path = FCPATH.'logs'.DIRECTORY_SEPARATOR . $accountDetails['account1Id'].DIRECTORY_SEPARATOR . 'so'.DIRECTORY_SEPARATOR;
			if(!is_dir($path)) { mkdir($path,0777,true);chmod(dirname($path), 0777); }
			file_put_contents($path. $orderId.'.logs',"\n\n BP Log Added On : ".date('c')." \n". json_encode($log),FILE_APPEND);
		}
	}
}
$this->postSalesPayment($orgOrderId);	
$this->fetchSalesPayment();		
?>