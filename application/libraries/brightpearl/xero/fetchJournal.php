<?php
$this->reInitialize();
$return	= array();
foreach($this->accountDetails as $account1Id => $accountDetails){
	$this->config	= $this->accountConfig[$account1Id];
	$account2Ids	= $this->account2Details[$account1Id];
	$datas			= $this->ci->db->order_by('id', 'desc')->get_where('cron_management', array('type' => 'journal'.$account1Id))->row_array();
	$cronTime		= $datas['saveTime'];	
	if(!$cronTime){
		$cronTime	= strtotime('-120 days');
	}
	$datetime			= new DateTime(date('c',$cronTime));
	$cronTime			= $datetime->format(DateTime::ATOM);
	$cronTime			= str_replace("+","%2B",$cronTime);
	$JournalTypeCode	= $this->config['journalType'];
	$JournalAccountCode	= $this->config['journalAccount'];
	if(!$journalId){
		$journalId	= array();
		$url		= '/accounting-service/journal-search?journalType='.$JournalTypeCode.'&nominalCode='.$JournalAccountCode.'&journalDate=' . $cronTime . '/';
		$response	= $this->getCurl($url, "GET", '', 'json', $account1Id);
		if(@$response[$account1Id]){
			$header	= array_column($response[$account1Id]['metaData']['columns'],"name");
			if($response[$account1Id]){
				foreach($response[$account1Id]['results'] as $result){
					$row			= array_combine($header,$result);		
					$journalId[]	= $row['journalId'];
				}
			}
			if($response[$account1Id]['metaData']){
				for($i = 500; $i <= $response[$account1Id]['metaData']['resultsAvailable']; $i = ($i + 500)){
					$url1		= $url . '&firstResult=' . $i;
					$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
					if($response1['results']){
						foreach($response1['results'] as $result){
							$row			= array_combine($header,$result);						
							$journalId[]	= $row['journalId'];
						}
					}
				}
			}
		}
		$journalId	= array_unique($journalId);
		if(!$journalId){
			continue;
		}
	}
	if(is_string($journalId)){
		$journalId	= array($journalId);
	}
	if(!$journalId){
		continue;
	}
	$resDatas		= $this->getResultById($journalId,'/accounting-service/journal',$account1Id,200,'0','');
	$returnKey		= 0;
	$updatedTimes	= array();
	foreach($resDatas as $resDatasss){
		foreach($resDatasss as $resData){
			$description	= $resData['description'];
			if($this->config['details']){
				if(!substr_count(strtolower($description),strtolower($this->config['details']))){
					continue;
				}
			}
			else{
				continue;
			}
			$journalsId	= $resData['id'];
			foreach($account2Ids as $account2Id){
				$saveAccId1	= ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($account1Id) : $account2Id['id'];
				$saveAccId2	= ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($account2Id['id']) : $account1Id;	
				if($resData['createdOn']){
					$updatedTimes[]	= strtotime(@$resData['createdOn']);
				}
				$debits	= $resData['debits'];
				if(!$debits['0']){
					$debits	= array($debits);
				}
				$dabitsDatas	= array();			
				foreach($debits as $debit){
					if($debit['orderId']){
						if(isset($dabitsDatas[$debit['orderId']])){
							$dabitsDatas[$debit['orderId']]['transactionAmount']	+= $debit['transactionAmount'];
						}
						else{
							$dabitsDatas[$debit['orderId']]	= $debit;
						}
					}
				}
				foreach($dabitsDatas as $orderId => $dabitsData){
					$return[$saveAccId1][$returnKey]	= array(
						'account1Id'     	=> $saveAccId1,
						'account2Id'     	=> $saveAccId2,
						'journalsId'     	=> $journalsId,
						'orderId'         	=> @$orderId,  
						'amount'            => $dabitsData['transactionAmount'],
						'journalTypeCode'   => $resData['journalTypeCode'],
						'debitNominalCode'	=> $dabitsData['nominalCode'],
						'taxCode'        	=> $dabitsData['taxCode'],
						'taxDate'        	=> date('Y-m-d H:i:s', strtotime($resData['createdOn'])),
						'params'        	=> json_encode($resData), 
					);
					$saveCronTime[]		= strtotime($resData['createdOn']);
					$returnKey++; 
				}
			}
		}
	}
}
$returns[$account1Id]	= array( 'return' => $return,'saveTime' => @max($saveCronTime));
return $returns;