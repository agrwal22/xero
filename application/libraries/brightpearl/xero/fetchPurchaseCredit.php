<?php
$this->reInitialize($accountId);
//get the functionalities value in the global configuration
$enableAvalaraTax			= $this->ci->globalConfig['enableAvalaraTax'];
$enableGiftCard				= $this->ci->globalConfig['enableGiftCard'];
$enableAmazonFees			= $this->ci->globalConfig['enableAmazonFees'];
$enableInventoryTransfer	= $this->ci->globalConfig['enableInventoryTransfer'];
$enableInventoryManagement	= $this->ci->globalConfig['enableInventoryManagement'];
$returns = array();
foreach ($this->accountDetails as $account1Id => $accountDetails) {
	$datas			= $this->ci->db->order_by('id', 'desc')->get_where('cron_management', array('type' => 'purchaseCredit'.$account1Id))->row_array();
	$cronTime		= $datas['saveTime'];	
	$return			= array();
	$saveCronTime	= array();
	$orderIds		= array();
	$logs			= array();
	if(!$cronTime){
		$cronTime	= strtotime('-10 days');
	}
	$datetime		= new DateTime(date('c',$cronTime));
	$cronTime		= $datetime->format(DateTime::ATOM);
	$cronTime		= str_replace("+","%2B",$cronTime);
	
	$datas	= $this->ci->db->order_by('id', 'desc')->get_where('cron_management', array('type' => 'uninvoicesales'.$account1Id))->row_array();
	$journalCronTime		= $datas['saveTime'];	
	$journalSaveCronTime	= array();
	if(!$journalCronTime){
		$journalCronTime	= strtotime('-90 days');
	}
	$datetime			= new DateTime(date('c',$journalCronTime));
	$journalCronTime	= $datetime->format(DateTime::ATOM);
	$journalCronTime	= str_replace("+","%2B",$journalCronTime);
	$this->config	= $this->accountConfig[$account1Id];			
	
	$journalOrderIds	= array();
	if(($this->config['UnInvoicingEnabled']) == 1){
		$invoice_refs	= $this->ci->db->select('orderId,generalIds')->get_where('purchase_credit_order')->result_array(); 
		$invoicedatas	= array();
		foreach($invoice_refs as $invoice_ref){
			$invoicedatas[$invoice_ref['orderId']]	= $invoice_ref;
		}
		$url				= '/accounting-service/journal-search?journalType=PC&nominalCode=9999&journalDate='.$journalCronTime.'/';
		$response			= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id]; 
		$journalOrderIds	= array();
		if(@$response['results']){
			$header	= array_column($response['metaData']['columns'],'name');
			foreach($response['results'] as $result){
				$journalFetchRow	= array_combine($header,$result);
				$journalOrderIds[$journalFetchRow['orderId']][$journalFetchRow['journalId']]	= $journalFetchRow['journalId'];
			}
			if($response['metaData']){
				for($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)){
					$url1		= $url . '&firstResult=' . $i;
					$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
					if($response1['results']){
						foreach($response1['results'] as $result){
							$journalFetchRow	= array_combine($header,$result);
							$journalOrderIds[$journalFetchRow['orderId']][$journalFetchRow['journalId']] = $journalFetchRow['journalId'];
						}
					}
				}
			}
		}
	}
	if($this->config['fetchPurchaseCredit']){
		$fetchStatusIds = explode(",",$this->config['fetchPurchaseCredit']);
	}
	$account2Ids	= $this->account2Details[$account1Id]; 
	$warehouseSales = $this->config['warehouseSales'];
	if($warehouseSales){ 
		$warehouseSales	= explode(",",$warehouseSales);
	}
	else{
		$warehouseSales	= explode(",",$this->config['warehouse']);
	}
	$url		= '/order-service/order-search?orderTypeId=4&updatedOn='.$cronTime.'/';
	$response	= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];  
	$goLiveDate		= $this->config['goLiveDate'];
	$saveTaxDate	= $this->config['taxDate'];
	$header			= @array_column($response['metaData']['columns'],'name');
	if (@$response['results']) {
		foreach ($response['results'] as $result) {
			$row		= array_combine($header,$result);
			$created	= gmdate('Ymd',strtotime($row['createdOn']));
			$taxDate	= gmdate('Ymd',strtotime($row['taxDate']));
			if($taxDate){
				if($taxDate < $saveTaxDate){
					continue;
				}
			}			
			elseif($goLiveDate){
				if($created < $goLiveDate){
					continue;
				}
			}
			$orderIds[$result['0']]	= $result['0'];
		}
		if ($response['metaData']) {
			for ($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)) {
				$url1		= $url . '&firstResult=' . $i;
				$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
				if ($response1['results']) {
					foreach ($response1['results'] as $result) {
						$row		= array_combine($header,$result);
						$created	= gmdate('Ymd',strtotime($row['createdOn']));
						$taxDate	= gmdate('Ymd',strtotime($row['taxDate']));
						if($taxDate){
							if($taxDate < $saveTaxDate){
								continue;
							}
						}			
						elseif($goLiveDate){
							if($created < $goLiveDate){
								continue;
							}
						}
						$orderIds[$result['0']]	= $result['0'];
					}
				}
			}
		}
	}
	$jOrderIds		= array();
	$jOrderIds		= array_keys($journalOrderIds);
	if($orderIds){
		$orderIds	= array_merge($orderIds,$jOrderIds);
		sort($orderIds);
		$orderDatas	= $this->getResultById($orderIds,'/order-service/order/',$account1Id,200,0,'?includeOptional=customFields,nullCustomFields');
		foreach($orderDatas as $OrderInfoList){
			$logs[$OrderInfoList['id']]	= $OrderInfoList;
			foreach ($account2Ids as $account2Id) {
				$orderId			= $OrderInfoList['id'];
				$channelId			= @$OrderInfoList['assignment']['current']['channelId']; 
				$statusId			= $OrderInfoList['orderStatus']['orderStatusId'];
				$warehouseId		= $OrderInfoList['warehouseId'];
				$tempSaveAcc1		= $account1Id;
				$tempSaveAcc2		= '';
				$config2			= $this->account2Config[$account2Id['id']];
				$invoiceReference	= $OrderInfoList['invoices']['0']['invoiceReference'];
				$uninvoiced			= 0;
				$invoiced			= 0;
				if($invoiceReference){
					$invoiced	= 1;
				}
				$totalJournalIds	= array();
				if(($this->config['UnInvoicingEnabled']) == 1){
					$journalOrderId = $journalOrderIds[$orderId];
					$saveJournalIds = $invoicedatas[$orderId];
					if($saveJournalIds){
						$saveJournalIds	= json_decode($saveJournalIds['generalIds'],true);
						foreach($saveJournalIds as $saveJournalId){
							$totalJournalIds[$saveJournalId] = $saveJournalId;
							unset($journalOrderId[$saveJournalId]);
						}
					}
					if($journalOrderId){
						foreach($journalOrderId as $jid){
							$totalJournalIds[$jid]	= $jid;
						}						
						$uninvoiced	= 1;
					}
				}
				if((!$uninvoiced) && (!$invoiceReference)){
					continue;
				}
				if(!$uninvoiced){
					if(!in_array($statusId,$fetchStatusIds)){
						continue;
					}
				}
				if($uninvoiced){
					if(!isset($invoicedatas[$orderId])){
						$invoiced	= 1;
					}
				}
				if($config2['warehouses']){
					$config2Warehouses	= explode(",",$config2['warehouses']);
				}
				if($config2['channelIds']){
					$config2Channels	= explode(",",$config2['channelIds']);
				}
				if($config2Channels || $config2Warehouses){
					if((!$channelId) && (!$warehouseId)){
						continue;
					}
					if($config2Channels){
						if(!in_array($channelId,$config2Channels)){
							continue;
						}
					}	
					if($config2Warehouses){
						if(!in_array($warehouseId,$config2Warehouses)){
							continue;
						}
					}
					$tempSaveAcc2	= $account2Id['id'];
				}
				else{
					$tempSaveAcc2	= $account2Id['id'];
				}
				if(!$tempSaveAcc2){
					continue;
				}
				$saveAccId1	= ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($tempSaveAcc1) : $tempSaveAcc2;			
				$saveAccId2	= ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($tempSaveAcc2) : $tempSaveAcc1;
				$return[$account1Id][$orderId]['orders']		= array(
					'account1Id'        => $saveAccId1,
					'account2Id'        => $saveAccId2,
					'orderId'           => $orderId,
					'delAddressName'	=> $OrderInfoList['parties']['supplier']['addressFullName'],
					'delPhone' 		    => $OrderInfoList['parties']['supplier']['telephone'],
					'customerEmail'     => $OrderInfoList['parties']['supplier']['email'],
					'customerId'        => $OrderInfoList['parties']['supplier']['contactId'],
					'reference'    	    => $OrderInfoList['reference'], 
					'parentOrderId'     => $OrderInfoList['parentOrderId'], 
					'warehouse'   	    => $OrderInfoList['warehouseId'], 
					'totalAmount'       => $OrderInfoList['totalValue']['total'],
					'totalTax'          => $OrderInfoList['totalValue']['taxAmount'],
					'shippingMethod'    => @$OrderInfoList['delivery']['shippingMethodId'],
					'created'           => date('Y-m-d H:i:s', strtotime($OrderInfoList['createdOn'])),
					'rowData'           => json_encode($OrderInfoList),
					'uninvoiced'		=> $uninvoiced,
					'invoiced'			=> $invoiced,
					'generalIds'		=> json_encode($totalJournalIds),
				);
				$address = $OrderInfoList['parties']['delivery'];
				$return[$account1Id][$orderId]['address'][]		= array(
					'account1Id'		=> $saveAccId1, 
					'account2Id'        => $saveAccId2,
					'orderId'           => $orderId,
					'fname' 		    => @$address['addressFullName'],
					'companyName'       => @$address['companyName'],
					'line1'   		    => @$address['addressLine1'],
					'line2' 		    => @$address['addressLine2'],
					'line3'      	    => @$address['addressLine3'],
					'line4'      	    => @$address['addressLine4'],
					'postalCode'        => @$address['postalCode'],
					'countryName'       => @$address['countryIsoCode'],
					'telephone'         => @$address['telephone'],
					'email'      	    => @$address['email'],
					'type'   	 	    => 'ST',
				);		
				$address = $OrderInfoList['parties']['billing'];
				$return[$account1Id][$orderId]['address'][]		= array(
					'account1Id'		=> $saveAccId1,
					'account2Id'        => $saveAccId2, 
					'orderId'           => $orderId,
					'fname' 		    => @$address['addressFullName'],
					'companyName'       => @$address['companyName'],
					'line1'   		    => @$address['addressLine1'],
					'line2' 		    => @$address['addressLine2'],
					'line3'      	    => @$address['addressLine3'],
					'line4'      	    => @$address['addressLine4'],
					'postalCode'        => @$address['postalCode'],
					'countryName'       => @$address['countryIsoCode'],
					'telephone'         => @$address['telephone'],
					'email'      	    => @$address['email'],
					'type'   	 	    => 'BY',
				);
				foreach ($OrderInfoList['orderRows'] as $rowId => $items) {
					$return[$account1Id][$orderId]['items'][]	= array(
						'account1Id'	    => $saveAccId1,
						'account2Id'        => $saveAccId2,
						'orderId'           => $orderId, 
						'rowId'    	        => $rowId, 
						'sku'  		        => @$items['productSku'],
						'name'  	        => $items['productName'],
						'productId'         => $items['productId'],
						'qty'               => $items['quantity']['magnitude'],
						'price'             => @($items['rowValue']['rowNet']['value'] / $items['quantity']['magnitude']),
						'discountedPrice'	=> '',
						'tax'   	        => @($items['rowValue']['rowTax']['value'] / $items['quantity']['magnitude'] ), 								
						'rowData'           => json_encode($items),
					);
				}
				$saveCronTime[] = strtotime($OrderInfoList['updatedOn']);
			}
		}
	}
	if($logs){
		foreach($logs as $orderId => $log){
			$path	= FCPATH.'logs'.DIRECTORY_SEPARATOR . $accountDetails['account1Id'].DIRECTORY_SEPARATOR . 'pc'.DIRECTORY_SEPARATOR;
			if(!is_dir($path)){
				mkdir($path,0777,true);
				chmod(dirname($path), 0777);
			}
			file_put_contents($path. $orderId.'.logs',"\n\n BP Log Added On : ".date('c')." \n". json_encode($log),FILE_APPEND);
		}
	}
	$returns[$account1Id]	= array( 'return' => $return,'saveTime' => @max($saveCronTime) );
}
$path	= FCPATH.'logs'.DIRECTORY_SEPARATOR.'request'. DIRECTORY_SEPARATOR . 'purchasecredit'.DIRECTORY_SEPARATOR . date('Y').DIRECTORY_SEPARATOR .date('m').DIRECTORY_SEPARATOR .date('d').DIRECTORY_SEPARATOR;
if(!is_dir($path)){
	mkdir($path,0777,true);
	chmod(dirname($path), 0777);
} 
$logs	= json_encode($this->response);
file_put_contents($path. date('Ymd-His-').uniqid().'.logs',$logs);
?>