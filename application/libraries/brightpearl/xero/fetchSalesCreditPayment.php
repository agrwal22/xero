<?php
$this->reInitialize();
$returns	= array(); 
foreach ($this->accountDetails as $account1Id => $accountDetails) {
	$datas		= $this->ci->db->order_by('id', 'desc')->get_where('cron_management', array('type' => 'salescreditpayment'.$account1Id))->row_array();
	$cronTime	= $datas['saveTime'];
	if(@!$cronTime){
		$cronTime	= strtotime('-10 days');
	}
	$return			= array();
	$updatedTimes	= array();
	$responseDatas	= array();
	$logs			= array();
	$this->config	= $this->accountConfig[$account1Id];
	$datetime		= new DateTime(date('c',$cronTime));
	$cronTime		= $datetime->format(DateTime::ATOM);
	$cronTime		= str_replace("+","%2B",$cronTime);
	$url			= '/accounting-service/customer-payment-search?paymentType=PAYMENT&createdOn='.$cronTime.'/';
	if($this->config['PaymentReversalEnabled'] == 1){
		$url	= '/accounting-service/customer-payment-search?createdOn='.$cronTime.'/';
	}
	$paymentResponses	= array();
	$response = $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
	if (@$response['results']){ 
		$paymentResponses[]	= $response;
		if ($response['metaData']['resultsAvailable'] > 500) {
			for ($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)) {
				$url1		= $url . '&firstResult=' . $i;
				$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
				if ($response1['results']) {
					$paymentResponses[]	= $response1;
				}

			}
		}
	}
	if($objectId){
		if(!is_array($objectId)){
			$objectId	= array($objectId);
		}
		$objectId	= array_filter($objectId);
		$objectId	= array_unique($objectId);
		sort($objectId);
		$objectIds	= array_chunk($objectId,200);
		foreach($objectIds as $objectId){
			if($objectIds){
				$url		= '/accounting-service/customer-payment-search?paymentType=PAYMENT&orderId='.implode(",",$objectId).'&sort=createdOn|DESC'; 
				if($this->config['PaymentReversalEnabled'] == 1){
					$url	= '/accounting-service/customer-payment-search?orderId='.implode(",",$objectId).'&sort=createdOn|DESC';
				}
				$response1	= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
				if (@$response1['results']){
					$paymentResponses[] = $response1;
				}
			}
		}		
	}
	if($paymentResponses){
		$pendingPayments		= array();
		$pendingPaymentsTemps	= $this->ci->db->select('orderId,createOrderId,isPaymentCreated,sendPaymentTo,paymentDetails')->get_where('sales_credit_order',array(/* 'isPaymentCreated' => '0', */'orderId <>' => '','account1Id' => $account1Id))->result_array();
		foreach($pendingPaymentsTemps as $pendingPaymentsTemp){
			$pendingPayments[$pendingPaymentsTemp['orderId']]	= $pendingPaymentsTemp;
		}
		$batchUpdates	= array();
		$orderIds		= array();		
		foreach($paymentResponses as $paymentDatas){
			if(@$paymentDatas['results']){
				$headerKeys	= array_column($paymentDatas['metaData']['columns'],'name');
				foreach($paymentDatas['results'] as $result){
					$result		    = array_combine($headerKeys,$result);
					$amount         = $result['amountPaid'];
					$orderId	    = $result['orderId'];
					$paymentId      = $result['journalId'];
					$paymentId2     = $result['paymentId'];
					$paymentDate	= $result['paymentDate'];
					$currencyCode   = $result['currencyCode'];
					$CurrencyRate   = $result['CurrencyRate'];
					$paymentMethod	= $result['paymentMethodCode'];
					$transactionRef	= $result['transactionRef'];
					$paymentType	= $result['paymentType'];
					if(strtolower($paymentType) == 'receipt'){
						$amount	= '-'.$amount;
					}
					if(!isset($pendingPayments[$orderId])){
						continue;
					}
					if(!isset($batchUpdates[$orderId]['paymentDetails'])){
						$paymentDetails	= @json_decode($pendingPayments[$orderId]['paymentDetails'],true);
					}
					else{
						$paymentDetails	= $batchUpdates[$orderId]['paymentDetails'];
					}
					$logs[$orderId][]	= $result;
					if(@$paymentDetails[$paymentId]){
						continue;
					}
					if(@$paymentDetails[$paymentId2]){
						continue;
					}
					$updatedTimes[]		= strtotime($result['createdOn']);
					@$paymentDetails[$paymentId2]	= array(
						'amount' 				=> $amount,
						'sendPaymentTo'			=> $this->ci->globalConfig['account2Liberary'],
						'status' 				=> '0',
						'Reference' 			=> $transactionRef,
						'currency' 				=> $currencyCode,
						'CurrencyRate' 			=> $CurrencyRate,
						'paymentMethod' 		=> $paymentMethod,
						'paymentDate' 			=> $paymentDate,
						'paymentType'			=> $paymentType,
						'paymentInitiatedIn'	=> 'brightpearl',
					);
					if($paymentId){
						@$paymentDetails[$paymentId]	= array(
							'amount' 				=> 0.00,
							'sendPaymentTo' 		=> $this->ci->globalConfig['account2Liberary'],
							'status' 				=> '1',
							'Reference' 			=> $transactionRef,
							'currency' 				=> $currencyCode,
							'CurrencyRate' 			=> $result['CurrencyRate'],
							'paymentMethod' 		=> $paymentMethod,
						);					
					}
					$batchUpdates[$orderId]			= array(
						'paymentDetails'		=> $paymentDetails,
						'orderId' 				=> $orderId,
						'sendPaymentTo' 		=> $this->ci->globalConfig['account2Liberary'],
					);
				}
			} 
		}
		if($batchUpdates){
			if($updatedTimes){
				$this->ci->db->insert('cron_management', array('type' => 'salescreditpayment'.$account1Id, 'saveTime' => max($updatedTimes), 'runTime' => date('c'))); 
			}
			foreach($batchUpdates as $key => $batchUpdate){
				$batchUpdates[$key]['paymentDetails'] = json_encode($batchUpdate['paymentDetails']);
			} 
			$batchUpdates = array_chunk($batchUpdates,200);
			foreach($batchUpdates as $batchUpdate){
				if($batchUpdate){
					$this->ci->db->update_batch('sales_credit_order',$batchUpdate,'orderId');
				}
			}			
		} 
	}
	if($logs){
		foreach($logs as $orderId => $log){
			$path	= FCPATH.'logs'.DIRECTORY_SEPARATOR . $accountDetails['account1Id'].DIRECTORY_SEPARATOR . 'sc'.DIRECTORY_SEPARATOR;
			if(!is_dir($path)){
				mkdir($path,0777,true);
				chmod(dirname($path), 0777);
			}
			file_put_contents($path. $orderId.'.logs',"\n\n BP Log Added On : ".date('c')." \n". json_encode($log),FILE_APPEND);
		}
	}
}
$path	= FCPATH.'logs'.DIRECTORY_SEPARATOR.'request'. DIRECTORY_SEPARATOR . 'salecreditspayment'.DIRECTORY_SEPARATOR . date('Y').DIRECTORY_SEPARATOR .date('m').DIRECTORY_SEPARATOR .date('d').DIRECTORY_SEPARATOR;
if(!is_dir($path)){
	mkdir($path,0777,true);
	chmod(dirname($path), 0777);
}
$logs	= json_encode($this->response);
file_put_contents($path. date('Ymd-His-').uniqid().'.logs',$logs);
?>