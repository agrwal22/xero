<?php
$this->reInitialize();
$enableAvalaraTax			= $this->ci->globalConfig['enableAvalaraTax'];
$enableGiftCard				= $this->ci->globalConfig['enableGiftCard'];
$enableAmazonFees			= $this->ci->globalConfig['enableAmazonFees'];
$enableInventoryTransfer	= $this->ci->globalConfig['enableInventoryTransfer'];
$enableInventoryManagement	= $this->ci->globalConfig['enableInventoryManagement'];
$returns					= array();

foreach ($this->accountDetails as $account1Id => $accountDetails) {
	$datas    = $this->ci->db->order_by('id', 'desc')->get_where('cron_management', array('type' => 'salespayment'.$account1Id))->row_array();
	$cronTime = $datas['saveTime'];	
	$cronTime = strtotime('-90 days');
	if(@!$cronTime){$cronTime = strtotime('-90 days');}
	$return		      = array();
	$updatedTimes     = array();
	$responseDatas    = array();
	$logs		      = array();
	$orderId	      = array();
	$this->config     = $this->accountConfig[$account1Id];
	$datetime	      = new DateTime(date('c',$cronTime));
	$cronTime	      = $datetime->format(DateTime::ATOM);
	$cronTime		  = str_replace("+","%2B",$cronTime);
	$url			  = '/accounting-service/customer-payment-search?paymentType=RECEIPT&createdOn='.$cronTime.'/';
	if($this->config['PaymentReversalEnabled'] == 1){
		$url	= '/accounting-service/customer-payment-search?createdOn='.$cronTime.'/';
	}
	$paymentResponses	= array();
	$response			= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
	if (@$response['results']){
		$paymentResponses[]	= $response;
		if ($response['metaData']['resultsAvailable'] > 500) {
			for ($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)) {
				$url1		= $url . '&firstResult=' . $i;
				$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
				if ($response1['results']) {
					$paymentResponses[] = $response1;
				}

			}
		}
	}
	if($objectId){
		if(!is_array($objectId)){
			$objectId	= array($objectId);
		}
		$objectId	= array_filter($objectId);
		$objectId	= array_unique($objectId);
		sort($objectId);
		$objectIds = array_chunk($objectId,50);
		foreach($objectIds as $objectId){
			if($objectIds){
				$url	= '/accounting-service/customer-payment-search?paymentType=RECEIPT&orderId='.implode(",",$objectId).'&sort=createdOn|DESC';
				if($this->config['PaymentReversalEnabled'] == 1){
					$url	= '/accounting-service/customer-payment-search?orderId='.implode(",",$objectId).'&sort=createdOn|DESC';
				}
				$response1	= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
				if (@$response1['results']){
					$paymentResponses[]	= $response1;
				}
			}
		}
	} 
	if($paymentResponses){
		$pendingPayments		= array();
		$completedpayments		= array();
		$pendingPaymentsTemps	= $this->ci->db->select('orderId,createOrderId,isPaymentCreated,sendPaymentTo,paymentDetails')->get_where('sales_order',array('isPaymentCreated' => '0','orderId <>' => '','account1Id' => $account1Id))->result_array();

		$completedpaymentdatas	= $this->ci->db->select('orderId,createOrderId,isPaymentCreated,sendPaymentTo,paymentDetails')->get_where('sales_order',array('isPaymentCreated' => '1','orderId <>' => '','account1Id' => $account1Id))->result_array();
		
		foreach($completedpaymentdatas as $completedpaymentdata){
			$completedpayments[$completedpaymentdata['orderId']]	= $completedpaymentdata;
		}
		
		foreach($pendingPaymentsTemps as $pendingPaymentsTemp){
			$pendingPayments[$pendingPaymentsTemp['orderId']]		= $pendingPaymentsTemp;
		}		
		$batchUpdates	= array();
		$orderIds		= array();		
		foreach($paymentResponses as $paymentDatas){
			if(@$paymentDatas['results']){
				$headerKeys	= array_column($paymentDatas['metaData']['columns'],'name');
				foreach($paymentDatas['results'] as $result){
					$result			= array_combine($headerKeys,$result);
					$orderId		= $result['orderId'];
					$paymentId		= $result['journalId'];
					$paymentId2		= $result['paymentId'];
					$amount			= $result['amountPaid'];
					$paymentMethod	= $result['paymentMethodCode'];
					$paymentType	= $result['paymentType'];
					$paymentDate	= $result['paymentDate'];
					
					if(strtolower($paymentType) == 'payment'){
						$amount	= '-'.$amount;
					}
					$transactionRef	= $result['transactionRef'];
					$currencyCode	= $result['currencyCode'];
					if(isset($completedpayments[$orderId])){
						$this->ci->db->update('sales_order',array('isPaymentCreated' => '0','status' => '1'),array('orderId' => $orderId));
					}	
					if(!isset($pendingPayments[$orderId])){
						if(!isset($completedpayments[$orderId])){
							continue;
						}
					}	
					$logs[$orderId][]	= $result;
					$paymentDetails		= array();
					if(isset($batchUpdates[$orderId]['paymentDetails'])){
						$paymentDetails	= $batchUpdates[$orderId]['paymentDetails'];
					}
					else if(isset($completedpayments[$orderId]['paymentDetails'])){
						$paymentDetails	= json_decode($completedpayments[$orderId]['paymentDetails'],true);
					}
					else if(isset($pendingPayments[$orderId]['paymentDetails'])){
						$paymentDetails	= json_decode($pendingPayments[$orderId]['paymentDetails'],true);
					}
					if($paymentId2){
						if(isset($paymentDetails[$paymentId2])){
							if($amount != 0){
								if($paymentDetails[$paymentId2]['amount'] != 0){continue;}
							}
							else{
								continue;
							}
						}
					}
					if($paymentId){
						if(isset($paymentDetails[$paymentId])){
							if(isset($paymentDetails[$paymentId2])){
								continue;
							}
						}
					}
					$updatedTimes[]	= strtotime($result['createdOn']);					
					@$paymentDetails[$paymentId2]	= array(
						'amount' 				=> $amount,
						'sendPaymentTo'			=> $this->ci->globalConfig['account2Liberary'],
						'status' 				=> '0',
						'journalId' 			=> $result['journalId'],
						'Reference' 			=> $transactionRef,
						'currency' 				=> $currencyCode,
						'CurrencyRate' 			=> $Payments['CurrencyRate'],
						'paymentMethod' 		=> $paymentMethod,
						'paymentType'			=> $paymentType,
						'paymentDate'			=> $paymentDate,
						'paymentInitiatedin'	=> 'brightpearl',
					);
					@$paymentDetails[$paymentId]	= array(
						'amount' 				=> 0.00,
						'sendPaymentTo' 		=> $this->ci->globalConfig['account2Liberary'],
						'status' 				=> '1',
						'journalId' 			=> $result['journalId'],
						'Reference' 			=> $transactionRef,
						'currency' 				=> $currencyCode,
						'CurrencyRate' 			=> $Payments['CurrencyRate'],
						'paymentMethod' 		=> $paymentMethod,
					);					
					$batchUpdates[$orderId]			= array(
						'paymentDetails' 		=> $paymentDetails,
						'orderId' 				=> $orderId,
						'sendPaymentTo' 		=> $this->ci->globalConfig['account2Liberary'],
					);
				}
			}
		}
		if($batchUpdates){
			if($updatedTimes){
				$this->ci->db->insert('cron_management', array('type' => 'salespayment'.$account1Id, 'saveTime' => max($updatedTimes), 'runTime' => date('c'))); 
			}
			foreach($batchUpdates as $key => $batchUpdate){
				$batchUpdates[$key]['paymentDetails'] = json_encode($batchUpdate['paymentDetails']);
			} 
			$batchUpdates = array_chunk($batchUpdates,200);
			foreach($batchUpdates as $batchUpdate){
				if($batchUpdate){
					$this->ci->db->update_batch('sales_order',$batchUpdate,'orderId');
				}
			}
		}
	}
	if($logs){
		foreach($logs as $orderId => $log){
			$path	= FCPATH.'logs'.DIRECTORY_SEPARATOR . $accountDetails['account1Id'].DIRECTORY_SEPARATOR . 'so'.DIRECTORY_SEPARATOR;
			if(!is_dir($path)){ 
				mkdir($path,0777,true);
				chmod(dirname($path), 0777);
			}
			file_put_contents($path. $orderId.'.logs',"\n\n BP Log Added On : ".date('c')." \n". json_encode($log),FILE_APPEND);
		}
	}
}
$path	= FCPATH.'logs'.DIRECTORY_SEPARATOR.'request'. DIRECTORY_SEPARATOR . 'salespayment'.DIRECTORY_SEPARATOR . date('Y').DIRECTORY_SEPARATOR .date('m').DIRECTORY_SEPARATOR .date('d').DIRECTORY_SEPARATOR;
if(!is_dir($path)){
	mkdir($path,0777,true);
	chmod(dirname($path), 0777);
}
$logs	= json_encode($this->response);
file_put_contents($path. date('Ymd-His-').uniqid().'.logs',$logs);
?>