<?php
$this->reInitialize();
$return	= array();
foreach ($this->accountDetails as $account1Id => $accountDetails){	
	$this->config	= $this->accountConfig[$account1Id];	
	$orderDatas		= $this->ci->db->get_where('sales_credit_order',array(/* 'isPaymentCreated' => '0', */'status <=' => '4','sendPaymentTo' => 'brightpearl','account1Id' => $account1Id))->result_array();
	$exchangeDatas	= $this->getExchangeRate($account1Id)[$account1Id];
	foreach($orderDatas as $orderData){
		$orderId			= $orderData['orderId'];
		$fileLogs			= array();
		$orderRowData		= json_decode($orderData['rowData'],true);
		$createdRowData 	= json_decode($orderData['createdRowData'],true);
		$paymentDetails		= json_decode($orderData['paymentDetails'],true);
		$payurl				= '/accounting-service/customer-payment';
		$timezone			= $this->config['timezone'];
		
		
		/* 	PAYMENT REVERSAL STARTS */
		if($this->config['PaymentReversalEnabled']){
			foreach($paymentDetails as $Mykey => $paymentDetail){
				if((@$paymentDetail['sendPaymentTo'] == 'brightpearl') && (@$paymentDetail['IsReversal'] == '1') && (@$paymentDetail['DeletedonBrightpearl'] == 'NO')){
					if(($paymentDetail['status'] == 0)){
						if(@$paymentDetail['brightpearlPayID']){
							$updateArray	= array();
							$paymentKEY		= $paymentDetail['brightpearlPayID'];
							$reversepayurl	= "/accounting-service/customer-payment";
							$reverseRequest	= array(
								"paymentMethodCode" 	=> $paymentDetails[$paymentKEY]['paymentMethod'],
								"paymentType"       	=> "RECEIPT",
								"orderId"           	=> $orderData['orderId'],
								"currencyIsoCode"   	=> strtoupper($paymentDetails[$paymentKEY]['currency']),
								"exchangeRate"      	=> $paymentDetails[$paymentKEY]['exchangeRate'],
								"amountPaid"        	=> $paymentDetails[$Mykey]['amount'],
								"paymentDate"       	=> $paymentDetails[$Mykey]['paymentDate'],
							);
							$ReverseResponse	= $this->getCurl($reversepayurl, "POST", json_encode($reverseRequest), 'json' , $account1Id )[$account1Id];
							if(!$ReverseResponse['errors']){
								$paymentDetails[$Mykey]['status']				= 1;
								$paymentDetails[$Mykey]['DeletedonBrightpearl']	= "YES"; 
							}
							$paymentDetails[$ReverseResponse]	= array(
								'amount' 				=> 0.00,
								'sendPaymentTo'			=> '',
								'status' 				=> '1',
								'AmountReversedIn'		=> 'brightpearl',
								'ParentXeroReverseId'	=> $Mykey,
							);
							$updateArray['isPaymentCreated']		= 0;
							$updateArray['status']					= 1;
							$updateArray['sendPaymentTo']			= '';
							$updateArray['paymentDetails']			= json_encode($paymentDetails);
							$this->ci->db->where(array('orderId' => $orderData['orderId'],'account1Id' => $account1Id))->update('sales_credit_order',$updateArray); 
						}					
					}
				}
			}
			$updatedPaymentdata	= $this->ci->db->select('paymentDetails')->get_where('sales_credit_order',array('orderId' => $orderData['orderId']))->row_array();
			$paymentDetails		= json_decode($updatedPaymentdata['paymentDetails'],true); 
		}
		/*	PAYMENT REVERSAL ENDS */
		
		
		foreach($paymentDetails as $keys => $paymentDetail){
			$amount			= 0;
			$reference		= '';
			$currencyCode	= $orderRowData['currency']['accountingCurrencyCode'];
			$paymentMethod	= $this->config['defaultPaymentMethod'];
			$totalReceivedPaidAmount	= array_sum(array_column($paymentDetails,'amount'));
			if(($paymentDetail['sendPaymentTo'] == 'brightpearl')&&($paymentDetail['status'] == '0') && ($paymentDetail['IsReversal'] == '0')){
				$orderPaymentMethod	 = '';
				$exchangeRate		 = 1;
				$amount				+= $paymentDetail['amount'];
				if(@$paymentDetail['Reference']){				
					$reference			= $paymentDetail['Reference'];
				}
				if(@$paymentDetail['paymentMethod']){				
					$orderPaymentMethod	= $paymentDetail['paymentMethod'];
				}
				if(@$paymentDetail['CurrencyRate']){				
					$exchangeRate		= $paymentDetail['CurrencyRate'];
				}
				if(@$paymentDetail['currency']){			
					$currencyCode		= $paymentDetail['currency'];
				}		
				if($orderPaymentMethod){
					$paymentMathodMappings	= $this->ci->db->get_where('mapping_payment',array('account2Id' => $orderData['account2Id'],'account2PaymentId' => $orderPaymentMethod,'account1Id' => $account1Id))->row_array();
					if($orderPaymentMethod){
						$paymentMethod	= $paymentMathodMappings['account1PaymentId'];
					}
				}
				if(@$paymentDetail['paymentDate']){
					$paymentDate	= $paymentDetail['paymentDate'];
					$dDate			= date('Y-m-d',strtotime($paymentDate));
					$date			= new DateTime($dDate); 
					$date->setTimezone(new DateTimeZone($timezone));
					$paymentDate	= $date->format('Y-m-d');
				}
				else{
					$paymentDate = date('c');
				}
				if($amount > 0 ){
					$customerPaymentRequest		= array(
						"paymentMethodCode"			=> $paymentMethod,
						"paymentType"       	    => "PAYMENT",
						"orderId"           	    => $orderData['orderId'],
						"currencyIsoCode"   	    => strtoupper($currencyCode),
						"exchangeRate"      	    => $exchangeRate,
						"amountPaid"        	    => $amount,
						"paymentDate"       	    => @$paymentDate,
						"journalRef"        	    => ($reference)?($reference):"Purchase receipt for order : " . $orderData['orderId'],
					);
					$customerPaymentRequestRes = $this->getCurl($payurl,"POST",json_encode($customerPaymentRequest),'json',$account1Id)[$account1Id];
					$fileLogs[]	= array(
						'Request Data '.date('c').$customerPaymentRequest,
						'Response Data '.date('c').$customerPaymentRequestRes,
					);
					$createdRowData['send payment to brightpearl request date : '.date('Y-m-d\TH-i-s')] 	= $customerPaymentRequest;
					$createdRowData['send payment to brightpearl response date : '.date('Y-m-d\TH-i-s')]	= $customerPaymentRequestRes;
					$this->ci->db->where(array('orderId' => $orderData['orderId'],'account1Id' => $account1Id))->update('sales_credit_order',array('createdRowData' => json_encode($createdRowData)));
					if(!isset($customerPaymentRequestRes['errors'])){
						$paymentDetails[$keys]['status']			= '1';
						$paymentDetails[$keys]['brightpearlPayID']	= $customerPaymentRequestRes;
						$paymentDetails[$customerPaymentRequestRes]	= array(
							'amount' 			=> $amount,
							'sendPaymentTo'		=> 'brightpearl',
							'status' 			=> '1',
							'amountCreditedIn'	=> 'brightpearl',
							'parentPaymentId'	=> $keys,
							'paymentMethod'		=> $paymentMethod,
							'currency'			=> strtoupper($currencyCode),
							"exchangeRate"      => $exchangeRate,
						);
						if($totalReceivedPaidAmount >= $orderRowData['totalValue']['total']){
							$updateArray	= array(
								'isPaymentCreated'	=> '1',
								'status' 			=> '3',
							);
						}
						$updateArray['paymentDetails']	= json_encode($paymentDetails);
						$this->ci->db->where(array('orderId' => $orderData['orderId'],'account1Id' => $account1Id))->update('sales_credit_order',$updateArray); 
					} 
				}
			}
		}
		if($fileLogs){
			if($orderId){
				$path	= FCPATH.'logs'.DIRECTORY_SEPARATOR . $accountDetails['id'].DIRECTORY_SEPARATOR . 'sc'.DIRECTORY_SEPARATOR;
				if(!is_dir($path)){
					mkdir($path,0777,true);
					chmod(dirname($path), 0777);
				}
				file_put_contents($path. $orderId.'.logs',"\n BP Payment Log Added Log On : ".date('c')." \n". json_encode($fileLogs),FILE_APPEND);
			}
		}
	}
}
?>