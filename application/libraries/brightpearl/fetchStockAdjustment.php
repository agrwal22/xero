<?php 
if(!$cronTime){$cronTime = date('Y-m-d\TH:i:s\/',strtotime('-10 days'));}
$this->reInitialize($accountId);
$return = array();
foreach ($this->accountDetails as $account1Id => $accountDetails) {
	$orderIds = array();
	$this->config = $this->accountConfig[$account1Id];			
	$account2Ids     = $this->account2Details[$account1Id]; 
	$warehouseSales = $this->config['warehouseSales'];
	if($warehouseSales){ 
		$warehouseSales  = explode(",",$warehouseSales);
	}
	else{
		$warehouseSales = explode(",",$this->config['warehouse']);
	}
	$saveCronTime = array();
	foreach($warehouseSales as $warehouseSale){	
		$url = '/warehouse-service/goods-movement-search?warehouseId='.$warehouseSale.'&sort=goodsNoteId|DESC&updatedOn='.$cronTime;
		$response = $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];  
		if($response['results']){
			foreach ($account2Ids as $account2Id) {
				$saveAccId1     = ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($account1Id) : $account2Id['id'];
				$saveAccId2     = ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($account2Id['id']) : $account1Id;
				$headers = array_column($response['metaData']['columns'],'name');
				foreach($response['results'] as $results){
					$row = array_combine($headers,$results);
					if($row['salesOrderRowId']){continue;}
					if($row['purchaseOrderRowId']){continue;}
					$return[$account1Id][$row['goodsMovementId']][$row['productId']] = array(
						'account1Id'    => $saveAccId1,
						'account2Id'    => $saveAccId2,
						'orderId' 		=> $row['goodsMovementId'],
						'warehouseId' 	=> $row['warehouseId'],
						'productId' 	=> $row['productId'],
						'qty' 			=> $row['quantity'],
						'price' 		=> $row['productValue'],
						'created' 		=> $row['updatedOn'],
						'rowData' 		=> json_encode($row),
					);
					$saveCronTime[] = strtotime($results['15']);
				}
			}
		}
	}	
}
?>