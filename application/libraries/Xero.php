<?php 
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
if (!class_exists('Net_SFTP')) {
    @include_once FCPATH . 'application' . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'ftp' . DIRECTORY_SEPARATOR . 'Sftp.php';
}
class Xero  {
    public $appurl, $headers, $accountDetails, $accountConfig,$account2Config, $account2Details, $account1id, $account2Id,$response,$nonce,$oauth_timestamp,$countRequest;
    public function __construct(){
        $this->ci		= &get_instance();
		$this->appurl	= 'https://api.xero.com/api.xro/';
    }
	public function reInitialize($account1Id = ''){
		$this->accountDetails 	= $this->ci->account2Account;
		$this->accountConfig	= array();
		foreach($this->ci->account2Config as $account2Config){
			$this->accountConfig[$account2Config[$this->ci->globalConfig['account2Liberary'].'AccountId']]	= $account2Config;
		}
	}
	public function initializeConfig($accountId, $method = 'GET', $suburl = '',$params = array()){
		if ($accountId) {
            $accountDetails			= @$this->accountDetails[$accountId];
			if(!$accountDetails['OAuthVersion']){
				$accountDetails['OAuthVersion'] = 1;
			}
			if($accountDetails['OAuthVersion'] == 1){
				$this->nonce			= $this->getNonce();
				$this->oauth_timestamp	= time();
				$oauth_params			= array(
					'oauth_consumer_key' 		=>  $accountDetails['username'],
					'oauth_signature_method'	=> 'RSA-SHA1',
					'oauth_timestamp' 			=> $this->oauth_timestamp,
					'oauth_nonce' 				=> $this->nonce,
					'oauth_callback' 			=> 'https://localhost/',
					'oauth_version' 			=> '1.0',
					'oauth_token' 				=> $accountDetails['username'],
				);
				$sbs_params = array_merge($params, $oauth_params);
				ksort($sbs_params);		
				$string		=  sprintf(
					'%s&%s&%s',
					$method,
					rawurlencode($this->appurl . $suburl),
					rawurlencode($this->flattenAssocArray($sbs_params, '%s=%s', '&', true))
				);
				$this->signature	= $this->generateSignature($string);
				$oauth_params['oauth_signature'] = rawurlencode($this->signature);
				$header				= 'OAuth ' . $this->flattenAssocArray($oauth_params, '%s="%s"', ', ');
				$this->headers		= array(
					'Content-Type'		=> 'application/json', 
					'Authorization'		=> $header,
				);
			}
		}
    }
	public function getCurl($suburl, $method = 'GET', $field = '', $type = 'json', $account2Id = '',$oauthProcess = 0 , $retry_count = 0 ){
		$milliseconds	= round(microtime(true) * 1000);
		$calMin			= sprintf("%.5f",($milliseconds / 60000));
		$nextMilisec	= ((int)$calMin + 1) - $calMin;
		$nextMilisec	= (int)((int) ($nextMilisec * 100)) / 1.666;
		$remainingSec	= $nextMilisec + 3;
		$limitName		= (int)($calMin); 
		$insertData		= array(
			'name'			=> $limitName,
			'limitcount'	=> 0,
		);
		$sql		= $this->ci->db->insert_string('api_call', $insertData) . ' ON DUPLICATE KEY UPDATE limitcount=limitcount + 1';
		$this->ci->db->query($sql);
		$limitRate	= $this->ci->db->get_where('api_call',array('name' => $limitName))->row_array();
		if($limitRate['limitcount'] >= 55){
			sleep($remainingSec);
		}
		else if($limitRate['limitUsed']){
			sleep($remainingSec);
		}
		$this->response	= array();
		$returnData     = array();
        $return			= array();
        if (@$account2Id) {
            foreach ($this->accountDetails as $t1) {
                if ($t1['id'] == $account2Id){
                    $accountDetails = array($t1);
				}
			}
		} 
		else{
            $accountDetails = $this->accountDetails;
        }
        foreach ($accountDetails as $acId => $accountDetail) {
			if(!$accountDetail['OAuthVersion']){
				$accountDetail['OAuthVersion'] = 1;
			}
			if($accountDetail['OAuthVersion'] == 1){
				$url	= $this->appurl . ltrim($suburl, "/");
				if (is_array($field)) {
					$postvars	= http_build_query($field);
				} 
				else {
					$postvars	= $field;
				}
				$ch	= curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_AUTOREFERER, true);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper($method));
				if ($postvars) {
					curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
				}
				if ($this->headers) {
					$header_array	= $this->flattenAssocArray($this->headers, '%s: %s');
					curl_setopt($ch, CURLOPT_HTTPHEADER, $header_array);
				}
				$response	= curl_exec($ch);
				$curlinfo	= curl_getinfo($ch);
				$api_call_log	= array(
					'ApiEndPoint'	=> $url,
					'ApiResponse'	=> $response,
					'CurlInfo'		=> json_encode($curlinfo,true),
					'ApiRequest'	=> $field,
				);
				if(is_array($field)){
					$api_call_log['ApiRequest']	= json_encode($field);
				}
				$this->ci->db->insert('api_call_log', $api_call_log);
				if(substr_count($response,'oauth_problem=rate%20limit%20exceeded') AND $retry_count <= 3){
					$insertData	= array(
						'name'		=> $limitName,
						'limitUsed'	=> 1,
					);
					$this->ci->db->where(array('name' => $limitName))->update('api_call',$insertData);
					$remainingSec	= $remainingSec + 5;
					$retry_count++;
					sleep($remainingSec);
					return $this->getCurl($suburl,$method,$field,$type,$account2Id,$oauthProcess,$retry_count);
				}
				if(substr_count($curlinfo['content_type'],"text/xml;")){
					$response	= json_encode(simplexml_load_string($response));
				}		
				$returnData[$accountDetail['id']]		= json_decode($response, true);
				$this->response[$accountDetail['id']]	= $response;
			}
			elseif($accountDetail['OAuthVersion'] == 2){
				if((!$oauthProcess) && ( strtotime("now") > ($accountDetail['tokenFetchTime'] + $accountDetail['expiresIn'] ))){
					$this->refreshToken($accountDetail['id']);
					$accountDetail = $this->accountDetails[$accountDetail['id']];
				}
				if (is_array($field)) {
					$postvars = http_build_query($field);
				}
				else {
					
					$postvars = $field; 
				} 
				if(!$oauthProcess){
					$url = $this->appurl . ltrim($suburl, "/");
				}
				else{
					$url = $suburl;
				}
				$ch		= curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper($method));
				if ($postvars) {
					curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
				}
				$xeroOrganizationID	= $accountDetail['companyId'];
				if($type=='json'){
					$authorizationheader	= 'Bearer ' . $accountDetail['accessToken'];
					$header					= array('Content-Type: application/json','Authorization: ' . $authorizationheader,"Xero-tenant-id: $xeroOrganizationID");			
				}
				else{ 
					$header	= array('Content-Type: application/x-www-form-urlencoded');	
				}
				if($oauthProcess){
					$header	= $this->headers;
				}	
				curl_setopt($ch, CURLOPT_HTTPHEADER,     $header);
				$response	= curl_exec($ch);
				$curlinfo	= curl_getinfo($ch);
				if($curlinfo['http_code'] == 429){
					$response = 'api_limit_crossed';
				}
				$api_call_log	= array(
					'ApiEndPoint'	=> $url,
					'ApiResponse'	=> $response,
					'CurlInfo'		=> json_encode($curlinfo,true),
					'ApiRequest'	=> $field,
				);
				if(is_array($field)){
					$api_call_log['ApiRequest']	= json_encode($field);
				}
				$this->ci->db->insert('api_call_log', $api_call_log);
				$checkerror	= json_decode($response,true);
				if((substr_count($checkerror['Detail'],'TokenExpired')) OR (substr_count($checkerror['Detail'],'TokenInvalidSignature'))){
					$this->sendTokenalert($accountDetail['name'], $checkerror);
				}
				if(($curlinfo['http_code'] == 429) AND $retry_count <= 3){
					$insertData	= array(
						'name'		=> $limitName,
						'limitUsed'	=> 1,
					);
					$this->ci->db->where(array('name' => $limitName))->update('api_call',$insertData);
					$remainingSec	= $remainingSec + 5;
					$retry_count++;
					sleep($remainingSec);
					return $this->getCurl($suburl,$method,$field,$type,$account2Id,$oauthProcess,$retry_count);
				}
				if(substr_count($curlinfo['content_type'],"text/xml;")){
					$response	= json_encode(simplexml_load_string($response));
				}		
				$returnData[$accountDetail['id']]		= json_decode($response, true);
				$this->response[$accountDetail['id']]	= $response;		
			}
        }
		return $returnData;
    }
	public function refreshToken($orgAccountId = ''){
		$this->reInitialize($orgAccountId);
		$code	= trim($this->ci->input->get('code'));
		if($code){
			$accountDetails	= $this->ci->session->userdata('accountDetails');
			if(!$accountDetails){
				return false;
			}
			$data	= array(
				'code' 			=> $code,
				'grant_type'	=> 'authorization_code',
				'client_id' 	=> $accountDetails['username'],
				'client_secret' => $accountDetails['password'],
				'redirect_uri'	=> rawurldecode($accountDetails['redirectUrl']),
		
			);	
			$encodedClientIDClientSecrets = base64_encode($accountDetails['username'] . ':' . $accountDetails['password']);
			$authorizationheader	= 'Basic ' . $encodedClientIDClientSecrets;
		
			$this->headers	= array(
				'Accept'		=> 	'application/json',
				'Authorization'	=> $authorizationheader,
				'Content-Type'	=> 'application/x-www-form-urlencoded'
			);
			$authenticationURL	= 'https://identity.xero.com/connect/token';
			if(@!$this->accountDetails){
				$this->reInitialize($accountDetails['id']);
			}
			$result = $this->getCurl($authenticationURL,'POST',$data,'', $accountDetails['id'],'1')[$accountDetails['id']];
			if(@$result['access_token']){
				$saveData = array(
					"id"			=> $accountDetails['id'],
					"accessToken" 	=> $result['access_token'],
					"tokenType" 	=> $result['token_type'],
					"expires" 		=> $result['x_refresh_token_expires_in'],
					"expiresIn" 	=> $result['expires_in'],
					"refreshToken"  => $result['refresh_token'],
				);
				$this->ci->db->where(array('id' => $accountDetails['id']))->update('account_xero_account',$saveData);
				$this->reInitialize($orgAccountId);
			}  
		}
		else{
			if(@!$this->accountDetails){
				$this->reInitialize($orgAccountId);
			}
			foreach($this->accountDetails as $accountId => $accountDetails){
				if($accountId == $orgAccountId){
					$accountDetails	= $this->ci->db->get_where('account_xero_account',array('id' => $accountId ))->row_array();
					if(@!$accountDetails['accessToken']){
						$parameters	= array(
							'response_type'	=> 'code',
							'client_id'		=> $accountDetails['username'],
							'redirect_uri'	=> rawurldecode($accountDetails['redirectUrl']),
							'scope'			=> 'openid email profile offline_access accounting.settings accounting.transactions accounting.contacts accounting.journals.read accounting.reports.read accounting.attachments',
						);
						$authorizationRequestUrl	= 'https://login.xero.com/identity/connect/authorize?' . http_build_query($parameters, null, '&', PHP_QUERY_RFC1738);
						$tst	= $this->ci->session->set_userdata('accountDetails', $accountDetails);
						header('Location: '.$authorizationRequestUrl);
						
					}
					else{
						$data	= array(
							'client_id' 	=> $accountDetails['username'],
							'client_secret'	=> $accountDetails['password'],
							'redirect_uri'	=> $accountDetails['redirectUrl'],
							'grant_type'	=> 'refresh_token',
							'refresh_token'	=> $accountDetails['refreshToken'],
						);
						$encodedClientIDClientSecrets	= base64_encode($accountDetails['username'] . ':' . $accountDetails['password']);
						$authorizationheader			= 'Basic ' . $encodedClientIDClientSecrets;
						$this->headers	= array(
							'Accept' 		=> 'application/json',
							'Authorization'	=> $authorizationheader,
							'Content-Type' 	=> 'application/x-www-form-urlencoded'
						);
						$url			= 'https://identity.xero.com/connect/token';
						$tokenFetchTime	= strtotime("now") - 60;
						$result			= $this->getCurl($url,'POST',$data,'application/x-www-form-urlencoded',$accountId,'1')[$accountId];
						if($result['access_token']){
							$saveData	= array(
								"accessToken" 		=> $result['access_token'],
								"tokenType" 		=> $result['token_type'],
								"expires" 			=> $result['x_refresh_token_expires_in'],
								"refreshToken" 		=> $result['refresh_token'],
								"expiresIn" 		=> $result['expires_in'],
								"tokenFetchTime"	=> $tokenFetchTime, 
							);
							$this->ci->db->where(array('id' => $accountDetails['id']))->update('account_xero_account',$saveData);
							$this->accountDetails[$accountId]['accessToken'] 	= $result['access_token'];
							$this->accountDetails[$accountId]['refreshToken'] 	= $result['refresh_token']; 
							$this->accountDetails[$accountId]['tokenType'] 		= $result['token_type'];
							$this->accountDetails[$accountId]['expires'] 		= $result['x_refresh_token_expires_in'];
							$this->accountDetails[$accountId]['expiresIn'] 		= $result['expires_in'];
							$this->accountDetails[$accountId]['tokenFetchTime']	= $tokenFetchTime; 
						}
					}
				}
			}	
		}		
	}
	public function flattenAssocArray(array $array,$format,$glue = null,$escape = false) {
        $pairs	= [];
        foreach ($array as $key => $val) {
            if ($escape) {
                $key	= rawurlencode($key);
                $val	= rawurlencode($val);
            }
            $pairs[]	= sprintf($format, $key, $val);
        }
        if ($glue === null){
            return	$pairs;
        } 
		else{
            return implode($glue, $pairs);
        }
    }
	public function getNonce($length = 20){
        $parts	= explode('.', number_format(microtime(true), 22, '.', ''));
        if (!isset($parts[1])) {
            $parts[1]	= 0;
        }
        $nonce	= base_convert($parts[1], 10, 36);

        for ($i = 0; $i < $length - strlen($nonce); $i++) {
            $nonce	.= base_convert(mt_rand(0, 35), 10, 36);
        }
        return $nonce;
    }
	public function generateSignature($sbs){
		$rsa_private_key = $this->ci->config->item('rsa_private_key');
		$private_key_id = openssl_pkey_get_private($rsa_private_key);		
		openssl_sign($sbs, $signature, $private_key_id);
        openssl_free_key($private_key_id);
        return base64_encode($signature);
    }
	
	public function getConnections($id = ''){
		$this->reInitialize();
		foreach($this->accountDetails as $account1Id => $accountDetails){
			$config	= $this->accountConfig[$account2Id];
			if($id){
				$suburl = 'https://api.xero.com/api.xro/2.0/connection'.$id; 
			}
			else{
				$suburl	= 'https://api.xero.com/connections';
			}
			$results	= $this->getCurl($suburl, 'get', '', 'json', $account1Id); 
		}
	}
	public function getProducts($id = ''){
		$this->reInitialize();
		foreach($this->accountDetails as $account1Id => $accountDetails){
			$config = $this->accountConfig[$account2Id];
			if($id){
				$suburl = '2.0/Items/'.$id; 
			}
			else{
				$suburl	= '2.0/Items/0f2591c0-8a9b-46b7-97ac-9cc5dcf7353f';
			}
			$this->initializeConfig($account1Id, 'GET', $suburl);
			$results	= $this->getCurl($suburl, 'get', '', 'json', $account1Id); 
		}
	}
	public function postProducts($orgObjectId = ''){
		$this->callFunction('postProducts',$orgObjectId);
    }
	public function postCustomers($orgObjectId = ''){
		$this->callFunction('postCustomers',$orgObjectId);
    }
	public function postSales($orgObjectId = ''){
		$this->callFunction('postSales',$orgObjectId);
    }
	public function postSalesCredit($orgObjectId = ''){
		$this->callFunction('postSalesCredit',$orgObjectId);
    }
	public function postPurchase($orgObjectId = ''){
		$this->callFunction('postPurchase',$orgObjectId);
    }
	public function postPurchaseCredit($orgObjectId = ''){
		$this->callFunction('postPurchaseCredit',$orgObjectId);
    }
	public function postStockAdjustment($orgObjectId = ''){
		$this->callFunction('postStockAdjustment',$orgObjectId);
    }
	public function fetchSalesPayment($orgObjectId = ''){	
		$this->callFunction('fetchSalesPayment',$orgObjectId);
	}
	public function postSalesPayment($orgObjectId = ''){
		$this->callFunction('postSalesPayment',$orgObjectId);
	}
	public function fetchSalesCreditPayment($orgObjectId = ''){
		$this->callFunction('fetchSalesCreditPayment',$orgObjectId);
	}
	public function postSalesCreditPayment($orgObjectId = ''){
		$this->callFunction('postSalesCreditPayment',$orgObjectId);
	}
	public function fetchPurchasePayment($orgObjectId = ''){
		$this->callFunction('fetchPurchasePayment',$orgObjectId);
	}
	public function fetchPurchaseCreditPayment($orgOrderId = ''){
		$this->callFunction('fetchPurchaseCreditPayment');
	}
	public function postJournal($orgObjectId = ''){
		$this->callFunction('postJournal',$orgObjectId); 
	}
	public function getAllLocation($accountId = ''){
		return false;
	}
	public function getAllShippingMethod($accountId = ''){
		return false;
	}
	public function getAllRegisters($accountId = ''){
		return false; 
	}
	public function getAllSalesrep($accountId = ''){
		return false;
	}
	public function getAllAllowance($accountId = ''){
		return false;
	}
	public function getTradingPartnerId($accountId = ''){
		return false;
	}
	public function getAllBrand($accountId = ''){
		return false;
	}
	public function getAllSupplier($accountId = ''){
		return false;
	}
	public function getAccountInfo(){
		return false;
	}
	public function getAllPriceList(){
		return false;
	}
	public function getAllChannelMethod(){
		$this->reInitialize();
		$return	= array();
		foreach($this->accountDetails as $account1Id => $accountDetails){
			$url				= "2.0/TrackingCategories";
			$this->initializeConfig($account1Id, 'GET', $url);
			$results			= $this->getCurl($url, 'get', '', 'json', $account1Id)[$account1Id]; 
			$TrackingCategorys	= $results['TrackingCategories']['TrackingCategory'];
			if(!isset($TrackingCategorys['0'])){
				$TrackingCategorys	= array($TrackingCategorys);
			}
			foreach($TrackingCategorys as $TrackingCategory){
				if(@$TrackingCategory['Options']){
					if(!isset($TrackingCategory['Options']['Option']['0'])){
						$TrackingCategory['Options']['Option']	= array($TrackingCategory['Options']['Option']);
					}
					foreach($TrackingCategory['Options']['Option'] as $accountDatas){
						$return[$account1Id][$TrackingCategory['Name'] .'~='.$accountDatas['Name']]	= array(
							'id' 	=> $TrackingCategory['Name'] .'~='.$accountDatas['Name'],
							'name'	=> $accountDatas['Name'],
						);
					}
				}
			}
		}
		return $return;
	}
	public function getAllPaymentMethod($accountId = ''){
		$this->reInitialize();
		$return	= array();
		foreach($this->accountDetails as $account1Id => $accountDetails){
			$url		= "2.0/Accounts";
			$this->initializeConfig($account1Id, 'GET', $url);
			$results	= $this->getCurl($url, 'get', '', 'json', $account1Id)[$account1Id]; 
			foreach($results['Accounts'] as $accountDatas){
				foreach($accountDatas as $accountData){
					if(($accountData['EnablePaymentsToAccount'] != 'false')||(strtolower($accountData['Type']) == 'bank')){
						if(!$accountData['Code']){
							continue;
						}
						$return[$account1Id][$accountData['Code']]	= array(
							'id'	=> $accountData['Code'],
							'name'	=> $accountData['Name'] .' ('.$accountData['Class'].')',
						);
					}
				}
			}
		}
		return $return;
	}
	public function getAllTax($accountId = ''){
		$this->reInitialize();
		$return	= array();
		foreach($this->accountDetails as $account1Id => $accountDetails){
			$url			= "2.0/TaxRates";
			$this->initializeConfig($account1Id, 'GET', $url);
			$results		= $this->getCurl($url, 'get', '', 'json', $account1Id)[$account1Id]; 
			$excludeStatus	= array('deleted','archived','pending');
			foreach($results['TaxRates']['TaxRate'] as $accountData){
				if(in_array(strtolower($accountData['Status']),$excludeStatus)){
					continue;
				}
				$return[$account1Id][$accountData['TaxType']]	= array(
					'id'	=> $accountData['TaxType'],
					'name'	=> $accountData['Name'] .' ('.$accountData['TaxType'].')',
				);
			}
		}
		return $return;
	}
	public function getAccountDetails(){
		$this->reInitialize();
		$return	= array();
		foreach($this->accountDetails as $account1Id => $accountDetails){
			$url		= "2.0/Accounts";
			$this->initializeConfig($account1Id, 'GET', $url);
			$results	= $this->getCurl($url, 'get', '', 'json', $account1Id)[$account1Id]; 
			if(@$results['Accounts']){
				foreach($results['Accounts'] as $accountDatas){
					foreach($accountDatas as $accountData){
						if(!$accountData['Code']){
							continue;
						}
						$return[$account1Id][$accountData['Code']]	= array(
							'id'	=> $accountData['Code'],
							'name'	=> $accountData['Name'] .' ('.$accountData['Class'].')',
						);
					}
				}
			}
		}
		return $return;
	}
	public function getOrganizationInfo($id = ''){
		$this->reInitialize();
		$return	= array();
		foreach($this->accountDetails as $account1Id => $accountDetails){
			$suburl	= '2.0/Organisation/'; 
			$this->initializeConfig($account1Id, 'GET', $suburl);
			$results	= $this->getCurl($suburl, 'get', '', 'json', $account1Id);
			$newres		= reset($results);
			if($newres['Organisations']){
				foreach($newres['Organisations'] as $orgDatas){
					if($accountDetails['companyId']){
						if($orgDatas['OrganisationID'] != $accountDetails['companyId']){
							continue;
						}
						else{
							$return[$account1Id]	= $orgDatas;
						}
					}
					else{
						$return[$account1Id]	= $orgDatas;
					}
				}
			}
		}
		return $return;
	}
	public function callFunction($functionName = '',$orgObjectId = ''){
		if($functionName){
			if(file_exists(dirname(__FILE__). DIRECTORY_SEPARATOR . APPNAME. DIRECTORY_SEPARATOR . CLIENTCODE. DIRECTORY_SEPARATOR .$functionName.'.php')){
				include(dirname(__FILE__). DIRECTORY_SEPARATOR . APPNAME. DIRECTORY_SEPARATOR . CLIENTCODE. DIRECTORY_SEPARATOR .$functionName.'.php');
			}
			else if(file_exists(dirname(__FILE__). DIRECTORY_SEPARATOR . APPNAME. DIRECTORY_SEPARATOR . $functionName.'.php')){
				include(dirname(__FILE__). DIRECTORY_SEPARATOR . APPNAME. DIRECTORY_SEPARATOR . $functionName.'.php');
			}
			else{
				include(dirname(__FILE__). DIRECTORY_SEPARATOR .'xero'. DIRECTORY_SEPARATOR .$functionName.'.php'); 
			}
		}
	}
	public function array_to_xml($array, &$xml_user_info){
		foreach($array as $key => $value) {
			if(is_array($value)) {
				if(($key === 0)&& (is_array($value))){
					foreach($value as $val){
						foreach($val as $key1 => $va){
							if(is_array($va)) {
								if(!is_numeric($key1)){
									if(@$va['domAttribute']){
										$subnode	= $xml_user_info->addChild("$key1",$va['value']);
										$subnode->addAttribute($va['domAttribute'], $va['domAttributeValue']);
										if(@$va['domAttribute1'])
											$subnode->addAttribute($va['domAttribute1'], $va['domAttributeValue1']);
										if(@$va['domAttribute2'])
											$subnode->addAttribute($va['domAttribute2'], $va['domAttributeValue2']);
										if(@$va['domAttribute3'])
											$subnode->addAttribute($va['domAttribute3'], $va['domAttributeValue3']);
										if($va['itemSubElement']){
											$va		= $va['itemSubElement'];
											$this->array_to_xml($va, $subnode);
										}
										else{
											unset($va);								
										}
									}
									else{		
										$subnode	= $xml_user_info->addChild("$key1");
										$this->array_to_xml($va, $subnode);
									}								
								}
								else{
									$subnode	= $xml_user_info->addChild("$key1");
									$this->array_to_xml($key1, $subnode);
								}
							}
							else {
								$xml_user_info->addChild("$key",htmlspecialchars("$va"));
							}						
						}
					}
				}
				else if(@($value['0']) && (@!$value['0']['0'])){ 
					foreach($value as $val){
						if(is_array($val)) { 
							if(!is_numeric($key)){
								$subnode	= $xml_user_info->addChild("$key");
								$this->array_to_xml($val, $subnode);
							}
							else{
								$subnode	= $xml_user_info->addChild("$key");
								$this->array_to_xml($key, $subnode);
							}
						}
						else {
							$xml_user_info->addChild("$key",htmlspecialchars("$val"));
						}						
					}
				}				
				else{
					if(!is_numeric($key)){
						if(@$value['domAttribute']){
							$subnode	= $xml_user_info->addChild("$key",$value['value']);
							$subnode->addAttribute($value['domAttribute'], $value['domAttributeValue']);
							if(@$value['domAttribute1'])
								$subnode->addAttribute($value['domAttribute1'], $value['domAttributeValue1']);
							if(@$value['domAttribute2'])
								$subnode->addAttribute($value['domAttribute2'], $value['domAttributeValue2']);
							if(@$value['domAttribute3'])
								$subnode->addAttribute($value['domAttribute3'], $value['domAttributeValue3']);
							if($value['itemSubElement']){
								$value	= $value['itemSubElement'];
								$this->array_to_xml($value, $subnode);
							}
							else{
								unset($value);								
							}
						}
						else{		
							$subnode	= $xml_user_info->addChild("$key");
							$this->array_to_xml($value, $subnode);
						}
					}
					else{
						$subnode	= $xml_user_info->addChild("item$key");
						$this->array_to_xml($value, $subnode);
					}
				} 
			}
			else{
				$xml_user_info->addChild("$key",htmlspecialchars("$value"));
			}
		}
	}
	public function fetchAvalaraTaxRate($country = '',  $postal = ''){
		$avalaraDetail		= $this->ci->db->get('Avalara_account')->row_array();
		$credentials		= $avalaraDetail['email'].':'.$avalaraDetail['password'];
		$credentials		= base64_encode($credentials);
		$URL				= $avalaraDetail['url']."country=$country&postalCode=$postal";
		$ch_1				= curl_init($URL);
		curl_setopt($ch_1, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch_1, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch_1, CURLOPT_HTTPHEADER, array("Authorization: Basic $credentials"));
		$AvalaraTaxDetails	= json_decode(curl_exec($ch_1),true);	
		$info				= curl_getinfo($ch_1);
		if($info['http_code'] == '200'){
			$this->countRequest	= 0;
			return $AvalaraTaxDetails;
		}
		elseif($info['http_code'] != '200' AND $this->countRequest < 3){			
			if($info['http_code'] == '429'){		
				sleep(60);
				$this->countRequest++;
				$datas	= $this->fetchAvalaraTaxRate($country,$postal);
				return $datas;
			}
			else{
				return $AvalaraTaxDetails;
			}
		}
		else{
			return $AvalaraTaxDetails;
		} 
	}
	public function fetchSalesReport(){
		$this->reInitialize();
		foreach($this->accountDetails as $account1Id => $accountDetails){
			$reportDatasTemps	= $this->ci->db->get_where('daily_report',array('createOrderId <>' => ''))->result_array();
			$orderDatas			= $this->ci->db->get_where('sales_order',array('createOrderId <>' => ''))->result_array();
			foreach($reportDatasTemps as $reportDatasTemp){
				$reportDatas[$reportDatasTemp['createOrderId']]		= $reportDatasTemp;
			}
			foreach($orderDatas as $orderDatasTemps){
				$orderDatasss[$orderDatasTemps['createOrderId']]	= $orderDatasTemps;
			}
			$allXeroIds_salesOrderTable		=    array_keys($orderDatasss);
			$allXeroIds_dailyreportTable	=    array_keys($reportDatas);
			
			$url		= "2.0/Invoices";
			$this->initializeConfig($account1Id, 'GET', $url);			
			$url		= '2.0/Invoices';
			$this->headers['If-Modified-Since'] = gmdate('c',strtotime('-10 days'));
			$results	= $this->getCurl($url, 'GET','', 'json', $account1Id)[$account1Id];
			
			if($results['Invoices']['Invoice']){
				foreach($results['Invoices']['Invoice'] as $invoicesData){
					$UpdateRecord	= array();
					$insertRecord	= array();
					if(!in_array($invoicesData['InvoiceID'], $allXeroIds_salesOrderTable)){
						continue;
					}
					if(!in_array($invoicesData['InvoiceID'], $allXeroIds_dailyreportTable)){
						$insertRecord	= array(
							'createOrderId'		=> $invoicesData['InvoiceID'],
							'orderAmountXero'	=> $invoicesData['Total'],
							'taxAmountXero'		=> $invoicesData['TotalTax'],
							'paidAmountXero'	=> $invoicesData['AmountPaid'],
							'XeroCreateDate'	=> date('Y-m-d H:i:s',strtotime($invoicesData['Date'])),
						);
						if($invoicesData['Payments']){
							foreach($invoicesData['Payments']['Payment'] as $paymentsdata){
								$insertRecord['XeroPaymentDate']	= date('Y-m-d H:i:s',strtotime(@$paymentsdata['Date']));
							}
						}
						$insertRecord['xeroorderresponse']	= json_encode($invoicesData);
						$this->ci->db->insert('daily_report', $insertRecord);
					}
					else{
						$UpdateRecord	= array(
							'orderAmountXero'	=> $invoicesData['Total'],
							'taxAmountXero'		=> $invoicesData['TotalTax'],
							'paidAmountXero'	=> $invoicesData['AmountPaid'],
							'XeroCreateDate'	=> date('Y-m-d H:i:s',strtotime($invoicesData['Date'])),
						);
						if($invoicesData['Payments']){
							foreach($invoicesData['Payments']['Payment'] as $paymentsdata){
								$UpdateRecord['XeroPaymentDate']	= date('Y-m-d H:i:s',strtotime(@$paymentsdata['Date']));
							}
						}
						$UpdateRecord['xeroorderresponse']	= json_encode($invoicesData);
						$this->ci->db->where(array('createOrderId' => $invoicesData['InvoiceID']))->update('daily_report',$UpdateRecord);
					}
					
				}
			}
		}
	}
	public function sendTokenalert($organizationName = '' ,$checkerror = ''){
		include_once('Mailer.php');
		$obj	= new Mailer();
		if(!$organizationName){
			$organizationName	= 'Xero App';
		}
		$subject	= 'Xero Unauthorized Error';
		$from		= array('contact@bsitc-repo.com' => 'Contact');
		$to			= 'deepakgoyal@businesssolutionsinthecloud.com,dean@businesssolutionsinthecloud.com,aherve@businesssolutionsinthecloud.com,rachel@businesssolutionsinthecloud.com,veena@businesssolutionsinthecloud.com,avinash@businesssolutionsinthecloud.com';
		if((substr_count($checkerror['Detail'],'TokenExpired'))){
			$subject	= 'Xero Access Token Expired';
		}
		if((substr_count($checkerror['Detail'],'TokenInvalidSignature'))){
			$subject	= 'Xero Invalid Access Token';
		}
		$body		= 'Hi,<br><br>You Have the following Error for <b>'.$organizationName.'</b> Xero Organization.<br><br>
						<b>Error Message :</b> '.$checkerror['Detail'].'<br><br>Thanks & Regards<br>The BSITC Team' ;
		$obj->send($to,$subject,$body,$from); 
		die;
	}
} 
?>