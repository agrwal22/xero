<?php
class Journal_model extends CI_Model{
    public function __construct(){
        parent::__construct();
		$this->ci	= get_instance();
    }
    public function fetchJournal($journalId = ''){
		$fetchby		= $journalId;
		$datas			= $this->ci->db->order_by('id', 'desc')->get_where('cron_management', array('type' => 'journal'))->row_array();
		$cronTime		= ($datas['saveTime']) ? (date('Y-m-d\TH:i:s',$datas['saveTime'])) : (date('Y-m-d\TH:i:s',strtotime('-20 days')));
		$saveTime		= date('Y-m-d\TH:i:s',strtotime('-20 days'));
		$journalDatasss	= $this->{$this->globalConfig['fetchProduct']}->fetchJournal($journalId, $cronTime);
		foreach($journalDatasss as $fetchAccount1Id => $journalDatass){
			$journalDatas	= $journalDatass['return'];
			if(@$journalDatass['saveTime']){
				$saveTime	= $journalDatass['saveTime'] - (60*30);
			}
			$saveDatasTemps	= $this->db->get('amazon_ledger')->result_array();
			$saveDatas		= array();
			$saveId2Datas	= array();
			foreach ($saveDatasTemps as $saveDatasTemp) {
				$key				= @trim(preg_replace("/[^a-zA-Z_0-9\s_\.\(\)\-]/", "", strtolower($saveDatasTemp['account1Id'].'_'.$saveDatasTemp['account2Id'].'_'.$saveDatasTemp['journalsId'])));
				$saveDatas[$key]	= $saveDatasTemp;
			}
			$batchInsert	= array(); $batchUpdate = array();
			foreach ($journalDatas as $account1Id => $journalData) {
				foreach ($journalData as $row) {
					if((!$row['account1Id']) || (!$row['account2Id'])){
						continue;
					}
					$key	=  @trim(preg_replace("/[^a-zA-Z_0-9\s_\.\(\)\-]/", "", strtolower($row['account1Id'].'_'.$row['account2Id'].'_'.$row['journalsId'])));
					if(@$saveDatas[$key]){
						//
					}
					else{
						$batchInsert[]	= $row;
					}
				}
			}	
			$checkInsertFlag	= false;
			if($batchInsert){
				$batchInserts	= array_chunk($batchInsert, 1000);
				$checkInsertFlag	= true;
				foreach ($batchInserts as $key => $batchInsert) {
					$insertedBatchRes	= $this->db->insert_batch('amazon_ledger', $batchInsert);
				}
			}
			if($batchUpdate){
				$batchUpdates		= array_chunk($batchUpdate, 1000);
				$checkInsertFlag	= true;
				foreach ($batchUpdates as $key => $batchUpdate) {
					$batchUpdateRes	= $this->db->update_batch('amazon_ledger',$batchUpdate, 'id');
				}
			}
			if($checkInsertFlag){
				if(!$fetchby){
					$this->ci->db->insert('cron_management', array('type' => 'journal'.$fetchAccount1Id, 'runTime' => $cronTime, 'saveTime' => $saveTime)); 
				}
			}
		}
    }    
	public function postJournal($sku = ''){
        $this->{$this->globalConfig['postProduct']}->postJournal($sku);
    }    
	public function getJournal(){
        $groupAction		= $this->input->post('customActionType');
        $records			= array();
        $records["data"]	= array();
		if($this->input->post('order')){
 			$orderData	= array($this->router->directory.$this->router->class => $this->input->post('order'));
 			$this->session->set_userdata($orderData);
        } 
        if ($groupAction == 'group_action') {
            $ids	= $this->input->post('id');
            if ($ids) {
                $status	= $this->input->post('customActionName');
                if ($status != '') {
                    $this->db->where_in('id', $ids)->update('amazon_ledger', array('status' => $status));
                    $records["customActionStatus"]	= "OK";
                    $records["customActionMessage"]	= "Group action successfully has been completed. Well done!";
                }
            }
        }
        $where	= array();
        $query	= $this->db;
        if ($this->input->post('action') == 'filter') {
            if (trim($this->input->post('journalsId'))) {
                $where['journalsId']			= trim($this->input->post('journalsId'));
            }
            if (trim($this->input->post('createdJournalsId'))) {
                $where['createdJournalsId']		= trim($this->input->post('createdJournalsId'));
            }
            if (trim($this->input->post('orderId'))) {
                $where['orderId']				= trim($this->input->post('orderId'));
            }
            if (trim($this->input->post('journalTypeCode'))) {
                $where['journalTypeCode']		= trim($this->input->post('journalTypeCode'));
            }
			if (strlen($this->input->post('status')) > 0) {
                $where['status']				= trim($this->input->post('status'));
            }
        }
        if (trim($this->input->post('updated_from'))) {
            $query->where('date(taxDate) >= ', "date('" . $this->input->post('updated_from') . "')", false);
        }
        if (trim($this->input->post('updated_to'))) {
            $query->where('date(taxDate) <= ', "date('" . $this->input->post('updated_to') . "')", false);
        }
        if ($where) {
            $query->like($where);
        }
        $totalRecord	= @$query->select('count("id") as countpro')->get('amazon_ledger')->row_array()['countpro'];
        $limit			= intval($this->input->post('length'));
        $limit			= $limit < 0 ? $totalRecord : $limit;
        $start			= intval($this->input->post('start'));
        $query			= $this->db;
        if (trim($this->input->post('updated_from'))) {
            $query->where('date(taxDate) >= ', "date('" . $this->input->post('updated_from') . "')", false);
        }
        if (trim($this->input->post('updated_to'))) {
            $query->where('date(taxDate) <= ', "date('" . $this->input->post('updated_to') . "')", false);
        }
        if ($where) {
            $query->like($where);
        }
        $status              	= array('0' => 'Pending', '1' => 'Sent', '2' => 'Updated', '3' => 'Error', '4' => 'Archive');
        $statusColor         	= array('0' => 'default', '1' => 'success', '2' => 'info', '3' => 'warning', '4' => 'danger');
        $displayProRowHeader	= array('id',  'journalsId', 'createdJournalsId','journalTypeCode','orderId', 'amount', 'taxCode','status', 'message');
        if (@$this->session->userdata($this->router->directory.$this->router->class)) {
            foreach (@$this->session->userdata($this->router->directory.$this->router->class) as $ordering) {
                if (@$displayProRowHeader[$ordering['column']]) {
                    $query->order_by($displayProRowHeader[$ordering['column']], $ordering['dir']);
                }
            }
        }
        $datas	= $query->limit($limit, $start)->get('amazon_ledger')->result_array();
        foreach ($datas as $data) {
			$viewBill	=	''; 
			$message	= '';
			if($data['createdJournalsId']){
				$viewBill	.=	'<li>
										<a target="_blank" href="https://go.xero.com/AccountsPayable/View.aspx?InvoiceID='.$data['createdJournalsId'].'"> View Bill in Xero</a>
									</li>';
			}
			if($data['paymentCreated'] == 1){				
				$message	=	'<span class="label label-sm label-success">Payment Created</span>'; 
			}
            $records["data"][]	= array(
                '<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
                $data['journalsId'],
                $data['createdJournalsId'],
                $data['journalTypeCode'],
                $data['orderId'],
				$data['amount'],
                $data['taxDate'],
                '<span class="label label-sm label-' . $statusColor[$data['status']] . '">' . $status[$data['status']] . '</span>',
				$message,
                '<div class="btn-group">
                    <a class="btn btn-circle btn-default dropdown-toggle" href="javascript:;" data-toggle="dropdown">
                        <i class="fa fa-share"></i>
                        <span class="hidden-xs"> Tools </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <div class="dropdown-menu pull-right">                       
                        <li>
                            <a class="btnactionsubmit" href="' . base_url('journal/journal/postJournal/' . $data['orderId']) . '">Post Amazon Fee</a>
                        </li>
						<li>
							<a target = "_blank" href="'.base_url('/journal/journal/journalinfo/'.$data['orderId']).'">Amazon Fee Info</a>
						</li>
						'.$viewBill.'
                    </div>
                </div>',
            );
        }
        $draw                       = intval($this->input->post('draw'));
        $records["draw"]            = $draw;
        $records["recordsTotal"]	= $totalRecord;
        $records["recordsFiltered"]	= $totalRecord;
        return $records;
    }
}