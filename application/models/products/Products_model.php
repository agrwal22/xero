<?php
class Products_model extends CI_Model{
    public function __construct(){
        parent::__construct();
        $this->ci = get_instance();
    }
    public function fetchProducts($productId = '' ){
		$fetchby		= $productId;
		$productDatass	= $this->{$this->globalConfig['fetchProduct']}->fetchProducts($productId);
		foreach($productDatass as $fetchAccount1Id => $productDatasTemp){
			$saveTime		= date('Y-m-d\TH:i:s',strtotime('-5 days'));
			$productDatas	= $productDatasTemp['return'];
			if(@$productDatasTemp['saveTime']){
				$saveTime	= $productDatasTemp['saveTime'] - (60*10);
			}
			$saveDatasTemps = $this->db->select('id,productId,account1Id,account2Id,sku,size,color,size,status')->get_where('products')->result_array();
			$saveDatas		= array();
			$saveId2Datas	= array();
			foreach ($saveDatasTemps as $saveDatasTemp) {
				$key	= @trim(preg_replace("/[^a-zA-Z_0-9\s_\.\(\)\-]/", "", strtolower($saveDatasTemp['account1Id'].'_'.$saveDatasTemp['account2Id'].'_'.$saveDatasTemp['productId'])));
				$saveDatas[$key]	= $saveDatasTemp;
			}
			$batchInsert	= array();
			$batchUpdate	= array();
			foreach ($productDatas as $account1Id => $productData) {
				foreach ($productData as $row) {
					if((!$row['account1Id']) || (!$row['account2Id'])){
						continue;
					}
					$key	=  @trim(preg_replace("/[^a-zA-Z_0-9\s_\.\(\)\-]/", "", strtolower($row['account1Id'].'_'.$row['account2Id'].'_'.$row['productId'])));
					if(@$saveDatas[$key]){
						$row['id']		= $saveDatas[$key]['id'];
						$this->db->where(array('id' => $row['id']))->update('products',$row);
						$afftectedRows	= $this->db->affected_rows();
						if($afftectedRows){
							$row['status']	= ($saveDatas[$key]['status'])?(2):0;
							$row['sendTo']	= $this->globalConfig['postProduct'] ;
							$batchUpdate[]	= $row; 
						}
					}
					else{
						$row['sendTo']	= $this->globalConfig['postProduct'];
						$batchInsert[]	= $row;
					}
				}
			}		
			$checkInsertFlag	= false;
			if($batchInsert){
				$batchInserts		= array_chunk($batchInsert, 1000);
				$checkInsertFlag	= true;
				foreach ($batchInserts as $key => $batchInsert) {
					$insertedBatchRes	= $this->db->insert_batch('products', $batchInsert);
				}
			}
			if($batchUpdate){
				$batchUpdates		= array_chunk($batchUpdate, 1000);
				$checkInsertFlag	= true;
				foreach ($batchUpdates as $key => $batchUpdate) {
					$batchUpdateRes	= $this->db->update_batch('products',$batchUpdate, 'id');
				}
			}
			if($checkInsertFlag){
				if(!$fetchby){
					$this->ci->db->insert('cron_management', array('type' => 'bpproduct'.$fetchAccount1Id, 'runTime' => $cronTime, 'saveTime' => $saveTime)); 
				}
			}
		}
    }
    public function postProducts($productId = ''){
		$this->{$this->globalConfig['postProduct']}->postProducts($productId);
    }		
	public function postLoadedProduct($productId = ''){
		$productLoads		= array();
		$productLoadsTemps	= $this->db->select('account1Id,account2Id,productId,sku')->get_where('products')->result_array();
		foreach($productLoadsTemps as $productLoadsTemp){
			$key			= $productLoadsTemp['account1Id'].'-'.$productLoadsTemp['account2Id'].'-'.strtolower(trim($productLoadsTemp['sku']));
			$products[$key]	= $productLoadsTemp;
		}
		$productLoadsTemps	= $this->db->get_where('product_load',array('status' => '0'))->result_array();
		$orderId			= 3;
		$batchInsert		= array();
		foreach($productLoadsTemps as $productLoadsTemp){
			$key			= $productLoadsTemp['account1Id'].'-'.$productLoadsTemp['account2Id'].'-'.strtolower(trim($productLoadsTemp['sku']));
			$batchInsert[]	= array(
				'orderId'		=> $orderId,
				'account1Id' 	=> $productLoadsTemp['account1Id'],
				'account2Id' 	=> $productLoadsTemp['account2Id'],
				'qty' 			=> $productLoadsTemp['qty'],
				'sku' 			=> $productLoadsTemp['sku'],
				'price' 		=> $productLoadsTemp['unitPrice'],
				'accountCode' 	=> $productLoadsTemp['accountCode'],
				'productId' 	=> $products[$key]['productId'],
				'created' 		=> date('Y-m-d',strtotime($productLoadsTemp['availableDate'])),
				'status' 		=> '0',
			);
		}
		$batchInserts	= array_chunk($batchInsert,200);
		foreach($batchInserts as $batchInsert){
			if($batchInsert['0']){
				$this->db->insert_batch('stock_adjustment',$batchInsert);
			}
		}
		$this->db->update('product_load',array('status' => '1'));
    }
    public function getProduct(){
        $groupAction		= $this->input->post('customActionType');
        $records			= array();
        $records["data"]	= array();
		if($this->input->post('order')){
 			$orderData	= array($this->router->directory.$this->router->class => $this->input->post('order'));
 			$this->session->set_userdata($orderData);
        }
        if ($groupAction == 'group_action') {
            $ids	= $this->input->post('id');
            if ($ids) {
                $status	= $this->input->post('customActionName');
                if ($status != '') {
                    $this->db->where_in('id', $ids)->update('products', array('status' => $status));
                    $records["customActionStatus"]	= "OK";
                    $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
                }
            }
        }
        $where	= array();
        $query	= $this->db;
        if ($this->input->post('action') == 'filter') {
            if (trim($this->input->post('productId'))) {
                $where['productId']			= trim($this->input->post('productId'));
            }
            if (trim($this->input->post('createdProductId'))) {
                $where['createdProductId']	= trim($this->input->post('createdProductId'));
            }
			if (trim($this->input->post('ean'))) {
                $where['ean']				= trim($this->input->post('ean'));
            }
            if (trim($this->input->post('name'))) {
                $where['name']				= trim($this->input->post('name'));
            }
            if (trim($this->input->post('sku'))) {
                $where['sku']				= trim($this->input->post('sku'));
            }
            if (trim($this->input->post('color'))) {
                $where['color']				= trim($this->input->post('color'));
            }
			if (trim($this->input->post('account1Id'))) {
                $where['account1Id']		= trim($this->input->post('account1Id'));
            }
			if (trim($this->input->post('account2Id'))) {
                $where['account2Id']		= trim($this->input->post('account2Id'));
            }
            if (trim($this->input->post('size'))) {
                $where['size']				= trim($this->input->post('size'));
            }
            if (trim($this->input->post('status')) >= '0') {
                $where['status']			= trim($this->input->post('status'));
            }
        }
        if (trim($this->input->post('updated_from'))) {
            $query->where('date(updated) >= ', "date('" . $this->input->post('updated_from') . "')", false);
        }
        if (trim($this->input->post('updated_to'))) {
            $query->where('date(updated) <= ', "date('" . $this->input->post('updated_to') . "')", false);
        }
        if ($where) {
            $query->like($where);
        }
        $totalRecord	= @$query->get('products')->num_rows();
        $limit			= intval($this->input->post('length'));
        $limit			= $limit < 0 ? $totalRecord : $limit;
        $start			= intval($this->input->post('start'));
        $query			= $this->db;
        if (trim($this->input->post('updated_from'))) {
            $query->where('date(updated) >= ', "date('" . $this->input->post('updated_from') . "')", false);
        }
        if (trim($this->input->post('updated_to'))) {
            $query->where('date(updated) <= ', "date('" . $this->input->post('updated_to') . "')", false);
        }
        if ($where) {
            $query->like($where);
        }
        $status					= array('0' => 'Pending', '1' => 'Sent', '2' => 'Updated', '3' => 'Error', '4' => 'Archive');
        $statusColor			= array('0' => 'default', '1' => 'success', '2' => 'info', '3' => 'warning', '4' => 'danger');
        $displayProRowHeader	= array('id','account1Id','account2Id', 'productId', 'createdProductId', 'sku', 'color', 'size', 'name','updated', 'status', 'message');
        if ($this->session->userdata($this->router->directory.$this->router->class)) {
            foreach ($this->input->post('order') as $ordering) {
                if (@$displayProRowHeader[$ordering['column']]) {
                    $query->order_by($displayProRowHeader[$ordering['column']], $ordering['dir']);
                }
            }
        }		
        $datas					= $query->select('id,productId,createdProductId,name,newSku,sku,color,updated,status,size,account1Id,account2Id,filename,ean,message,barcode')->limit($limit, $start)->get('products')->result_array();
		$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['fetchProduct'].'_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){			
			$account1Mappings[$account1MappingTemp['id']]	= $account1MappingTemp;
		}
		$account2MappingTemps = $this->db->get_where('account_'.$this->globalConfig['postProduct'].'_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){			
			$account2Mappings[$account2MappingTemp['id']]	= $account2MappingTemp;
		}		
        foreach ($datas as $data) { 
			$downloadLink	= '';
			if($data['filename']){
				$downloadLink	=	'<li>
										<a class="" target="_blank" href="'.base_url($data['filename']).'" download="'.basename($data['filename']).'"> Download Product File </a>
									</li>';
			}
			if($data['createdProductId']){
				$downloadLink	.=	'<li>
										<a target="_blank" href="https://go.xero.com/Accounts/Inventory/'.$data['createdProductId'].'"> View Product in  '.$account2Mappings[$data['account2Id']]['name'].'</a>
									</li>';
			}
            $records["data"][]	= array(
                '<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
                @$account1Mappings[$data['account1Id']]['name'], 
                @$account2Mappings[$data['account2Id']]['name'], 
                $data['productId'],
                $data['createdProductId'],
                '<a href="#" class="showDetails" data-newSku = "'.$data['newSku'].'" data-color = "'.$data['color'].'" >'.$data['sku'].'</a>',
                $data['color'],
                $data['size'],
                $data['name'],
                $data['updated'],
                '<span class="label label-sm label-' . $statusColor[$data['status']] . '">' . $status[$data['status']] . '</span>',
                '<div class="btn-group">
                    <a class="btn btn-circle btn-default dropdown-toggle" href="javascript:;" data-toggle="dropdown">
                        <i class="fa fa-share"></i>
                        <span class="hidden-xs"> Tools </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <div class="dropdown-menu pull-right">
                        <li>
                            <a class="btnactionsubmit" href="'.base_url('products/products/fetchProducts/'.$data['productId']).'"> Fetch Product </a>
                        </li>
                        <li>
                            <a class="btnactionsubmit" href="'.base_url('products/products/postProducts/'.$data['productId']).'"> Post Product </a>
                        </li>
                        <li>
                            <a target="_blank" href="'.base_url('products/products/productInfo/'.$data['productId']).'"> Product Info </a>
                        </li>
						<li>
                            <a target="_blank" href="'.$account1Mappings[$data['account1Id']]['viewUrl'].'/patt-op.php?scode=product&pID='.$data['productId'].'"> View Product in  '.$account1Mappings[$data['account1Id']]['name'].'</a>
                        </li>
						'.$downloadLink.'
                    </div>
                </div>',
            );
        }
        $draw                       = intval($this->input->post('draw'));
        $records["draw"]            = $draw;
        $records["recordsTotal"]    = $totalRecord;
        $records["recordsFiltered"]	= $totalRecord;
        return $records;
    }
	public function getLoadProduct(){
        $groupAction		= $this->input->post('customActionType');
        $records			= array();
        $records["data"]	= array();
        if ($groupAction == 'group_action') {
            $ids	= $this->input->post('id');
            if ($ids) {
                $status	= $this->input->post('customActionName');
                if ($status != '') {
                    $this->db->where_in('id', $ids)->update('product_load', array('status' => $status));
					if($status == 2){
						$this->db->where_in('id', $ids)->delete('product_load');
					}
                    $records["customActionStatus"]  = "OK";
                    $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
                }
            }
        }
        $where	= array( );
        $query	= $this->db;
        if ($this->input->post('action') == 'filter') {
            if (trim($this->input->post('qty'))) {
                $where['qty']			= trim($this->input->post('qty'));
            }
            if (trim($this->input->post('sku'))) {
                $where['sku']			= trim($this->input->post('sku'));
            }
            if (trim($this->input->post('unitPrice'))) {
                $where['unitPrice']		= trim($this->input->post('unitPrice'));
            }
            if (trim($this->input->post('availableDate'))) {
                $where['availableDate'] = trim($this->input->post('availableDate'));
            }
			if (trim($this->input->post('status'))) {
                $where['status']		= trim($this->input->post('status'));
            }			
        }       
        if ($where) {
            $query->like($where);
        }
        $totalRecord	= @$query->get('product_load')->num_rows();
        $limit			= intval($this->input->post('length'));
        $limit			= $limit < 0 ? $totalRecord : $limit;
        $start			= intval($this->input->post('start'));
        $query			= $this->db;
        if ($where) {
            $query->like($where);
        }

        $status              	= array('0' => 'Pending', '1' => 'Sent', '2' => 'Updated', '3' => 'Error', '4' => 'Archive');
        $statusColor         	= array('0' => 'default', '1' => 'success', '2' => 'info', '3' => 'warning', '4' => 'danger');
        $displayProRowHeader	= array('id', 'sku', 'qty', 'unitPrice', 'availableDate', 'status');
        if ($this->input->post('order')) {
            foreach ($this->input->post('order') as $ordering) {
                if (@$displayProRowHeader[$ordering['column']]) {
                    $query->order_by($displayProRowHeader[$ordering['column']], $ordering['dir']);
                }
            }
        }
        $datas	= $query->limit($limit, $start)->get('product_load')->result_array();
        foreach ($datas as $data) {
            $records["data"][]	= array(
                '<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
                $data['account1Id'],
                $data['account2Id'],
                $data['sku'],
                $data['qty'],
                $data['unitPrice'],
                $data['availableDate'],
                $data['accountCode'],
                '<span class="label label-sm label-' . $statusColor[$data['status']] . '">' . $status[$data['status']] . '</span>',
				'',
            );
        }
        $draw                       = intval($this->input->post('draw'));
        $records["draw"]            = $draw;
        $records["recordsTotal"]    = $totalRecord;
        $records["recordsFiltered"]	= $totalRecord;
        return $records;
    }
}