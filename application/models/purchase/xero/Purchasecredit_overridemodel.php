<?php
include_once(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'Purchasecredit_model.php');
class Purchasecredit_overridemodel extends Purchasecredit_model{
	public function __construct(){
        parent::__construct();
        $this->ci	= get_instance();
    }	
	public function postPurchaseCredit($orderId = ''){
        $this->{$this->globalConfig['postPurchaseCredit']}->postPurchaseCredit($orderId);
		$this->{$this->globalConfig['fetchPurchaseOrder']}->postPurchaseCreditPayment();
    }
	public function getCredit(){
        $groupAction		= $this->input->post('customActionType');
        $records			= array();
        $records["data"]	= array();
        if ($groupAction == 'group_action') {
            $ids	= $this->input->post('id');
            if ($ids) {
                $status	= $this->input->post('customActionName');
                if ($status != '') {
                    $this->db->where_in('id', $ids)->update('purchase_credit_order', array('status' => $status));
                    $records["customActionStatus"]	= "OK";
                    $records["customActionMessage"]	= "Group action successfully has been completed. Well done!";
                }
            }
        }
        $where	= array();
        $query	= $this->db;
        if ($this->input->post('action') == 'filter') {
			if (trim($this->input->post('account1Id'))) {
                $where['account1Id']		= trim($this->input->post('account1Id'));
            }
			if (trim($this->input->post('account2Id'))) {
                $where['account2Id']		= trim($this->input->post('account2Id'));
            }
            if (trim($this->input->post('orderId'))) {
                $where['orderId']			= trim($this->input->post('orderId'));
            }
            if (trim($this->input->post('createOrderId'))) {
                $where['createOrderId']		= trim($this->input->post('createOrderId'));
            }
            if (trim($this->input->post('orderNo'))) {
                $where['orderNo']			= trim($this->input->post('orderNo'));
            }
            if (trim($this->input->post('customerId'))) {
                $where['customerId']		= trim($this->input->post('customerId'));
            }
			if (trim($this->input->post('type'))) {
                $where['type']				= trim($this->input->post('type'));
            }
            if (trim($this->input->post('customerEmail'))) {
                $where['customerEmail']		= trim($this->input->post('customerEmail'));
            }
            if (trim($this->input->post('paymentMethod'))) {
                $where['paymentMethod']		= trim($this->input->post('paymentMethod'));
            }
            if (trim($this->input->post('status')) >= '0') {
                $where['status']			= trim($this->input->post('status'));
            }
        }
        if (trim($this->input->post('updated_from'))) {
            $query->where('date(created) >= ', "date('" . $this->input->post('updated_from') . "')", false);
        }
        if (trim($this->input->post('updated_to'))) {
            $query->where('date(created) < ', "date('" . $this->input->post('updated_to') . "')", false);
        }
        if ($where) {
            $query->like($where);
        }
        $totalRecord	= @$query->select('count("id") as countpurchase')->get('purchase_credit_order')->row_array()['countpurchase'];
        $limit			= intval($this->input->post('length'));
        $limit			= $limit < 0 ? $totalRecord : $limit;
        $start			= intval($this->input->post('start'));
        $query			= $this->db;
        if (trim($this->input->post('updated_from'))) {
            $query->where('date(created) >= ', "date('" . $this->input->post('updated_from') . "')", false);
        }
        if (trim($this->input->post('updated_to'))) {
            $query->where('date(created) < ', "date('" . $this->input->post('updated_to') . "')", false);
        }
        if ($where) {
            $query->like($where);
        }
        $status					= array('0'=>'Pending','1'=>'Sent','2' =>'Acknowledgement','3' =>'Payment Created','4'=>'Invoice','5'=>'Archive');
        $statusColor			= array('0'=>'default','1'=>'success','2'=>'success','3'=>'info','4'=>'success','5'=>'danger');
        $displayProRowHeader	= array('id','account1Id','account2Id', 'orderId','createOrderId', 'customerEmail', 'updated', 'status', 'message');  
        if ($this->input->post('order')) {
            foreach ($this->input->post('order') as $ordering) {
                if (@$displayProRowHeader[$ordering['column']]) {
                    $query->order_by($displayProRowHeader[$ordering['column']], $ordering['dir']);
                }
            }
        }
        $datas	= $query->select('account1Id,account2Id,id,orderNo,createOrderId,customerEmail,customerEmail,orderId,updated,status,paymentMethod,type,typeDetails,rowData,dispatchConfirmation,cancelRequest,message,orderAcknowledged,acknowledgedFilename,dispatchFileName,salesFileName,invoiceRequest,sendPaymentTo,uninvoiced')->limit($limit, $start)->get('purchase_credit_order')->result_array();
		$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['fetchSalesOrder'].'_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){			
			$account1Mappings[$account1MappingTemp['id']]	= $account1MappingTemp;
		}
		$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postSalesOrder'].'_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){			
			$account2Mappings[$account2MappingTemp['id']]	= $account2MappingTemp;
		}
        foreach ($datas as $data) {
        	$params		= json_decode($data['rowData'],true);
			$message	= $data['message'];
			if($data['uninvoiced'] == 1){				
				$message	=	'<span class="label label-sm label-info">Order Uninvoiced on Brightpearl</span>'; 
			}
			else if($data['uninvoiced'] == 2){				
				$message	=	'<span class="label label-sm label-info">Invoice Void On Xero</span>'; 
			}
			if(($data['status'] == '1') && ($data['orderAcknowledged'])){ 
				$message	=	'Received order acknowledgement';
			}
			if(($data['status'] <= '3') && ($data['invoiceRequest'])){
				$message	=	'Received invoice confirmation';
			}		
			$downloadHtml	=	''; 
			if($data['salesFileName']){
				$downloadHtml	.=	'<li>
										<a  href="'.base_url($data['salesFileName']).'" download = "'.basename($data['salesFileName']).'"> Download purchase file </a>
									</li>';
			}
			if($data['acknowledgedFilename']){
				$downloadHtml	.=	'<li>
										<a  href="'.base_url($data['acknowledgedFilename']).'" download = "'.basename($data['acknowledgedFilename']).'"> Download acknowledgement file </a>
									</li>';
			}
			if($data['dispatchFileName']){
				$downloadHtml	.=	'<li>
										<a  href="'.base_url($data['dispatchFileName']).'" download = "'.basename($data['dispatchFileName']).'"> Download invoice file </a>
									</li>';
			}
			else if($data['sendPaymentTo'] == 'brightpearl'){				
				$message		=	'<span class="label label-sm label-info">Payment initiated in Xero</span>'; 
			}
			else if($data['sendPaymentTo'] == 'xero'){				
				$message		=	'<span class="label label-sm label-info">Payment initiated in Brightpearl</span>'; 
			}
			if($data['createOrderId']){
				$downloadHtml	.=	'<li>
										<a target="_blank" href="https://go.xero.com/AccountsPayable/ViewCreditNote.aspx?creditNoteID='.$data['createOrderId'].'"> View Invoice in  '.$account2Mappings[$data['account2Id']]['name'].'</a>
									</li>';
			}
            $records["data"][]	= array(
                '<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
				@$account1Mappings[$data['account1Id']]['name'],
                @$account2Mappings[$data['account2Id']]['name'],
                '<a href="'.base_url('purchase/credit/creditItem/'.$data['orderId']).'">'.$data['orderId'].'</a>',
                $data['createOrderId'],
                $data['customerEmail'],
                $data['updated'],
                '<span class="label label-sm label-' . $statusColor[$data['status']] . '">' . $status[$data['status']] . '</span>',
				($data['cancelRequest'])?('<span class="label label-sm label-danger">Cancel request received</span>'):($message),
                '<div class="btn-group">
					<a class="btn btn-circle btn-default dropdown-toggle" href="javascript:;" data-toggle="dropdown">
						<i class="fa fa-share"></i>
						<span class="hidden-xs"> Tools </span>
						<i class="fa fa-angle-down"></i>
					</a>
					<div class="dropdown-menu pull-right">						
						<li>
							<a class="btnactionsubmit" href="'.base_url('purchase/credit/fetchPurchaseCredit/'.$data['orderId']).'"> Fetch Purchase Credit </a>
						</li>
						<li>
							<a class="btnactionsubmit" href="'.base_url('purchase/credit/postPurchaseCredit/'.$data['orderId']).'"> Post Purchase Credit </a>
						</li>
						<li>
							<a target = "_blank" href="'.base_url('purchase/credit/purchaseInfo/'.$data['orderId']).'"> Purchase Info </a>
						</li>
						<li>
                            <a target="_blank" href="'.$account1Mappings[$data['account1Id']]['viewUrl'].'/patt-op.php?scode=invoice&oID='.$data['orderId'].'"> View PO refund in  '.$account1Mappings[$data['account1Id']]['name'].'</a>
                        </li>
						'.$downloadHtml.'
					</div>
				</div>',
            );
        }
        $draw                       = intval($this->input->post('draw'));
        $records["draw"]            = $draw;
        $records["recordsTotal"]    = $totalRecord;
        $records["recordsFiltered"]	= $totalRecord;
        return $records;
    }
}
?>