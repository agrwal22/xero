<?php
class Purchase_model extends CI_Model{
    public function __construct(){
        parent::__construct();
    }
    public function fetchPurchase($orderId = '',$accountId = ''){
		$fetchby		= $orderId;
		$saveTime		= date('c',strtotime('-4 hours')); 		
        $purchaseDatass = $this->{$this->globalConfig['fetchPurchaseOrder']}->fetchPurchase($orderId, $accountId);
		foreach($purchaseDatass as $fetchAccount1Id => $purchaseDatassTemps){
			if(@$purchaseDatassTemps['saveTime']){
				$saveTime	= $purchaseDatassTemps['saveTime'] - (60*10);
			}
			$salesDatas		        = $purchaseDatassTemps['return'];
			$batchInsert	        = array();
			$batchInsertItems       = array();
			$batchInsertAddresss	= array();
			$batchUpdateItem        = array();
			$batchUpdate            = array(); 
			foreach($salesDatas as $account1Id => $salesData){
				$orderIds		= array_keys($salesData);
				$orderItemInfo	= array();
				$orderGoodsInfo = array();
				$orderInfo		= array();
				$tempItemDatas	= $this->db->select('id,orderId,createOrderId')->get_where('purchase_order',array('account1Id' => $account1Id))->result_array();  
				foreach($tempItemDatas as $tempItemData){
					$orderInfo[$tempItemData['orderId']]	= $tempItemData;
				}
				$allFetchedOrderIds	= array_keys($salesData);
				if($allFetchedOrderIds){
					$allFetchedOrderIds	= array_filter($allFetchedOrderIds);
					sort($allFetchedOrderIds);
					$this->ci->db->where_in('orderId',$allFetchedOrderIds)->delete('purchase_item');
				}				
				foreach($salesData as $orderId => $row){
					if(!$orderId){
						continue;
					}
					$isAllreadyInserted	= 0;
					if(@$orderInfo[$orderId]){
						$isAllreadyInserted		= 1;
						$row['orders']['id']	= $orderInfo[$orderId]['id'];
						$batchUpdate[]			= $row['orders'];
					}
					else{
						$batchInsert[]	= $row['orders']; 
						foreach ($row['address'] as $address) {
							$batchInsertAddresss[]	= $address;
						}
					}		
					if(@$row['items'])
					foreach ($row['items'] as $items) {
						if(@$orderItemInfo[$items['rowId']]){
							$items['id']			= $orderItemInfo[$items['rowId']]['id'];
							$batchUpdateItem[]		= $items;
						}
						else{						
							$batchInsertItems[]		= $items;
						}
					}				
				}
			}	
			$inserted		= 0;		
			$updateOrder	= 100;
			if($batchUpdate){
				$inserted		= '1';
				$batchUpdates	= array_chunk($batchUpdate,$updateOrder,true);
				foreach($batchUpdates as $batchUpdate){
					if($batchUpdate){
						$this->db->update_batch('purchase_order', $batchUpdate,'id');   
					}
				}				
			}
			if($batchUpdateItem){
				$inserted			= '1';
				$batchUpdateItems	= array_chunk($batchUpdateItem,$updateOrder,true);
				foreach($batchUpdateItems as $batchUpdateItem){
					if($batchUpdateItem){
						$this->db->update_batch('purchase_item', $batchUpdateItem,'id'); 
					}
				}
			}		
			if($batchInsert){
				$inserted		= '1';
				$batchInserts	= array_chunk($batchInsert,$updateOrder,true); 
				foreach($batchInserts as $batchInsert){
					$this->db->insert_batch('purchase_order', $batchInsert); 
				}
			}
			if($batchInsertAddresss){
				$batchInsertAddressss	= array_chunk($batchInsertAddresss,$updateOrder,true);
				foreach($batchInsertAddressss as $batchInsertAddresss){
					$this->db->insert_batch('purchase_address', $batchInsertAddresss);
				}
			}		
			if($batchInsertItems){
				$inserted			= '1';
				$batchInsertItemss	= array_chunk($batchInsertItems,$updateOrder,true);
				foreach($batchInsertItemss as $batchInsertItems){
					$this->db->insert_batch('purchase_item', $batchInsertItems);
				}
			}
			if($inserted){
				if(!$fetchby){
					$this->db->insert('cron_management', array('type' => 'purchase'.$fetchAccount1Id, 'runTime' => $cronTime, 'saveTime' => $saveTime)); 
				}
			}	
		}		
    }
    public function postPurchase($orderId = ''){
       $this->{$this->globalConfig['postPurchaseOrder']}->postPurchase($orderId);
    }
	public function fetchAcknowledgement(){
       $this->{$this->globalConfig['postPurchaseOrder']}->fetchAcknowledgement();
    }
	public function postAcknowledgement(){
       $this->{$this->globalConfig['postReceipt']}->postAcknowledgement();
    }	
	public function fetchReceipt(){
       $this->{$this->globalConfig['fetchReceipt']}->fetchReceipt();
    }	
	public function postReceipt(){
       $this->{$this->globalConfig['postReceipt']}->postReceipt();
    }	
	public function fetchInvoice(){
       $this->{$this->globalConfig['fetchReceipt']}->fetchInvoice();
    }	
	public function postInvoice(){
       $this->{$this->globalConfig['postReceipt']}->postInvoice();
    }
    public function getPurchase(){
        $groupAction		= $this->input->post('customActionType');
        $records			= array();
        $records["data"]	= array();
		if($this->input->post('order')){
 			$orderData	= array($this->router->directory.$this->router->class => $this->input->post('order'));
 			$this->session->set_userdata($orderData);
        }
        if ($groupAction == 'group_action') {
            $ids	= $this->input->post('id');
            if ($ids) {
                $status	= $this->input->post('customActionName');
                if ($status != '') {
                    $this->db->where_in('id', $ids)->update('purchase_order', array('status' => $status));
                    $records["customActionStatus"]  = "OK";
                    $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
                }
            }
        }
        $where	= array();
        $query	= $this->db;
        if ($this->input->post('action') == 'filter') {
            if (trim($this->input->post('orderId'))) {
                $where['orderId']		= trim($this->input->post('orderId'));
            }
            if (trim($this->input->post('createOrderId'))) {
                $where['createOrderId']	= trim($this->input->post('createOrderId'));
            }
            if (trim($this->input->post('createdOrderId'))) {
                $where['createOrderId'] = trim($this->input->post('createdOrderId'));
            }
            if (trim($this->input->post('customerId'))) {
                $where['customerId']	= trim($this->input->post('customerId'));
            }
			if (trim($this->input->post('type'))) {
                $where['type']			= trim($this->input->post('type'));
            }
            if (trim($this->input->post('customerEmail'))) {
                $where['customerEmail'] = trim($this->input->post('customerEmail'));
            }
            if (trim($this->input->post('paymentMethod'))) {
                $where['paymentMethod'] = trim($this->input->post('paymentMethod'));
            }
            if (trim($this->input->post('status')) >= '0') {
                $where['status']		= trim($this->input->post('status'));
            }
        }
        if (trim($this->input->post('updated_from'))) {
            $query->where('date(created) >= ', "date('" . $this->input->post('updated_from') . "')", false);
        }
        if (trim($this->input->post('updated_to'))) {
            $query->where('date(created) < ', "date('" . $this->input->post('updated_to') . "')", false);
        }
        if ($where) {
            $query->like($where);
        }
        $totalRecord	= @$query->select('count("id") as countpurchase')->get('purchase_order')->row_array()['countpurchase'];
        $limit       	= intval($this->input->post('length'));
        $limit       	= $limit < 0 ? $totalRecord : $limit;
        $start       	= intval($this->input->post('start'));
        $query			= $this->db;
        if (trim($this->input->post('updated_from'))) {
            $query->where('date(created) >= ', "date('" . $this->input->post('updated_from') . "')", false);
        }
        if (trim($this->input->post('updated_to'))) {
            $query->where('date(created) < ', "date('" . $this->input->post('updated_to') . "')", false);
        }
        if ($where) {
            $query->like($where);
        }
        $status              	= array('0' => 'Pending', '1' => 'Sent', '2' => 'Acknowledgement','3' => 'Received', '4' => 'Invoice', '5' => 'Archive');
        $statusColor			= array('0' => 'default', '1' => 'success', '2' => 'success', '3' => 'info', '4' => 'success', '5' => 'danger');
        $displayProRowHeader	= array('id', 'orderId','createOrderId', 'customerEmail', 'updated', 'status', 'message'); 
        if ($this->input->post('order')) {
            foreach ($this->input->post('order') as $ordering) {
                if (@$displayProRowHeader[$ordering['column']]) {
                    $query->order_by($displayProRowHeader[$ordering['column']], $ordering['dir']);
                }
            }
        }
        $datas	= $query->select('id,orderNo,createOrderId,customerEmail,customerEmail,orderId,updated,status,paymentMethod,type,typeDetails,rowData,dispatchConfirmation,cancelRequest,message,orderAcknowledged,acknowledgedFilename,dispatchFileName,salesFileName,invoiceRequest')->limit($limit, $start)->get('purchase_order')->result_array();
        foreach ($datas as $data) {
        	$params		= json_decode($data['rowData'],true);
			$message	= $data['message'];
			if($data['uninvoiced'] == 1){				
				$message	= '<span class="label label-sm label-info">Order Uninvoiced on Brightpearl</span>'; 
			}
			else if($data['uninvoiced'] == 2){				
				$message	= '<span class="label label-sm label-info">Invoice Void On Xero</span>'; 
			}
			if(($data['status'] == '1') && ($data['orderAcknowledged'])){ 
				$message	= 'Received order acknowledgement';
			}
			if(($data['status'] < '3') && ($data['invoiceRequest'])){
				$message	= 'Received invoice confirmation';
			}	
			else if(($data['status'] < '3') && ($data['dispatchConfirmation'])){
				$message	= '<span class="label label-sm label-success">Receipt confirmation received</span>';
			}
			$downloadHtml	= ''; 
			if($data['salesFileName']){
				$salesFileNames	= array_filter(explode(",",$data['salesFileName']));
				foreach($salesFileNames as $salesFileName){
					$downloadHtml .=	'<li>
											<a  href="'.base_url($salesFileName).'" download = "'.basename($salesFileName).'"> Download purchase file </a>
										</li>';
				}
			}
			if($data['acknowledgedFilename']){
				$downloadHtml	.=	'<li>
										<a  href="'.base_url($data['acknowledgedFilename']).'" download = "'.basename($data['acknowledgedFilename']).'"> Download acknowledgement file </a>
									</li>';
			}
			if($data['dispatchFileName']){
				$downloadHtml	.=	'<li>
										<a  href="'.base_url($data['dispatchFileName']).'" download = "'.basename($data['dispatchFileName']).'"> Download invoice file </a>
									</li>';
			}			
            $records["data"][]	= array(
                '<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
                '<a href="'.base_url('purchase/purchase/purchaseItem/'.$data['orderId']).'">'.$data['orderId'].'</a>',
                $data['createOrderId'],
                $data['customerEmail'],
                $data['updated'],
                '<span class="label label-sm label-' . $statusColor[$data['status']] . '">' . $status[$data['status']] . '</span>',
				($data['cancelRequest'])?('<span class="label label-sm label-danger">Cancel request received</span>'):($message),
                '<div class="btn-group">
					<a class="btn btn-circle btn-default dropdown-toggle" href="javascript:;" data-toggle="dropdown">
						<i class="fa fa-share"></i>
						<span class="hidden-xs"> Tools </span>
						<i class="fa fa-angle-down"></i>
					</a>
					<div class="dropdown-menu pull-right">
						'.$downloadHtml.'
						<li>
							<a class="btnactionsubmit" href="'.base_url('purchase/purchase/fetchpurchase/'.$data['orderId']).'"> Fetch Purchase Order </a>
						</li>
						<li>
							<a class="btnactionsubmit" href="'.base_url('purchase/purchase/postpurchase/'.$data['orderId']).'"> Post Purchase Order </a>
						</li>
						<li>
							<a target = "_blank" href="'.base_url('purchase/purchase/purchaseInfo/'.$data['orderId']).'"> Purchase Info </a>
						</li>
					</div>
				</div>',
            );
        }
        $draw                       = intval($this->input->post('draw'));
        $records["draw"]            = $draw;
        $records["recordsTotal"]    = $totalRecord;
        $records["recordsFiltered"]	= $totalRecord;
        return $records;
    }
	public function getPurchaseItem($orderId){
		$datas	= $this->db->get_where('purchase_item',array('orderId' => $orderId))->result_array();
		return $datas;
	}
}