<?php
class Purchasecredit_model extends CI_Model{
    public function __construct(){
        parent::__construct(); 
    }
    public function fetchPurchaseCredit($orderId = '', $accountId = '', $isWebhook = '0'){
		$fetchby				= $orderId;
		$saveTime				= date('c',strtotime('-4 hours')); 		
        $purchaseCreditDatas	= $this->{$this->globalConfig['fetchPurchaseCredit']}->fetchPurchaseCredit($orderId, $accountId);
		foreach($purchaseCreditDatas as $fetchAccount1Id => $purchaseCreditDatasTemps){
			if(@$purchaseCreditDatasTemps['saveTime']){
				$saveTime	= $purchaseCreditDatasTemps['saveTime'] - (60*10);
			}
			$salesDatas				= $purchaseCreditDatasTemps['return'];
			$batchInsert			= array();
			$batchInsertItems		= array();
			$batchInsertAddresss	= array();
			$batchUpdateItem		= array();
			$batchUpdate			= array(); 
			foreach($salesDatas as $account1Id => $salesData){
				$orderIds		= array_keys($salesData);
				$orderItemInfo	= array();
				$orderGoodsInfo = array();
				$orderInfo		= array();
				$tempItemDatas	= $this->db->select('id,orderId,createOrderId')->get_where('purchase_credit_order',array('account1Id' => $account1Id))->result_array();  
				foreach($tempItemDatas as $tempItemData){
					$orderInfo[$tempItemData['orderId']]	= $tempItemData;
				}
				$allFetchedOrderIds	= array_keys($salesData);
				if($allFetchedOrderIds){
					$allFetchedOrderIds	= array_filter($allFetchedOrderIds);
					sort($allFetchedOrderIds);
					$this->ci->db->where_in('orderId',$allFetchedOrderIds)->delete('purchase_credit_item');
				}
				foreach($salesData as $orderId => $row){
					if(!$orderId){
						continue;
					}
					if(@$orderInfo[$orderId]){
						$row['orders']['id']	= $orderInfo[$orderId]['id'];
						$batchUpdate[]			= $row['orders'];
					}
					else{
						$batchInsert[]	= $row['orders']; 
						foreach ($row['address'] as $address) {
							$batchInsertAddresss[]	= $address;
						}
					}		
					if(@$row['items']){
						foreach ($row['items'] as $items) {
							if(@$orderItemInfo[$items['rowId']]){
								$items['id']		= $orderItemInfo[$items['rowId']]['id'];
								$batchUpdateItem[]	= $items;
							}
							else{						
								$batchInsertItems[]	= $items;
							}
						}				
					}
				}
			}	
			$inserted		= 0;		
			$updateOrder	= 100;
			if($batchUpdate){
				$inserted		= '1';
				$batchUpdates	= array_chunk($batchUpdate,$updateOrder,true);
				foreach($batchUpdates as $batchUpdate){
					if($batchUpdate)
					$this->db->update_batch('purchase_credit_order', $batchUpdate,'id');   
				}				
			}
			if($batchUpdateItem){
				$inserted			= '1';
				$batchUpdateItems	= array_chunk($batchUpdateItem,$updateOrder,true);
				foreach($batchUpdateItems as $batchUpdateItem){
					if($batchUpdateItem){
						$this->db->update_batch('purchase_credit_item', $batchUpdateItem,'id'); 
					}
				}
			}		
			if($batchInsert){
				$inserted		= '1';
				$batchInserts	= array_chunk($batchInsert,$updateOrder,true); 
				foreach($batchInserts as $batchInsert){
					$this->db->insert_batch('purchase_credit_order', $batchInsert); 
				}
			}
			if($batchInsertAddresss){
				$batchInsertAddressss	= array_chunk($batchInsertAddresss,$updateOrder,true);
				foreach($batchInsertAddressss as $batchInsertAddresss){
					$this->db->insert_batch('purchase_credit_address', $batchInsertAddresss);
				}
			}		
			if($batchInsertItems){
				$inserted			= '1';
				$batchInsertItemss	= array_chunk($batchInsertItems,$updateOrder,true);
				foreach($batchInsertItemss as $batchInsertItems){
					$this->db->insert_batch('purchase_credit_item', $batchInsertItems);
				}
			}
			if($inserted){
				if(!$fetchby){
					$this->db->insert('cron_management', array('type' => 'purchaseCredit'.$fetchAccount1Id, 'runTime' => $cronTime, 'saveTime' => $saveTime)); 
				}
			} 	
		}		
    }
    public function postPurchaseCredit($orderId = ''){
       $this->{$this->globalConfig['postPurchaseCredit']}->postPurchaseCredit($orderId);	   
    }
	public function fetchAcknowledgement(){
       $this->{$this->globalConfig['postPurchaseCredit']}->fetchAcknowledgement();
    }
	public function postAcknowledgement(){
       $this->{$this->globalConfig['postReceipt']}->postAcknowledgement();
    }	
	public function fetchReceipt(){
       $this->{$this->globalConfig['fetchReceipt']}->fetchReceipt();
    }	
	public function postReceipt(){
       $this->{$this->globalConfig['postReceipt']}->postReceipt();
    }	
	public function fetchInvoice(){
       $this->{$this->globalConfig['fetchReceipt']}->fetchInvoice();
    }	
	public function postInvoice(){
       $this->{$this->globalConfig['postReceipt']}->postInvoice();
    }
    public function getCredit(){
        $groupAction		= $this->input->post('customActionType');
        $records			= array();
        $records["data"]	= array();
        if ($groupAction == 'group_action') {
            $ids	= $this->input->post('id');
            if ($ids) {
                $status	= $this->input->post('customActionName');
                if ($status != '') {
                    $this->db->where_in('id', $ids)->update('purchase_credit_order', array('status' => $status));
                    $records["customActionStatus"]	= "OK";
                    $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
                }
            }
        }
        $where	= array();
        $query	= $this->db;
        if ($this->input->post('action') == 'filter') {
            if (trim($this->input->post('orderId'))) {
                $where['orderId']		= trim($this->input->post('orderId'));
            }
            if (trim($this->input->post('createOrderId'))) {
                $where['createOrderId'] = trim($this->input->post('createOrderId'));
            }
            if (trim($this->input->post('orderNo'))) {
                $where['orderNo']		= trim($this->input->post('orderNo'));
            }
            if (trim($this->input->post('customerId'))) {
                $where['customerId']	= trim($this->input->post('customerId'));
            }
			if (trim($this->input->post('type'))) {
                $where['type']			= trim($this->input->post('type'));
            }
            if (trim($this->input->post('customerEmail'))) {
                $where['customerEmail'] = trim($this->input->post('customerEmail'));
            }
            if (trim($this->input->post('paymentMethod'))) {
                $where['paymentMethod'] = trim($this->input->post('paymentMethod'));
            }
            if (trim($this->input->post('status')) >= '0') {
                $where['status']		= trim($this->input->post('status'));
            }
        }
        if (trim($this->input->post('updated_from'))) {
            $query->where('date(created) >= ', "date('" . $this->input->post('updated_from') . "')", false);
        }
        if (trim($this->input->post('updated_to'))) {
            $query->where('date(created) < ', "date('" . $this->input->post('updated_to') . "')", false);
        }
        if ($where) {
            $query->like($where);
        }
        $totalRecord	= @$query->select('count("id") as countpurchase')->get('purchase_credit_order')->row_array()['countpurchase'];
        $limit       	= intval($this->input->post('length'));
        $limit       	= $limit < 0 ? $totalRecord : $limit;
        $start       	= intval($this->input->post('start'));
        $query			= $this->db;
        if (trim($this->input->post('updated_from'))) {
            $query->where('date(created) >= ', "date('" . $this->input->post('updated_from') . "')", false);
        }
        if (trim($this->input->post('updated_to'))) {
            $query->where('date(created) < ', "date('" . $this->input->post('updated_to') . "')", false);
        }
        if ($where) {
            $query->like($where);
        }
        $status              	= array('0' => 'Pending', '1' => 'Sent', '2' => 'Acknowledgement','3' => 'Received', '4' => 'Invoice', '5' => 'Archive');
        $statusColor         	= array('0' => 'default', '1' => 'success', '2' => 'success', '3' => 'info', '4' => 'success', '5' => 'danger');
        $displayProRowHeader	= array('id', 'orderId', 'customerEmail', 'updated', 'status', 'message'); 
        if ($this->input->post('order')) {
            foreach ($this->input->post('order') as $ordering) {
                if (@$displayProRowHeader[$ordering['column']]) {
                    $query->order_by($displayProRowHeader[$ordering['column']], $ordering['dir']);
                }
            }
        }
        $datas	= $query->select('id,orderNo,createOrderId,customerEmail,customerEmail,orderId,updated,status,paymentMethod,type,typeDetails,rowData,dispatchConfirmation,cancelRequest,message,orderAcknowledged,acknowledgedFilename,dispatchFileName,salesFileName,invoiceRequest')->limit($limit, $start)->get('purchase_credit_order')->result_array();
        foreach ($datas as $data) {
        	$params		= json_decode($data['rowData'],true);
			$message	= $data['message'];
			if(($data['status'] == '1') && ($data['orderAcknowledged'])){ 
				$message	= 'Received order acknowledgement';
			}
			if(($data['status'] <= '3') && ($data['invoiceRequest'])){
				$message	= 'Received invoice confirmation';
			}		
			$downloadHtml	= ''; 
			if($data['salesFileName']){
				$downloadHtml	.=	'<li>
										<a  href="'.base_url($data['salesFileName']).'" download = "'.basename($data['salesFileName']).'"> Download purchase file </a>
									</li>';
			}
			if($data['acknowledgedFilename']){
				$downloadHtml	.=	'<li>
										<a  href="'.base_url($data['acknowledgedFilename']).'" download = "'.basename($data['acknowledgedFilename']).'"> Download acknowledgement file </a>
									</li>';
			}
			if($data['dispatchFileName']){
				$downloadHtml	.=	'<li>
										<a  href="'.base_url($data['dispatchFileName']).'" download = "'.basename($data['dispatchFileName']).'"> Download invoice file </a>
									</li>';
			}
            $records["data"][]	= array(
                '<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
                '<a href="'.base_url('purchase/credit/creditItem/'.$data['orderId']).'">'.$data['orderId'].'</a>',
                $data['createOrderId'],
                $data['customerEmail'],
                $data['updated'],
                '<span class="label label-sm label-' . $statusColor[$data['status']] . '">' . $status[$data['status']] . '</span>',
				($data['cancelRequest'])?('<span class="label label-sm label-danger">Cancel request received</span>'):($message),
                '<div class="btn-group">
					<a class="btn btn-circle btn-default dropdown-toggle" href="javascript:;" data-toggle="dropdown">
						<i class="fa fa-share"></i>
						<span class="hidden-xs"> Tools </span>
						<i class="fa fa-angle-down"></i>
					</a>
					<div class="dropdown-menu pull-right">
						'.$downloadHtml.'
						<li>
							<a class="btnactionsubmit" href="'.base_url('purchase/credit/fetchPurchaseCredit/'.$data['orderId']).'"> Fetch Purchase Credit </a>
						</li>
						<li>
							<a class="btnactionsubmit" href="'.base_url('purchase/credit/postPurchaseCredit/'.$data['orderId']).'"> Post Purchase Credit </a>
						</li>
						<li>
							<a target = "_blank" href="'.base_url('purchase/credit/purchaseInfo/'.$data['orderId']).'"> Purchase Info </a>
						</li>
					</div>
				</div>',
            );
        }
        $draw                       = intval($this->input->post('draw'));
        $records["draw"]            = $draw;
        $records["recordsTotal"]    = $totalRecord;
        $records["recordsFiltered"]	= $totalRecord;
        return $records;
    }
	public function getCreditItem($orderId){
		$datas	= $this->db->get_where('purchase_credit_item',array('orderId' => $orderId))->result_array();
		return $datas;
	}
}