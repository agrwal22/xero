<?php
class Adjustment_model extends CI_Model {
    public function __construct(){
        parent::__construct();
    }
    public function fetchStockAdjustment($orderId = '', $accountId = ''){
		$fetchby		= $orderId;
		$saveTime		= date('Y-m-d\TH:i:s',strtotime('-250 min'));
        $salesDatass	= $this->{$this->globalConfig['fetchStockAdjustment']}->fetchStockAdjustment($orderId, $accountId);
		foreach($salesDatass as $fetchAccount1Id => $salesDatassTemps){
			if(@$salesDatassTemps['saveTime']){
				$saveTime	= $salesDatassTemps['saveTime'] - (60*10);
			}
			$salesDatas			= $salesDatassTemps['return'];
			$batchInsert		= array();
			$batchGoodsInserts	= array();
			$batchGoodsInsert	= array(); 
			foreach($salesDatas as $account1Id => $salesData){
				$orderIds		= array_keys($salesData);
				$orderInfo		= array();
				$tempItemDatas	= $this->db->select('id,orderId')->get_where('stock_adjustment',array('account1Id' => $account1Id))->result_array();  
				foreach($tempItemDatas as $tempItemData){
					$orderInfo[$tempItemData['orderId']]	= $tempItemData;
				}
				foreach($salesData as $orderId => $row){
					if(!$orderId){
						continue;
					}
					$isAllreadyInserted = 0;
					if(@$orderInfo[$orderId]){
						continue;
					}
					foreach ($row as $productId => $gitems) {
						$batchGoodsInsert[]	= $gitems;											
					}
				}
			}
			$inserted		= 0;
			$updateOrder	= 500;
			if($batchGoodsInsert){ 
				$inserted			= '1';
				$batchGoodsInserts	= array_chunk($batchGoodsInsert,$updateOrder,true);
				foreach($batchGoodsInserts as $batchGoodsInsert){
					$this->db->insert_batch('stock_adjustment', $batchGoodsInsert);
				}
			}
			if($inserted){
				if(!$fetchby){
					$this->db->insert('cron_management', array('type' => 'stockadjustment'.$fetchAccount1Id, 'runTime' => $cronTime, 'saveTime' => $saveTime)); 
				}
			}
		}
    }
	public function postStockAdjustment($id){
        $inventoryDatas	= $this->{$this->globalConfig['postStockAdjustment']}->postStockAdjustment($id);
    }
	public function getAdjustment(){
        $groupAction		= $this->input->post('customActionType');
        $records			= array();
        $records["data"]	= array();
		if($this->input->post('order')){
 			$orderData	= array($this->router->directory.$this->router->class => $this->input->post('order'));
 			$this->session->set_userdata($orderData);
        }
        if ($groupAction == 'group_action') {
            $ids	= $this->input->post('id');
            if ($ids) {
                $status	= $this->input->post('customActionName');
                if ($status != '') {
                    $this->db->where_in('id', $ids)->update('stock_adjustment', array('status' => $status));
                    $records["customActionStatus"]	= "OK"; // pass custom message(useful for getting status of group actions)
                    $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
                }
            }
        }
        $where	= array();
        $query	= $this->db;
        if ($this->input->post('action') == 'filter') {            
            if (trim($this->input->post('tradingIdPartnerId'))) {
                $where['tradingIdPartnerId']	= trim($this->input->post('tradingIdPartnerId'));
            }
			if (trim($this->input->post('filename'))) {
                $where['filename']				= trim($this->input->post('filename'));
            }	
			if (trim($this->input->post('warehouseId'))) {
                $where['warehouseId']			= trim($this->input->post('warehouseId'));
            }	
			if (trim($this->input->post('orderId'))) {
                $where['orderId']				= trim($this->input->post('orderId'));
            }	
			if (trim($this->input->post('ActivityId'))) {
                $where['ActivityId']			= trim($this->input->post('ActivityId'));
            }
			if (trim($this->input->post('GoodNotetype'))) {
                $where['GoodNotetype']			= trim($this->input->post('GoodNotetype'));
            }
            if (trim($this->input->post('productId'))) {
                $where['productId']				= trim($this->input->post('productId'));
            }
            if (trim($this->input->post('status')) >= '0') {
                $where['status']				= trim($this->input->post('status'));
            }
        }
        if (trim($this->input->post('updated_from'))) {
            $query->where('date(created) >= ', "date('" . $this->input->post('updated_from') . "')", false);
        }
        if (trim($this->input->post('updated_to'))) {
            $query->where('date(created) < ', "date('" . $this->input->post('updated_to') . "')", false);
        }
        if ($where) {
            $query->like($where);
        }
        $totalRecord	= @$query->select('count("id") as countpro')->get('stock_adjustment')->row_array()['countpro'];
        $limit			= intval($this->input->post('length'));
        $limit			= $limit < 0 ? $totalRecord : $limit;
        $start			= intval($this->input->post('start'));
        $query			= $this->db;
        if (trim($this->input->post('updated_from'))) {
            $query->where('date(created) >= ', "date('" . $this->input->post('updated_from') . "')", false);
        }
        if (trim($this->input->post('updated_to'))) {
            $query->where('date(created) < ', "date('" . $this->input->post('updated_to') . "')", false);
        }
        if ($where) {
            $query->like($where);
        }
        $status              	= array('0' => 'Pending', '1' => 'Sent', '2' => 'Updated', '3' => 'Error', '4' => 'Archive');
        $statusColor         	= array('0' => 'default', '1' => 'success', '2' => 'info', '3' => 'warning', '4' => 'danger');
        $displayProRowHeader	= array('id','ActivityId','account1Id','account2Id', 'orderId','warehouseId', 'productId', 'qty', 'price','GoodNotetype','created', 'status');
        if ($this->input->post('order')) {
            foreach ($this->input->post('order') as $ordering) {
                if (@$displayProRowHeader[$ordering['column']]) {
                    $query->order_by($displayProRowHeader[$ordering['column']], $ordering['dir']);
                }
            }
        }
        $datas	= $query->limit($limit, $start)->get('stock_adjustment')->result_array();
		$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['fetchSalesOrder'].'_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){			
			$account1Mappings[$account1MappingTemp['id']]	= $account1MappingTemp;
		}
		$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postSalesOrder'].'_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){			
			$account2Mappings[$account2MappingTemp['id']]	= $account2MappingTemp;
		}
        foreach ($datas as $data) {
			$postID	= $data['orderId'];
			if($data['GoodNotetype'] == 'GO'){
				$transfertype	= 'Inventory Transfer';
				$postID			= $data['ActivityId'];
			}
			if($data['GoodNotetype'] == 'SC'){
				$transfertype	= 'Inventory Adjustment';
				$postID			= $data['orderId'];
			}
            $records["data"][]	= array(
                '<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
				@$account1Mappings[$data['account1Id']]['name'],
                @$account2Mappings[$data['account2Id']]['name'],
                $data['orderId'],
				$data['ActivityId'],
                $data['warehouseId'],               
                $data['productId'],
                $data['qty'],
                $data['price'],
				$transfertype,
                $data['created'],
                '<span class="label label-sm label-' . $statusColor[$data['status']] . '">' . $status[$data['status']] . '</span>',
                '<div class="btn-group">
                    <a class="btn btn-circle btn-default dropdown-toggle" href="javascript:;" data-toggle="dropdown">
                        <i class="fa fa-share"></i>
                        <span class="hidden-xs"> Tools </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <div class="dropdown-menu pull-right">
                        <li>
                            <a class="btnactionsubmit" href="' . base_url('stock/adjustment/postStockAdjustment/'.$postID) . '"> Post Stock Adjustment </a>
                        </li>  
						<li>
                            <a class="" target="_blnak" href="' . base_url('stock/adjustment/adjustmentinfo/'.$data['id']) . '"> Stock Adjustment  Info</a>
                        </li>  
						
                    </div>
                </div>',
            );
		}
        $draw                       = intval($this->input->post('draw'));
        $records["draw"]            = $draw;
        $records["recordsTotal"]    = $totalRecord;
        $records["recordsFiltered"]	= $totalRecord;
        return $records;
    }
}