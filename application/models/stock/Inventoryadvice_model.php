<?php 
class Inventoryadvice_model extends CI_Model {
    public function __construct(){
        parent::__construct();
    }
    public function fetchInventoradvice(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts();
        $inventoryDatas = $this->{$this->globalConfig['fetchInventoradvice']}->fetchInventoradvice();
		$this->{$this->globalConfig['postInventoradvice']}->fetchInventoradvice($inventoryDatas); 
    }
   public function postInventoradvice($id){
        $inventoryDatas     = $this->{$this->globalConfig['postInventoradvice']}->postInventoradvice($id);
    }
    public function getInventoradvice(){
        $groupAction        = $this->input->post('customActionType');
        $records            = array();
        $records["data"]    = array();
		if($this->input->post('order')){
 			$orderData      = array($this->router->directory.$this->router->class => $this->input->post('order'));
 			$this->session->set_userdata($orderData);
        }
        if ($groupAction == 'group_action') {
            $ids    = $this->input->post('id');
            if ($ids) {
                $status = $this->input->post('customActionName');
                if ($status != '') {
                    $this->db->where_in('id', $ids)->update('stock_sync', array('status' => $status));
                    $records["customActionStatus"]  = "OK"; // pass custom message(useful for getting status of group actions)
                    $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
                }
            }
        }
        $where  = array();
        $query  = $this->db;
        if ($this->input->post('action') == 'filter') {            
            if (trim($this->input->post('tradingIdPartnerId'))) {
                $where['tradingIdPartnerId']    = trim($this->input->post('tradingIdPartnerId'));
            }
			if (trim($this->input->post('filename'))) {
                $where['filename']              = trim($this->input->post('filename'));
            }	
			if (trim($this->input->post('warehouseId'))) {
                $where['warehouseId']           = trim($this->input->post('warehouseId'));
            }	
            if (trim($this->input->post('size'))) {
                $where['size']                  = trim($this->input->post('size'));
            }
            if (trim($this->input->post('status')) >= '0') {
                $where['status']                = trim($this->input->post('status'));
            }
        }
        if (trim($this->input->post('updated_from'))) {
            $query->where('date(created) >= ', "date('" . $this->input->post('updated_from') . "')", false);
        }
        if (trim($this->input->post('updated_to'))) {
            $query->where('date(created) < ', "date('" . $this->input->post('updated_to') . "')", false);
        }
        if ($where) {
            $query->like($where);
        }
        $totalRecord    = @$query->select('count("id") as countpro')->get('stock_inventoryadvice')->row_array()['countpro'];
        $limit          = intval($this->input->post('length'));
        $limit          = $limit < 0 ? $totalRecord : $limit;
        $start          = intval($this->input->post('start'));
        $query          = $this->db;
        if (trim($this->input->post('updated_from'))) {
            $query->where('date(created) >= ', "date('" . $this->input->post('updated_from') . "')", false);
        }
        if (trim($this->input->post('updated_to'))) {
            $query->where('date(created) < ', "date('" . $this->input->post('updated_to') . "')", false);
        }
        if ($where) {
            $query->like($where);
        }
        $status                 = array('0' => 'Pending', '1' => 'Sent', '2' => 'Updated', '3' => 'Error', '4' => 'Archive');
        $statusColor            = array('0' => 'default', '1' => 'success', '2' => 'info', '3' => 'warning', '4' => 'danger');
        $displayProRowHeader    = array('id', 'tradingIdPartnerId', 'filename', 'noOfProduct', 'warehouseId', 'created', 'updated', 'status');
        if ($this->input->post('order')) {
            foreach ($this->input->post('order') as $ordering) {
                if (@$displayProRowHeader[$ordering['column']]) {
                    $query->order_by($displayProRowHeader[$ordering['column']], $ordering['dir']);
                }
            }
        }
        $datas  = $query->limit($limit, $start)->get('stock_inventoryadvice')->result_array();
        foreach ($datas as $data) {
            $records["data"][]  = array(
                '<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
				'<a href="'.base_url($data['filename']).'" download="'.basename($data['filename']).'">'. basename($data['filename']).' </a>',
                $data['noOfProduct'],
                $data['warehouseId'],
                $data['updated'],
                $data['created'],
                '<span class="label label-sm label-' . $statusColor[$data['status']] . '">' . $status[$data['status']] . '</span>',
                '<div class="btn-group">
                    <a class="btn btn-circle btn-default dropdown-toggle" href="javascript:;" data-toggle="dropdown">
                        <i class="fa fa-share"></i>
                        <span class="hidden-xs"> Tools </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <div class="dropdown-menu pull-right">
                        <li>
                            <a class="btnactionsubmit" href="' . base_url('stock/inventoryadvice/postInventoradvice/'.$data['id']) . '"> Post Inventory Advice </a>
                        </li>
                        <li>
                            <a href="'.base_url($data['filename']).'" download="'.basename($data['filename']).'"> Download Inventory Advice File </a>
                        </li>                       
                    </div>
                </div>',
            );
        }
        $draw                       = intval($this->input->post('draw'));
        $records["draw"]            = $draw;
        $records["recordsTotal"]    = $totalRecord;
        $records["recordsFiltered"] = $totalRecord;
        return $records;
    }
}