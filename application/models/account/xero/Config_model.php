<?php
class Config_model extends CI_Model{
    public function get($type = ''){
        $data				= array();
        $data['data']		= $this->db->get('account_xero_config')->result_array();
		$saveAccountTemps	= $this->db->get('account_xero_account')->result_array();
		$saveAccount		= array();
		foreach($saveAccountTemps as $saveAccountTemp){
			$saveAccount[$saveAccountTemp['id']]	= $saveAccountTemp;
		}
        $data['saveAccount']	= $saveAccount;
        if ($type == 'account1') {
			//$data['pricelist']		= $this->{$this->globalConfig['account1Liberary']}->getAllPriceList();
        }
		else {
			$data['accountInfo']		= $this->{$this->globalConfig['account2Liberary']}->getOrganizationInfo();
			$data['IncomeAccountRef']	= $this->{$this->globalConfig['account2Liberary']}->getAccountDetails();
			$data['PaymentMethodRef']	= $this->{$this->globalConfig['account2Liberary']}->getAllPaymentMethod(); 
			$data['getAllTax']			= $this->{$this->globalConfig['account2Liberary']}->getAllTax(); 
			$data['channel']			= $this->{$this->globalConfig['account1Liberary']}->getAllChannel();
			$data['warehouse']			= $this->{$this->globalConfig['account1Liberary']}->getAllLocation();
			$data['pricelist']			= $this->{$this->globalConfig['account1Liberary']}->getAllPriceList();
			$data['prepaymentAccount']	= $this->{$this->globalConfig['account1Liberary']}->getAllPaymentMethod();
		}
		return $data;
    }
    public function delete($id){
        $this->db->where(array('id' => $id))->delete('account_xero_config');
    }
    public function save($data){
        $shopifyAccount	= $this->db->get_where('account_xero_account', array('id' => $data['xeroAccountId']))->row_array();
        $data['name']	= $shopifyAccount['name'];
		if(@is_array($data['channelIds'])){
			$data['channelIds']	= $data['channelIds'] ? implode(",",$data['channelIds']) : "";
		}
		if(@is_array($data['warehouses'])){
			$data['warehouses']	= $data['warehouses'] ? implode(",",$data['warehouses']) : ""; 
		}
        if ($data['id']) {
            $status	= $this->db->where(array('id' => $data['id']))->update('account_xero_config', $data);
        }
		else {
            $saveConfig	= $this->db->get_where('account_xero_config', array('xeroAccountId' => $data['xeroAccountId']))->row_array();
            if ($saveConfig) {
                $data['id'] = $saveConfig['id'];
                $status     = $this->db->where(array('id' => $data['id']))->update('account_xero_config', $data);
            }
			else {
                $status     = $this->db->insert('account_xero_config', $data);
                $data['id'] = $this->db->insert_id();
            }
        }
        $data	= $this->db->get_where('account_xero_config', array('id' => $data['id']))->row_array();
        if ($data['id']) {
            $data['status']	= '1';
        }
        return $data;
    }
}