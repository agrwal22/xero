<?php
class Config_model extends CI_Model{
    public function __construct(){
        parent::__construct();
        $this->ci = get_instance();
    }
    public function get($type = ''){
        $data                   = array();
        $data['data']           = $this->db->get('account_brightpearl_config')->result_array();
        $data['saveAccount']    = $this->db->get('account_brightpearl_account')->result_array();
        if($type == 'account1'){
            $data['accountinfo']            = $this->{$this->globalConfig['account1Liberary']}->getAccountInfo();
            $data['pricelist']              = $this->{$this->globalConfig['account1Liberary']}->getAllPriceList();
            $data['warehouse']              = $this->{$this->globalConfig['account1Liberary']}->getAllLocation();
            $data['channel']                = $this->{$this->globalConfig['account1Liberary']}->getAllChannel();
            $data['orderstatus']            = $this->{$this->globalConfig['account1Liberary']}->getAllOrderStatus();
            $data['shippingmethods']        = $this->{$this->globalConfig['account1Liberary']}->getAllShippingMethod();
            $data['tax']                    = $this->{$this->globalConfig['account1Liberary']}->getAllTax();
            $data['tag']                    = $this->{$this->globalConfig['account1Liberary']}->getAllTag();
            $data['nominalCode']            = $this->{$this->globalConfig['account1Liberary']}->nominalCode();
            $data['defaultPaymentMethod']   = $this->{$this->globalConfig['account1Liberary']}->getAllPaymentMethod();
        }
        else{
            $data['accountinfo']            = $this->{$this->globalConfig['account2Liberary']}->getAccountInfo();
            $data['pricelist']              = $this->{$this->globalConfig['account2Liberary']}->getAllPriceList();
            $data['warehouse']              = $this->{$this->globalConfig['account2Liberary']}->getAllLocation();
            $data['channel']                = $this->{$this->globalConfig['account2Liberary']}->getAllChannel();
            $data['orderstatus']            = $this->{$this->globalConfig['account2Liberary']}->getAllOrderStatus();
            $data['shippingmethods']        = $this->{$this->globalConfig['account2Liberary']}->getAllShippingMethod();
            $data['tax']                    = $this->{$this->globalConfig['account2Liberary']}->getAllTax();
            $data['tag']                    = $this->{$this->globalConfig['account2Liberary']}->getAllTag();
            $data['nominalCode']            = $this->{$this->globalConfig['account2Liberary']}->nominalCode(); 
			$data['defaultPaymentMethod']   = $this->{$this->globalConfig['account1Liberary']}->getAllPaymentMethod();
        }
        return $data;
    }
    public function delete($id){
        $this->db->where(array('id' => $id))->delete('account_brightpearl_config');
    }
    public function save($data){
        $shopifyAccount = $this->db->get_where('account_brightpearl_account', array('id' => $data['brightpearlAccountId']))->row_array();
        $data['name']   = $shopifyAccount['name'];
		if(@is_array($data['fetchSalesOrderStatus'])){
            $data['fetchSalesOrderStatus']  = $data['fetchSalesOrderStatus'] ? implode(",",$data['fetchSalesOrderStatus']) : "";
        }
		if(@is_array($data['warehouseSales'])){
            $data['warehouseSales']         = $data['warehouseSales'] ? implode(",",$data['warehouseSales']) : "";
        }
		if(@is_array($data['channelId'])){
            $data['channelId']              = $data['channelId'] ? implode(",",$data['channelId']) : "";
        }
        if ($data['id']) {
            $status = $this->db->where(array('id' => $data['id']))->update('account_brightpearl_config', $data);
        }
        else {
            $saveConfig = $this->db->get_where('account_brightpearl_config', array('brightpearlAccountId' => $data['brightpearlAccountId']))->row_array();
            if ($saveConfig) {
                $data['id'] = $saveConfig['id'];
                $status     = $this->db->where(array('id' => $data['id']))->update('account_brightpearl_config', $data);
            }
            else {
                $status     = $this->db->insert('account_brightpearl_config', $data);
                $data['id'] = $this->db->insert_id();
            }
        }
        $data   = $this->db->get_where('account_brightpearl_config', array('id' => $data['id']))->row_array();
        if ($data['id']) {
            $data['status'] = '1';
        }
        return $data;
    }
}