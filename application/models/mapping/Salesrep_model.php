<?php
class Salesrep_model extends CI_Model{
	public function get(){
		$data				= array();
		$data['data']		= $this->db->get('mapping_salesrep')->result_array();
		$account1IdTemps	= $this->db->get('account_'.$this->globalConfig['account1Liberary'].'_account')->result_array();
		$account1Id			= array();
		foreach ($account1IdTemps as $account1IdTemp) {
			$account1Id[$account1IdTemp['id']]	= $account1IdTemp;
		}
		$account2IdTemps	= $this->db->get('account_'.$this->globalConfig['account2Liberary'].'_account')->result_array();
		$account2Id			= array();
		foreach ($account2IdTemps as $account2IdTemp) {
			$account2Id[$account2IdTemp['id']]	= $account2IdTemp;
		}
		$data['account1Id'] = $account1Id;
		$data['account2Id'] = $account2Id;
		return $data;
	}
	public function delete($id){
		$this->db->where(array('id' => $id))->delete('mapping_salesrep');
	}
	public function save($data){
		if($data['id']){
			$data['status']	= $this->db->where(array('id' => $data['id']))->update('mapping_salesrep',$data);
		}
		else{
			$data['status'] = $this->db->insert('mapping_salesrep',$data);
			$data['id']		= $this->db->insert_id();
		}
		return $data;
	}
}
?>