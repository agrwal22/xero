<?php
class Credit_model extends CI_Model{	
    public function __construct() {
        parent::__construct();
    }
    public function fetchSalesCredit($orderId = '', $accountId = '', $isWebhook = '0'){		
		$fetchby			= $orderId;
		$saveTime			= date('c',strtotime('-4 hours')); 	
        $salesCreditDatas	= $this->{$this->globalConfig['fetchSalesCredit']}->fetchSalesCredit($orderId, $accountId);
		$fatchedOrderIds	= array();
		foreach($salesCreditDatas as $fetchAccount1Id => $salesCreditDatasTemps){
			if(@$salesCreditDatasTemps['saveTime']){
				$saveTime	= $salesCreditDatasTemps['saveTime'] - (60*10);
			}
			$salesDatas				= $salesCreditDatasTemps['return'];
			$batchInsert			= array();
			$batchInsertItems 		= array();
			$batchInsertAddresss	= array();
			$batchUpdateItem		= array();
			$batchUpdate			= array(); 
			foreach($salesDatas as $account1Id => $salesData){
				$orderIds		= array_keys($salesData);
				$orderItemInfo	= array();
				$orderGoodsInfo = array();
				$orderInfo		= array();
				$tempItemDatas	= $this->db->select('id,orderId,createOrderId')->get_where('sales_credit_order',array('account1Id' => $account1Id))->result_array();  
				foreach($tempItemDatas as $tempItemData){
					$orderInfo[$tempItemData['orderId']]	= $tempItemData;
				}
				$allFetchedOrderIds	= array_keys($salesData);
				if($allFetchedOrderIds){
					$allFetchedOrderIds	= array_filter($allFetchedOrderIds);
					sort($allFetchedOrderIds);
					$this->ci->db->where_in('orderId',$allFetchedOrderIds)->delete('sales_credit_item');
				}
				foreach($salesData as $orderId => $row){
					if(!$orderId){
						continue;
					}
					$fatchedOrderIds[]	= $orderId;
					if(@$orderInfo[$orderId]){
						$row['orders']['id']	= $orderInfo[$orderId]['id'];
						$batchUpdate[]			= $row['orders'];
					}
					else{
						$batchInsert[]	= $row['orders']; 
						foreach ($row['address'] as $address) {
							$batchInsertAddresss[]	= $address;
						}
					}		
					if(@$row['items']){
						foreach ($row['items'] as $items) {
							if(@$orderItemInfo[$items['rowId']]){
								$items['id']		= $orderItemInfo[$items['rowId']]['id'];
								$batchUpdateItem[]	= $items;
							}
							else{						
								$batchInsertItems[]	= $items;
							}
						}				
					}
				}
			}	
			$inserted		= 0;		
			$updateOrder	= 100;
			if($batchUpdate){
				$inserted		= '1';
				$batchUpdates	= array_chunk($batchUpdate,$updateOrder,true);
				foreach($batchUpdates as $batchUpdate){
					if($batchUpdate){
						$this->db->update_batch('sales_credit_order', $batchUpdate,'id');   
					}
				}				
			}
			if($batchUpdateItem){
				$inserted			= '1';
				$batchUpdateItems	= array_chunk($batchUpdateItem,$updateOrder,true);
				foreach($batchUpdateItems as $batchUpdateItem){
					if($batchUpdateItem){
						$this->db->update_batch('sales_credit_item', $batchUpdateItem,'id'); 
					}
				}
			}		
			if($batchInsert){
				$inserted		= '1';
				$batchInserts	= array_chunk($batchInsert,$updateOrder,true); 
				foreach($batchInserts as $batchInsert){
					$this->db->insert_batch('sales_credit_order', $batchInsert); 
				}
			}
			if($batchInsertAddresss){
				$batchInsertAddressss	= array_chunk($batchInsertAddresss,$updateOrder,true);
				foreach($batchInsertAddressss as $batchInsertAddresss){
					$this->db->insert_batch('sales_credit_address', $batchInsertAddresss);
				}
			}		
			if($batchInsertItems){
				$inserted			= '1';
				$batchInsertItemss	= array_chunk($batchInsertItems,$updateOrder,true);
				foreach($batchInsertItemss as $batchInsertItems){
					$this->db->insert_batch('sales_credit_item', $batchInsertItems);
				}
			}
			if($inserted){
				if(!$fetchby){
					$this->db->insert('cron_management', array('type' => 'salesCredit'.$fetchAccount1Id, 'runTime' => $cronTime, 'saveTime' => $saveTime)); 
				}
			}
		}
		$this->{$this->globalConfig['fetchSalesCredit']}->fetchSalesCreditPayment($fatchedOrderIds);
		
    }
	public function postSalesCredit($orderId = ''){ 
		$this->{$this->globalConfig['postSalesCredit']}->postSalesCredit($orderId);
		$this->{$this->globalConfig['fetchSalesCredit']}->postSalesCreditPayment($orderId);
    }
	public function fetchSalesCreditConfirmation($orderId = '', $accountId = '', $isWebhook = '0'){		 
		$this->{$this->globalConfig['postSalesCredit']}->fetchSalesCreditConfirmation($orderId, $accountId, $isWebhook );
	}
	public function postSalesCreditConfimation($orderId = '', $accountId = '', $isWebhook = '0'){
		$this->{$this->globalConfig['fetchSalesCredit']}->postSalesCreditConfimation($orderId, $accountId, $isWebhook );
	}
	public function getCredit(){
		$shippingmethods	= $this->{$this->globalConfig['fetchSalesCredit']}->getAllShippingMethod();
        $groupAction		= $this->input->post('customActionType');
        $records			= array();
        $records["data"]	= array();
        if($this->input->post('order')){
 			$orderData	= array($this->router->directory.$this->router->class => $this->input->post('order'));
 			$this->session->set_userdata($orderData);
        }
        if ($groupAction == 'group_action') {
            $ids	= $this->input->post('id');
            if ($ids) {
                $status	= $this->input->post('customActionName');
                if ($status != '') {
                    $this->db->where_in('id', $ids)->update('sales_credit_order', array('status' => $status));
                    $records["customActionStatus"]  = "OK";
                    $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
                }
            }
        }
        $where	= array();
        $query	= $this->db;
        if ($this->input->post('action') == 'filter') {
            if (trim($this->input->post('orderId'))) {
                $where['orderId']			= trim($this->input->post('orderId'));
            }
            if (trim($this->input->post('createOrderId'))) {
                $where['createOrderId']		= trim($this->input->post('createOrderId'));
            }
			if (trim($this->input->post('delAddressName'))) {
                $where['delAddressName']	= trim($this->input->post('delAddressName'));
            }
            if (trim($this->input->post('orderNo'))) {
                $where['orderNo']			= trim($this->input->post('orderNo'));
            }
            if (trim($this->input->post('customerId'))) {
                $where['customerId']		= trim($this->input->post('customerId'));
            }
			if (trim($this->input->post('type'))) {
                $where['type']				= trim($this->input->post('type'));
            }
            if (trim($this->input->post('customerEmail'))) {
                $where['customerEmail']		= trim($this->input->post('customerEmail'));
            }
            if (trim($this->input->post('paymentMethod'))) {
                $where['paymentMethod']		= trim($this->input->post('paymentMethod'));
            }
            if (trim($this->input->post('status')) >= '0') {
                $where['status']			= trim($this->input->post('status'));
            }
        }
        if (trim($this->input->post('updated_from'))) {
            $query->where('date(updated) >= ', "date('" . $this->input->post('updated_from') . "')", false);
        }
        if (trim($this->input->post('updated_to'))) {
            $query->where('date(updated) <= ', "date('" . $this->input->post('updated_to') . "')", false);
        }
        if ($where) {
            $query->like($where);
        }
        $totalRecord	= @$query->select('count("id") as countsales')->get('sales_credit_order')->row_array()['countsales'];
        $limit       	= intval($this->input->post('length'));
        $limit       	= $limit < 0 ? $totalRecord : $limit;
        $start       	= intval($this->input->post('start'));
        $query			= $this->db;
        if (trim($this->input->post('updated_from'))) {
            $query->where('date(updated) >= ', "date('" . $this->input->post('updated_from') . "')", false);
        }
        if (trim($this->input->post('updated_to'))) {
            $query->where('date(updated) <= ', "date('" . $this->input->post('updated_to') . "')", false);
        }
        if ($where) {
            $query->like($where);
        }
        $status					= array(
			'0'	=> 'Pending',
			'1'	=> 'Sent',
			'2'	=> 'Partially Credit Confirmation',
			'3' => 'Fully Credit Confirmation',
			'4' => 'Archive',
		);
        $statusColor			= array(
			'0'	=> 'default',
			'1' => 'success',
			'2' => 'info',
			'3' => 'success',
			'4' => 'danger',
		);
        $displayProRowHeader	= array('id','orderId','delAddressName','shippingMethod','updated','status','message');
        if ($this->session->userdata($this->router->directory.$this->router->class)) {
            foreach ($this->input->post('order') as $ordering) {
                if (@$displayProRowHeader[$ordering['column']]) {
                    $query->order_by($displayProRowHeader[$ordering['column']], $ordering['dir']);
                }
            }
        }
        $datas					= $query->select('id,orderNo,createOrderId,customerEmail,customerEmail,orderId,updated,status,paymentMethod,shippingMethod,type,typeDetails,rowData,dispatchConfirmation,cancelRequest,message,isReturn,creditId,account1Id,account2Id,delAddressName,salesFileName')->limit($limit, $start)->get('sales_credit_order')->result_array();
		$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['fetchSalesCredit'].'_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){
			$account1Mappings[$account1MappingTemp['id']]	= $account1MappingTemp;
		}
		
		$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postSalesCredit'].'_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){
			$account2Mappings[$account2MappingTemp['id']]	= $account2MappingTemp;
		}
        foreach ($datas as $data){
        	$params		= json_decode($data['rowData'],true);
			$message	= $data['message'];
			if($data['cancelRequest']){
				$message	=	'<span class="label label-sm label-danger">Cancel request received</span>';
			}
			else if($data['creditId']){
				$message	=	'';
			}
			else if($data['isReturn']){
				$message	=	'<span class="label label-sm label-info">Return requested</span>'; 
			}
			else if($data['dispatchConfirmation']){
				$message	=	'<span class="label label-sm label-info">Dispatch Confirmation received</span>'; 
			}
			$downlaodSalesFile	= '';
			$salesFileNames		= @array_filter(explode(",",$data['salesFileName']));
			if($salesFileNames){
				foreach($salesFileNames as $salesFileName){
					$downlaodSalesFile	.=	'<li>
												<a class="" href="'.base_url($salesFileName).'" download="'.basename($salesFileName).'"> Download Credit File </a>
											</li>';
				}
			}
            $records["data"][]	= array(
                '<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
                '<a href="'.base_url('sales/credit/creditItem/'.$data['orderId']).'">'.$data['orderId'].'</a>',
                @$data['delAddressName'],
                @($data['shippingMethod'])?($shippingmethods[$data['account1Id']][$data['shippingMethod']]['name']):(''), 
                $data['updated'],
                '<span class="label label-sm label-' . $statusColor[$data['status']] . '">' . $status[$data['status']] . '</span>',
				$message,
                '<div class="btn-group">
					<a class="btn btn-circle btn-default dropdown-toggle" href="javascript:;" data-toggle="dropdown">
						<i class="fa fa-share"></i>
						<span class="hidden-xs"> Tools </span>
						<i class="fa fa-angle-down"></i>
					</a>
					<div class="dropdown-menu pull-right">
						<li>
							<a class="btnactionsubmit" href="'.base_url('sales/credit/fetchSalesCredit/'.$data['orderId']).'"> Fetch Sales Credit </a>
						</li>
						<li>
							<a class="btnactionsubmit" href="'.base_url('sales/credit/postSalesCredit/'.$data['orderId']).'"> Post Sales Credit </a>
						</li>
						<li>
							<a target = "_blank" href="'.base_url('sales/credit/creditInfo/'.$data['orderId']).'"> Sales Info </a>
						</li>
						'.$downlaodSalesFile.'
					</div>
				</div>',
            );
        }
        $draw                       = intval($this->input->post('draw'));
        $records["draw"]            = $draw;
        $records["recordsTotal"]    = $totalRecord;
        $records["recordsFiltered"]	= $totalRecord;
        return $records;
    }
	public function getCreditItem($orderId){
		$datas	= $this->db->get_where('sales_credit_item',array('orderId' => $orderId))->result_array();
		return $datas;
	}
}