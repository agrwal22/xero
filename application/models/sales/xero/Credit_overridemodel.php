<?php
include_once(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'Credit_model.php');
class Credit_overridemodel extends Credit_model{
	public function __construct(){
        parent::__construct();
        $this->ci = get_instance();
    }	
	public function getCredit(){
		$shippingmethods	= $this->{$this->globalConfig['fetchSalesCredit']}->getAllShippingMethod();
        $groupAction		= $this->input->post('customActionType');
        $records			= array();
        $records["data"]	= array();
        if($this->input->post('order')){
 			$orderData	= array($this->router->directory.$this->router->class => $this->input->post('order'));
 			$this->session->set_userdata($orderData);
        }
        if ($groupAction == 'group_action') {
            $ids	= $this->input->post('id');
            if ($ids) {
                $status	= $this->input->post('customActionName');
                if ($status != '') {
                    $this->db->where_in('id', $ids)->update('sales_credit_order', array('status' => $status));
                    $records["customActionStatus"]	= "OK";
                    $records["customActionMessage"]	= "Group action successfully has been completed. Well done!";
                }
            }
        }
        $where	= array();
        $query	= $this->db;
        if ($this->input->post('action') == 'filter') {
			if (trim($this->input->post('account1Id'))) {
                $where['account1Id']		= trim($this->input->post('account1Id'));
            }
			if (trim($this->input->post('account2Id'))) {
                $where['account2Id']		= trim($this->input->post('account2Id'));
            }
            if (trim($this->input->post('orderId'))) {
                $where['orderId']			= trim($this->input->post('orderId'));
            }
            if (trim($this->input->post('createOrderId'))) {
                $where['createOrderId']		= trim($this->input->post('createOrderId'));
            }
			if (trim($this->input->post('delAddressName'))) {
                $where['delAddressName'] 	= trim($this->input->post('delAddressName'));
            }
            if (trim($this->input->post('orderNo'))) {
                $where['orderNo']			= trim($this->input->post('orderNo'));
            }
            if (trim($this->input->post('customerId'))) {
                $where['customerId']		= trim($this->input->post('customerId'));
            }
			if (trim($this->input->post('type'))) {
                $where['type']				= trim($this->input->post('type'));
            }
            if (trim($this->input->post('customerEmail'))) {
                $where['customerEmail']		= trim($this->input->post('customerEmail'));
            }
            if (trim($this->input->post('paymentMethod'))) {
                $where['paymentMethod']		= trim($this->input->post('paymentMethod'));
            }
            if (trim($this->input->post('status')) >= '0') {
                $where['status']			= trim($this->input->post('status'));
            }
        }
        if (trim($this->input->post('updated_from'))) {
            $query->where('date(updated) >= ', "date('" . $this->input->post('updated_from') . "')", false);
        }
        if (trim($this->input->post('updated_to'))) {
            $query->where('date(updated) <= ', "date('" . $this->input->post('updated_to') . "')", false);
        }
        if ($where) {
            $query->like($where);
        }
        $totalRecord	= @$query->select('count("id") as countsales')->get('sales_credit_order')->row_array()['countsales'];
        $limit			= intval($this->input->post('length'));
        $limit			= $limit < 0 ? $totalRecord : $limit;
        $start			= intval($this->input->post('start'));
        $query			= $this->db;
        if (trim($this->input->post('updated_from'))) {
            $query->where('date(updated) >= ', "date('" . $this->input->post('updated_from') . "')", false);
        }
        if (trim($this->input->post('updated_to'))) {
            $query->where('date(updated) <= ', "date('" . $this->input->post('updated_to') . "')", false);
        }
        if ($where) {
            $query->like($where);
        }
        $status					= array(
			'0' => 'Pending',
			'1' => 'Sent',
			'2' => 'Partially Credit Confirmation',
			'3' => 'Payment Created',
			'4' => 'Archive',
		);
        $statusColor			= array(
			'0' => 'default',
			'1' => 'success',
			'2' => 'info',
			'3' => 'success',
			'4' => 'danger',
		);
        $displayProRowHeader	= array('id','account1Id','account2Id','orderId','createOrderId','delAddressName','shippingMethod','updated','status','message');
        if ($this->session->userdata($this->router->directory.$this->router->class)) {
            foreach ($this->input->post('order') as $ordering) {
                if (@$displayProRowHeader[$ordering['column']]) {
                    $query->order_by($displayProRowHeader[$ordering['column']], $ordering['dir']);
                }
            }
        }
        $datas	= $query->select('id,orderNo,createOrderId,customerEmail,customerEmail,orderId,updated,status,paymentMethod,shippingMethod,type,typeDetails,rowData,dispatchConfirmation,cancelRequest,message,isReturn,creditId,account1Id,account2Id,delAddressName,salesFileName,sendPaymentTo,uninvoiced')->limit($limit, $start)->get('sales_credit_order')->result_array();
		$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['fetchSalesCredit'].'_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){
			$account1Mappings[$account1MappingTemp['id']]	= $account1MappingTemp;
		}
		$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postSalesCredit'].'_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){
			$account2Mappings[$account2MappingTemp['id']]	= $account2MappingTemp;
		}
        foreach ($datas as $data){
        	$params		= json_decode($data['rowData'],true);
			$message	= $data['message'];
			if($data['uninvoiced'] == 1){				
				$message	= '<span class="label label-sm label-info">Order Uninvoiced on Brightpearl</span>'; 
			}
			else if($data['uninvoiced'] == 2){				
				$message	= '<span class="label label-sm label-info">Invoice Void On Xero</span>'; 
			}
			elseif($data['cancelRequest']){
				$message	= '<span class="label label-sm label-danger">Cancel request received</span>';
			}
			else if($data['creditId']){
				$message	= '';
			}
			else if($data['isReturn']){
				$message	= '<span class="label label-sm label-info">Return requested</span>'; 
			}
			else if($data['dispatchConfirmation']){
				$message	= '<span class="label label-sm label-info">Dispatch Confirmation received</span>'; 
			}
			else if($data['sendPaymentTo'] == 'brightpearl'){				
				$message	= '<span class="label label-sm label-info">Payment initiated in Xero</span>'; 
			}
			else if($data['sendPaymentTo'] == 'xero'){			
				$message	= '<span class="label label-sm label-info">Payment initiated in Brightpearl</span>'; 
			}
			$downlaodSalesFile	= '';
			$salesFileNames		= @array_filter(explode(",",$data['salesFileName']));
			if($salesFileNames){
				foreach($salesFileNames as $salesFileName){
					$downlaodSalesFile	.=	'<li>
												<a class="" href="'.base_url($salesFileName).'" download="'.basename($salesFileName).'"> Download Credit File </a>
											</li>';
				}
			}
			if($data['createOrderId']){
				$downlaodSalesFile		.=	'<li>
												<a target="_blank" href="https://go.xero.com/AccountsReceivable/ViewCreditNote.aspx?creditNoteID='.$data['createOrderId'].'"> View Refund in  '.$account2Mappings[$data['account2Id']]['name'].'</a>
											</li>';
			}
            $records["data"][]	= array(
                '<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
				@$account1Mappings[$data['account1Id']]['name'],
                @$account2Mappings[$data['account2Id']]['name'],
                '<a href="'.base_url('sales/credit/creditItem/'.$data['orderId']).'">'.$data['orderId'].'</a>',
                '<a href="'.base_url('sales/credit/creditItem/'.$data['orderId']).'">'.$data['createOrderId'].'</a>',
                @$data['delAddressName'],
                @($data['shippingMethod'])?($shippingmethods[$data['account1Id']][$data['shippingMethod']]['name']):(''), 
                $data['updated'],
                '<span class="label label-sm label-' . $statusColor[$data['status']] . '">' . $status[$data['status']] . '</span>',
				$message,
                '<div class="btn-group">
					<a class="btn btn-circle btn-default dropdown-toggle" href="javascript:;" data-toggle="dropdown">
						<i class="fa fa-share"></i>
						<span class="hidden-xs"> Tools </span>
						<i class="fa fa-angle-down"></i>
					</a>
					<div class="dropdown-menu pull-right">
						<li>
							<a class="btnactionsubmit" href="'.base_url('sales/credit/fetchSalesCredit/'.$data['orderId']).'"> Fetch Sales Credit </a>
						</li>
						<li>
							<a class="btnactionsubmit" href="'.base_url('sales/credit/postSalesCredit/'.$data['orderId']).'"> Post Sales Credit </a>
						</li>
						<li>
							<a target = "_blank" href="'.base_url('sales/credit/creditInfo/'.$data['orderId']).'"> Sales Info </a>
						</li>
						<li>
                            <a target="_blank" href="'.$account1Mappings[$data['account1Id']]['viewUrl'].'/patt-op.php?scode=invoice&oID='.$data['orderId'].'"> View Refund in  '.$account1Mappings[$data['account1Id']]['name'].'</a>
                        </li>
						'.$downlaodSalesFile.'
					</div>
				</div>',
            );
        }
        $draw                       = intval($this->input->post('draw'));
        $records["draw"]            = $draw;
        $records["recordsTotal"]    = $totalRecord;
        $records["recordsFiltered"]	= $totalRecord;
        return $records;
    }	
	public function postSales($orderId = ''){
		$this->{$this->globalConfig['fetchSalesCredit']}->postSalesCreditPayment();
		$this->{$this->globalConfig['postSalesCredit']}->postSalesCredit($orderId);
    }
}
?>