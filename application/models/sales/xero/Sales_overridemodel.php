<?php
include_once(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'Sales_model.php');
class Sales_overridemodel extends Sales_model{
	public function __construct(){
        parent::__construct();
        $this->ci = get_instance();
    }	
	public function getSales(){
        $groupAction     = $this->input->post('customActionType');
        $records         = array();
        $records["data"] = array();
        if($this->input->post('order')){
 			$orderData	= array($this->router->directory.$this->router->class => $this->input->post('order'));
 			$this->session->set_userdata($orderData);
        }
        if ($groupAction == 'group_action') {
            $ids	= $this->input->post('id');
            if ($ids) {
                $status	= $this->input->post('customActionName');
                if ($status != '') {
                    $this->db->where_in('id', $ids)->update('sales_order', array('status' => $status));
                    $records["customActionStatus"]	= "OK";
                    $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
                }
            }
        }
        $where	= array();
        $query	= $this->db;
        if ($this->input->post('action') == 'filter') {
			if (trim($this->input->post('account1Id'))) {
                $where['account1Id']		= trim($this->input->post('account1Id'));
            }
			if (trim($this->input->post('account2Id'))) {
                $where['account2Id']		= trim($this->input->post('account2Id'));
            }
            if (trim($this->input->post('orderId'))) {
                $where['orderId']			= trim($this->input->post('orderId'));                
            }
            if (trim($this->input->post('delAddressName'))) {
                $where['delAddressName']	= trim($this->input->post('delAddressName'));
            }
            if (trim($this->input->post('orderNo'))) {
                $where['orderNo']			= trim($this->input->post('orderNo'));
            }
            if (trim($this->input->post('delPhone'))) {
                $where['delPhone']			= trim($this->input->post('delPhone'));
            }
            if (trim($this->input->post('status')) >= '0') {
                $where['status']			= trim($this->input->post('status'));
            }
        }
        if (trim($this->input->post('updated_from'))) {
            $query->where('date(created) >= ', "date('" . $this->input->post('updated_from') . "')", false);
        }
        if (trim($this->input->post('updated_to'))) {
            $query->where('date(created) < ', "date('" . $this->input->post('updated_to') . "')", false);
        }
        if ($where) {
            $query->like($where);
        }
        $totalRecord	= @$query->select('count("id") as countsales')->get('sales_order')->row_array()['countsales'];
        $limit			= intval($this->input->post('length'));
        $limit			= $limit < 0 ? $totalRecord : $limit;
        $start			= intval($this->input->post('start'));
        $query			= $this->db;
        if (trim($this->input->post('updated_from'))) {
            $query->where('date(updated) >= ', "date('" . $this->input->post('updated_from') . "')", false);
        }
        if (trim($this->input->post('updated_to'))) {
            $query->where('date(updated) <= ', "date('" . $this->input->post('updated_to') . "')", false);
        }
        if ($where) {
            $query->like($where);
        }
        $status					= array(
			'0' => 'Pending',
			'1' => 'Order Sent',
			'2' => 'Invoice Created',
			'3' => 'Payment Created',
			'4' => 'Archive',
		);
        $statusColor			= array(
			'0' => 'default',
			'1' => 'success',
			'2' => 'info',
			'3' => 'success',
			'4' => 'danger',
		);
        $displayProRowHeader	= array('id','account1Id','account2Id','orderId','createOrderId','delAddressName','delPhone','updated','status');
        if ($this->session->userdata($this->router->directory.$this->router->class)) {
            foreach ($this->session->userdata($this->router->directory.$this->router->class) as $ordering) {
                if (@$displayProRowHeader[$ordering['column']]) {
                    $query->order_by($displayProRowHeader[$ordering['column']], $ordering['dir']);
                }
            }
        }
        $datas	= $query->select('id,orderNo,createOrderId,customerEmail,customerEmail,orderId,updated,status,paymentMethod,type,typeDetails,rowData,dispatchConfirmation,cancelRequest,message,isReturn,creditId,account1Id,account2Id,delAddressName,delPhone,salesFileName,orderFailedAcknowledged,ackMessage,sendPaymentTo,sendPaymentTo,uninvoiced')->limit($limit, $start)->get('sales_order')->result_array();
		$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['fetchSalesOrder'].'_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){			
			$account1Mappings[$account1MappingTemp['id']]	= $account1MappingTemp;
		}
		$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postSalesOrder'].'_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){			
			$account2Mappings[$account2MappingTemp['id']]	= $account2MappingTemp;
		}
        foreach ($datas as $data) {
			$salesFileNames		= @array_filter(explode(",",$data['salesFileName']));
			$downlaodSalesFile	= '';
			if($salesFileNames){
				foreach($salesFileNames as $salesFileName){
					$downlaodSalesFile	.=	'<li>
												<a class="" href="'.base_url($salesFileName).'" download="'.basename($salesFileName).'"> Download Sales File </a>
											</li>';
				}
			}
			$reprocess	= '';
			$message	= $data['message'];
			if(($data['status'] < 3) && ($data['orderFailedAcknowledged'])){
				$message	=	'<span class="label label-sm label-danger">Radial Ack with error</span><br>'.$data['ackMessage'];
				$reprocess	=	'<li>
									<a class="btnactionsubmit" href="'.base_url('/sales/sales/reprocess/'.$data['orderId']).'"> Re-process Sales Order </a>
								</li>';
			}
			if($data['uninvoiced'] == 1){				
				$message	=	'<span class="label label-sm label-info">Order Uninvoiced on Brightpearl</span>'; 
			}
			else if($data['uninvoiced'] == 2){				
				$message	=	'<span class="label label-sm label-info">Invoice Void On Xero</span>'; 
			}
			else if($data['cancelRequest']){
				$message	=	'<span class="label label-sm label-danger">Cancel request received</span>';
			}
			else if($data['creditId']){
				$message	=	'';
			}
			else if($data['isReturn']){
				$message	=	'<span class="label label-sm label-info">Return requested</span>'; 
			} 
			else if($data['dispatchConfirmation']){
				$message	=	'<span class="label label-sm label-info">Dispatch Confirmation received</span>'; 
			}
			else if($data['sendPaymentTo'] == 'brightpearl'){				
				$message	=	'<span class="label label-sm label-info">Payment initiated in Xero</span>'; 
			}
			else if($data['sendPaymentTo'] == 'xero'){				
				$message	=	'<span class="label label-sm label-info">Payment initiated in Brightpearl</span>'; 
			}
			if($data['createOrderId']){
				$downlaodSalesFile	.=	'<li>
											<a target="_blank" href="https://go.xero.com/AccountsReceivable/View.aspx?InvoiceID='.$data['createOrderId'].'"> View Invoice in  '.$account2Mappings[$data['account2Id']]['name'].'</a>
										</li>';
			}
            $records["data"][]	= array(
                '<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
				@$account1Mappings[$data['account1Id']]['name'],
                @$account2Mappings[$data['account2Id']]['name'],
                '<a href="'.base_url('/sales/sales/salesItem/'.$data['orderId']).'">'.$data['orderId'].'</a>',
                '<a href="'.base_url('/sales/sales/salesItem/'.$data['orderId']).'">'.$data['createOrderId'].'</a>',
                @$data['delAddressName'],
                @$data['delPhone'], 
                $data['updated'],
                '<span class="label label-sm label-' . $statusColor[$data['status']] . '">' . $status[$data['status']] . '</span>',
				$message,
                '<div class="btn-group">
					<a class="btn btn-circle btn-default dropdown-toggle" href="javascript:;" data-toggle="dropdown">
						<i class="fa fa-share"></i>
						<span class="hidden-xs"> Tools </span>
						<i class="fa fa-angle-down"></i>
					</a>
					<div class="dropdown-menu pull-right">
						<li>
							<a class="btnactionsubmit" href="'.base_url('/sales/sales/fetchSales/'.$data['orderId']).'"> Fetch Sales Order </a>
						</li>
						<li>
							<a class="btnactionsubmit" href="'.base_url('/sales/sales/postSales/'.$data['orderId']).'"> Post Sales Order </a>
						</li>
						'.$reprocess.'						
						<li>
							<a target = "_blank" href="'.base_url('/sales/sales/salesInfo/'.$data['orderId']).'"> Sales Info </a>
						</li>	
						<li>
                            <a target="_blank" href="'.$account1Mappings[$data['account1Id']]['viewUrl'].'/patt-op.php?scode=invoice&oID='.$data['orderId'].'"> View Invoice in  '.$account1Mappings[$data['account1Id']]['name'].'</a>
                        </li>
						'.$downlaodSalesFile.'
					</div>
				</div>',
            );
        }
        $draw                       = intval($this->input->post('draw'));
        $records["draw"]            = $draw;
        $records["recordsTotal"]    = $totalRecord;
        $records["recordsFiltered"] = $totalRecord;
        return $records;
    }
	public function postSales($orderId = ''){
		$this->{$this->globalConfig['postSalesOrder']}->postSales($orderId);
		$this->{$this->globalConfig['fetchSalesOrder']}->postSalesPayment();
    }
}
?>