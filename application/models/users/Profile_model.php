<?php
class Profile_model extends CI_Model{
	public function getUserDetails(){
		if(@!$this->session_data){			
			return @$this->db->get_where('admin_user')->row_array();
		}
		else{
			return @$this->db->get_where('admin_user',array('user_id' => $this->session_data['user_session_data']['user_id']))->row_array();			
		}
	}
	public function getGlobalConfig(){
		return $this->db->get('global_config')->row_array();
	}
	public function saveBasic($data){
		if($this->session_data['user_session_data']['user_id']){
			$this->db->where(array('user_id' => $this->session_data['user_session_data']['user_id']))->update('admin_user',$data);
		}
	}
	public function updatePassword($data){
		$this->db->where(array('user_id' => $this->session_data['user_session_data']['user_id'] , 'password' => md5($data['password'])))->update('admin_user', array('password' => md5($data['newpassword'])));
		return $this->db->affected_rows();
	}
	public function update( $data = array() ){
		if(count($data)>0){
			$this->db->where('user_id', $data['user_id']);
			return $this->db->update(ADMIN_USER, $data);	
 		}
		else{ 
			return false;
		}
	}
	public function uploadedProfiePic($data){
		$this->db->where(array('user_id' => $this->session_data['user_session_data']['user_id']))->update('admin_user',$data);
		return $this->db->affected_rows();
	}
	public function executeMultipleSql($filePath = ''){
		if(file_exists($filePath)){
			$fileData	= file_get_contents($filePath);
			if($fileData){
				$sqls	= explode(';', $fileData);
				foreach($sqls as $statement){
					if(strlen($statement) > 5){
						$statment	= $statement . ";";
						$this->db->query($statement); 
					}  
				}
			}
		}
	}
	public function saveGlobalConfig($data){
		if($data['account1Liberary']){
			$sql	= "SHOW TABLES LIKE 'account_".$data['account1Liberary']."%'";
			$query	= $this->db->query($sql);
			$rows	= $query->row_array();
			if(!$rows){
				$filePath	= BSITCPATH.'installation'.DIRECTORY_SEPARATOR.'sql'.DIRECTORY_SEPARATOR.'account'.DIRECTORY_SEPARATOR.$data['account1Liberary'].'.sql';
				$this->executeMultipleSql($filePath);
			}
		}
		if($data['account2Liberary']){
			$sql	= "SHOW TABLES LIKE 'account_".$data['account2Liberary']."%'";
			$query	= $this->db->query($sql);
			$rows	= $query->row_array();
			if(!$rows){
				$filePath	= BSITCPATH.'installation'.DIRECTORY_SEPARATOR.'sql'.DIRECTORY_SEPARATOR.'account'.DIRECTORY_SEPARATOR.$data['account2Liberary'].'.sql';
				$this->executeMultipleSql($filePath);
			}
		}		
		foreach($data as $key => $value){
			if($value){
				if(substr_count($key,'Mapping')){
					$mappingKey	= str_replace('enable','',$key);
					$mappingKey	= str_replace('Mapping','',$mappingKey);
					if($mappingKey){
						$filePath	= BSITCPATH.'installation'.DIRECTORY_SEPARATOR.'sql'.DIRECTORY_SEPARATOR.'mapping'.DIRECTORY_SEPARATOR.strtolower($mappingKey).'.sql';
						$this->executeMultipleSql($filePath);
					}
				}				
			}
		}
		if(@$data['enableProduct']){
			$filePath	= BSITCPATH.'installation'.DIRECTORY_SEPARATOR.'sql'.DIRECTORY_SEPARATOR.'product.sql';
			$this->executeMultipleSql($filePath);
		}
		if(@$data['enableCustomer']){
			$filePath	= BSITCPATH.'installation'.DIRECTORY_SEPARATOR.'sql'.DIRECTORY_SEPARATOR.'customers.sql';
			$this->executeMultipleSql($filePath);
		}
		if(@$data['enableSalesOrder']){
			$filePath	= BSITCPATH.'installation'.DIRECTORY_SEPARATOR.'sql'.DIRECTORY_SEPARATOR.'sales'.DIRECTORY_SEPARATOR.'sales.sql';
			$this->executeMultipleSql($filePath);
		}
		if(@$data['enableSalesCredit']){
			$filePath	= BSITCPATH.'installation'.DIRECTORY_SEPARATOR.'sql'.DIRECTORY_SEPARATOR.'sales'.DIRECTORY_SEPARATOR.'credit.sql';
			$this->executeMultipleSql($filePath);
		}
		if(@$data['enablePurchaseOrder']){
			$filePath	= BSITCPATH.'installation'.DIRECTORY_SEPARATOR.'sql'.DIRECTORY_SEPARATOR.'purchase'.DIRECTORY_SEPARATOR.'purchase.sql';
			$this->executeMultipleSql($filePath);
		}
		if(@$data['enablePurchaseCredit']){
			$filePath	= BSITCPATH.'installation'.DIRECTORY_SEPARATOR.'sql'.DIRECTORY_SEPARATOR.'purchase'.DIRECTORY_SEPARATOR.'credit.sql';
			$this->executeMultipleSql($filePath);
		}
		if(@$data['enableStockTransfer']){
			$filePath	= BSITCPATH.'installation'.DIRECTORY_SEPARATOR.'sql'.DIRECTORY_SEPARATOR.'transfer.sql';
			$this->executeMultipleSql($filePath);
		}
		if(@$data['enableStockSync']){
			$filePath	= BSITCPATH.'installation'.DIRECTORY_SEPARATOR.'sql'.DIRECTORY_SEPARATOR.'sync.sql';
			$this->executeMultipleSql($filePath);
		}
		if(@$data['enableInventoryadvice']){
			$filePath	= BSITCPATH.'installation'.DIRECTORY_SEPARATOR.'sql'.DIRECTORY_SEPARATOR.'advice.sql';
			$this->executeMultipleSql($filePath);
		}		
		$getGlobalConfig	= $this->getGlobalConfig();
		$this->db->where(array('id' => $getGlobalConfig['id']))->update('global_config',$data);
	}
}
?>