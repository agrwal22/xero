<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_Controller extends CI_Controller {

	public $user_session_data;
	public $session_data = array();

	public function __construct(){
		parent::__construct();
        if(!$this->is_logged_in()){
        	redirect('users/login/','');
        }		
		$this->session_data['user_session_data'] = $this->user_session_data;			        
	}

	function is_logged_in() { 
	    // Get current CodeIgniter instance
		$this->user_session_data = $this->session->userdata('login_user_data');		    	               
	    if (!isset($this->user_session_data)) { 
		    return false; 
		} else { 
			return true; 
		}
	}

	function checkUser($id){
		$sql = "SELECT * FROM users WHERE id = ?";
		$query = $this->db->query($sql, array($id));		
		return $query->num_rows();
	}
}