<div class="page-sidebar-wrapper">               
	<div class="page-sidebar navbar-collapse collapse">                   
		<ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-closed" data-keep-expanded="true" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
			<li class="sidebar-toggler-wrapper hide">
				<div class="sidebar-toggler"></div>
			</li>
		  	<li class="nav-item dashboard">
				<a href="<?php echo base_url();?>dashboard" class="nav-link ">
					<i class="icon-home"></i>
					<span class="title">Dashboard</span>
					<span class="selected"></span>
				</a>
			</li>
			<li class="nav-item account">
				<a href="javascript:;" class="nav-link nav-toggle">
					<i class="fa fa-cogs"></i>
					<span class="title">Account Settings</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">		
					<li class="nav-item">
						<a href="javascript:;" class="nav-link nav-toggle">
							<i class="fa fa-sun-o"></i>
							<span class="title"><?php echo $this->globalConfig['account1Name'];?> Settings</span>
							<span class="selected"></span>
							<span class="arrow open"></span>
						</a>
						<ul class="sub-menu">		
							<li class="nav-item  ">
								<a href="<?php echo base_url('account/account1/account');?>" class="nav-link ">
									<i class="glyphicon glyphicon-chevron-right "></i>
									<span class="title">Accounts</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url('account/account1/config');?>" class="nav-link ">
									<i class="glyphicon glyphicon-chevron-right "></i>
									<span class="title">Default Configuration</span>
								</a>
							</li>

						</ul>
					</li>
					<li class="nav-item">
						<a href="javascript:;" class="nav-link nav-toggle">
							<i class="fa fa-sun-o"></i>
							<span class="title"><?php echo $this->globalConfig['account2Name'];?> Settings</span>
							<span class="selected"></span>
							<span class="arrow open"></span>
						</a>
						<ul class="sub-menu">		
							<li class="nav-item  ">
								<a href="<?php echo base_url('account/account2/account');?>" class="nav-link ">
									<i class="glyphicon glyphicon-chevron-right "></i>
									<span class="title">Accounts</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url('account/account2/config');?>" class="nav-link ">
									<i class="glyphicon glyphicon-chevron-right "></i>
									<span class="title">Default Configuration</span>
								</a>
							</li>

						</ul>
					</li>					
				</ul>
			</li>
			<?php 
				if($this->globalConfig['enableMapping']){
			?>
			<li class="nav-item mapping">				
				<a href="javascript:;" class="nav-link nav-toggle">
					<span aria-hidden="true" class="icon-handbag"></span>
					<span class="title">Mapping</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">
					<?php 
						$mappings		= $this->config->item('mapping');
						$mappingIcon	= array('brand' => 'map','category' => 'map', 'channel' => 'bar-chart','leadsource' => 'calculator','payment' => 'credit-card', 'pricelist' => 'calculator' , 'shipping' => 'bag','salesrep' => 'map', 'tax' => 'bar-chart', 'warehouse' => 'map','box' => 'present','packedmapping' => 'briefcase','inventoryadvice' => 'bar-chart');
						foreach($mappings as $mapKey => $mapping){ 
							if(@$this->globalConfig['enable'.ucwords($mapKey).'Mapping']){
					?>
					<li class="nav-item <?php echo $mapKey;?>">
						<a href="<?php echo base_url();?>mapping/<?php echo $mapKey;?>" class="nav-link ">
							<span aria-hidden="true" class="icon-<?php echo (@$mappingIcon[$mapKey])?($mappingIcon[$mapKey]):'share';?>"></span>
							<span class="title"><?php echo $mapping;?> Mapping</span>
						</a>
					</li>  
					<?php 
							} 
						} ?> 	
				</ul>
			</li>   
			<?php 
				} 
			?>			
			<?php 
				if(@$this->globalConfig['enableFieldMapping']){
			?>
			<li class="nav-item mapping">				
				<a href="javascript:;" class="nav-link nav-toggle">
					<span aria-hidden="true" class="icon-handbag"></span>
					<span class="title">Field Configuration</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">
					<?php 
						if($this->globalConfig['enableFieldMapping']){ 
							$mappings		= $this->config->item('fieldmapping');
							$mappingIcon	= array('ack' => 'map', 'packing' => 'bag','invoice' => 'bar-chart');
							foreach($mappings as $mapKey => $mapping){ 
								if(@$this->globalConfig['enable'.ucwords($mapKey).'Mapping']){
					?>
					<li class="nav-item <?php echo $mapKey;?>">
						<a href="<?php echo base_url();?>fieldconfig/<?php echo $mapKey;?>" class="nav-link ">
							<span aria-hidden="true" class="icon-<?php echo (@$mappingIcon[$mapKey])?($mappingIcon[$mapKey]):'share';?>"></span>
							<span class="title"><?php echo $mapping;?></span>
						</a>
					</li>
					<?php 	
								}
							}
						}
					?> 
				</ul>
			</li>   
			<?php
				} 
			?>
			<?php 
				if(@$this->globalConfig['enableProduct']){
			?>
			<li class="nav-item products">				
				<a href="javascript:;" class="nav-link nav-toggle">
					<i class="icon-graph"></i>
					<span class="title">Products</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">					
					<li class="nav-item products">
						<a href="<?php echo base_url();?>products/products" class="nav-link ">
							<i class="icon-graph"></i>
							<span class="title">Products</span>
						</a>
					</li> 					
					<?php
						if($this->globalConfig['enablePrebook']){
					?>                    
					<li class="nav-item preproducts ">
						<a href="<?php echo base_url();?>products/products/preproducts" class="nav-link ">
							<i class="fa fa-graph"></i>
							<span class="title">Pre-Order</span>
						</a>
					</li>
					<?php 
						} 
					?>   					
				</ul>
			</li>
			<?php 
				} 
			?>
			<?php 
				if(@$this->globalConfig['enableCustomer']){
			?>
			<li class="nav-item customers">
				<a href="<?php echo base_url();?>customers/customers" class="nav-link ">
					<i class="icon-user"></i>
					<span class="title">Customers / Vendors</span>
					<span class="selected"></span>
				</a>
			</li>
			<?php 
				}
			?>
			<?php 
				if(@$this->globalConfig['enableSalesOrder']){ 
			?>
			<li class="nav-item sales">				
				<a href="javascript:;" class="nav-link nav-toggle">
					<i class="icon-basket"></i>
					<span class="title">Sales Order</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">					
					<li class="nav-item  sales">
						<a href="<?php echo base_url();?>sales/sales" class="nav-link ">
							<i class="icon-basket"></i>
							<span class="title">Sales Order</span>
						</a>
					</li>           
					<?php 
						if(@$this->globalConfig['enableSalesCredit']){ ?>                    
					<li class="nav-item credit ">
						<a href="<?php echo base_url();?>sales/credit" class="nav-link ">
							<i class="fa fa-truck"></i>
							<span class="title">Sales Credit</span>
						</a>
					</li>
					<?php 
						} 
					?>
				</ul>
			</li>   
			<?php 
				} 
			?>
			<?php 
				if(@$this->globalConfig['enablePacking']){
			?>
			<li class="nav-item packed">
				<a href="<?php echo base_url();?>packed/packed" class="nav-link ">
					<i class="icon-drawer"></i>
					<span class="title">Packing</span>
					<span class="selected"></span>
				</a>
			</li>
			<?php 
				} 
			?>
			<?php 
				if(@$this->globalConfig['enablePurchaseOrder']){
			?>
			<li class="nav-item purchase">				
				<a href="javascript:;" class="nav-link nav-toggle">
					<span aria-hidden="true" class="icon-handbag"></span>
					<span class="title">Purchase Orders</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">					
					<li class="nav-item  purchase">
						<a href="<?php echo base_url();?>purchase/purchase" class="nav-link ">
							<i class="icon-basket"></i>
							<span class="title">Purchase Orders</span>
						</a>
					</li>           
					<?php 
						if($this->globalConfig['enableReceipt']){
					?>                    
					<li class="nav-item  receipt">
						<a href="<?php echo base_url();?>purchase/receipt" class="nav-link ">
							<i class="fa fa-truck"></i>
							<span class="title">Receipt Confirmation</span>
						</a>
					</li>
					<?php 
						} 
					?>  
					<?php 
						if($this->globalConfig['enablePurchaseCredit']){
					?>                    
					<li class="nav-item  credit">
						<a href="<?php echo base_url();?>purchase/credit" class="nav-link ">
							<i class="fa fa-truck"></i>
							<span class="title">Purchase Credit</span>
						</a>
					</li>
					<?php
						} 
					?>
				</ul>
			</li>   
			<?php 
				} 
			?>
			<?php 
				if((@$this->globalConfig['enableStockSync'])||(@$this->globalConfig['enableStockTransfer'])||(@$this->globalConfig['enableInventoryadvice']) || (@$this->globalConfig['enableStockAdjustment'])){
			?>
			<li class="nav-item stock">				
				<a href="javascript:;" class="nav-link nav-toggle">
					<i class="fa fa-recycle"></i>
					<span class="title">Stock Details</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">	
					<?php 
						if($this->globalConfig['enableStockTransfer']){
					?>
					<li class="nav-item  transfer">
						<a href="<?php echo base_url();?>stock/transfer" class="nav-link ">
							<i class="icon-user"></i>
							<span class="title">Stock Transfer</span>
							<span class="selected"></span>
						</a>
					</li>
					<?php
						} 
					?>
					<?php 
						if($this->globalConfig['enableInventoryadvice']){
					?>                    
					<li class="nav-item  adjustment">
						<a href="<?php echo base_url();?>stock/inventoryadvice" class="nav-link ">
							<i class="fa fa-cubes"></i>
							<span class="title">Inventory Advice</span>
						</a>
					</li>           
					<?php 
						} 
					?>  
					<?php 
						if($this->globalConfig['enableStockAdjustment']){
					?>                    
					<li class="nav-item  adjustment">
						<a href="<?php echo base_url();?>stock/adjustment" class="nav-link ">
							<i class="fa fa-cubes"></i>
							<span class="title">Stock Adjustment</span>
						</a>
					</li>           
					<?php 
						}
					?>
					<?php
						if($this->globalConfig['enableStockSync']){
					?>            
					<li class="nav-item  sync">
						<a href="<?php echo base_url();?>stock/sync" class="nav-link ">
							<i class="fa fa-recycle"></i>
							<span class="title">Stock Sync</span>
						</a>
					</li>
					<li class="nav-item  synclog">
						<a href="<?php echo base_url();?>stock/synclog" class="nav-link ">
							<i class="fa fa-trash-o"></i>
							<span class="title">Stock Sync Log</span>
						</a>
					</li>
					<?php 
						} 
					?>
				</ul>
			</li>   
			<?php 
				} 
			?>
			<?php
				if($this->globalConfig['enableReports']){
			?>
			<li class="nav-item report">				
				<a href="javascript:;" class="nav-link nav-toggle">
					<span aria-hidden="true" class="icon-briefcase"></span>
					<span class="title">Reports</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">					
					<li class="nav-item  sales">
						<a href="<?php echo base_url();?>report/sales" class="nav-link ">
							<i class="icon-briefcase"></i>
							<span class="title">Sales Report</span>
						</a>
					</li> 
					
				</ul>
			</li>
			<?php
				}
			?>
			<?php
				if($this->globalConfig['enableAmazonFees']){
			?>
			<li class="nav-item journal">
				<a href="<?php echo base_url();?>journal/journal" class="nav-link ">
					<i class="fa fa-cubes"></i>
					<span class="title">Amazon Fees</span>
					<span class="selected"></span>
				</a>
			</li>
			<?php
				}
			?>
	   </ul>
	</div>
</div>
<?php	
	$controllerName	= @$this->router->class;
	$allDirectory	= @json_encode(array_filter(explode("/",$this->router->directory)));
	$method			= @$this->router->method;
?>
<script>	
	var directory		=<?php echo $allDirectory;?>;
	var controllerName	= '<?php echo $controllerName;?>';
	var method			= '<?php echo $method;?>';
	var activeClass		= '';
	for (index in directory) {
		value	= directory[index];
		if(jQuery(activeClass + " ."+value).length){
			activeClass	+= " ." + value;
		}
	}
	var activeClass1	= '';	
	if(controllerName !=""){
		if(jQuery(activeClass + " ."+controllerName).length){
			activeClass	+= " ." + controllerName;
		}		
	}
	if( (method !="") && (method != 'index')){
		if(jQuery(activeClass + " ."+method).length){
			activeClass	+= " ." + method;
		}		
	}
	if(activeClass != ""){
		jQuery(activeClass).addClass('active open');
	}
</script>