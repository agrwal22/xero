<?php
$status              = array('0' => 'Pending', '1' => 'Sent', '2' => 'Acknowledgement','3' => 'Partially Invoice', '4' => 'Invoice', '5' => 'Archive');
$statusColor         = array('0' => 'default', '1' => 'success', '2' => 'success', '3' => 'info', '4' => 'success', '5' => 'danger');
$saveOrderInfo = json_decode($orderInfo['rowData'],true);
$addressInfo = $saveOrderInfo['parties'];

?>
<div class="page-content-wrapper"> 
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<!-- BEGIN PAGE BAR -->
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<a href="index.html">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<span>Order Details</span>
				</li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<!-- Begin: life time stats -->
				<div class="portlet light portlet-fit portlet-datatable bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-settings font-dark"></i>
							<span class="caption-subject font-dark sbold uppercase"> Order #<?php echo ($orderInfo['orderNo'])?($orderInfo['orderNo']):($orderInfo['orderId']);?>
								<span class="hidden-xs">| <?php echo ($orderInfo['created'])?(date('M d, Y H:i:s',strtotime($orderInfo['created']))):('');?></span>
							</span>
						</div>
						<div class="actions hide">
							<div class="btn-group btn-group-devided" data-toggle="buttons">
								<label class="btn btn-transparent green btn-outline btn-circle btn-sm active">
									<input type="radio" name="options" class="toggle" id="option1">Actions</label>
								<label class="btn btn-transparent blue btn-outline btn-circle btn-sm">
									<input type="radio" name="options" class="toggle" id="option2">Settings</label>
							</div>
							<div class="btn-group">
								<a class="btn red btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
									<i class="fa fa-share"></i>
									<span class="hidden-xs"> Tools </span>
									<i class="fa fa-angle-down"></i>
								</a>
								<ul class="dropdown-menu pull-right">
									<li>
										<a href="javascript:;"> Export to Excel </a>
									</li>
									<li>
										<a href="javascript:;"> Export to CSV </a>
									</li>
									<li>
										<a href="javascript:;"> Export to XML </a>
									</li>
									<li class="divider"> </li>
									<li>
										<a href="javascript:;"> Print Invoices </a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="portlet-body">
						<div class="tabbable-line">
							<ul class="nav nav-tabs nav-tabs-lg">
								<li class="active">
									<a href="#tab_1" data-toggle="tab"> Details </a>
								</li>								
								<li>
									<a href="#tab_2" data-toggle="tab"> Acknowledged Details
										<span class="badge badge-danger hide"> 2 </span>
									</a>
								</li>	
								<li>
									<a href="#tab_3" data-toggle="tab"> Invoice Details
										<span class="badge badge-danger hide"> 2 </span>
									</a>
								</li>	
								
							</ul>
							<div class="tab-content">
								<div class="tab-pane active" id="tab_1">
									<div class="row">
										<div class="col-md-6 col-sm-12">
											<div class="portlet grey-cascade box">
												<div class="portlet-title">
													<div class="caption">
														<i class="fa fa-cogs"></i>Order Details </div>
												</div>
												<div class="portlet-body">
													<div class="row static-info">
														<div class="col-md-5 name"> Order #: </div>
														<div class="col-md-7 value"> <?php echo ($orderInfo['orderNo'])?($orderInfo['orderNo']):($orderInfo['orderId']);?>
														</div>
													</div>
													<div class="row static-info">
														<div class="col-md-5 name"> Order Date & Time: </div>
														<div class="col-md-7 value"> <?php echo ($orderInfo['created'])?(date('M d, Y H:i:s',strtotime($orderInfo['created']))):('');?> </div>
													</div>
													<div class="row static-info">
														<div class="col-md-5 name"> Order Status: </div>
														<div class="col-md-7 value">
															 <?php echo '<span class="label label-sm label-' . @$statusColor[$orderInfo['status']] . '">' . @$status[$orderInfo['status']] . '</span>';?>
														</div>
													</div>
													<div class="row static-info">
														<div class="col-md-5 name"> Total: </div>
														<div class="col-md-7 value"> <?php echo sprintf("%.2f",$orderInfo['totalAmount']);?></div>
													</div>													
												</div>
											</div>
										</div>
										<div class="col-md-6 col-sm-12">
											<div class="portlet grey-cascade box">
												<div class="portlet-title">
													<div class="caption">
														<i class="fa fa-cogs"></i>Customer Information </div>
												</div>
												<div class="portlet-body">
													<div class="row static-info">
														<div class="col-md-5 name"> Customer Name: </div>
														<div class="col-md-7 value"> <?php echo $saveOrderInfo['parties']['supplier']['addressFullName'];?> </div>
													</div>
													<div class="row static-info">
														<div class="col-md-5 name"> Email: </div>
														<div class="col-md-7 value"> <?php echo $saveOrderInfo['parties']['supplier']['email'];?></div>
													</div>
													<div class="row static-info">
														<div class="col-md-5 name"> State: </div>
														<div class="col-md-7 value"> <?php echo $saveOrderInfo['parties']['supplier']['addressLine4'];?> </div>
													</div>
													<div class="row static-info">
														<div class="col-md-5 name"> Phone Number: </div>
														<div class="col-md-7 value"> <?php echo $saveOrderInfo['parties']['billing']['telephone'];?> </div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6 col-sm-12">
											<div class="portlet grey-cascade box">
												<div class="portlet-title">
													<div class="caption">
														<i class="fa fa-cogs"></i>Billing Address </div>
												</div>
												<div class="portlet-body">
													<div class="row static-info">
														<div class="col-md-12 value"> <?php echo $addressInfo['billing']['addressFullName'];?>
															<br> <?php echo $addressInfo['billing']['addressLine1'];?>
															<br> <?php echo $addressInfo['billing']['addressLine2'];?>
															<br> <?php echo $addressInfo['billing']['addressLine3'] .', '.$addressInfo['billing']['addressLine4'] .' ' .$addressInfo['billing']['postalCode'];?>
															<br> <?php echo $addressInfo['billing']['country'];?>
															<br> 
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-6 col-sm-12">
											<div class="portlet grey-cascade box">
												<div class="portlet-title">
													<div class="caption">
														<i class="fa fa-cogs"></i>Shipping Address </div>
												</div>
												<div class="portlet-body">
													<div class="row static-info">
														<div class="col-md-12 value"> <?php echo $addressInfo['delivery']['addressFullName'];?>
															<br> <?php echo $addressInfo['delivery']['addressLine1'];?>
															<br> <?php echo $addressInfo['delivery']['addressLine2'];?>
															<br> <?php echo $addressInfo['delivery']['addressLine3'] .', '.$addressInfo['delivery']['addressLine4'] .' ' .$addressInfo['delivery']['postalCode'];?>
															<br> <?php echo $addressInfo['delivery']['country'];?>
															<br>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 col-sm-12">
											<div class="portlet grey-cascade box">
												<div class="portlet-title">
													<div class="caption">
														<i class="fa fa-cogs"></i>Purchase Order Item Details </div>
												</div>
												<div class="portlet-body">
												<div class="table-responsive">
														<table class="table table-hover table-bordered table-striped">
															<thead>
																<tr>
																	<th> S.No</th>
																	<th> Order Row Id</th>
																	<th> Product Id</th>
																	<th> Product Sku</th>
																	<th> Size</th>
																	<th> Quantity </th>
																	<th> Price </th>
																</tr>
															</thead>
															<tbody>
															<?php
															foreach($items as $i => $item){
															?>
																<tr>
																	<td><?php echo $i + 1; ?> </td>
																	<td> <?php echo $item['rowId'];?> </td>
																	<td>
																		<a href="javascript:;"> <?php echo $item['productId'];?> </a>
																	</td>
																	<td> <?php echo $item['sku'];?></td>
																	<td> <?php echo $item['size'];?> </td>
																	<td> <?php echo $item['qty'];?> </td>
																	<td> <?php echo sprintf("%.2f",$item['price']);?> </td>
																	
																</tr>
															<?php
															}
															?>
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>								
								<div class="tab-pane" id="tab_2">
									<div class="row">
										<div class="col-md-12 col-sm-12">
											<div class="portlet grey-cascade box">
												<div class="portlet-title">
													<div class="caption">
														<i class="fa fa-cogs"></i>Acknowledged Item Details </div> 
												</div>
												<div class="portlet-body">
												<div class="table-responsive">
														<table class="table table-hover table-bordered table-striped">
															<thead>
																<tr>
																	<th> S.No</th>
																	<th> Order Row Id</th>
																	<th> Product Id</th>
																	<th> Product Sku</th>
																	<th> Quantity </th>
																	<th> Item Status </th>
																	<th> Fetched Date </th>
																	<th> Status </th>
																</tr>
															</thead>
															<tbody>
															<?php
															foreach($acknowledged as $i => $item){
																if($item){
															?>
																<tr>
																	<td><?php echo $i + 1; ?> </td>
																	<td>
																		<a href="javascript:;"> <?php echo $item['productId'];?> </a>
																	</td>
																	<td> <?php echo $item['sku'];?></td>
																	<td> <?php echo $item['rowId'];?> </td>
																	<td> <?php echo $item['qtyMessage'];?> </td>
																	<td> <?php echo $item['itemStatus'];?> </td>
																	<td> <?php echo date('M d, Y H:i:s',strtotime($item['created']));?> </td>
																	
																	<td>
																	<?php echo '<span class="label label-sm label-' . @$statusColor[$item['status']] . '">' . @$status[$item['status']] . '</span>';?>
																	</td>
																	
																</tr>
															<?php
															}
															}
															?>
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="tab_3">
									<div class="row">
										<div class="col-md-12 col-sm-12">
											<div class="portlet grey-cascade box">
												<div class="portlet-title">
													<div class="caption">
														<i class="fa fa-cogs"></i>Invoice Item Details </div>
												</div>
												<div class="portlet-body">
												<div class="table-responsive">
														<table class="table table-hover table-bordered table-striped">
															<thead>
																<tr>
																	<th> S.No</th>
																	<th> Order Row Id</th>
																	<th> Product Id</th>
																	<th> Product Sku</th>
																	<th> Quantity </th>
																	<th> Fetched Date </th>
																	<th> Status </th>
																</tr>
															</thead>
															<tbody>
															<?php
															foreach($receipt as $i => $item){
																if($item){
															?>
																<tr>
																	<td><?php echo $i + 1; ?> </td>
																	<td>
																		<a href="javascript:;"> <?php echo $item['productId'];?> </a>
																	</td>
																	<td> <?php echo $item['sku'];?></td>
																	<td> <?php echo $item['rowId'];?> </td>
																	<td> <?php echo $item['qtyMessage'];?> </td>
																	<td> <?php echo date('M d, Y H:i:s',strtotime($item['created']));?> </td>
																	<td>
																	<?php echo '<span class="label label-sm label-' . @$statusColor[$item['status']] . '">' . @$status[$item['status']] . '</span>';?>
																	</td>
																	
																</tr>
															<?php
															}
															}
															?>
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>								
							</div>
						</div>
					</div>
				</div>
				<!-- End: life time stats -->
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
