<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->>
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Mappings</span>
                </li>

            </ul>
        </div>
        <h3 class="page-title"> Packing
            <small>Mapping</small>
        </h3>
        <div class="portlet ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-shopping-cart"></i>Packing Mapping </div>
                <div class="actions">
                    <a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> Add New Mapping </span>
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="">          
                        <table class="table table-hover text-centered actiontable" id="sample_4">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="25%"> <?php echo $this->globalConfig['account2Name'];?> Trading Partner Id</th>
                                    <th width="25%"> <?php echo $this->globalConfig['account2Name'];?> Trading Partner Name</th>
                                    <th width="15%"> Save Id</th>
                                    <th width="15%"><?php echo $this->globalConfig['account2Name'];?> Save Id</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="clone hide">
                                    <td ><span class="value" data-value="id"></span></td>
                                    <td><span class="value" data-value="tradingIdPartnerId"></span></td>
                                    <td><span class="value" data-value="tradingIdPartnerId"></span></td>
                                    <td><span class="value" data-value="account1Id"></span></td>
                                    <td><span class="value" data-value="account2Id"></span></td>
                                    <td class="action">
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" delurl="<?php echo base_url('mapping/packedmapping/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php   foreach ($data['data'] as $key =>  $row) { ?>                               
                                <tr class="tr<?php echo $row['id'];?>">
                                    <td ><span class="value" data-value="id"><?php echo $key + 1;?></span></td>
                                    <td><span class="value" data-value="tradingIdPartnerId"><?php echo @($data['tradingIdPartnerId'][$row['tradingIdPartnerId']])?($data['tradingIdPartnerId'][$row['tradingIdPartnerId']]['name']):($row['tradingIdPartnerId']);?></span></td>
									<td><span class="value" data-value="tradingIdPartnerId"><?php echo @($this->tradingPartnerDatas[strtolower($row['tradingIdPartnerId'])])?($this->tradingPartnerDatas[strtolower($row['tradingIdPartnerId'])]['tradingIdPartnerName']):($row['tradingIdPartnerId']);?></span></td>
                                    <td><span class="value" data-value="account1Id"><?php echo @($data['account1Id'][$row['account1Id']])?($data['account1Id'][$row['account1Id']]['name']):($row['account1Id']);?></span></td>
                                    <td><span class="value" data-value="account2Id"><?php echo @($data['account2Id'][$row['account2Id']])?($data['account2Id'][$row['account2Id']]['name']):($row['account2Id']);?></span></td>
                                    <td class="action">
										<script> var data<?php echo $row['id'];?> = <?php echo json_encode($row);?>;</script>
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" onclick=editAction(data<?php echo $row['id'];?>) title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
										
                                        <a href="javascript:;" onclick="deleteAction('<?php echo base_url('mapping/packedmapping/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="actionmodal" role="dialog" data-backdrop="static">
            <div class="modal-dialog modal-lg">        
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Packing Mapping</h4>
                </div>
                <div class="modal-body">
                   <form action="<?php echo base_url('mapping/packedmapping/save');?>" method="post" id="saveActionForm" class="form-horizontal saveActionForm" novalidate="novalidate">
                        <div class="form-body">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                            
							<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-5"><?php echo $this->globalConfig['account1Name'];?> Save Id
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-7">
										<select name="data[account1Id]" data-required="1" class="form-control account1Id acc1list">
											<?php
											foreach ($data['account1Id'] as $account1Id) {
												echo '<option value = "'.$account1Id['id'].'">'.ucwords($account1Id['name']).'</option>';
											}
											?>
										</select>
									</div>
								</div>     
								<div class="form-group">
									<label class="control-label col-md-5"><?php echo $this->globalConfig['account2Name'];?> Save Id
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-7">
										<select name="data[account2Id]" data-required="1" class="form-control account2Id acc2list">
											<?php
											foreach ($data['account2Id'] as $account2Id) {
												echo '<option value = "'.$account2Id['id'].'">'.ucwords($account2Id['name']).'</option>';
											}
											?>
										</select>
									</div>
								</div>  
								<div class="form-group">
									<label class="control-label col-md-5">Default Box Id
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-7">
										<select name="data[boxId]" data-required="1" class="form-control boxId ">
											<?php
											foreach ($data['box'] as $box) {
												echo '<option value = "'.$box['id'].'">'.ucwords($box['boxName']).'</option>';
											}
											?>
										</select>
									</div>
								</div>  
								
								<div class="form-group">
									<label class="control-label col-md-5"> TradingIdPartnerId
										<span class="required" data-required="1" > * </span>
									</label>
									<div class="col-md-7">
										<input type="text" name="data[tradingIdPartnerId]"  class="form-control tradingIdPartnerId">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-5"> Tset Purpose Code
										<span class="required" data-required="1" > * </span>
									</label>
									<div class="col-md-7">
										<input type="text" name="data[TsetPurposeCode]"  class="form-control TsetPurposeCode">
									</div>
								</div>
								
								<div class="form-group">
									<label class="control-label col-md-5"> Tset Type Code
										<span class="required" data-required="1" > * </span>
									</label>
									<div class="col-md-7">
										<input type="text" name="data[TsetTypeCode]"  class="form-control TsetTypeCode">
									</div>
								</div>
								
								<div class="form-group">
									<label class="control-label col-md-5"> PackQualifier
										<span class="required" data-required="1" > * </span>
									</label>
									<div class="col-md-7">
										<input type="text" name="data[PackQualifier]"  class="form-control PackQualifier">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-5"> Pack Size
										<span class="required" data-required="1" > * </span>
									</label>
									<div class="col-md-7">
										<input type="text" name="data[PackSize]"  class="form-control PackSize">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-5"> PackUOM
										<span class="required"  > * </span>
									</label>
									<div class="col-md-7">
										<input type="text" name="data[PackUOM]"  class="form-control PackUOM">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-5"> PackingMaterial
										<span class="required"  > * </span>
									</label>
									<div class="col-md-7">
										<input type="text" name="data[PackingMaterial]"  class="form-control PackingMaterial">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-5"> ReferenceQual
										<span class="required"  > * </span>
									</label>
									<div class="col-md-7">
										<input type="text" name="data[ReferenceQual]"  class="form-control ReferenceQual">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-5"> MarksAndNumbersQualifier1
										<span class="required"  > * </span>
									</label>
									<div class="col-md-7">
										<input type="text" name="data[MarksAndNumbersQualifier1]"  class="form-control MarksAndNumbersQualifier1">
									</div>
								</div>
								
								</div>	
								<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-5"> PackWeightUOM
										<span class="required"  > * </span>
									</label>
									<div class="col-md-7">
										<input type="text" name="data[PackWeightUOM]"  class="form-control PackWeightUOM">
									</div>
								</div>	
								<div class="form-group">
									<label class="control-label col-md-5"> PackVolume
										<span class="required"  > * </span>
									</label>
									<div class="col-md-7">
										<input type="text" name="data[PackVolume]"  class="form-control PackVolume">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-5"> PackDimensionUOM
										<span class="required"  > * </span>
									</label>
									<div class="col-md-7">
										<input type="text" name="data[PackDimensionUOM]"  class="form-control PackDimensionUOM">
									</div>
								</div>	
								<div class="form-group">
									<label class="control-label col-md-5"> SurfaceLayerPositionCode
										<span class="required"  > * </span>
									</label>
									<div class="col-md-7">
										<input type="text" name="data[SurfaceLayerPositionCode]"  class="form-control SurfaceLayerPositionCode">
									</div>
								</div>								
								<div class="form-group">
									<label class="control-label col-md-5"> PackValue
										<span class="required"  > * </span>
									</label>
									<div class="col-md-7">
										<input type="text" name="data[PackValue]"  class="form-control PackValue">
									</div>
								</div>	
								<div class="form-group">
									<label class="control-label col-md-5"> AssignedID
										<span class="required"  > * </span>
									</label>
									<div class="col-md-7">
										<input type="text" name="data[AssignedID]"  class="form-control AssignedID">
									</div>
								</div>	
								<div class="form-group">
									<label class="control-label col-md-5"> PackingMedium
										<span class="required"  > * </span>
									</label>
									<div class="col-md-7">
										<input type="text" name="data[PackingMedium]"  class="form-control PackingMedium">
									</div>
								</div>	
								<div class="form-group">
									<label class="control-label col-md-5"> WeightQualifier
										<span class="required"  > * </span>
									</label>
									<div class="col-md-7">
										<input type="text" name="data[WeightQualifier]"  class="form-control WeightQualifier">
									</div>
								</div>	
								<div class="form-group">
									<label class="control-label col-md-5"> WeightUOM
										<span class="required"  > * </span>
									</label>
									<div class="col-md-7">
										<input type="text" name="data[WeightUOM]"  class="form-control WeightUOM">
									</div>
								</div>									
								<div class="form-group">
									<label class="control-label col-md-5"> ItemStatusCode
										<span class="required"  > * </span>
									</label>
									<div class="col-md-7">
										<input type="text" name="data[ItemStatusCode]"  class="form-control ItemStatusCode">
									</div>
								</div>	
								<div class="form-group">
									<label class="control-label col-md-5"> Send ShippingSerialID
										<span class="required"  > * </span>
									</label>
									<div class="col-md-7">
										<select name="data[sendShippingSerialID]"  class="form-control sendShippingSerialID">
											<option value="0">No</option>
											<option value="1">Yes</option>
											
										</select>
									</div>
								</div>	
								
									
								</div>  
								
								
								
							</div>     
                            
                        </div>
                        <input type="hidden" name="data[id]" class="id" />
                    </form>                         
                </div>
                <div class="modal-footer">
                  <button type="button" class="pull-left btn btn-primary submitAction">Save</button>
                  <button type="button" class="btn yellow btn-outline sbold" data-dismiss="modal">Close</button>
                </div>
              </div>                  
            </div>
        </div>
    </div>
</div>