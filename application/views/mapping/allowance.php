<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->>
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Mappings</span>
                </li>

            </ul>
        </div>
        <h3 class="page-title"> Allowance
            <small>Mapping</small>
        </h3>
        <div class="portlet ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-shopping-cart"></i>Allowance Mapping </div>
                <div class="actions">
                    <a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> Add New Mapping </span>
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="">          
                        <table class="table table-hover text-centered actiontable" id="sample_4">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="10%">Trading Partner Id</th>
                                    <th width="10%">Trading Partner Name</th>
                                    <th width="10%"><?php echo $this->globalConfig['account1Name'];?> Allowance Name</th>
                                    <th width="10%"><?php echo $this->globalConfig['account2Name'];?> Allowance Name</th>
                                    <th width="15%">Other Product Name</th>
                                    <th width="15%"><?php echo $this->globalConfig['account1Name'];?> Save Id</th>
                                    <th width="15%"><?php echo $this->globalConfig['account2Name'];?> Save Id</th>
                                    <th width="10%">Action</th>
                                </tr> 
                            </thead>
                            <tbody>
                                <tr class="clone hide">
                                    <td ><span class="value" data-value="id"></span></td>
                                    <td><span class="value" data-value="tradingPartnerId"></span></td>
                                    <td><span class="value" data-value="tradingPartnerId"></span></td>
                                    <td><span class="value" data-value="account1AllowanceId"></span></td>
                                    <td><span class="value" data-value="account2AllowanceId"></span></td>
                                    <td><span class="value" data-value="otherproductname"></span></td>
                                    <td><span class="value" data-value="account1Id"></span></td>
                                    <td><span class="value" data-value="account2Id"></span></td>
                                    <td class="action">
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" delurl="<?php echo base_url('mapping/allowance/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php   foreach ($data['data'] as $key =>  $row) { ?>                               
                                <tr class="tr<?php echo $row['id'];?>">
                                    <td ><span class="value" data-value="id"><?php echo $key + 1;?></span></td>
                                    <td><span class="value" data-value="tradingPartnerId"><?php echo @$row['tradingPartnerId'];?></span></td>
									<td><span class="value" data-value="tradingPartnerId"><?php echo @($this->tradingPartnerDatas[strtolower($row['tradingPartnerId'])])?($this->tradingPartnerDatas[strtolower($row['tradingPartnerId'])]['tradingIdPartnerName']):($row['tradingPartnerId']);?></span></td>

                                    <td><span class="value" data-value="account1AllowanceId"><?php echo @($data['account1AllowanceId'][$row['account1AllowanceId']])?($data['account1AllowanceId'][$row['account1AllowanceId']]['name']):($row['account1AllowanceId']);?></span></td>
                                    <td><span class="value" data-value="account2AllowanceId"><?php echo @($data['account2AllowanceId'][$row['account2AllowanceId']])?($data['account2AllowanceId'][$row['account2AllowanceId']]['name']):($row['account2AllowanceId']);?></span></td>
                                    <td><span class="value" data-value="otherproductname"><?php echo @$row['otherproductname'];?></span></td>
                                    <td><span class="value" data-value="account1Id"><?php echo @($data['account1Id'][$row['account1Id']])?($data['account1Id'][$row['account1Id']]['name']):($row['account1Id']);?></span></td>
                                    <td><span class="value" data-value="account2Id"><?php echo @($data['account2Id'][$row['account2Id']])?($data['account2Id'][$row['account2Id']]['name']):($row['account2Id']);?></span></td>
                                    <td class="action">
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" onclick='editAction(<?php echo json_encode($row);?>)' title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" onclick="deleteAction('<?php echo base_url('mapping/allowance/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="actionmodal" role="dialog" data-backdrop="static">
            <div class="modal-dialog">        
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Payment Mapping</h4>
                </div>
                <div class="modal-body">
                   <form action="<?php echo base_url('mapping/allowance/save');?>" method="post" id="saveActionForm" class="form-horizontal saveActionForm" novalidate="novalidate">
                        <div class="form-body">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-4"><?php echo $this->globalConfig['account1Name'];?> Save Id
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-7">
                                    <select name="data[account1Id]" data-required="1" class="form-control account1Id acc1list">
										<option value="">Select Brightpearl Product</option>
                                        <?php
                                        foreach ($data['account1Id'] as $account1Id) {
                                            echo '<option value = "'.$account1Id['id'].'">'.ucwords($account1Id['name']).'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>     
                            <div class="form-group">
                                <label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Save Id
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-7">
                                    <select name="data[account2Id]" data-required="1" class="form-control account2Id acc2list">
                                        <?php
                                        foreach ($data['account2Id'] as $account2Id) {
                                            echo '<option value = "'.$account2Id['id'].'">'.ucwords($account2Id['name']).'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>  

                            <div class="form-group">
                                <label class="control-label col-md-4"><?php echo $this->globalConfig['account1Name'];?> tradingPartnerId
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-7">
                                    <?php if(@$data['tradingPartnerId']){ ?> 
                                    <select name="data[tradingPartnerId]" data-required="1" class="form-control tradingPartnerId acc1listoption">
                                        <option value="">Select a <?php echo $this->globalConfig['account1Name'];?> Allowance Name</option>
                                        <?php
                                        foreach ($data['tradingPartnerId'] as $accountId => $account1AllowanceIds) {
                                            foreach ($account1AllowanceIds as $account1AllowanceId) {
                                                echo '<option class="acc1listoption'.$accountId.'"  value="'.$account1AllowanceId['id'].'">'.ucwords($account1AllowanceId['name']).'</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                    <?php } else { ?>
                                        <input type="text" name="data[tradingPartnerId]" data-required="1" class="form-control tradingPartnerId">
                                    <?php } ?>
                                </div>
                            </div> 
							<div class="form-group">
                                <label class="control-label col-md-4"><?php echo $this->globalConfig['account1Name'];?> Allowance Name
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-7">
                                    <?php if(@$data['account1AllowanceId']){ ?> 
                                    <select name="data[account1AllowanceId]" class="form-control account1AllowanceId acc1listoption">
                                        <option class="show" value="" style="display:block !important;">Select a <?php echo $this->globalConfig['account1Name'];?> Allowance Name</option>
                                        <?php
                                        foreach ($data['account1AllowanceId'] as $accountId => $account1AllowanceIds) {
                                            foreach ($account1AllowanceIds as $account1AllowanceId) {
                                                echo '<option class="acc1listoption'.$accountId.'"  value="'.$account1AllowanceId['id'].'">'.ucwords($account1AllowanceId['name']).'</option>';
                                            }
                                        }
                                        ?> 
                                    </select>
                                    <?php } else { ?>
                                        <input type="text" name="data[account1AllowanceId]" data-required="1" class="form-control account1AllowanceId">
                                    <?php } ?>
                                </div>
                            </div> 
							
							<div class="form-group">
                                <label class="control-label col-md-4">Or Other Product Name
                                    <span class="required" > * </span>
                                </label>
                                <div class="col-md-7">                                    
									<input type="text" name="data[otherproductname]" class="form-control otherproductname">
                                </div>
                            </div>    
							
                            <div class="form-group">
                                <label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Allowance Name
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-7">
                                    <?php if(@$data['account2AllowanceId']){ ?> 
                                    <select name="data[account2AllowanceId]" data-required="1" class="form-control account2AllowanceId acc2listoption">
                                        <option value="">Select a <?php echo $this->globalConfig['account2Name'];?> Allowance Name</option>
                                        <?php
                                        foreach ($data['account2AllowanceId'] as $accountId => $account2AllowanceIds) {
                                            foreach ($account2AllowanceIds as $account2AllowanceId) {
                                                echo '<option class="acc2listoption'.$accountId.'" value="'.$account2AllowanceId['id'].'">'.ucwords($account2AllowanceId['name']).'</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                    <?php } else { ?>
                                        <input type="text" name="data[account2AllowanceId]" data-required="1" class="form-control account2AllowanceId">
                                    <?php } ?>
                                </div>
                            </div>     
                            
                        </div>
                        <input type="hidden" name="data[id]" class="id" />
                    </form>                         
                </div>
                <div class="modal-footer">
                  <button type="button" class="pull-left btn btn-primary submitAction">Save</button>
                  <button type="button" class="btn yellow btn-outline sbold" data-dismiss="modal">Close</button>
                </div>
              </div>                  
            </div>
        </div>
    </div>
</div>