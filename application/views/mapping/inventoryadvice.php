<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->>
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Mappings</span>
                </li>

            </ul>
        </div>
        <h3 class="page-title"> Inventory Advice
            <small>Mapping</small>
        </h3>
        <div class="portlet ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-shopping-cart"></i>Inventory Advice Mapping </div>
                <div class="actions">
                    <a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> Add New Mapping </span>
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-responsive">          
                        <table class="table table-hover text-centered actiontable">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="25%"> <?php echo $this->globalConfig['account2Name'];?> Trading Partner Id</th>
                                    <th width="25%"> <?php echo $this->globalConfig['account2Name'];?> Trading Partner Name</th>
                                    <th width="15%"> Save Id</th>
                                    <th width="15%"><?php echo $this->globalConfig['account2Name'];?> Save Id</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="clone hide">
                                    <td ><span class="value" data-value="id"></span></td>
                                    <td><span class="value" data-value="tradingIdPartnerId"></span></td>
                                    <td><span class="value" data-value="tradingIdPartnerId"></span></td>
                                    <td><span class="value" data-value="account1Id"></span></td>
                                    <td><span class="value" data-value="account2Id"></span></td>
                                    <td class="action">
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" delurl="<?php echo base_url('mapping/inventoryadvice/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php   foreach ($data['data'] as $key =>  $row) { ?>                               
                                <tr class="tr<?php echo $row['id'];?>">
                                    <td ><span class="value" data-value="id"><?php echo $key + 1;?></span></td>
                                    <td><span class="value" data-value="tradingIdPartnerId"><?php echo @($data['tradingIdPartnerId'][$row['tradingIdPartnerId']])?($data['tradingIdPartnerId'][$row['tradingIdPartnerId']]['name']):($row['tradingIdPartnerId']);?></span></td>
									<td><span class="value" data-value="tradingIdPartnerId"><?php echo @($this->tradingPartnerDatas[strtolower($row['tradingIdPartnerId'])])?($this->tradingPartnerDatas[strtolower($row['tradingIdPartnerId'])]['tradingIdPartnerName']):($row['tradingIdPartnerId']);?></span></td>

                                    <td><span class="value" data-value="account1Id"><?php echo @($data['account1Id'][$row['account1Id']])?($data['account1Id'][$row['account1Id']]['name']):($row['account1Id']);?></span></td>
                                    <td><span class="value" data-value="account2Id"><?php echo @($data['account2Id'][$row['account2Id']])?($data['account2Id'][$row['account2Id']]['name']):($row['account2Id']);?></span></td>
                                    <td class="action">
										<script> var data<?php echo $row['id'];?> = <?php echo json_encode($row);?>;</script>
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" onclick=editAction(data<?php echo $row['id'];?>) title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
										
                                        <a href="javascript:;" onclick="deleteAction('<?php echo base_url('mapping/inventoryadvice/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="actionmodal" role="dialog" data-backdrop="static">
            <div class="modal-dialog modal-lg">        
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Inventory Advice Mapping</h4>
                </div>
                <div class="modal-body">
                   <form action="<?php echo base_url('mapping/inventoryadvice/save');?>" method="post" id="saveActionForm" class="form-horizontal saveActionForm" novalidate="novalidate">
                        <div class="form-body">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                            
							<div class="row">
							<div class="col-md-5">
								<div class="form-group">
									<label class="control-label col-md-6"><?php echo $this->globalConfig['account1Name'];?> Save Id
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<select name="data[account1Id]" data-required="1" class="form-control account1Id acc1list">
											<?php
											foreach ($data['account1Id'] as $account1Id) {
												echo '<option value = "'.$account1Id['id'].'">'.ucwords($account1Id['name']).'</option>';
											}
											?>
										</select>
									</div>
								</div>     
								<div class="form-group">
									<label class="control-label col-md-6"><?php echo $this->globalConfig['account2Name'];?> Save Id
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<select name="data[account2Id]" data-required="1" class="form-control account2Id acc2list">
											<?php
											foreach ($data['account2Id'] as $account2Id) {
												echo '<option value = "'.$account2Id['id'].'">'.ucwords($account2Id['name']).'</option>';
											}
											?>
										</select>
									</div>
								</div>  
								<div class="form-group hide">
									<label class="control-label col-md-6"><?php echo $this->globalConfig['account2Name'];?> Warehouse
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<?php if(@$data['warehouseId']){ ?> 
										<select name="data[warehouseId]" class="form-control warehouseId acc2list">
											<option value="" class="show">Select a <?php echo $this->globalConfig['account1Name'];?> Warehouse Method</option>
											<?php
											foreach ($data['warehouseId'] as $accountId => $warehouseIds) {
												foreach ($warehouseIds as $warehouseId) {
													echo '<option value="'.$warehouseId['id'].'">'.ucwords($warehouseId['name']).'</option>';
												}
											}
											?>
										</select>
										<?php } else { ?>
											<input type="text" name="data[warehouseId]"  data-required="1" class="form-control warehouseId">
										<?php } ?>
									</div>
								</div> 
								
								<div class="form-group">
									<label class="control-label col-md-6"> Trading Partner Id
										<span class="required" data-required="1" > * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[tradingIdPartnerId]"  class="form-control tradingIdPartnerId">
									</div>
								</div>								
								<div class="form-group">
									<label class="control-label col-md-6">Cost Price List
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<select name="data[defaultProductPriceList]" data-required="1" class="form-control defaultProductPriceList">
											<?php
											foreach ($data['pricelist'] as $accountId => $pricelists) {
												foreach ($pricelists as $pricelist) {
													echo '<option value="'.$pricelist['id'].'">'.ucwords($pricelist['name']).'</option>';
												}
											}
											?>
										</select> 
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-6"> Vendor
										<span class="required" data-required="1" > * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[Vendor]"  class="form-control Vendor">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-6"> File Type
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<select name="data[fileType]"  class="form-control fileType">
											<option value="0">Individual</option>
											<option value="1">Consolidated</option>
											<option value="2">Consolidated with Sub Location</option>
											
										</select>
									</div>
								</div> 
								<div class="form-group">
									<label class="control-label col-md-6"> TsetPurposeCode
										<span class="required" data-required="1" > * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[TsetPurposeCode]"  class="form-control TsetPurposeCode">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-6"> Report Type Code
										<span class="required"  > * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[ReportTypeCode]"  class="form-control ReportTypeCode">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-6"> Total Qty UOM
										<span class="required"  > * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[TotalQtyUOM]"  class="form-control TotalQtyUOM">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-6"> Item Description Type
										<span class="required"  > * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[ItemDescriptionType]"  class="form-control ItemDescriptionType">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-6"> Quantity Qualifier
										<span class="required"  > * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[QuantityQualifier]"  class="form-control QuantityQualifier">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-6"> Out Of Stock Qty Qualifier
										<span class="required"  > * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[QuantityQualifierOutStock]"  class="form-control QuantityQualifierOutStock">
									</div>
								</div>
								
								<div class="form-group">
									<label class="control-label col-md-6">Future Qty Qualifier
										<span class="required"  > * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[futureQuantityQualifier]"  class="form-control futureQuantityQualifier">
									</div>
								</div>	
								<div class="form-group">
									<label class="control-label col-md-6"> Date Time Qualifier
										<span class="required"  > * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[DateTimeQualifier1]"  class="form-control DateTimeQualifier1">
									</div>
								</div>	
								<div class="form-group">
									<label class="control-label col-md-6">Date Time Qualifier if available qty is 0
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<select name="data[sendDateAvailabeZero]"  class="form-control sendDateAvailabeZero">
											<option value="0">No</option>
											<option value="1">Yes</option>
											
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-6"> AddressTypeCode
										<span class="required"  > * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[AddressTypeCode]"  class="form-control AddressTypeCode">
									</div>
								</div>	
								
								</div>  
								
								
							<div class="col-md-7">					
								<div class="form-group">
									<label class="control-label col-md-7"> Send Global Address(Individual file)
										<span class="required"  > * </span>
									</label>
									<div class="col-md-5">
										<select name="data[sendGlobalAddressTypeCode]"  class="form-control sendGlobalAddressTypeCode">
											<option value="0">No</option>
											<option value="1">Yes</option>
											
										</select>
									</div>
								</div>	
								<div class="form-group">
									<label class="control-label col-md-7"> Global Address Type Code (Individual file)
										<span class="required"  > * </span>
									</label>
									<div class="col-md-5">
										<input type="text" name="data[globalAddressTypeCode]"  class="form-control globalAddressTypeCode">
									</div>
								</div>	
								
								<div class="form-group">
									<label class="control-label col-md-7"> AddressLocationNumber
										<span class="required" data-required="1" > * </span>
									</label>
									<div class="col-md-5">
										<input type="text" name="data[AddressLocationNumber]"  class="form-control AddressLocationNumber">
									</div>
								</div>	
							<div class="form-group">
								<label class="control-label col-md-7"> AddressName
									<span class="required"  > * </span>
								</label>
								<div class="col-md-5">
									<input type="text" name="data[AddressName]"  class="form-control AddressName">
								</div>
							</div>																
							<div class="form-group">
								<label class="control-label col-md-7"> Address1
									<span class="required"  > * </span>
								</label>
								<div class="col-md-5">
									<input type="text" name="data[Address1]"  class="form-control Address1">
								</div>
							</div>	
							<div class="form-group">
								<label class="control-label col-md-7"> City
									<span class="required"  > * </span>
								</label>
								<div class="col-md-5">
									<input type="text" name="data[City]"  class="form-control City">
								</div>
							</div>	
							<div class="form-group">
								<label class="control-label col-md-7"> State
									<span class="required"  > * </span>
								</label>
								<div class="col-md-5">
									<input type="text" name="data[State]"  class="form-control State">
								</div>
							</div>	
							<div class="form-group">
								<label class="control-label col-md-7"> PostalCode
									<span class="required"  > * </span>
								</label>
								<div class="col-md-5">
									<input type="text" name="data[PostalCode]"  class="form-control PostalCode">
								</div>
							</div>	
							<div class="form-group">
								<label class="control-label col-md-7"> Country
									<span class="required"  > * </span>
								</label>
								<div class="col-md-5">
									<input type="text" name="data[Country]"  class="form-control Country">
								</div>
							</div>
							<div class="form-group">
									<label class="control-label col-md-7"> Add BuyerPartNumber in Item
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-5">
										<select name="data[showBuyerPartNumber]"  class="form-control showBuyerPartNumber">
											<option value="0">No</option>
											<option value="1">Yes</option>
											
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-7"> Add VendorPartNumber in Item
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-5">
										<select name="data[showVenderPartNumber]"  class="form-control showVenderPartNumber">
											<option value="0">No</option>
											<option value="1">Yes</option>
											
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-7"> Add ConsumerPackageCode in Item
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-5">
										<select name="data[showConsumerPackageCode]"  class="form-control showConsumerPackageCode">
											<option value="0">No</option>
											<option value="1">Yes</option>
											
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-7"> Add sendDiscontinued in Item
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-5">
										<select name="data[sendDiscontinued]"  class="form-control sendDiscontinued">
											<option value="0">No</option>
											<option value="1">Yes</option>
											
										</select>
									</div>
								</div>
								
								<div class="form-group">
									<label class="control-label col-md-7"> Enable Unusable Quantity
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-5">
										<select name="data[enableUnsuableQty]"  class="form-control enableUnsuableQty">
											<option value="0">No</option>
											<option value="1">Yes</option>
											
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-7"> Add Future stock with onHand
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-5">
										<select name="data[futureStockType]"  class="form-control futureStockType">
											<option value="0">No</option>
											<option value="2">Always</option>
											<option value="1">If Qty Is 0</option>
											
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-7"> Add Future stock with ETA
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-5">
										<select name="data[futureEtaStockType]"  class="form-control futureEtaStockType">
											<option value="0">No</option>
											<option value="1">All mapped warehouse</option>
											<option value="2">Selected below warehouse</option>
											
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-7"> Warehouse for ETA stock
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-5">
										<input type="text" name="data[futureEtaStockWarehouse]"  class="form-control futureEtaStockWarehouse">
									</div>
								</div>
									
								
								
								
								</div>  
								
								
								
							</div>     
                            
                        </div>
                        <input type="hidden" name="data[id]" class="id" />
                    </form>                         
                </div>
                <div class="modal-footer">
                  <button type="button" class="pull-left btn btn-primary submitAction">Save</button>
                  <button type="button" class="btn yellow btn-outline sbold" data-dismiss="modal">Close</button>
                </div>
              </div>                  
            </div>
        </div>
    </div>
</div>