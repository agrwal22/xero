<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->>
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Mappings</span>
                </li>

            </ul>
        </div>
        <h3 class="page-title"> Box
            <small>Mapping</small>
        </h3>
        <div class="portlet ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-shopping-cart"></i>Box Mapping </div>
                <div class="actions">
                    <a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> Add New Box </span>
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="">          
                        <table class="table table-hover text-centered actiontable" id="sample_4">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="10%">Box Name</th>
                                    <th width="10%">Length</th>
                                    <th width="10%">Width</th>
                                    <th width="10%">Height</th>
                                    <th width="10%">Weight</th>                                   
                                    <th width="12%"><?php echo $this->globalConfig['account1Name'];?> Account</th>
                                    <th width="10%"><?php echo $this->globalConfig['account2Name'];?> Account</th>                              
                                    <th width="10%" class="action">Action</th>
                                </tr> 
                            </thead>
                            <tbody>
                                <tr class="clone hide">
                                    <td ><span class="value" data-value="id"></span></td>
                                    <td><span class="value" data-value="boxName"></span></td>
                                    <td><span class="value" data-value="length"></span></td>
                                    <td><span class="value" data-value="width"></span></td>
                                    <td><span class="value" data-value="height"></span></td>
                                    <td><span class="value" data-value="weight"></span></td>
                                    <td><span class="value" data-value="account1Id"></span></td>
                                    <td><span class="value" data-value="account2Id"></span></td>
                                    <td class="action">
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" delurl="<?php echo base_url('mapping/box/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php   foreach ($data['data'] as $key =>  $row) { ?>                               
                                <tr class="tr<?php echo $row['id'];?>">
                                    <td ><span class="value" data-value="id"><?php echo $key + 1;?></span></td>
                                    <td><span class="value" data-value="boxName"><?php echo @$row['boxName'];?></span></td>
                                    <td><span class="value" data-value="length"><?php echo @$row['length'];?></span></td>
                                    <td><span class="value" data-value="width"><?php echo @$row['width'];?></span></td>
                                    <td><span class="value" data-value="height"><?php echo @$row['height'];?></span></td>
                                    <td><span class="value" data-value="weight"><?php echo @$row['weight'];?></span></td>
                                    <td><span class="value" data-value="account1Id"><?php echo @($data['account1Id'][$row['account1Id']])?($data['account1Id'][$row['account1Id']]['name']):($row['account1Id']);?></span></td>
                                    <td><span class="value" data-value="account2Id"><?php echo @($data['account2Id'][$row['account2Id']])?($data['account2Id'][$row['account2Id']]['name']):($row['account2Id']);?></span></td>
                                    <td class="action">
										<script> var data<?php echo $row['id'];?> = <?php echo json_encode($row);?>;</script>
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" onclick='editAction(data<?php echo $row['id'];?>)' title="View"><i class="fa fa-edit" title="Edit settings" ></i></a> 
                                        <a href="javascript:;" onclick="deleteAction('<?php echo base_url('mapping/box/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="actionmodal" role="dialog" data-backdrop="static">
            <div class="modal-dialog">        
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Payment Mapping</h4>
                </div>
                <div class="modal-body">
                   <form action="<?php echo base_url('mapping/box/save');?>" method="post" id="saveActionForm" class="form-horizontal saveActionForm" novalidate="novalidate">
                        <div class="form-body">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-4"><?php echo $this->globalConfig['account1Name'];?> Save Id
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-7">
                                    <select name="data[account1Id]" data-required="1" class="form-control account1Id acc1list">
                                        <?php
                                        foreach ($data['account1Id'] as $account1Id) {
                                            echo '<option value = "'.$account1Id['id'].'">'.ucwords($account1Id['name']).'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>     
                            <div class="form-group">
                                <label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Save Id
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-7">
                                    <select name="data[account2Id]" data-required="1" class="form-control account2Id acc2list">
                                        <?php
                                        foreach ($data['account2Id'] as $account2Id) {
                                            echo '<option value = "'.$account2Id['id'].'">'.ucwords($account2Id['name']).'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div> 
							
							<div class="form-group">
                                <label class="control-label col-md-4">Box Name
                                    <span class="required" >  </span>
                                </label>
                                <div class="col-md-7">                                    
									<input type="text" name="data[boxName]" class="form-control boxName">
                                </div>
                            </div>    
							<div class="form-group">
                                <label class="control-label col-md-4">Length
                                    <span class="required" >  </span>
                                </label>
                                <div class="col-md-7">                                    
									<input type="text" name="data[length]" class="form-control length">
                                </div>
                            </div>    
							<div class="form-group">
                                <label class="control-label col-md-4">Width
                                    <span class="required" >  </span>
                                </label>
                                <div class="col-md-7">                                    
									<input type="text" name="data[width]" class="form-control width">
                                </div>
                            </div>    
							<div class="form-group">
                                <label class="control-label col-md-4">Height
                                    <span class="required" >  </span>
                                </label>
                                <div class="col-md-7">                                    
									<input type="text" name="data[height]" class="form-control height">
                                </div>
                            </div>    
							<div class="form-group">
                                <label class="control-label col-md-4">Weight
                                    <span class="required" >  </span>
                                </label>
                                <div class="col-md-7">                                    
									<input type="text" name="data[weight]" class="form-control weight">
                                </div>
                            </div>    
							
                                
                            
                        </div>
                        <input type="hidden" name="data[id]" class="id" />
                    </form>                         
                </div>
                <div class="modal-footer">
                  <button type="button" class="pull-left btn btn-primary submitAction">Save</button>
                  <button type="button" class="btn yellow btn-outline sbold" data-dismiss="modal">Close</button>
                </div>
              </div>                  
            </div>
        </div>
    </div>
</div>