<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->>
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Mappings</span>
                </li>

            </ul>
        </div>
        <h3 class="page-title"> Trading Partner
            <small>Mapping</small>
        </h3>
        <div class="portlet ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-shopping-cart"></i>Trading Partner Mapping </div>
                <div class="actions">
                    <a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> Add New Mapping </span>
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="">          
                        <table class="table table-hover text-centered actiontable" id="sample_4">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="20%"> <?php echo $this->globalConfig['account2Name'];?> Trading Partner Id</th>
                                    <th width="20%"> <?php echo $this->globalConfig['account2Name'];?> Trading Partner Name</th>
                                    <th width="15%"> <?php echo $this->globalConfig['account1Name'];?> Customer Id</th>
                                    <th width="15%"> Save Id</th>
                                    <th width="15%"><?php echo $this->globalConfig['account2Name'];?> Save Id</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="clone hide">
                                    <td ><span class="value" data-value="id"></span></td>
                                    <td><span class="value" data-value="tradingIdPartnerId"></span></td>
                                    <td><span class="value" data-value="tradingIdPartnerName"></span></td>
                                    <td><span class="value" data-value="customerId"></span></td>
                                    <td><span class="value" data-value="account1Id"></span></td>
                                    <td><span class="value" data-value="account2Id"></span></td>
                                    <td class="action">
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" delurl="<?php echo base_url('mapping/trading/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php   foreach ($data['data'] as $key =>  $row) { ?>                               
                                <tr class="tr<?php echo $row['id'];?>">
                                    <td ><span class="value" data-value="id"><?php echo $key + 1;?></span></td>
                                    <td><span class="value" data-value="tradingIdPartnerId"><?php echo @($data['tradingIdPartnerId'][$row['tradingIdPartnerId']])?($data['tradingIdPartnerId'][$row['tradingIdPartnerId']]['name']):($row['tradingIdPartnerId']);?></span></td>
                                    <td><span class="value" data-value="tradingIdPartnerName"><?php echo @($data['tradingIdPartnerName'][$row['tradingIdPartnerName']])?($data['tradingIdPartnerName'][$row['tradingIdPartnerName']]['name']):($row['tradingIdPartnerName']);?></span></td>
                                    <td><span class="value" data-value="customerId"><?php echo @($data['customerId'][$row['customerId']])?($data['customerId'][$row['customerId']]['name']):($row['customerId']);?></span></td>
                                    <td><span class="value" data-value="account1Id"><?php echo @($data['account1Id'][$row['account1Id']])?($data['account1Id'][$row['account1Id']]['name']):($row['account1Id']);?></span></td>
                                    <td><span class="value" data-value="account2Id"><?php echo @($data['account2Id'][$row['account2Id']])?($data['account2Id'][$row['account2Id']]['name']):($row['account2Id']);?></span></td>
                                    <td class="action">
										<script> var data<?php echo $row['id'];?> = <?php echo json_encode($row);?>;</script>
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" onclick=editAction(data<?php echo $row['id'];?>) title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" onclick="deleteAction('<?php echo base_url('mapping/trading/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="actionmodal" role="dialog" data-backdrop="static">
            <div class="modal-dialog modal-lg">        
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Payment Mapping</h4>
                </div>
                <div class="modal-body">
                   <form action="<?php echo base_url('mapping/trading/save');?>" method="post" id="saveActionForm" class="form-horizontal saveActionForm" novalidate="novalidate">
                        <div class="form-body">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                            
							<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-6"><?php echo $this->globalConfig['account1Name'];?> Save Id
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<select name="data[account1Id]" data-required="1" class="form-control account1Id acc1list">
											<?php
											foreach ($data['account1Id'] as $account1Id) {
												echo '<option value = "'.$account1Id['id'].'">'.ucwords($account1Id['name']).'</option>';
											}
											?>
										</select>
									</div>
								</div>     
								<div class="form-group">
									<label class="control-label col-md-6"><?php echo $this->globalConfig['account2Name'];?> Save Id
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<select name="data[account2Id]" data-required="1" class="form-control account2Id acc2list">
											<?php
											foreach ($data['account2Id'] as $account2Id) {
												echo '<option value = "'.$account2Id['id'].'">'.ucwords($account2Id['name']).'</option>';
											}
											?>
										</select>
									</div>
								</div>  
								<div class="form-group">
									<label class="control-label col-md-6"> Price List
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<?php if(@$data['pricelist']){ ?> 
										<select name="data[pricelist]" data-required="1" class="form-control pricelist">
											<option value="">Select a Price List</option>
											<?php
											foreach ($data['pricelist'] as $accountId => $account1TradingIds) {
												foreach ($account1TradingIds as $account1TradingId) {
													echo '<option class="acc1listoption'.$accountId.'"  value="'.$account1TradingId['id'].'">'.ucwords($account1TradingId['name']).'</option>';
												}
											}
											?>
										</select>
										<?php } else { ?>
											<input type="text" name="data[pricelist]" data-required="1" class="form-control pricelist">
										<?php } ?>
									</div>
								</div> 
								<div class="form-group">
									<label class="control-label col-md-6"> Channel
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<?php if(@$data['channel']){ ?> 
										<select name="data[channelId]"  class="form-control channelId">
											<option value="">Select a  Channel</option>
											<?php
											foreach ($data['channel'] as $accountId => $account1TradingIds) {
												foreach ($account1TradingIds as $account1TradingId) {
													echo '<option class="acc1listoption'.$accountId.'"  value="'.$account1TradingId['id'].'">'.ucwords($account1TradingId['name']).'</option>';
												}
											}
											?>
										</select>
										<?php } else { ?>
											<input type="text" name="data[channelId]"  class="form-control channelId">
										<?php } ?>
									</div>
								</div> 
								<div class="form-group">
									<label class="control-label col-md-6"> Shipping Method
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<?php if(@$data['shippingmethods']){ ?> 
										<select name="data[shippingMethod]"  class="form-control shippingMethod">
											<option value="">Select a Shipping Method</option>
											<?php
											foreach ($data['shippingmethods'] as $accountId => $account1TradingIds) {
												foreach ($account1TradingIds as $account1TradingId) {
													echo '<option class="acc1listoption'.$accountId.'"  value="'.$account1TradingId['id'].'">'.ucwords($account1TradingId['name']).'</option>';
												}
											}
											?>
										</select>
										<?php } else { ?>
											<input type="text" name="data[shippingMethod]"  class="form-control shippingMethod">
										<?php } ?>
									</div>
								</div> 
								<div class="form-group">
									<label class="control-label col-md-6"> Enable Packing Interface
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<select name="data[enablePacking]"  class="form-control enablePacking">
											<option value="0">No</option>
											<option value="1">Yes</option>
											
										</select>
									</div>
								</div> 
								<div class="form-group">
									<label class="control-label col-md-6"> Automatic allocate order
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<select name="data[allocateOrder]"  class="form-control allocateOrder">
											<option value="0">No</option>
											<option value="1">Yes</option>
											
										</select>
									</div>
								</div>								
								<div class="form-group">
									<label class="control-label col-md-6"> Automatic upload 855 file
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<select name="data[autoAcknowledgment]"  class="form-control autoAcknowledgment">
											<option value="0">No</option>
											<option value="1">Yes</option>
											
										</select>
									</div>
								</div> 
								<div class="form-group">
									<label class="control-label col-md-6"> Get SKU from file
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<select name="data[productSku]"  class="form-control productSku">
											<option value="vendorSKU">vendorSKU</option>
											<option value="merchantSKU">merchantSKU</option>
											<option value="shoppingCartSKU">shoppingCartSKU</option>
											<option value="UPC">UPC</option>
											
										</select>
									</div>
								</div> 
								<div class="form-group">
									<label class="control-label col-md-6"> Match SKU to Brightpearl
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<select name="data[searchProductInBP]"  class="form-control searchProductInBP">
											<option value="SKU">SKU</option>
											<option value="EAN">EAN</option>
											<option value="Barcode">Barcode</option>
											
										</select>
									</div>
								</div> 
								<div class="form-group">
									<label class="control-label col-md-6"> Invoice Type Code
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<select name="data[InvoiceTypeCode]"  class="form-control InvoiceTypeCode">
											<option value="">Select Invoice Type Code </option>
											<option value="CR">Credit</option>
											<option value="DR">Debit</option>
											<option value="DI">Debit DI</option>
											
										</select>
									</div>
								</div> 
								<div class="form-group">
									<label class="control-label col-md-6"> Allow Charge Indicator
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<select name="data[AllowChrgIndicator]"  class="form-control AllowChrgIndicator">
											<option value="">Select Allow Charge Indicator  </option>
											<option value="A">Allowance</option>
											<option value="C">Charge</option> 
											
										</select>
									</div>
								</div> 								 
								<div class="form-group">
									<label class="control-label col-md-6"> Mono-location mode
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<select name="data[monoLocation]"  class="form-control monoLocation">
											<option value="0">No</option>
											<option value="1">Yes</option>
											
										</select>
									</div>
								</div> 
								<div class="form-group">
									<label class="control-label col-md-6"> Enable backorder
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<select name="data[sendBackOrder]"  class="form-control sendBackOrder">
											<option value="0">No</option>
											<option value="1">Yes</option>
											
										</select>
									</div>
								</div> 
								<div class="form-group">
									<label class="control-label col-md-6"> Send 856 instead of 855
										<span class="required"  > * </span>
									</label>
									<div class="col-md-6">
										<select name="data[disableAck]"  class="form-control disableAck">
											<option value="0">No</option>
											<option value="1">Yes</option>
											
										</select>
									</div>
								</div>	
								<div class="form-group">
									<label class="control-label col-md-6"> Send 856 for canceled order
										<span class="required"  > * </span>
									</label>
									<div class="col-md-6">
										<select name="data[sendCancelAsPacked]"  class="form-control sendCancelAsPacked">
											<option value="0">No</option>
											<option value="1">Yes</option>
											
										</select>
									</div>
								</div>	
								
								<div class="form-group">
									<label class="control-label col-md-6"> Enable cancel reason 
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<select name="data[sendCancelReason]"  class="form-control sendCancelReason">
											<option value="0">No</option>
											<option value="1">Yes</option>
											
										</select>
									</div>
								</div> 
								<div class="form-group">
									<label class="control-label col-md-6"> Cancel Code
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[cancode]"  class="form-control cancode">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-6"> ItemScheduleQualifier
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[ItemScheduleQualifier]"  class="form-control ItemScheduleQualifier">
									</div>
								</div>
								
								<div class="form-group">
									<label class="control-label col-md-6"> Trading Partner Id
										<span class="required" data-required="1" > * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[tradingIdPartnerId]"  class="form-control tradingIdPartnerId">
									</div>
								</div> 
								<div class="form-group">
									<label class="control-label col-md-6"> Trading Partner Name
										<span class="required" data-required="1" > * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[tradingIdPartnerName]"  class="form-control tradingIdPartnerName">
									</div>
								</div> 
								<div class="form-group">
									<label class="control-label col-md-6"> Brightpearl CustomerId
										<span class="required" data-required="1" > * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[customerId]"  class="form-control customerId">
									</div>
								</div> 
								
								</div> 
								
								
								<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-6"> Default Country COde
										<span class="required" > * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[countryCode]"  class="form-control countryCode">
									</div>
								</div> 
								<div class="form-group">
									<label class="control-label col-md-6"> LocationCodeQualifier
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[LocationCodeQualifier]"  class="form-control LocationCodeQualifier">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-6"> AddressLocationNumber
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[AddressLocationNumber]"  class="form-control AddressLocationNumber">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-6"> Add Carrier in Invoice Item
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<select name="data[addCarrierItem]"  class="form-control addCarrierItem">
											<option value="0">No</option>
											<option value="1">Yes</option>
											
										</select>
									</div>
								</div> 								 
								<div class="form-group">
									<label class="control-label col-md-6"> Carrier Alpha Code
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[CarrierAlphaCode]"  class="form-control CarrierAlphaCode">
									</div>
								</div> 
								
								<div class="form-group">
									<label class="control-label col-md-6"> Carrier Routing
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[CarrierRouting]"  class="form-control CarrierRouting">
									</div>
								</div> 
								
								<div class="form-group">
									<label class="control-label col-md-6"> Carrier Trans Method Code
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[CarrierTransMethodCode]"  class="form-control CarrierTransMethodCode">
									</div>
								</div> 
								<div class="form-group">
									<label class="control-label col-md-6"> Order Qty UOM
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[TotalQtyInvoicedUOM]"  class="form-control TotalQtyInvoicedUOM">
									</div>
								</div> 
								<div class="form-group">
									<label class="control-label col-md-6"> SCAC Code
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[saccode]"  class="form-control saccode">
									</div>
								</div> 
								<div class="form-group">
									<label class="control-label col-md-6">Allow Chrg Percent Qual
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[allowChrgPercentQual]"  class="form-control allowChrgPercentQual">
									</div>
								</div> 
								<div class="form-group">
									<label class="control-label col-md-6"> Handling Fee Type
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<select name="data[handlingfeetype]"  class="form-control handlingfeetype">
											<option value="">Select handling fee type </option>
											<option value="FIXED">Fixed Value</option>
											<option value="PERCENTAGE">Percentage</option>
											
										</select>
									</div>
								</div> 
								<div class="form-group">
									<label class="control-label col-md-6"> Handling Fee
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[handlingfee]"  class="form-control handlingfee">
									</div>
								</div> 
								<div class="form-group">
									<label class="control-label col-md-6"> Handling Fee Title
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[handlingfeeTitle]"  class="form-control handlingfeeTitle">
									</div>
								</div> 
								<div class="form-group">
									<label class="control-label col-md-6"> Allow ChrgHandling Code
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[allowChrgHandlingCode]"  class="form-control allowChrgHandlingCode">
									</div>
								</div> 
								<div class="form-group">
									<label class="control-label col-md-6">TermsType
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[TermsType]"  class="form-control TermsType">
									</div>
								</div> 
								<div class="form-group">
									<label class="control-label col-md-6">TermsBasisDateCode
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[TermsBasisDateCode]"  class="form-control TermsBasisDateCode">
									</div>
								</div> 
								<div class="form-group">
									<label class="control-label col-md-6">TermsNetDueDays
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[TermsNetDueDays]"  class="form-control TermsNetDueDays">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-6">TermsDiscountPercentage
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[TermsDiscountPercentage]"  class="form-control TermsDiscountPercentage">
									</div>
								</div>								
								<div class="form-group">
									<label class="control-label col-md-6">TermsDiscountDueDays
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[TermsDiscountDueDays]"  class="form-control TermsDiscountDueDays">
									</div>
								</div> 
								<div class="form-group">
									<label class="control-label col-md-6">TermsDiscountAmount
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[TermsDiscountAmount]"  class="form-control TermsDiscountAmount">
									</div>
								</div> 
								<div class="form-group">
									<label class="control-label col-md-6">TermsDescription
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<input type="text" name="data[TermsDescription]"  class="form-control TermsDescription">
									</div>
								</div> 
								<div class="form-group">
									<label class="control-label col-md-6">PCF_WAYSPEED
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-6">
										<select name="data[PCF_WAYSPEED]"  class="form-control PCF_WAYSPEED">
											<option value="0">No</option>
											<option value="1">Yes</option>
											
										</select>
									</div>
								</div> 
								    
							</div>     
							</div>     
                            
                        </div>
                        <input type="hidden" name="data[id]" class="id" />
                    </form>                         
                </div>
                <div class="modal-footer">
                  <button type="button" class="pull-left btn btn-primary submitAction">Save</button>
                  <button type="button" class="btn yellow btn-outline sbold" data-dismiss="modal">Close</button>
                </div>
              </div>                  
            </div>
        </div>
    </div>
</div>