<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->>
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Mappings Nominal</span>
                </li>

            </ul>
        </div>
        <h3 class="page-title"> Nominal
            <small>Mapping</small>
        </h3>
        <div class="portlet ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-shopping-cart"></i>Nominal Mapping</div>
                <div class="actions">
                    <a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> Add New Nominal Mapping</span>
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                   <div class="">          
                        <table class="table table-hover text-centered actiontable" id="sample_4">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="15%"><?php echo $this->globalConfig['account1Name'];?> Nominal Code</th>
                                    <th width="15%"><?php echo $this->globalConfig['account1Name'];?> Channel Name</th>
                                    <th width="15%"><?php echo $this->globalConfig['account1Name'];?> Product Custom Field</th>
                                    <th width="15%"><?php echo $this->globalConfig['account2Name'];?> Account Name</th>
                                    <th width="10%"><?php echo $this->globalConfig['account1Name'];?> Account</th>
                                    <th width="10%"><?php echo $this->globalConfig['account2Name'];?> Account</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="clone hide">
                                    <td ><span class="value" data-value="id"></span></td>
                                    <td><span class="value" data-value="account1NominalId"></span></td>
                                    <td><span class="value" data-value="account1ChannelId"></span></td>
                                    <td><span class="value" data-value="account1CustomFieldValue"></span></td>
                                    <td><span class="value" data-value="account2NominalId"></span></td>
                                    <td><span class="value" data-value="account1Id"></span></td>
                                    <td><span class="value" data-value="account2Id"></span></td>
                                    <td class="action">
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" delurl="<?php echo base_url('mapping/nominal/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php   foreach ($data['data'] as $key =>  $row) {?> 
								<script> var data<?php echo $row['id'];?> = <?php echo json_encode($row);?>;</script>
                                <tr class="tr<?php echo $row['id'];?>">
                                    
                                    <td ><span class="value" data-value="id"><?php echo $key + 1;?></span></td>

                                    <td><span class="value" data-value="account1NominalId"><?php echo @($data['account1NominalId'][$row['account1Id']][$row['account1NominalId']])?($data['account1NominalId'][$row['account1Id']][$row['account1NominalId']]['name']):($row['account1NominalId']);?></span></td>
									
									<td><span class="value" data-value="account1ChannelId"><?php echo @($data['account1ChannelId'][$row['account1Id']][$row['account1ChannelId']])?($data['account1ChannelId'][$row['account1Id']][$row['account1ChannelId']]['name']):($row['account1ChannelId']);?></span></td>
									
									<td><span class="value" data-value="account1CustomFieldValue"><?php echo @$row['account1CustomFieldValue'];?></span></td>

                                    <td><span class="value" data-value="account2NominalId"><?php echo @($data['account2NominalId'][$row['account2Id']][$row['account2NominalId']])?($data['account2NominalId'][$row['account2Id']][$row['account2NominalId']]['name']):($row['account2NominalId']);?></span></td> 

                                    <td><span class="value" data-value="account1Id"><?php echo @($data['account1Id'][$row['account1Id']])?($data['account1Id'][$row['account1Id']]['name']):($row['account1Id']);?></span></td>

                                    <td><span class="value" data-value="account2Id"><?php echo @($data['account2Id'][$row['account2Id']])?($data['account2Id'][$row['account2Id']]['name']):($row['account2Id']);?></span></td>

                                    <td class="action">
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" onclick='editAction(data<?php echo $row['id'];?>)' title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" onclick="deleteAction('<?php echo base_url('mapping/nominal/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr> 
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="actionmodal" role="dialog">
            <div class="modal-dialog">        
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Nominal Mapping</h4>
                </div>
                <div class="modal-body">
                   <form action="<?php echo base_url('mapping/nominal/save');?>" method="post" id="saveActionForm" class="form-horizontal saveActionForm" novalidate="novalidate">
                        <div class="form-body">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-5"><?php echo $this->globalConfig['account1Name'];?> Account
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-7">
                                    <select name="data[account1Id]" data-required="1" class="form-control account1Id acc1list">
                                        <?php
                                        foreach ($data['account1Id'] as $account1Id) {
                                            echo '<option value = "'.$account1Id['id'].'">'.ucwords($account1Id['name']).'</option>';
                                        } 
                                        ?>
                                    </select>
                                </div>
                            </div>     
                            <div class="form-group">
                                <label class="control-label col-md-5"><?php echo $this->globalConfig['account2Name'];?> Account
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-7">
                                    <select name="data[account2Id]"  data-required="1" class="form-control account2Id acc2list">
                                        <?php
                                        foreach ($data['account2Id'] as $account2Id) {
                                            echo '<option value = "'.$account2Id['id'].'">'.ucwords($account2Id['name']).'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div> 
                            <div class="form-group">
                                <label class="control-label col-md-5"><?php echo $this->globalConfig['account1Name'];?> Nominal Code
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-7">
                                    <?php if(@$data['account1NominalId']){ ?> 
                                    <select name="data[account1NominalId]" data-required="1" class="form-control account1NominalId acc1listoption">
                                        <option value="" class="show">Select a <?php echo $this->globalConfig['account1Name'];?> Nominal Code</option>
                                        <?php
                                        foreach ($data['account1NominalId'] as $accountId => $account1NominalIds) {
                                            foreach ($account1NominalIds as $account1NominalId) {
                                                echo '<option class="acc1listoption'.$accountId.'" value="'.$account1NominalId['id'].'">'.ucwords($account1NominalId['name']).'</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                    <?php } else { ?>
                                        <input type="text" name="data[account1NominalId]" data-required="1" class="form-control account1NominalId">
                                    <?php } ?>
                                </div>
                            </div>  
							<div class="form-group">
                                <label class="control-label col-md-5"><?php echo $this->globalConfig['account1Name'];?> Channel Name
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-7">
                                    <?php if(@$data['account1ChannelId']){ ?> 
                                    <select name="data[account1ChannelId]" class="form-control account1ChannelId acc1listoption"> 
                                        <option value="" class="show">Select a <?php echo $this->globalConfig['account1Name'];?> Channel Name</option>
                                        <?php
                                        foreach ($data['account1ChannelId'] as $accountId => $account1NominalIds) {
                                            foreach ($account1NominalIds as $account1ChannelId) {
                                                echo '<option class="acc1listoption'.$accountId.'" value="'.$account1ChannelId['id'].'">'.ucwords($account1ChannelId['name']).'</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                    <?php } else { ?>
                                        <input type="text" name="data[account1ChannelId]" data-required="1" class="form-control account1ChannelId">
                                    <?php } ?>
                                </div>
                            </div>  
							<div class="form-group ">
                                <label class="control-label col-md-5"><?php echo $this->globalConfig['account1Name'];?> Product Custom Field Value
                                    <span class="required" aria-required="false"> * </span>
                                </label>
                                <div class="col-md-7">                                    
									<input type="text" name="data[account1CustomFieldValue]" class="form-control account1CustomFieldValue">
                                </div>
                            </div>  
							
							<div class="form-group">
                                <label class="control-label col-md-5"><?php echo $this->globalConfig['account2Name'];?> Account
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-7">
                                    <?php if(@$data['account2NominalId']){ ?> 
                                    <select name="data[account2NominalId]" data-required="1" class="form-control account2NominalId acc2listoption">
                                        <option value="">Select a <?php echo $this->globalConfig['account2Name'];?> Account</option>
                                        <?php
                                        foreach ($data['account2NominalId'] as $accountId => $account2PaymentIds) {
                                            foreach ($account2PaymentIds as $account2NominalId) {
                                                echo '<option class="acc2listoption'.$accountId.'" value="'.$account2NominalId['id'].'">'.ucwords($account2NominalId['name']).'</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                    <?php } else { ?>
                                        <input type="text" name="data[account2NominalId]" data-required="1" class="form-control account2NominalId">
                                    <?php } ?>
                                </div>
                            </div> 
							
						</div>
                        <input type="hidden" name="data[id]" class="id" />
                    </form>                         
                </div>
                <div class="modal-footer">
                  <button type="button" class="pull-left btn btn-primary submitAction">Save</button>
                  <button type="button" class="btn yellow btn-outline sbold" data-dismiss="modal">Close</button>
                </div>
              </div>                  
            </div>
        </div>
    </div>
</div>
