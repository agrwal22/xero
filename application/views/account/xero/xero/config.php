<?php
?>
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li><a href="index.html">Home</a><i class="fa fa-circle"></i></li>
                <li><span>Account Settings</span> </li>
			</ul>
        </div>
        <h3 class="page-title"> <?php echo $this->globalConfig['account2Name'];?>
            <small><?php echo $this->globalConfig['account2Name'];?></small>
        </h3>
        <div class="portlet ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-shopping-cart"></i> <?php echo $this->globalConfig['account2Name'];?>Configuration</div>
                <div class="actions">
                    <a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs">Add New Configuration</span>
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-responsive">          
                        <table class="table table-hover text-centered actiontable">
                            <thead>
                                <tr>
                                    <th width="5%">#</th> 
                                    <th width="25%"><?php echo $this->globalConfig['account2Name'];?> ID</th>
                                    <th width="25%">Account ID</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="clone hide">
                                    <td><span class="value" data-value="id"></span></td>
                                    <td><span class="value" data-value="xeroAccountId"></span></td>
                                    <td><span class="value" data-value="compte"></span></td>
                                    <td class="action">
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" delurl="<?php echo base_url('account/'.$data['type'].'/config/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php   
									foreach ($data['data'] as $key =>  $row){
								?>                               
                                <tr class="tr<?php echo $row['id'];?>">
                                    <td><span class="value" data-value="id"><?php echo $key + 1;?></span></td>
                                    <td><span class="value" data-value="xeroAccountId"><?php echo $row['xeroAccountId'];?></span></td>
                                    <td><span class="value" data-value="name"><?php echo $row['name'];?></span></td>
                                    <td class="action">
                                        <script> var data<?php echo $row['id'];?> = <?php echo json_encode($row);?>;</script>
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" onclick=editAction(data<?php echo $row['id'];?>) title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" onclick="deleteAction('<?php echo base_url('account/'.$data['type'].'/config/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php 
									} 
								?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="actionmodal" role="dialog" data-backdrop="static">
            <div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"> <?php echo $this->globalConfig['account2Name'];?> Account Settings</h4>
					</div>
					<div class="modal-body">
						<form action="<?php echo base_url('account/'.$data['type'].'/config/save');?>" method="post" id="saveActionForm" class="form-horizontal saveActionForm" novalidate="novalidate">
							<div class="form-body"></div>
							<input type="hidden" name="data[id]" class="id" />
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="pull-left btn btn-primary submitAction">Save</button>
						<button type="button" class="btn yellow btn-outline sbold" data-dismiss="modal">Close</button>
					</div>
				</div>                  
            </div>
        </div>
    </div>
</div>


<div class="confighml">
    <?php   
		$data['data'] = ($data['data'])?($data['data']):(array(''));
		foreach ($data['data'] as $key =>  $row){  
			$account1Id = $data['saveAccount'][$row['xeroAccountId']]['account1Id'];
	?>
	<div class="htmlaccount<?php echo @$row['id'];?>" style="display: none;">
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button> You have some form errors. Please check below. 
		</div>
		<div class="form-group">
			<label class="control-label col-md-4"> <?php echo $this->globalConfig['account2Name'];?> Id
				<span class="required" aria-required="true"> * </span>
			</label>
			<div class="col-md-7">
				<select name="data[xeroAccountId]" data-required="1" class="form-control xeroAccountId">
					<option value="">Select a <?php echo $this->globalConfig['account2Name'];?> account</option>
					<?php
						foreach ($data['saveAccount'] as $saveAccount) {
							echo '<option value="'.$saveAccount['id'].'">'.ucwords($saveAccount['name']).'</option>';
						}
					?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> COGSAccountRef
				<span class="required" aria-required="true"> * </span>
			</label>
			<div class="col-md-7">
				<select name="data[COGSAccountRef]" data-required="1" class="form-control COGSAccountRef">
					<option value="">Select Account</option>
					<?php
						foreach ($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef) {
							echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
						}
					?>
				</select> 
			</div> 
		</div>
		<div class="form-group">
			<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> IncomeAccountRef
				<span class="required" aria-required="true"> * </span>
			</label>
			<div class="col-md-7">
				<select name="data[IncomeAccountRef]" data-required="1" class="form-control IncomeAccountRef">
					<option value="">Select Account</option>
					<?php
						foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
							echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
						}
					?>
				</select> 
			</div> 
		</div>
		<div class="form-group">
			<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> AssetAccountRef
				<span class="required" aria-required="true"> * </span>
			</label>
			<div class="col-md-7">
				<select name="data[AssetAccountRef]" data-required="1" class="form-control AssetAccountRef">
					<option value="">Select Account</option>
					<?php
						foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
							echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> ExpenseAccountRef
				<span class="required" aria-required="true"> * </span>
			</label>
			<div class="col-md-7">
				<select name="data[ExpenseAccountRef]" data-required="1" class="form-control ExpenseAccountRef">
					<option value="">Select Account</option>
					<?php
						foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
							echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Stock Adjustment AccountRef </label>
			<div class="col-md-7">
				<select name="data[stockAdjustmentAccountRef]" class="form-control stockAdjustmentAccountRef">
					<option value="">Select Account</option>
					<?php
						foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
							echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>	
		<div class="form-group hide">
			<label class="control-label col-md-4"><?php echo $this->globalConfig['account1Name'];?> Prepayment AccountRef </label>
			<div class="col-md-7">
				<select name="data[prepaymentAccount]" class="form-control prepaymentAccount">
					<option value="">Select Account</option>
					<?php
						foreach($data['prepaymentAccount'][$account1Id] as $IncomeAccountRef){
							echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>	
		<div class="form-group hide"> 
			<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> PayType Account Ref</label>
			<div class="col-md-7">
				<select name="data[PayTypeAccRef]" class="form-control PayTypeAccRef">
					<option value="">Select Account</option>
					<?php
						foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
							echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Discount Account Ref</label>
			<div class="col-md-7">
				<select name="data[DiscountAccountRef]" class="form-control DiscountAccountRef">
					<option value="">Select Account</option>
					<?php
						foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
							echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Default Sales Order TaxCode</label>
			<div class="col-md-7">
				<select name="data[TaxCode]" class="form-control TaxCode">
					<option value="">Select Tax</option>
					<?php
						foreach($data['getAllTax'][$row['xeroAccountId']] as $getAllTax){
							echo '<option value="'.$getAllTax['id'].'">'.ucwords($getAllTax['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Default Sales Order No TaxCode</label>
			<div class="col-md-7">
				<select name="data[salesNoTaxCode]" class="form-control salesNoTaxCode">
					<option value="">Select Tax</option>
					<?php
						foreach($data['getAllTax'][$row['xeroAccountId']] as $getAllTax){
							echo '<option value="'.$getAllTax['id'].'">'.ucwords($getAllTax['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Default Purchase Order TaxCode</label>
			<div class="col-md-7">
				<select name="data[PurchaseTaxCode]" class="form-control PurchaseTaxCode">
					<option value="">Select Tax</option>
					<?php
						foreach($data['getAllTax'][$row['xeroAccountId']] as $getAllTax){
							echo '<option value="'.$getAllTax['id'].'">'.ucwords($getAllTax['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Default Purchase Order No TaxCode</label>
			<div class="col-md-7">
				<select name="data[PurchaseNoTaxCode]" class="form-control PurchaseNoTaxCode">
					<option value="">Select Tax</option>
					<?php
						foreach($data['getAllTax'][$row['xeroAccountId']] as $getAllTax){
							echo '<option value="'.$getAllTax['id'].'">'.ucwords($getAllTax['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Default Sales Line Item TaxCode</label>
			<div class="col-md-7">
				<select name="data[orderLineTaxCode]" class="form-control orderLineTaxCode">
					<option value="">Select Tax</option>
					<?php
						foreach($data['getAllTax'][$row['xeroAccountId']] as $getAllTax){
							echo '<option value="'.$getAllTax['id'].'">'.ucwords($getAllTax['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Product Purchase Price List</label>
			<div class="col-md-7">
				<select name="data[productCostPriceList]"  class="form-control productCostPriceList">
					<option value="">Select Price List</option>
					<?php
						foreach ($data['pricelist'][$account1Id] as $pricelist) {
							echo '<option value="'.$pricelist['id'].'">'.ucwords($pricelist['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Product Sell Price List</label>
			<div class="col-md-7">
				<select name="data[productRetailPriceList]"  class="form-control productRetailPriceList">
					<option value="">Select Price List</option>
					<?php
						foreach($data['pricelist'][$account1Id] as $pricelist){
							echo '<option value="'.$pricelist['id'].'">'.ucwords($pricelist['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">GiftCard Item</span></label>
			<div class="col-md-7"><input name="data[giftCardItem]"  class="form-control giftCardItem" type="text" placeholder="Enter GiftCard Item Code"></div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Generic Sku</label>
			<div class="col-md-7"><input name="data[genericSku]" class="form-control genericSku" type="text" placeholder="Enter Generic SKU Item Code"></div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Shipping Item</label>
			<div class="col-md-7"><input name="data[shippingItem]" class="form-control shippingItem" type="text" placeholder="Enter Shipping Item Code"></div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-4">Discount Item</label>
			<div class="col-md-7"><input name="data[discountItem]" class="form-control discountItem" type="text" placeholder="Enter Discount Item Code"></div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Coupon Item</label>
			<div class="col-md-7"><input name="data[couponItem]" class="form-control couponItem" type="text" placeholder="Enter Coupon Item Code"></div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">RoundOff Item</label>
			<div class="col-md-7"><input name="data[roundOffItem]" class="form-control roundOffItem" type="text" placeholder="Enter RoundOff Item Code"></div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Purchase Credit Default Item</label>
			<div class="col-md-7"><input name="data[PurchaseCreditItem]" class="form-control PurchaseCreditItem" type="text" placeholder="Enter Purchase Credit Item Code"></div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Shipping Product Identify Nominal Code</label>
			<div class="col-md-7"><input name="data[nominalCodeForShipping]" class="form-control nominalCodeForShipping" type="text" placeholder="Enter Shipping Identify Nominal Code"></div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">GiftCard Product Identify Nominal Code</label>
			<div class="col-md-7"><input name="data[nominalCodeForGiftCard]" class="form-control nominalCodeForGiftCard" type="text" placeholder="Enter GiftCard Identify Nominal Code"></div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Coupon Product Identify Nominal Code</label>
			<div class="col-md-7"><input name="data[nominalCodeForDiscount]" class="form-control nominalCodeForDiscount" type="text" placeholder="Enter Coupon Identify Nominal Code"></div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Default Currency</label>
			<div class="col-md-7"><input name="data[defaultCurrency]" class="form-control defaultCurrency" type="text" placeholder="Enter Your Default Currency"></div>
		</div>
		<?php 
			if($this->globalConfig['enableAmazonFees']){
		?>
		<div class="form-group">
			<label class="control-label col-md-4">Amazon Fee PaymentMethod Ref</label>
			<div class="col-md-7">
				<select name="data[amazonfeePaymentMethodRef]"  class="form-control amazonfeePaymentMethodRef">
					<option value="">Select Payment Method</option>
					<?php
						foreach($data['PaymentMethodRef'][$row['xeroAccountId']] as $IncomeAccountRef){
							echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group hide">
			<label class="control-label col-md-4">Amazon Fee DepositToAccount Ref</label>
			<div class="col-md-7">
				<select name="data[amazonfeeDepositToAccountRef]"  class="form-control amazonfeeDepositToAccountRef">
					<option value="">Select Account</option>
					<?php
						foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
							echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Amazon Fee Account Ref</label>
			<div class="col-md-7">
				<select name="data[amazonfeeAccountRef]"  class="form-control amazonfeeAccountRef">
					<option value="">Select Account</option>
					<?php
						foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
							echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Amazon Fee Default Supplier</label>
			<div class="col-md-7">
				<input name="data[amazondefaultSupplier]" class="form-control amazondefaultSupplier" type="text" placeholder="Enter Xero Amazon Fee Supplier's ID" />
			</div>
		</div>
		<?php 
			}
		?>
		<?php
			if($this->globalConfig['enableInventoryManagement']){
		?>
		<div class="form-group">
			<label class="control-label col-md-4">Inventory Management Enabled</label>
			<div class="col-md-7">
				<select name="data[InventoryManagementEnabled]" class="form-control InventoryManagementEnabled">
				   	<option value="">Select</option>
					<option value="0">Yes</option>
				   	<option value="1">No</option>
				</select> 
			</div>
		</div> 
		<div class="form-group">
			<label class="control-label col-md-4">IM Product Purchase Nominal Code</label>
			<div class="col-md-7">
				<select name="data[InventoryManagementProductPurchaseNominalCode]" class="form-control InventoryManagementProductPurchaseNominalCode">
					<option value="">Select Nominal</option>
					<?php
						foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
							echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
						}
					?>
				</select> 
			</div> 
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">IM Product Sell Nominal Code</label>
			<div class="col-md-7">
				<select name="data[InventoryManagementProductSellNominalCode]" class="form-control InventoryManagementProductSellNominalCode">
					<option value="">Select Nominal</option>
					<?php
						foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
							echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
						}
					?>
				</select> 
			</div> 
		</div>
		<?php
			}
		?>
		<?php 
			if($this->globalConfig['enableInventoryTransfer']){
		?>
		<div class="form-group">
			<label class="control-label col-md-4">Inventory Transfer Sell AccountRef</label>
			<div class="col-md-7">
				<select name="data[InventoryTransferSalesAccountRef]" class="form-control InventoryTransferSalesAccountRef">
					<option value="">Select Account</option>
					<?php
						foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
							echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Inventory Transfer Purchase AccountRef</label>
			<div class="col-md-7">
				<select name="data[InventoryTransferPurchaseAccountRef]" class="form-control InventoryTransferPurchaseAccountRef">
					<option value="">Select Account</option>
					<?php
						foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
							echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
						}
					?>
				</select> 
			</div> 
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Inventory Transfer Inter Co. Supplier</label>
			<div class="col-md-7"><input name="data[InterCoSupplier]" class="form-control InterCoSupplier" type="text" placeholder="Enter Xero Inventory Transfer Supplier ID"></div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Inventory Transfer Inter Co. Customer</label>
			<div class="col-md-7"><input name="data[InterCoCustomer]" class="form-control InterCoCustomer" type="text" placeholder="Enter Xero Inventory Transfer Customer ID"></div>
		</div>
		<?php
			}
		?>
		<div class="form-group hide">
			<label class="control-label col-md-4">Xero Anonymous Customer ID</label>
			<div class="col-md-7"><input name="data[XeroAnonymousCustomer]" class="form-control XeroAnonymousCustomer" type="text" placeholder="Enter Xero Anonymous Customer ID"></div>
		</div>
		<div class="form-group hide">
			<label class="control-label col-md-4">Brightpearl Anonymous Customer Name</label>
			<div class="col-md-7"><input name="data[BrightpearlAnonymousCustomer]" class="form-control BrightpearlAnonymousCustomer" type="text" placeholder="Enter BP's Anonymous Customer Full Name"></div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Use As Reference On Invoice/CreditNote</label>
			<div class="col-md-7">
				<select name="data[TakeAsRefrence]" class="form-control TakeAsRefrence">
					<option value="">Select Any Option</option>
					<option value="BrightpearlCustomerReference">Brightpearl Customer Ref. No.</option>
					<option value="BrightpearlOrderID">Brightpearl Order ID</option>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Send Total Tax as Line Item</label>
			<div class="col-md-7">
				<select name="data[SendTaxAsLineItem]" class="form-control SendTaxAsLineItem">
					<option value="">Select</option>
				   	<option value="1">Yes</option>
				   	<option value="0">No</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Tax Item Line Nominal Code</label>
			<div class="col-md-7">
				<select name="data[TaxItemLineNominal]" class="form-control TaxItemLineNominal">
					<option value="">Select Account</option>
					<?php
						foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
							echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
						}
					?>
				</select> 
			</div> 
		</div>		
		<div class="form-group">
			<label class="control-label col-md-4">Default Channel</label>
			<div class="col-md-7">
				<select name="data[channelIds][]"  class="form-control channelIds"  multiple="true">
					<option value=""> Select Channel </option>
					<?php
						foreach($data['channel'][$account1Id] as $channel){
							echo '<option value="'.$channel['id'].'">'.ucwords($channel['name']).'</option>';
						}
					?>
				</select>  
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Default Warehouse</label>
			<div class="col-md-7">
				<select name="data[warehouses][]" class="form-control warehouses" multiple="true">
					<option value="">Select warehouse</option>
					<?php
						foreach($data['warehouse'][$account1Id] as $warehouse){
							echo '<option value="'.$warehouse['id'].'">'.ucwords($warehouse['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Default Xero Timezone</label>
			<div class="col-md-7">
				<input type="text" name="data[XeroTimeZone]" value="<?php echo $data['accountInfo'][$account1Id]['Timezone']; ?>"  class="form-control" readonly="readonly"/>
				
			</div>
		</div>
	</div> 
    <?php 
		} 
	?>
</div>