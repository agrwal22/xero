<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li><a href="index.html">Home</a><i class="fa fa-circle"></i></li>
                <li><span>Account Settings</span></li>
            </ul>
        </div>
        <div class="portlet ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-shopping-cart"></i>Brightpearl Configuration </div>
                <div class="actions">
                    <a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> Add New Configuration </span>
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-responsive">          
                        <table class="table table-hover text-centered actiontable">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="25%">Brightpearl Id</th>
                                    <th width="25%">Account Name</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="clone hide">
                                    <td><span class="value" data-value="id"></span></td>
                                    <td><span class="value" data-value="brightpearlAccountId"></span></td>
                                    <td><span class="value" data-value="compte"></span></td>
                                    <td class="action">
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" delurl="<?php echo base_url('/account/'.$data['type'].'/config/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php  
									foreach ($data['data'] as $key =>  $row){
								?>                               
                                <tr class="tr<?php echo $row['id'];?>">
                                    <td><span class="value" data-value="id"><?php echo $key + 1;?></span></td>
                                    <td><span class="value" data-value="brightpearlAccountId"><?php echo $row['brightpearlAccountId'];?></span></td>
                                    <td><span class="value" data-value="name"><?php echo $row['name'];?></span></td>
                                    <td class="action">
                                        <script>var data<?php echo $row['id'];?> = <?php echo json_encode($row);?>;</script>
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" onclick=editAction(data<?php echo $row['id'];?>) title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" onclick="deleteAction('<?php echo base_url('/account/'.$data['type'].'/config/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php 
									} 
								?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="actionmodal" role="dialog" data-backdrop="static">
            <div class="modal-dialog modal-lg">          
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Brightpearl Account Settings</h4>
					</div>
					<div class="modal-body">
						<form action="<?php echo base_url('/account/'.$data['type'].'/config/save');?>" method="post" id="saveActionForm" class="form-horizontal saveActionForm" novalidate="novalidate">
							<div class="form-body"></div>        
							<div class="form-group">
								<label class="control-label col-md-5">Fetch sales order status
									<span class="required" aria-required="true"> * </span>
								</label>
								<div class="col-md-6">
									<select name="data[fetchSalesOrderStatus][]"  multiple="multiple"   class="form-control chosen-select">
										<?php
										$saveChanels	= explode(",",$row['fetchSalesOrderStatus']);
										foreach ($data['orderstatus'][$row['brightpearlAccountId']] as $orderstatus) {
											$selected	= (in_array($orderstatus['id'],$saveChanels))?('selected="selected"'):('');
											if($orderstatus['orderTypeCode'] == 'SO'){
												echo '<option value="'.$orderstatus['id'].'" '.$selected.'>'.ucwords($orderstatus['name']).'</option>';
											}
										}
										?>									
									</select> 
								</div>
							</div>
							<input type="hidden" name="data[id]" class="id" />
						</form>                         
					</div>				
					<div class="modal-footer">
						<button type="button" class="pull-left btn btn-primary submitAction">Save</button>
						<button type="button" class="btn yellow btn-outline sbold" data-dismiss="modal">Close</button>
					</div>
				</div>                  
            </div>
        </div>
    </div>
</div>
<div class="confighml">
    <?php   
    $data['data']	= ($data['data'])?($data['data']):(array(''));
    foreach ($data['data'] as $key =>  $row) {
	?>
	<div class="htmlaccount<?php echo @$row['id'];?>" style="display: none;">
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>You have some form errors. Please check below.</div>
		<div class="form-group">
			<label class="control-label col-md-5">Brightpearl Id<span class="required" aria-required="true"> * </span></label>
			<div class="col-md-6">
				<select name="data[brightpearlAccountId]" data-required="1" class="form-control brightpearlAccountId">
					<option value="">Select a Brightpearl account</option>
					<?php
						foreach ($data['saveAccount'] as $saveAccount) {
							echo '<option value="'.$saveAccount['id'].'">'.ucwords($saveAccount['name']).'</option>';
						}
					?>
				</select>
			</div>
		</div>  			
		<div class="form-group">
			<label class="control-label col-md-5">Default Warehouse<span class="required" aria-required="true"> * </span></label>
			<div class="col-md-6">
				<select name="data[warehouse]" data-required="1" class="form-control warehouse">
					<option value="">Select Warehouse</option>
					<?php
						foreach ($data['warehouse'][$row['brightpearlAccountId']] as $warehouse) {
							echo '<option value="'.$warehouse['id'].'">'.ucwords($warehouse['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>   
		<?php 
			if($this->globalConfig['enablePurchaseOrder']){
		?>
		<div class="form-group">
			<label class="control-label col-md-5">Fetch Purchase Order Status<span class="required" aria-required="true"> * </span></label>
			<div class="col-md-6">
				<select name="data[fetchPurchaseStatus]" data-required="1" class="form-control fetchPurchaseStatus">
					<option value="">Select Purchase Order Status</option>
					<?php
						foreach ($data['orderstatus'][$row['brightpearlAccountId']] as $orderstatus) {
							if($orderstatus['orderTypeCode'] == 'PO'){
								echo '<option value="'.$orderstatus['id'].'">'.ucwords($orderstatus['name']).'</option>';
							}
						}
					?>
				</select> 
			</div>
		</div>
		<?php 
			}
		?>
		<?php 
			if($this->globalConfig['enablePurchaseCredit']){
		?>
		<div class="form-group">
			<label class="control-label col-md-5">Fetch Purchase Credit Status<span class="required" aria-required="true"> * </span></label>
			<div class="col-md-6">
				<select name="data[fetchPurchaseCredit]" data-required="1" class="form-control fetchPurchaseCredit">
					<option value="">Select Purchase Credit Status</option>
					<?php
						foreach ($data['orderstatus'][$row['brightpearlAccountId']] as $orderstatus) {
							if($orderstatus['orderTypeCode'] == 'PC'){
								echo '<option value="'.$orderstatus['id'].'">'.ucwords($orderstatus['name']).'</option>';
							}
						}
					?>
				</select> 
			</div>
		</div>
		<?php 
			}
		?> 
		<?php
			if($this->globalConfig['enableSalesCredit']){
		?>
		<div class="form-group">
			<label class="control-label col-md-5">Fetch Sales Credit Status<span class="required" aria-required="true"> * </span></label>
			<div class="col-md-6">
				<select name="data[fetchSalesCredit]" data-required="1" class="form-control fetchSalesCredit">
					<option value="">Select Sales Credit Status</option>
					<?php
						foreach ($data['orderstatus'][$row['brightpearlAccountId']] as $orderstatus) {
							if($orderstatus['orderTypeCode'] == 'SC'){
								echo '<option value="'.$orderstatus['id'].'">'.ucwords($orderstatus['name']).'</option>';
							}
						}
					?>
				</select> 
			</div>
		</div>
		<?php 
			}
		?>
		<div class="form-group">
			<label class="control-label col-md-5">Default Nominal Code</label>
			<div class="col-md-6">
				<select name="data[nominalCode]" class="form-control nominalCode">
					<option value="">Select Nominal Code</option>
					<?php
						foreach ($data['nominalCode'][$row['brightpearlAccountId']] as $nominalCode) {
							echo '<option value="'.$nominalCode['id'].'">'.ucwords($nominalCode['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>			
		<div class="form-group">
			<label class="control-label col-md-5">Default payment method</label>
			<div class="col-md-6">
				<select name="data[defaultPaymentMethod]" class="form-control defaultPaymentMethod">
					<option value="">Select Payment Method</option>
					<?php
						foreach($data['defaultPaymentMethod'][$row['brightpearlAccountId']] as $defaultPaymentMethod){
							echo '<option value="'.$defaultPaymentMethod['id'].'">'.ucwords($defaultPaymentMethod['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>			
		<div class="form-group">
			<label class="control-label col-md-5">Default Currency</label>
			<div class="col-md-6">
				<input type="text" name="data[currencyCode]" class="form-control currencyCode" placeholder="Enter Your Default Currency"/>
			</div>
		</div>                        						
		<?php 
			if($this->globalConfig['enableGiftCard']){
		?>
		<div class="form-group">
			<label class="control-label col-md-5">Gift Card Payment Method</label>
			<div class="col-md-6">
				<select name="data[giftCardPayment]" class="form-control giftCardPayment">
					<option value="">Select Payment Method</option>
					<?php
						foreach ($data['defaultPaymentMethod'][$row['brightpearlAccountId']] as $defaultPaymentMethod) {
							echo '<option value="'.$defaultPaymentMethod['id'].'">'.ucwords($defaultPaymentMethod['name']).'</option>';
						}
					?>
				</select> 
			</div>
		</div>
		<?php 
			} 
		?>        
		<?php 
			if($this->globalConfig['enableAmazonFees']){
		?>
		<div class="form-group">
			<label class="control-label col-md-5">Amazon Fee Journal Type</label>
			<div class="col-md-6"><input type="text" name="data[journalType]" class="form-control journalType" placeholder="Enter Amazon Fee Journal Type"/></div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-5">Amazon Fee Journal Account</label>
			<div class="col-md-6">
				<select name="data[journalAccount]"  class="form-control journalAccount">
					<option value="">Select Account</option>
					<?php
						foreach($data['nominalCode'][$row['brightpearlAccountId']] as $nominalCode){
							echo '<option value="'.$nominalCode['id'].'">'.ucwords($nominalCode['name']).'</option>';
						}
					?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-5">Amazon Fee Journal Reference</label>
			<div class="col-md-6"><input type="text" name="data[details]"  class="form-control details" placeholder="Enter Amazon Fee Journal Reference"/></div>
		</div>
		<?php 
			} 
		?>
		<div class="form-group">
			<label class="control-label col-md-5">Default BP Timezone</label>
			<div class="col-md-6">
				<input type="text" name="data[timezone]" value="<?php echo $data['accountinfo'][$row['brightpearlAccountId']]['configuration']['timeZone']; ?>"  class="form-control" readonly="readonly"/>
			</div>
		</div>
		<div class="form-group hide">
			<label class="control-label col-md-5">Un-Invoicing Enabled</label>
			<div class="col-md-6">
				<select name="data[UnInvoicingEnabled]" class="form-control UnInvoicingEnabled">
					<option value="">Select</option>
					<option value="1">Yes</option>
					<option value="0">No</option>
				</select> 
			</div>
		</div>
		<div class="form-group hide">
			<label class="control-label col-md-5">Payment Reversal Enabled</label>
			<div class="col-md-6">
				<select name="data[PaymentReversalEnabled]" class="form-control PaymentReversalEnabled">
					<option value="">Select</option>
					<option value="1">Yes</option>
					<option value="0">No</option>
				</select> 
			</div>
		</div>      
	</div> 
	<?php 
	}
	?>
</div>
<link href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.js" type="text/javascript"></script>
<script>
$(".chosen-select").chosen({width: "100%"}); 
jQuery(".actioneditbtn").on("click",function(){
	 $(".chosen-select").trigger("liszt:updated");
});
</script>