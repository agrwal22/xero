<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->>
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Account Settings</span>
                </li>

            </ul>
        </div>
        <div class="portlet ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-shopping-cart"></i>Brightpearl Configuration</div>
                <div class="actions">
                    <a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> Add New Configuration </span>
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-responsive">          
                        <table class="table table-hover text-centered actiontable">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="25%">Brightpearl Id</th>
                                    <th width="25%">Account Name</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="clone hide">
                                    <td ><span class="value" data-value="id"></span></td>
                                    <td><span class="value" data-value="brightpearlAccountId"></span></td>
                                    <td><span class="value" data-value="compte"></span></td>
                                    <td class="action">
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" delurl="<?php echo base_url('/account/'.$data['type'].'/config/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php   foreach ($data['data'] as $key =>  $row) { ?>                               
                                <tr class="tr<?php echo $row['id'];?>">
                                    <td ><span class="value" data-value="id"><?php echo $key + 1;?></span></td>
                                    <td><span class="value" data-value="brightpearlAccountId"><?php echo $row['brightpearlAccountId'];?></span></td>
                                    <td><span class="value" data-value="name"><?php echo $row['name'];?></span></td>
                                    <td class="action">
                                        <script> var data<?php echo $row['id'];?> = <?php echo json_encode($row);?>;</script>
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" onclick=editAction(data<?php echo $row['id'];?>) title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" onclick="deleteAction('<?php echo base_url('/account/'.$data['type'].'/config/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="actionmodal" role="dialog" data-backdrop="static">
            <div class="modal-dialog">        
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Brightpearl Account Settings</h4>
                </div>
                <div class="modal-body">
                   <form action="<?php echo base_url('/account/'.$data['type'].'/config/save');?>" method="post" id="saveActionForm" class="form-horizontal saveActionForm" novalidate="novalidate">
                        <div class="form-body">
                                                                                  
                        </div>
						<div class="form-group">
							<label class="control-label col-md-5">Fetch Sales Order Status (Exclude)
								<span class="required" aria-required="true"> * </span>
							</label>
							<div class="col-md-6">
								<select name="data[fetchSalesOrderStatus][]"  multiple="multiple"   class="form-control chosen-select">
									<?php
									$saveChanels = explode(",",$row['fetchSalesOrderStatus']);
									foreach ($data['orderstatus'][$row['brightpearlAccountId']] as $orderstatus) {
										$selected = (in_array($orderstatus['id'],$saveChanels))?('selected="selected"'):('');
										if($orderstatus['orderTypeCode'] == 'SO')
											echo '<option value="'.$orderstatus['id'].'" '.$selected.'>'.ucwords($orderstatus['name']).'</option>';
									}
									?>									
								</select> 
							</div>
						</div>
                        <div class="form-group">
                            <label class="control-label col-md-5">Warehouse For Sale
                                <span class="required" aria-required="true"> * </span>
                            </label>

                            <div class="col-md-6">
                                <select name="data[warehouseSales][]"  multiple="multiple"   class="form-control chosen-select">
                                    <?php
                                    $saveWarehouses = explode(",",$row['warehouseSales']);
                                    foreach ($data['warehouse'][$row['brightpearlAccountId']] as $warehouse) {
                                        $selected = (in_array($warehouse['id'], $saveWarehouses)) ? ('selected="selected"') :('');
                                        echo '<option value="'.$warehouse['id'].'" '.$selected.'>'.ucwords($warehouse['name']).'</option>';
                                    }
                                    ?>                                  
                                </select> 
                            </div>
                        </div>
						<div class="form-group">
							<label class="control-label col-md-5">Default Channel
								<span class="required" aria-required="true"> * </span> 
							</label>
							<div class="col-md-6">
								<select name="data[channelId][]"  multiple="multiple" class="form-control chosen-select">
									<?php
									$saveChanels = explode(",",$row['channelId']);
									foreach ($data['channel'][$row['brightpearlAccountId']] as $channel) {
										$selected = (in_array($channel['id'],$saveChanels))?('selected="selected"'):('');
										echo '<option value="'.$channel['id'].'" '.$selected.'>'.ucwords($channel['name']).'</option>';
									}
									?>
								</select> 
							</div>
						</div>
                        <input type="hidden" name="data[id]" class="id" />
                    </form>                         
                </div>				
                <div class="modal-footer">
                  <button type="button" class="pull-left btn btn-primary submitAction">Save</button>
                  <button type="button" class="btn yellow btn-outline sbold" data-dismiss="modal">Close</button>
                </div>
              </div>                  
            </div>
        </div>
    </div>
</div>


<div class="confighml">
    <?php   
    $data['data'] = ($data['data'])?($data['data']):(array(''));
    foreach ($data['data'] as $key =>  $row) {  ?>
        <div class="htmlaccount<?php echo @$row['id'];?>" style="display: none;">
            <div class="alert alert-danger display-hide">
				<button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
			<div class="form-group">
				<label class="control-label col-md-5">Brightpearl Id
					<span class="required" aria-required="true"> * </span>
				</label>
				<div class="col-md-6">
					<select name="data[brightpearlAccountId]" data-required="1" class="form-control brightpearlAccountId">
						<option value="">Select a save Brightpearl account</option>
						<?php
						foreach ($data['saveAccount'] as $saveAccount) {
							echo '<option value="'.$saveAccount['id'].'">'.ucwords($saveAccount['name']).'</option>';
						}
						?>
					</select>
				</div>
			</div>  			
            <div class="form-group">
                <label class="control-label col-md-5">Default warehouse
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-6">
                    <select name="data[warehouse]" data-required="1" class="form-control warehouse">
                        <?php
                        foreach ($data['warehouse'][$row['brightpearlAccountId']] as $warehouse) {
                            echo '<option value="'.$warehouse['id'].'">'.ucwords($warehouse['name']).'</option>';
                        }
                        ?>
                    </select> 
                </div>
            </div>            
            <div class="form-group hide">
                <label class="control-label col-md-5">Create sales order status
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-6">
                    <select name="data[createOrderStatus]" class="form-control createOrderStatus">
                        <?php
                        foreach ($data['orderstatus'][$row['brightpearlAccountId']] as $orderstatus) {
                            if($orderstatus['orderTypeCode'] == 'SO')
                                echo '<option value="'.$orderstatus['id'].'">'.ucwords($orderstatus['name']).'</option>';
                        }
                        ?>
                    </select> 
                </div>
            </div>
            <div class="form-group hide">
                <label class="control-label col-md-5">PO channel name to fetch inventory
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-6">
                    <select name="data[poInventoryChannelName]"  class="form-control poInventoryChannelName">
                        <?php
                        foreach ($data['channel'][$row['brightpearlAccountId']] as $channel) {
                            echo '<option value="'.$channel['id'].'">'.ucwords($channel['name']).'</option>';
                        }
                        ?>
                    </select> 
                </div>
            </div>
			<?php if($this->globalConfig['enablePurchaseOrder']){ ?>
			<div class="form-group">
                <label class="control-label col-md-5">Fetch Purchase order status
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-6">
                    <select name="data[fetchPurchaseStatus]" data-required="1" class="form-control fetchPurchaseStatus">
                        <?php
                        foreach ($data['orderstatus'][$row['brightpearlAccountId']] as $orderstatus) {
                            if($orderstatus['orderTypeCode'] == 'PO')
                                echo '<option value="'.$orderstatus['id'].'">'.ucwords($orderstatus['name']).'</option>';
                        }
                        ?>
                    </select> 
                </div>
            </div>			
			<div class="form-group hide">
                <label class="control-label col-md-5">Update PO status
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-6">
                    <select name="data[postPurchaseStatus]" class="form-control postPurchaseStatus">
                        <?php
                        foreach ($data['orderstatus'][$row['brightpearlAccountId']] as $orderstatus) {
                            if($orderstatus['orderTypeCode'] == 'PO')
                                echo '<option value="'.$orderstatus['id'].'">'.ucwords($orderstatus['name']).'</option>';
                        }
                        ?>
                    </select> 
                </div>
            </div>
			<div class="form-group hide">
                <label class="control-label col-md-5">PO acknowledged status
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-6">
                    <select name="data[purchaseAcknowledgedStatus]" class="form-control purchaseAcknowledgedStatus">
                        <?php
                        foreach ($data['orderstatus'][$row['brightpearlAccountId']] as $orderstatus) {
                            if($orderstatus['orderTypeCode'] == 'PO')
                                echo '<option value="'.$orderstatus['id'].'">'.ucwords($orderstatus['name']).'</option>';
                        }
                        ?>
                    </select> 
                </div>
            </div>
			<div class="form-group">
                <label class="control-label col-md-5">All PO Stock Received Status
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-6">
                    <select name="data[purchaseReceiptStatus]"  class="form-control purchaseReceiptStatus">
                        <?php
                        foreach ($data['orderstatus'][$row['brightpearlAccountId']] as $orderstatus) {
                            if($orderstatus['orderTypeCode'] == 'PO')
                                echo '<option value="'.$orderstatus['id'].'">'.ucwords($orderstatus['name']).'</option>';
                        }
                        ?>
                    </select> 
                </div>
            </div>
			
			
			<?php }?>
			
            <div class="form-group">
                <label class="control-label col-md-5">Default shipping method
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-6">
                    <select name="data[shippingmethods]" data-required="1" class="form-control shippingmethods">
                        <?php
                        foreach ($data['shippingmethods'][$row['brightpearlAccountId']] as $shippingmethods) {
                            echo '<option value="'.$shippingmethods['id'].'">'.ucwords($shippingmethods['name']).'</option>';
                        }
                        ?>
                    </select> 
                </div>
            </div>
            <div class="form-group hide">
                <label class="control-label col-md-5">Cost pricelist for sales order
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-6">
                    <select name="data[defaultSalesPriceList]"  class="form-control defaultSalesPriceList">
                        <?php
                        foreach ($data['pricelist'][$row['brightpearlAccountId']] as $pricelist) {
                            echo '<option value="'.$pricelist['id'].'">'.ucwords($pricelist['name']).'</option>';
                        }
                        ?>
                    </select> 
                </div>
            </div>
			<div class="form-group">
                <label class="control-label col-md-5">Cost pricelist for stock sync
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-6">
                    <select name="data[defaultProductPriceList]"  class="form-control defaultProductPriceList">
                        <?php
                        foreach ($data['pricelist'][$row['brightpearlAccountId']] as $pricelist) {
                            echo '<option value="'.$pricelist['id'].'">'.ucwords($pricelist['name']).'</option>';
                        }
                        ?>
                    </select> 
                </div>
            </div>
			
            <div class="form-group">
                <label class="control-label col-md-5">Cost pricelist for Product
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-6">
                    <select name="data[defaultPriceListForProduct]"  class="form-control defaultPriceListForProduct">
                        <?php
                        foreach ($data['pricelist'][$row['brightpearlAccountId']] as $pricelist) {
                            echo '<option value="'.$pricelist['id'].'">'.ucwords($pricelist['name']).'</option>';
                        }
                        ?>
                    </select> 
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-5">Cost pricelist for Retails
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-6">
                    <select name="data[defaultPriceListForRetail]"  class="form-control defaultPriceListForRetail">
                        <?php
                        foreach ($data['pricelist'][$row['brightpearlAccountId']] as $pricelist) {
                            echo '<option value="'.$pricelist['id'].'">'.ucwords($pricelist['name']).'</option>';
                        }
                        ?>
                    </select> 
                </div>
            </div>

            <div class="form-group hide">
                <label class="control-label col-md-5">Cost pricelist for customer
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-6">
                    <select name="data[contactPriceList]" class="form-control contactPriceList">
                        <?php
                        foreach ($data['pricelist'][$row['brightpearlAccountId']] as $pricelist) {
                            echo '<option value="'.$pricelist['id'].'">'.ucwords($pricelist['name']).'</option>';
                        }
                        ?>
                    </select> 
                </div>
            </div>
            <div class="form-group hide">
                <label class="control-label col-md-5">Default Customer Tag
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-6">
                    <select name="data[contactDefaultTag]" class="form-control contactDefaultTag">
                        <?php
                        foreach ($data['tag'][$row['brightpearlAccountId']] as $tag) {
                            echo '<option value="'.$tag['id'].'">'.ucwords($tag['name']).'</option>';
                        }
                        ?>
                    </select> 
                </div>
            </div>

            <div class="form-group hide">
                <label class="control-label col-md-5">Default tax code
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-6">
                    <select name="data[taxCode]" data-required="1" class="form-control taxCode">
                        <?php
                        foreach ($data['tax'][$row['brightpearlAccountId']] as $tax) {
                            echo '<option value="'.$tax['id'].'">'.ucwords($tax['name']).'</option>';
                        }
                        ?>
                    </select> 
                </div>
            </div>
			
			<div class="form-group hide">
                <label class="control-label col-md-5">Default nominal code
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-6">
                    <select name="data[nominalCode]" data-required="1" class="form-control nominalCode">
                        <?php
                        foreach ($data['nominalCode'][$row['brightpearlAccountId']] as $nominalCode) {
                            echo '<option value="'.$nominalCode['id'].'">'.ucwords($nominalCode['name']).'</option>';
                        }
                        ?>
                    </select> 
                </div>
            </div>
			
			<div class="form-group hide">
                <label class="control-label col-md-5">Default payment method
                    <span class="required" > * </span>
                </label>
                <div class="col-md-6">
                    <select name="data[defaultPaymentMethod]" class="form-control defaultPaymentMethod">
                        <?php
                        foreach ($data['defaultPaymentMethod'][$row['brightpearlAccountId']] as $defaultPaymentMethod) {
                            echo '<option value="'.$defaultPaymentMethod['id'].'">'.ucwords($defaultPaymentMethod['name']).'</option>';
                        }
                        ?>
                    </select> 
                </div>
            </div>
			
            <div class="form-group">
                <label class="control-label col-md-5">Default Currency
                </label>
                <div class="col-md-6">
                    <input type="text" name="data[currencyCode]"  class="form-control currencyCode" />
                </div>
            </div>
            <div class="form-group hide">
                <label class="control-label col-md-5">Product wholesale custom field
                </label>
                <div class="col-md-6">
                    <input type="text" name="data[productCustField]"  class="form-control productCustField" />
                </div>
            </div> 
			<div class="form-group hide">
                <label class="control-label col-md-5">Product style number custom field
                </label>
                <div class="col-md-6">
                    <input type="text" name="data[productStyleNumber]"  class="form-control productStyleNumber" />
                </div>
            </div> 
			
            <div class="form-group hide">
                <label class="control-label col-md-5">Customer wholesale custom field
                </label>
                <div class="col-md-6">
                    <input type="text" name="data[customerCustomField]"  class="form-control customerCustomField" />
                </div>
            </div>
            <div class="form-group hide">
                <label class="control-label col-md-5">Ship start custom field
                </label>
                <div class="col-md-6">
                    <input type="text" name="data[shipStartDate]"  class="form-control shipStartDate" />
                </div>
            </div>
            <div class="form-group hide">
                <label class="control-label col-md-5">Ship end custom field
                </label>
                <div class="col-md-6">
                    <input type="text" name="data[shipEndDate]"  class="form-control shipEndDate" />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-5">Default BP Timezone
                </label>
                <?php

                 /*   echo "<pre>";
                    print_r($data['accountinfo']);
                    die;
                 echo $data['accountinfo'][$row['brightpearlAccountId']]['configuration']['timeZone'];*/ ?>
                <div class="col-md-6">
                    <input type="text" name="data[timezone]" value="<?php echo $data['accountinfo'][$row['brightpearlAccountId']]['configuration']['timeZone']; ?>"  class="form-control" readonly="readonly"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-5">Avalara Taxrate Enabled</label>
                <div class="col-md-6">
                    <select name="data[AvalaraTaxRateEnabled]" class="form-control AvalaraTaxRateEnabled">
                       <option value="1">Yes</option>
                       <option value="0">No</option>
                    </select> 
                </div>
            </div>
        </div> 
    <?php } ?>
</div>
<link href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.js" type="text/javascript"></script>
<script>
$(".chosen-select").chosen({width: "100%"}); 
jQuery(".actioneditbtn").on("click",function(){
	 $(".chosen-select").trigger("liszt:updated");
});
</script>