<?php
$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['fetchProduct'].'_account')->result_array();
$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postProduct'].'_account')->result_array();
?>
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="index.html">Home</a><i class="fa fa-circle"></i></li>
				<li><span>Customers Details</span></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-shopping-cart"></i>Customers/Vendors Listing</div>
						<div class="actions">
							<a href="<?php echo base_url('customers/customers/fetchCustomers');?>" class="btn btn-circle btn-info btnactionsubmit">
								<i class="fa fa-download"></i>
								<span class="hidden-xs"> Fetch Contacts </span>
							</a>
							<a href="<?php echo base_url('customers/customers/postCustomers');?>" class="btn btn-circle green-meadow btnactionsubmit hide">
								<i class="fa fa-upload"></i>
								<span class="hidden-xs"> Post Contacts </span>
							</a>						
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<div class="table-actions-wrapper">
								<span></span>
								<select class="table-group-action-input form-control input-inline input-small input-sm">
									<option value="">Select...</option>
									<option value="0">Pending</option>
									<option value="1">Sent</option>
									<option value="2">Updated</option>
									<option value="3">Error</option>
									<option value="4">Archive</option>
								</select>
								<button class="btn btn-sm btn-success table-group-action-submit"><i class="fa fa-check"></i>Submit</button>
							</div>
							<table class="table table-striped table-bordered table-hover table-checkable" id="datatable_products"> 
								<thead>
									<tr role="row" class="heading">
										<th width="1%"><input type="checkbox" class="group-checkable" /></th>
										<th width="15%"><?php echo ucwords($this->globalConfig['fetchCustomer']);?>&nbsp;Account</th>
										<th width="15%"><?php echo ucwords($this->globalConfig['postCustomer']);?>&nbsp;Account</th>
										<th width="15%"><?php echo ucwords($this->globalConfig['fetchCustomer']);?>&nbsp;Id</th>
										<th width="15%"><?php echo ucwords($this->globalConfig['postCustomer']);?>&nbsp;Id</th>
										<th width="10%">Email</th>
										<th width="15%">First&nbsp;Name</th>
										<th width="15%">Last&nbsp;Name</th>
										<th width="10%">Phone</th>
										<th width="10%">City</th>
										<th width="10%">State</th>
										<th width="10%">Country</th>
										<th width="10%">Zip</th>
										<th width="15%">Date&nbsp;Updated</th>
										<th width="10%">Status </th>
										<th width="10%">Actions</th>
									</tr>
									<tr role="row" class="filter">
										<td></td>
										<td>
											<select name="account1Id" class="form-control form-filter input-sm">
												<option value="">Select an option</option>
												<?php
													foreach($account1MappingTemps as $account1MappingTemp){	
														echo '<option value="'.$account1MappingTemp['id'].'">'.$account1MappingTemp['name'].'</option>';
													}		
												?>												
											</select>												
										</td>
										<td>
											<select name="account2Id" class="form-control form-filter input-sm">
												<option value="">Select an option</option>
												<?php
													foreach($account2MappingTemps as $account1MappingTemp){	
														echo '<option value="'.$account1MappingTemp['id'].'">'.$account1MappingTemp['name'].'</option>';
													}		
												?>												
											</select>
										</td>
										<td><input type="text" class="form-control form-filter input-sm" name="customerId"> </td>
										<td><input type="text" class="form-control form-filter input-sm" name="createdCustomerId"> </td>
										<td><input type="text" class="form-control form-filter input-sm" name="email"> </td>
										<td><input type="text" class="form-control form-filter input-sm" name="fname"> </td>
										<td><input type="text" class="form-control form-filter input-sm" name="lname"> </td>	
										<td><input type="text" class="form-control form-filter input-sm" name="phone"> </td>				
										<td><input type="text" class="form-control form-filter input-sm" name="city"> </td>
										<td><input type="text" class="form-control form-filter input-sm" name="state"> </td>
										<td><input type="text" class="form-control form-filter input-sm" name="country"> </td>
										<td><input type="text" class="form-control form-filter input-sm" name="zip"> </td>
										<td>
											<div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm" readonly name="updated_from" placeholder="From" />
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
											<div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm" readonly name="updated_to " placeholder="To" />
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
										</td>
										<td>
											<select name="status" class="form-control form-filter input-sm">
												<option value="">Select...</option>
												<option value="0">Pending</option>
												<option value="1">Sent</option>
												<option value="2">Updated</option>
												<option value="3">Error</option>
												<option value="4">Archive</option>
											</select>
										</td>
										<td>
											<div class="margin-bottom-5">
												<button class="btn btn-sm btn-success filter-submit margin-bottom"><i class="fa fa-search"></i>Search</button>
											</div>
											<button class="btn btn-sm btn-default filter-cancel"><i class="fa fa-times"></i>Reset</button>
										</td>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>	
<?php
	if(@!$this->session->userdata($this->router->directory.$this->router->class)[0]){
		echo '<script type="text/javascript"> jsOrder =  [ 1, "desc" ];</script>'; 
	}
?>
<script>
	loadUrl	=	'<?php echo base_url('customers/customers/getCustomers');?>';
</script>