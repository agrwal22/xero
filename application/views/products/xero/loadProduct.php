<?php
$this->brightpearl->reInitialize();
$this->xero->reInitialize();
?>
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="index.html">Home</a><i class="fa fa-circle"></i></li>
				<li><span>Product Details</span></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-shopping-cart"></i>Product Listing</div>
							<div class="actions">
								<a href="<?php echo base_url('products/products/clearProducts');?>" class="btn btn-circle btn-danger btnactionsubmit"> 
									<i class="fa fa-trash"></i>
									<span class="hidden-xs">Clear products</span>
								</a>
								<a href="<?php echo base_url('products/products/uploadProduct');?>" class="btn btn-circle green-meadow uplaodpreorder">
									<i class="fa fa-upload"></i>
									<span class="hidden-xs">Upload product file</span>
								</a>
								<a href="<?php echo base_url('products/products/postLoadedProduct');?>" class="btn btn-circle green-meadow">
									<i class="fa fa-upload"></i>
									<span class="hidden-xs">Post loaded products</span>
								</a>
								
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-container">
								<div class="table-actions-wrapper">
									<span> </span>
									<select class="table-group-action-input form-control input-inline input-small input-sm">
										<option value="">Select...</option>
										<option value="0">Pending</option>
										<option value="1">Sent</option>										
										<option value="2">Removed</option>										
									</select>
									<button class="btn btn-sm btn-success table-group-action-submit">
										<i class="fa fa-check"></i> Submit</button>
									</div>
									<table class="table table-striped table-bordered table-hover table-checkable" id="datatable_products">
										<thead>
											<tr role="row" class="heading">
												<th width="1%"><input type="checkbox" class="group-checkable"> </th>
												<th width="10%"><?php echo $this->globalConfig['account1Name'];?>&nbsp;Account</th>
												<th width="10%"><?php echo $this->globalConfig['account2Name'];?>&nbsp;Account</th>
												<th width="15%">Product&nbsp;SKU</th>
												<th width="10%">Qty</th>
												<th width="10%">Unit Price</th>
												<th width="10%">Available date</th>
												<th width="10%">Account Code</th>
												<th width="10%">Status</th>
											</tr>
											<tr role="row" class="filter">
												<td></td>
												<td><input type="text" class="form-control form-filter input-sm" name="account1Id"> </td>
												<td><input type="text" class="form-control form-filter input-sm" name="account2Id"> </td>
												<td><input type="text" class="form-control form-filter input-sm" name="sku"> </td>
												<td><input type="text" class="form-control form-filter input-sm" name="qty"> </td>
												<td><input type="text" class="form-control form-filter input-sm" name="unitPrice"> </td>
												<td><input type="text" class="form-control form-filter input-sm" name="availableDate"> </td>
												<td><input type="text" class="form-control form-filter input-sm" name="accountCode"> </td>
												<td>
													<select name="status" class="form-control form-filter input-sm">
														<option value="">Select...</option>	
														<option value="0">Pending</option>
														<option value="1">Sent</option>
													</select>
												</td>
												<td>
													<div class="margin-bottom-5">
														<button class="btn btn-sm btn-success filter-submit margin-bottom"><i class="fa fa-search"></i>Search</button>
													</div>
													<button class="btn btn-sm btn-default filter-cancel">	<i class="fa fa-times"></i>Reset</button>
												</td>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="popup" role="dialog" data-backdrop="static">
			<div class="modal-dialog modal-lg">
			  	<div class="modal-content">
					<div class="modal-header">
				  		<button type="button" class="close" data-dismiss="modal">&times;</button>
				  		<h4 class="modal-title">Upload product file</h4>
					</div>
					<div class="modal-body" style="display: inline-block;width:100%">
						<form method="post"  action ="<?php echo base_url("products/products/uploadProduct");?>" id="uploadform" enctype="multipart/form-data">
							<div class="col-md-12" style="float: left;text-align: center;width: 100%;">
								<div class="col-md-3">
									<select class="form-control chosen-select" name="account1Id">
										<?php
										foreach($this->brightpearl->accountDetails as $account1Id => $accountDetails){
											echo '<option value="'.$account1Id.'">'.$accountDetails['name'].'</option>';
										}
										?>
									</select>
								</div>
								<div class="col-md-3">
									<select class="form-control chosen-select" name="account2Id">
										<?php
										foreach($this->xero->accountDetails as $account1Id => $accountDetails){
											echo '<option value="'.$account1Id.'">'.$accountDetails['name'].'</option>';
										}
										?>
									</select>
								</div>
								<div class="col-md-3">
									<div class="" >
										<input required="true" type="file" name="uploadprefile"  accept=".csv" style="display: inline;padding-top: 5px;"  />
									</div>
								</div>
								<div class="col-md-3">
									<input type="Submit" class="btn btn-primary uploadform" value="upload" value="Submit" /> 
								</div>
							</div>
						</form>                       
					</div>
					<div class="modal-footer"></div>
			  </div>                  
			</div>
		</div>
		<div class="loader" style="display:none;">
			<div class="loader-inner">
				<img src="<?php echo base_url('assets/img/ajax-loader.gif');?>" />
			</div>
		</div>
		<?php if(@!$this->session->userdata($this->router->directory.$this->router->class)[0]){	echo '<script type="text/javascript"> jsOrder =  [ 3, "desc" ];</script>'; } ?>
		<script>
			loadUrl	=  '<?php echo base_url('products/products/getLoadProduct');?>';
			jQuery(document).on("click",".showDetails",function(e){
				e.preventDefault();
				newSku	= jQuery(this).attr('data-newsku');
				color	= jQuery(this).attr('data-color');
				if(newSku == ''){
					alert("Product have no any varient");
				}
				else{
					jQuery(".loader").show();
					jQuery.post('<?php echo base_url('products/products/getVarient');?>',{'newSku': newSku,'color':color},function(res){
						jQuery("#myModal").modal('show');
						jQuery(".modal-title").text(name);
						jQuery(".body-text").html(res); 
						jQuery(".loader").hide();
					});
				}		 
			});
			jQuery(".uplaodpreorder").click(function(e){
				e.preventDefault();
				jQuery("#popup").modal('show');
			})
		</script>
