<?php
	$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['fetchProduct'].'_account')->result_array();
	$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postProduct'].'_account')->result_array();
?>
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="index.html">Home</a><i class="fa fa-circle"></i></li>
				<li><span>Product Details</span></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-shopping-cart"></i>Product Listing</div>
							<div class="actions">
								<a href="<?php echo base_url('products/products/fetchProducts');?>" class="btn btn-circle btn-info btnactionsubmit"> 
									<i class="fa fa-download"></i>
									<span class="hidden-xs">Fetch Products</span>
								</a>
								<a href="<?php echo base_url('products/products/postProducts');?>" class="btn btn-circle green-meadow btnactionsubmit hide">
									<i class="fa fa-upload"></i>
									<span class="hidden-xs">Post Products</span>
								</a>						
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-container">
								<div class="table-actions-wrapper">
									<span></span>
									<select class="table-group-action-input form-control input-inline input-small input-sm">
										<option value="">Select...</option>
										<option value="0">Pending</option>
										<option value="1">Sent</option>
										<option value="2">Updated</option>
										<option value="3">Error</option>
										<option value="4">Archive</option>
									</select>
									<button class="btn btn-sm btn-success table-group-action-submit"><i class="fa fa-check"></i> Submit</button>
									</div>
									<table class="table table-striped table-bordered table-hover table-checkable" id="datatable_products">
										<thead>
											<tr role="row" class="heading">
												<th width="1%"><input type="checkbox" class="group-checkable"> </th>
												<th width="15%"><?php echo $this->globalConfig['account1Name'];?>&nbsp;Account</th>
												<th width="15%"><?php echo $this->globalConfig['account2Name'];?>&nbsp;Account</th>
												<th width="15%"><?php echo $this->globalConfig['account1Name'];?>&nbsp;Id</th>
												<th width="15%"><?php echo $this->globalConfig['account2Name'];?>&nbsp;Id</th>
												<th width="15%">Product&nbsp;SKU</th>
												<th width="10%">Color</th>
												<th width="10%">Size</th>
												<th width="15%">Product&nbsp;Name</th>
												<th width="15%">Date&nbsp;Updated</th>
												<th width="10%">Status</th>
												<th width="10%">Actions</th>
											</tr>
											<tr role="row" class="filter">
												<td></td>
												<td>
													<select name="account1Id" class="form-control form-filter input-sm">
														<option value="">Select an option</option>
														<?php
															foreach($account1MappingTemps as $account1MappingTemp){	
																echo '<option value="'.$account1MappingTemp['id'].'">'.$account1MappingTemp['name'].'</option>';
															}		
														?>
													</select>
												</td>
												<td>
													<select name="account2Id" class="form-control form-filter input-sm">
														<option value="">Select an option</option>
														<?php
															foreach($account2MappingTemps as $account1MappingTemp){	
																echo '<option value="'.$account1MappingTemp['id'].'">'.$account1MappingTemp['name'].'</option>';
															}		
														?>
												</select>
												</td>
												<td><input type="text" class="form-control form-filter input-sm" name="productId"> </td>
												<td><input type="text" class="form-control form-filter input-sm" name="createdProductId"> </td>
												<td><input type="text" class="form-control form-filter input-sm" name="sku"> </td>
												<td><input type="text" class="form-control form-filter input-sm" name="color"> </td>	
												<td><input type="text" class="form-control form-filter input-sm" name="size"> </td>
												<td><input type="text" class="form-control form-filter input-sm" name="name"> </td>
												<td>
													<div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
														<input type="text" class="form-control form-filter input-sm" readonly name="updated_from" placeholder="From" />
														<span class="input-group-btn">
															<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>	
														</span>
													</div>
													<div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
														<input type="text" class="form-control form-filter input-sm" readonly name="updated_to" placeholder="To" />
														<span class="input-group-btn">
															<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>	
														</span>
													</div>
												</td>
												<td>
													<select name="status" class="form-control form-filter input-sm">
														<option value="">Select...</option>
														<option value="0">Pending</option>
														<option value="1">Sent</option>
														<option value="2">Updated</option>
														<option value="3">Error</option>
														<option value="4">Archive</option>
													</select>
												</td>
												<td>
													<div class="margin-bottom-5">
														<button class="btn btn-sm btn-success filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
													</div><button class="btn btn-sm btn-default filter-cancel"><i class="fa fa-times"></i>Reset</button>
												</td>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"></h4>
					</div>
					<div class="modal-body">
						<div class="body-text"></div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<div class="loader" style="display:none;">
			<div class="loader-inner">
				<img src="<?php echo base_url('assets/img/ajax-loader.gif');?>" />
			</div>
		</div>
		<?php 
			if(@!$this->session->userdata($this->router->directory.$this->router->class)[0]){
				echo '<script type="text/javascript"> jsOrder =  [ 3, "desc" ];</script>'; 
			} 
		?>
		<script>
			loadUrl	=  '<?php echo base_url('products/products/getProduct');?>';
			jQuery(document).on("click",".showDetails",function(e){
				e.preventDefault();
				newSku	= jQuery(this).attr('data-newsku');
				color	= jQuery(this).attr('data-color');
				if(newSku == ''){
					alert("Product have no any varient");
				}
				else{
					jQuery(".loader").show();
					jQuery.post('<?php echo base_url('products/products/getVarient');?>',{'newSku': newSku,'color':color},function(res){
						jQuery("#myModal").modal('show');
						jQuery(".modal-title").text(name);
						jQuery(".body-text").html(res); 
						jQuery(".loader").hide();
					});
				}		 
			});
		</script>