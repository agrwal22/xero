<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="index.html">Home</a><i class="fa fa-circle"></i></li>
				<li><span>Sales Repport Details</span></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-file"></i>Sales Report Listing </div>
						<div class="actions hide">
							<a href="<?php echo base_url('sales/sales/fetchSales');?>" class="btn btn-circle btn-info btnactionsubmit">
								<i class="fa fa-download"></i>
								<span class="hidden-xs"> Fetch Sales</span>
							</a>
							<a href="<?php echo base_url('sales/sales/postSales');?>" class="btn btn-circle green-meadow btnactionsubmit">
								<i class="fa fa-upload"></i>
								<span class="hidden-xs"> Post Sales</span>
							</a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<div class="table-actions-wrapper">
								<span></span>
								<select class="table-group-action-input form-control input-inline input-small input-sm">
									<option value="">Select...</option>
									<option value="0">Pending</option>
									<option value="1">Sent</option>
									<option value="2">Acknowledgement</option>
									<option value="3">Full Dispatch</option>
									<option value="4">Archive</option> 
								</select>
								<button class="btn btn-sm btn-success table-group-action-submit"><i class="fa fa-check"></i>Submit</button>
							</div>
							<table class="table table-striped table-bordered table-hover table-checkable" id="datatable_products">
								<thead>
									<tr role="row" class="heading">
										<th width="1%"><input type="checkbox" class="group-checkable"> </th>
										<th width="10%"><?php echo ucwords($this->globalConfig['fetchSalesOrder']);?>&nbsp;orderId </th>
										<th width="15%">orderAmountBP</th>
										<th width="10%">orderAmountXero</th>
										<th width="10%">taxAmountBP</th>
										<th width="10%">taxAmountXero</th>
										<th width="10%">paidAmountBP</th>
										<th width="10%">paidAmountXero</th>
										<th width="10%">bpCreateDate</th>
										<th width="10%">bpTaxDate</th>
										<th width="10%">bpPaymentDate</th>
										<th width="10%">updateDate</th>
										<th width="10%">XeroCreateDate</th>
										<th width="10%">Action</th>
									</tr>
									<tr role="row" class="filter">
										<td></td>
										<td><input type="text" class="form-control form-filter input-sm" name="orderId"></td>
										<td><input type="text" class="form-control form-filter input-sm" name="orderAmountBP"></td>
										<td><input type="text" class="form-control form-filter input-sm" name="orderAmountXero"></td>
										<td><input type="text" class="form-control form-filter input-sm" name="taxAmountBP"></td>
										<td><input type="text" class="form-control form-filter input-sm" name="taxAmountXero"></td>
										<td><input type="text" class="form-control form-filter input-sm" name="paidAmountBP"></td>
										<td><input type="text" class="form-control form-filter input-sm" name="paidAmountXero"></td>
										<td>
											<div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm" readonly name="bpCreateDate_from" placeholder="From">
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
											<div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm" readonly name="bpCreateDate_to" placeholder="To">
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
										</td>
										<td>
											<div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm" readonly name="bpTaxDate_from" placeholder="From">
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
											<div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm" readonly name="bpTaxDate_to" placeholder="To">
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
										</td>
										<td><input type="text" class="form-control form-filter input-sm" name="BPPaymentDate"></td>
										<td><input type="text" class="form-control form-filter input-sm" name="UpdateDate"></td>
										<td>
											<div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm" readonly name="XeroCreateDate_from" placeholder="From">
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
											<div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm" readonly name="XeroCreateDate_to" placeholder="To">
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
										</td>
										<td>
											<div class="margin-bottom-5">
												<button class="btn btn-sm btn-success filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
											</div>
											<button class="btn btn-sm btn-default filter-cancel"><i class="fa fa-times"></i> Reset</button>
										</td>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>		
<script type="text/javascript">
	loadUrl	= '<?php echo base_url('report/sales/getSales');?>';
</script>
<style>
.datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-right.datepicker-orient-bottom {
    top: 28px !important;
}
.datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom {
    top: 28px !important;
}
</style>