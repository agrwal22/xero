<?php
$librariesNames = $this->config->item('librariesNames');
$mappings       = $this->config->item('mapping');
$fieldmappings  = $this->config->item('fieldmapping');
?>
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li><a href="index.html">Home</a><i class="fa fa-circle"></i></li>
                <li><span>User</span></li>
            </ul>
        </div>
        <h3 class="page-title"> New User Profile | Account<small>user account page</small></h3>
        <div class="row">
            <div class="col-md-12">
                <div class="profile-sidebar">
                    <div class="portlet light profile-sidebar-portlet ">
                        <div class="profile-userpic">
                            <img src="<?php echo $user_session_data['profileimage'];?>" class="img-responsive" alt=""> </div>
                        <div class="profile-usertitle">
                            <div class="profile-usertitle-name"> <?php echo $data['user']['firstname'] ." " . $data['user']['lastname']  ;?> </div>
                        </div>                  
                    </div>
                </div>
                <div class="profile-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light ">
                                <div class="portlet-title tabbable-line">
                                    <div class="caption caption-md">
                                        <i class="icon-globe theme-font hide"></i>
                                        <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                                    </div>
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tab_1_1" data-toggle="tab">Personal Info</a></li>
                                        <li><a href="#tab_1_2" data-toggle="tab">Change Avatar</a></li>
                                        <li><a href="#tab_1_3" data-toggle="tab">Change Password</a></li> 
                                        <li><a href="#tab_1_4" data-toggle="tab">Privacy Settings</a></li>
                                    </ul>
                                </div>
                                <div class="portlet-body">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_1_1">
                                            <form role="form" action="<?php echo base_url('users/profile/saveBasic');?>">
                                                <div class="alert alert-success hide">      
                                                     <strong>Info!</strong> Data saved successfully.                                     
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">First Name</label>
                                                    <input type="text" name="firstname" value="<?php echo $data['user']['firstname'];?>" placeholder="First Name" class="form-control" /> </div>
                                                <div class="form-group">
                                                    <label class="control-label">Last Name</label>
                                                    <input type="text" placeholder="Last Name" name="lastname" value="<?php echo $data['user']['lastname'];?>"  class="form-control" /> </div>
                                                <div class="form-group">
                                                    <label class="control-label">Mobile Number</label>
                                                    <input type="text" placeholder="Mobile Number" name="phone" value="<?php echo $data['user']['phone'];?>"  class="form-control" /> </div>
                                                <div class="form-group">
                                                    <label class="control-label">Email</label>
                                                    <input type="email" placeholder="Email" name="email" value="<?php echo $data['user']['email'];?>"  class="form-control" /> </div>
                                                <div class="margiv-top-10">
                                                    <a href="javascript:;" class="btn green savefrombtn"> <span class="text"> Save Changes </span> <img style="display:none;width: 30px" src="<?php echo $this->config->item('script_url').'assets/layouts/layout/img/ajax-loader-1.gif';?>"> </a>
                                                    <a href="javascript:;" class="btn default"> Cancel </a>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane" id="tab_1_2">
                                            <p> Update profile picture. </p>
                                            <form action="<?php echo base_url('users/profile/updateProfilePic');?>" role="form" enctype="multipart/form-data">
                                                 <div class="alert alert-success hide">      
                                                     <strong>Info!</strong> Data saved successfully.                                     
                                                </div>
                                                <div class="form-group">
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                        <div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" name="..." /> </span>
                                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                        </div>
                                                    </div>                                                
                                                </div>
                                                <div class="margin-top-10">
                                                    <a href="javascript:;" class="btn green savefrombtn">  Submit </span> <img style="display:none;width: 30px" src="<?php echo $this->config->item('script_url') .'assets/layouts/layout/img/ajax-loader-1.gif';?>"> </a>
                                                    <a href="javascript:;" class="btn default"> Cancel </a>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane" id="tab_1_3">
                                            <form role="form" action="<?php echo base_url('users/profile/updatePassword');?>">
                                                <div class="alert alert-success hide">      
                                                     <strong>Info!</strong> Data saved successfully.                                     
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Current Password</label>
                                                    <input type="password" name="password" class="form-control" /> </div>
                                                <div class="form-group">
                                                    <label class="control-label">New Password</label>
                                                    <input type="password" name="newpassword" class="form-control" /> </div>
                                                <div class="form-group">
                                                    <label class="control-label">Re-type New Password</label>
                                                    <input type="password" name="newpassword2" class="form-control" /> </div>
                                                <div class="margin-top-10">
                                                    <a href="javascript:;" class="btn green savefrombtn">  Changes Password </span> <img style="display:none;width: 30px" src="<?php echo base_url('assets/layouts/layout/img/ajax-loader-1.gif');?>"> </a>
                                                    <a href="javascript:;" class="btn default"> Cancel </a>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane" id="tab_1_4">
                                            <form role="form" action="<?php echo base_url('users/profile/saveGlobalConfig');?>">
                                                <div class="alert alert-success hide">
                                                    <strong>Info!</strong> Data saved successfully.
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Application Name</label>
                                                    <input type="text" name="app_name" value="<?php echo $data['globalConfig']['app_name'];?>" class="form-control" /> </div>
                                                <div class="row enableAccountSettings">
                                                    <div class="form-group col-md-6">
                                                        <label for="account1Name">Enter Account 1 Name</label>
                                                        <input type="text" name="account1Name" value="<?php echo $data['globalConfig']['account1Name'];?>" class="form-control" />
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="account1Liberary">Select Account 1 Library</label>
                                                        <select name="account1Liberary" class="form-control" value="<?php echo $data['globalConfig']['account1Liberary']; ?>">
                                                        <?php
                                                            foreach ($librariesNames as $key => $librariesName) {
                                                                echo '<option value="'.$key.'">'.$librariesName.'</option>';
                                                            }
                                                        ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row enableAccountSettings">
                                                    <div class="form-group col-md-6">
                                                        <label for="account2Name">Enter Account 2 Name</label>
                                                        <input type="text" name="account2Name" value="<?php echo $data['globalConfig']['account2Name'];?>" class="form-control" />
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="account2Liberary">Select Account 2 Library</label>
                                                        <select name="account2Liberary" class="form-control" value="<?php echo $data['globalConfig']['account2Liberary']; ?>">
                                                        <?php
                                                            foreach ($librariesNames as $key => $librariesName) {
                                                                echo '<option value="'.$key.'">'.$librariesName.'</option>';
                                                            }
                                                        ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Enable Mapping</label>
                                                    <div class="class="form-control"">
                                                    <input type="checkbox" name="enableMapping" value="1" <?php echo ($data['globalConfig']['enableMapping'])?('checked="checked"'):('');?> class="switchbtn" /> </div>
                                                </div>
                                                <div class="row enableMapping">
                                                    <?php
                                                        foreach($mappings as $mapKey => $mapping){ ?>
                                                    <div class="form-group col-md-3">
                                                        <label for="enable<?php echo ucwords($mapKey);?>Mapping"><?php echo $mapping;?> Mapping</label>
                                                        <input type="checkbox" name="enable<?php echo ucwords($mapKey);?>Mapping" value="1" <?php echo ($data['globalConfig']['enable'.ucwords($mapKey).'Mapping'])?('checked="checked"'):('');?> class="switchbtn"  />
                                                    </div>
                                                    <?php 
                                                        }	
                                                    ?>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Enable Field Confirmation</label>
                                                    <div class="class="form-control"">
                                                    <input type="checkbox" name="enableFieldMapping" value="1" <?php echo ($data['globalConfig']['enableFieldMapping'])?('checked="checked"'):('');?> class="switchbtn"  /></div> 
                                                </div>
                                                <div class="row enableFieldMapping">
                                                    <?php
                                                        foreach($fieldmappings as $mapKey => $mapping){
                                                    ?>
                                                    <div class="form-group col-md-3">
                                                        <label for="enable<?php echo ucwords($mapKey);?>Mapping"><?php echo $mapping;?> Mapping</label>
                                                        <input type="checkbox" name="enable<?php echo ucwords($mapKey);?>Mapping" value="1" <?php echo ($data['globalConfig']['enable'.ucwords($mapKey).'Mapping'])?('checked="checked"'):('');?> class="switchbtn"  />
                                                    </div>
                                                    <?php 
                                                        }	
                                                    ?>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-3">
                                                        <label class="control-label">Enable Avalara Tax</label>
                                                        <div class="class="form-control"">
                                                        <input type="checkbox" name="enableAvalaraTax" value="1" <?php echo ($data['globalConfig']['enableAvalaraTax'])?('checked="checked"'):('');?> class="switchbtn"  /></div> 
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label class="control-label">Enable Inventory Transfer</label>
                                                        <div class="class="form-control"">
                                                        <input type="checkbox" name="enableInventoryTransfer" value="1" <?php echo ($data['globalConfig']['enableInventoryTransfer'])?('checked="checked"'):('');?> class="switchbtn"  /></div> 
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label class="control-label">Enable Inventory Management</label>
                                                        <div class="class="form-control"">
                                                        <input type="checkbox" name="enableInventoryManagement" value="1" <?php echo ($data['globalConfig']['enableInventoryManagement'])?('checked="checked"'):('');?> class="switchbtn"  /></div>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label class="control-label">Enable Amazon Fee</label>
                                                        <div class="class="form-control"">
                                                        <input type="checkbox" name="enableAmazonFees" value="1" <?php echo ($data['globalConfig']['enableAmazonFees'])?('checked="checked"'):('');?> class="switchbtn"  /></div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-3">
                                                        <label class="control-label">Enable Reports</label>
                                                        <div class="class="form-control"">
                                                        <input type="checkbox" name="enableReports" value="1" <?php echo ($data['globalConfig']['enableReports'])?('checked="checked"'):('');?> class="switchbtn"  /></div>
                                                    </div>
                                                    <div class="form-group col-md-3"></div>
                                                    <div class="form-group col-md-3"></div>
                                                    <div class="form-group col-md-3"></div>
                                                </div>
                                                <div class="form-group">
                                                   <label class="control-label">Enable Product</label>
                                                   <div class="class="form-control"">
                                                    <input type="checkbox" name="enableProduct" value="1" <?php echo ($data['globalConfig']['enableProduct'])?('checked="checked"'):('');?> class="switchbtn"  /> </div> 
                                                </div>
                                                <div class="row enableProduct">
                                                    <div class="form-group col-md-5">
                                                        <label for="fetchProduct">Fetch Product From</label>
                                                        <select name="fetchProduct" class="form-control" value="<?php echo $data['globalConfig']['fetchProduct']; ?>">
                                                        <?php
                                                            foreach ($librariesNames as $key => $librariesName) {
                                                                echo '<option value="'.$key.'">'.$librariesName.'</option>';
                                                            }
                                                        ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-5">
                                                        <label for="postProduct">Post Product To</label>
                                                        <select name="postProduct" class="form-control" value="<?php echo $data['globalConfig']['postProduct']; ?>">
                                                        <?php
                                                            foreach ($librariesNames as $key => $librariesName) {
                                                                echo '<option value="'.$key.'">'.$librariesName.'</option>';
                                                            }
                                                        ?>
                                                        </select>
                                                    </div>   
                                                    <div class="form-group">
                                                        <label class="control-label">Enable Prebook</label>
                                                        <div class="class="form-control"">
                                                        <input type="checkbox" name="enablePrebook" value="1" <?php echo ($data['globalConfig']['enablePrebook'])?('checked="checked"'):('');?> class="switchbtn"  /></div> 
                                                    </div>                                             
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Enable Customer</label>
                                                    <div class="class="form-control"">
                                                    <input type="checkbox" name="enableCustomer" value="1" <?php echo ($data['globalConfig']['enableCustomer'])?('checked="checked"'):('');?> class="switchbtn"  /> </div> 
                                                </div>
                                                <div class="row enableCustomer">
                                                    <div class="form-group col-md-6">
                                                        <label for="fetchCustomer">Fetch Customer From</label>
                                                        <select name="fetchCustomer" class="form-control" value="<?php echo $data['globalConfig']['fetchCustomer']; ?>">
                                                        <?php
                                                            foreach ($librariesNames as $key => $librariesName) {
                                                                echo '<option value="'.$key.'">'.$librariesName.'</option>';
                                                            }
                                                        ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="postCustomer">Post Customer To</label>
                                                        <select name="postCustomer" class="form-control" value="<?php echo $data['globalConfig']['postCustomer']; ?>">
                                                        <?php
                                                            foreach ($librariesNames as $key => $librariesName) {
                                                                echo '<option value="'.$key.'">'.$librariesName.'</option>';
                                                            }
                                                        ?>
                                                        </select>
                                                    </div>                                                
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Enable Sales Order</label>
                                                    <div class="class="form-control"">
                                                    <input type="checkbox" name="enableSalesOrder" value="1" <?php echo ($data['globalConfig']['enableSalesOrder'])?('checked="checked"'):('');?> class="switchbtn"  /> </div> 
                                                </div>
                                                <div class="row enableSalesOrder">
                                                    <div class="form-group col-md-6">
                                                        <label for="fetchSalesOrder">Fetch Sales Order From</label>
                                                        <select name="fetchSalesOrder" class="form-control" value="<?php echo $data['globalConfig']['fetchSalesOrder']; ?>">
                                                        <?php
                                                            foreach ($librariesNames as $key => $librariesName) {
                                                                echo '<option value="'.$key.'">'.$librariesName.'</option>';
                                                            }
                                                        ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="postSalesOrder">Post Sales Order To</label>
                                                        <select name="postSalesOrder" class="form-control" value="<?php echo $data['globalConfig']['postSalesOrder']; ?>">
                                                        <?php
                                                            foreach ($librariesNames as $key => $librariesName) {
                                                                echo '<option value="'.$key.'">'.$librariesName.'</option>';
                                                            }
                                                        ?>
                                                        </select>
                                                    </div>                                                
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Enable Dispatch Confirmation</label>
                                                    <div class="class="form-control"">
                                                    <input type="checkbox" name="enableDispatchConfirmation" value="1" <?php echo ($data['globalConfig']['enableDispatchConfirmation'])?('checked="checked"'):('');?> class="switchbtn"  /></div> 
                                                </div>
                                                <div class="row enableDispatchConfirmation">
                                                    <div class="form-group col-md-6">
                                                        <label for="fetchDispatchConfirmation">Fetch Dispatch Confirmation From</label>
                                                        <select name="fetchDispatchConfirmation" class="form-control" value="<?php echo $data['globalConfig']['fetchDispatchConfirmation']; ?>">
                                                        <?php
                                                            foreach ($librariesNames as $key => $librariesName) {
                                                                echo '<option value="'.$key.'">'.$librariesName.'</option>';
                                                            }
                                                        ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="postDispatchConfirmation">Post Dispatch Confirmation To</label>
                                                        <select name="postDispatchConfirmation" class="form-control" value="<?php echo $data['globalConfig']['postDispatchConfirmation']; ?>" >
                                                        <?php
                                                            foreach ($librariesNames as $key => $librariesName) {
                                                                echo '<option value="'.$key.'">'.$librariesName.'</option>';
                                                            }
                                                        ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Enable Packing Interface</label>
                                                    <div class="class="form-control"">
                                                    <input type="checkbox" name="enablePacking" value="1" <?php echo ($data['globalConfig']['enablePacking'])?('checked="checked"'):('');?> class="switchbtn"  /> </div> 
                                                </div>
                                                <div class="row enablePacking">
                                                    <div class="form-group col-md-6">
                                                        <label for="fetchPacking">Fetch Packing From</label>
                                                        <select name="fetchPacking" class="form-control" value="<?php echo $data['globalConfig']['fetchPacking']; ?>">
                                                        <?php
                                                            foreach ($librariesNames as $key => $librariesName) {
                                                                echo '<option value="'.$key.'">'.$librariesName.'</option>';
                                                            }
                                                        ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="postPacking">Post Packing To</label>
                                                        <select name="postPacking" class="form-control" value="<?php echo $data['globalConfig']['postPacking']; ?>" >
                                                        <?php
                                                            foreach ($librariesNames as $key => $librariesName) {
                                                                echo '<option value="'.$key.'">'.$librariesName.'</option>';
                                                            }
                                                        ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Enable Sales Credit</label>
                                                    <div class="class="form-control"">
                                                    <input type="checkbox" name="enableSalesCredit" value="1" <?php echo ($data['globalConfig']['enableSalesCredit'])?('checked="checked"'):('');?> class="switchbtn"  /></div> 
                                                </div>
                                                <div class="row enableSalesCredit">
                                                    <div class="form-group col-md-6">
                                                        <label for="fetchSalesCredit">Fetch Sales Credit From</label>
                                                        <select name="fetchSalesCredit" class="form-control" value="<?php echo $data['globalConfig']['fetchSalesCredit']; ?>">
                                                        <?php
                                                            foreach ($librariesNames as $key => $librariesName) {
                                                                echo '<option value="'.$key.'">'.$librariesName.'</option>';
                                                            }
                                                        ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="postSalesCredit">Post Sales Credit To</label>
                                                        <select name="postSalesCredit" class="form-control" value="<?php echo $data['globalConfig']['postSalesCredit']; ?>" >
                                                        <?php
                                                            foreach ($librariesNames as $key => $librariesName) {
                                                                echo '<option value="'.$key.'">'.$librariesName.'</option>';
                                                            }
                                                        ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Enable Purchase Order</label>
                                                    <div class="class="form-control"">
                                                    <input type="checkbox" name="enablePurchaseOrder" value="1" <?php echo ($data['globalConfig']['enablePurchaseOrder'])?('checked="checked"'):('');?> class="switchbtn"  /></div> 
                                                </div>
                                                <div class="row enablePurchaseOrder">
                                                    <div class="form-group col-md-6">
                                                        <label for="fetchPurchaseOrder">Fetch Purchase Order From</label>
                                                        <select name="fetchPurchaseOrder" class="form-control" value="<?php echo $data['globalConfig']['fetchPurchaseOrder']; ?>" >
                                                        <?php
                                                            foreach ($librariesNames as $key => $librariesName) {
                                                                echo '<option value="'.$key.'">'.$librariesName.'</option>';
                                                            }
                                                        ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="postPurchaseOrder">Post Purchase Order To</label>
                                                        <select name="postPurchaseOrder" class="form-control" value="<?php echo $data['globalConfig']['postPurchaseOrder']; ?>">
                                                        <?php
                                                            foreach ($librariesNames as $key => $librariesName) {
                                                                echo '<option value="'.$key.'">'.$librariesName.'</option>';
                                                            }
                                                        ?>
                                                        </select>
                                                    </div>                                                
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Enable Receipt Confirmation Order</label>
                                                    <div class="class="form-control"">
                                                    <input type="checkbox" name="enableReceipt" value="1" <?php echo ($data['globalConfig']['enableReceipt'])?('checked="checked"'):('');?> class="switchbtn"  /> </div> 
                                                </div>
                                                <div class="row enableReceipt">
                                                    <div class="form-group col-md-6">
                                                        <label for="fetchReceipt">Fetch Receipt Confirmation From</label>
                                                        <select name="fetchReceipt" class="form-control" value="<?php echo $data['globalConfig']['fetchReceipt']; ?>">
                                                        <?php
                                                            foreach ($librariesNames as $key => $librariesName) {
                                                                echo '<option value="'.$key.'">'.$librariesName.'</option>';
                                                            }
                                                        ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="postReceipt">Post Receipt Confirmation To</label>
                                                        <select name="postReceipt" class="form-control" value="<?php echo $data['globalConfig']['postReceipt']; ?>">
                                                        <?php
                                                            foreach ($librariesNames as $key => $librariesName) {
                                                                echo '<option value="'.$key.'">'.$librariesName.'</option>';
                                                            }
                                                        ?>
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Enable Purchase Credit</label>
                                                    <div class="class="form-control"">
                                                    <input type="checkbox" name="enablePurchaseCredit" value="1" <?php echo ($data['globalConfig']['enablePurchaseCredit'])?('checked="checked"'):('');?> class="switchbtn"  /> </div> 
                                                </div>
                                                <div class="row enablePurchaseCredit">
                                                    <div class="form-group col-md-6">
                                                        <label for="fetchPurchaseCredit">Fetch Purchase Credit From</label>
                                                        <select name="fetchPurchaseCredit" class="form-control" value="<?php echo $data['globalConfig']['fetchPurchaseCredit']; ?>">
                                                        <?php
                                                            foreach ($librariesNames as $key => $librariesName) {
                                                                echo '<option value="'.$key.'">'.$librariesName.'</option>';
                                                            }
                                                        ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="postPurchaseCredit">Post Purchase Credit To</label>
                                                        <select name="postPurchaseCredit" class="form-control" value="<?php echo $data['globalConfig']['postPurchaseCredit']; ?>">
                                                        <?php
                                                            foreach ($librariesNames as $key => $librariesName) {
                                                                echo '<option value="'.$key.'">'.$librariesName.'</option>';
                                                            }
                                                        ?>
                                                      </select>
                                                    </div>                                                
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Enable Stock Transfer</label>
                                                    <div class="class="form-control"">
                                                    <input type="checkbox" name="enableStockTransfer" value="1" <?php echo ($data['globalConfig']['enableStockTransfer'])?('checked="checked"'):('');?> class="switchbtn"  /> </div> 
                                                </div>
                                                <div class="row enableStockTransfer">
                                                    <div class="form-group col-md-6">
                                                        <label for="fetchStockTransfer">Fetch Stock Transfer From</label>
                                                        <select name="fetchStockTransfer" class="form-control" value="<?php echo $data['globalConfig']['fetchStockTransfer']; ?>" >
                                                        <?php
                                                            foreach ($librariesNames as $key => $librariesName) {
                                                                echo '<option value="'.$key.'">'.$librariesName.'</option>';
                                                            }
                                                        ?>
                                                      </select>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="postStockTransfer">Post Stock Transfer To</label>
                                                        <select name="postStockTransfer" class="form-control" value="<?php echo $data['globalConfig']['postStockTransfer']; ?>">
                                                        <?php
                                                            foreach ($librariesNames as $key => $librariesName) {
                                                                echo '<option value="'.$key.'">'.$librariesName.'</option>';
                                                            }
                                                        ?>
                                                      </select>
                                                    </div>                                                
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Enable Stock Adjustment</label>
                                                    <div class="class="form-control"">
                                                    <input type="checkbox" name="enableStockAdjustment" value="1" <?php echo ($data['globalConfig']['enableStockAdjustment'])?('checked="checked"'):('');?> class="switchbtn"  /> </div> 
                                                </div>
                                                <div class="row enableStockAdjustment">
                                                    <div class="form-group col-md-6">
                                                        <label for="fetchStockAdjustment">Fetch Stock Adjustment From</label>
                                                        <select name="fetchStockAdjustment" class="form-control" value="<?php echo $data['globalConfig']['fetchStockAdjustment']; ?>">
                                                        <?php
                                                            foreach ($librariesNames as $key => $librariesName) {
                                                                echo '<option value="'.$key.'">'.$librariesName.'</option>';
                                                            }
                                                      ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="postStockAdjustment">Post Stock Adjustment To</label>
                                                        <select name="postStockAdjustment" class="form-control" value="<?php echo $data['globalConfig']['postStockAdjustment']; ?>">
                                                        <?php
                                                            foreach ($librariesNames as $key => $librariesName) {
                                                                echo '<option value="'.$key.'">'.$librariesName.'</option>';
                                                            }
                                                        ?>
                                                        </select>
                                                    </div>                                                
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Enable Stock Sync</label>
                                                    <div class="class="form-control"">
                                                    <input type="checkbox" name="enableStockSync" value="1" <?php echo ($data['globalConfig']['enableStockSync'])?('checked="checked"'):('');?> class="switchbtn"  /> </div> 
                                                </div>
                                                <div class="row enableStockSync">
                                                    <div class="form-group col-md-5">
                                                        <label for="fetchStockSync">Fetch Stock Sync From</label>
                                                        <select name="fetchStockSync" class="form-control" value="<?php echo $data['globalConfig']['fetchStockSync']; ?>">
                                                        <?php
                                                            foreach ($librariesNames as $key => $librariesName) {
                                                                echo '<option value="'.$key.'">'.$librariesName.'</option>';
                                                            }
                                                        ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-5">
                                                        <label for="postStockSync">Post Stock Sync To</label>
                                                        <select name="postStockSync" class="form-control" value="<?php echo $data['globalConfig']['postStockSync']; ?>">
                                                        <?php
                                                            foreach ($librariesNames as $key => $librariesName) {
                                                                echo '<option value="'.$key.'">'.$librariesName.'</option>';
                                                            }
                                                        ?>
                                                      </select>
                                                    </div> 
                                                    <div class="form-group">
                                                        <label class="control-label">Enable Preorder</label>
                                                        <div class="class="form-control"">
                                                        <input type="checkbox" name="enblePreorder" value="1" <?php echo ($data['globalConfig']['enblePreorder'])?('checked="checked"'):('');?> class="switchbtn"  /> </div> 
                                                    </div>                                               
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Enable Inventory Advice</label>
                                                    <div class="class="form-control"">
                                                    <input type="checkbox" name="enableInventoryadvice" value="1" <?php echo ($data['globalConfig']['enableInventoryadvice'])?('checked="checked"'):('');?> class="switchbtn"  /> </div> 
                                                </div>
                                                <div class="row enableInventoryadvice">
                                                    <div class="form-group col-md-5">
                                                        <label for="fetchInventoradvice">Fetch Inventory Advice From</label>
                                                        <select name="fetchInventoradvice" class="form-control" value="<?php echo $data['globalConfig']['fetchInventoradvice']; ?>">
                                                        <?php
                                                            foreach ($librariesNames as $key => $librariesName) {
                                                                echo '<option value="'.$key.'">'.$librariesName.'</option>';
                                                            }
                                                        ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-5">
                                                        <label for="postInventoradvice">Post Inventory Advice To</label>
                                                        <select name="postInventoradvice" class="form-control" value="<?php echo $data['globalConfig']['postInventoradvice']; ?>">
                                                        <?php
                                                            foreach ($librariesNames as $key => $librariesName) {
                                                                echo '<option value="'.$key.'">'.$librariesName.'</option>';
                                                            }
                                                        ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="margin-top-10">
                                                    <a href="javascript:;" class="btn green savefrombtn">  Save Changes </span> <img style="display:none;width: 30px" src="<?php echo $this->config->item('script_url').'assets/layouts/layout/img/ajax-loader-1.gif';?>"> </a>
                                                    <a href="javascript:;" class="btn default"> Cancel </a>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<link href="<?php echo $this->config->item('script_url');?>assets/pages/css/profile.css" rel="stylesheet" type="text/css" />
<script src="<?php echo $this->config->item('script_url');?>assets/pages/scripts/profile.js" type="text/javascript"></script>