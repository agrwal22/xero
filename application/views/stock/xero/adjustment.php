<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="index.html">Home</a><i class="fa fa-circle"></i></li>
				<li><span>Inventory Advice Details</span></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-shopping-cart"></i>Inventory Advice Listing </div>
						<div class="actions">
							<a href="<?php echo base_url('stock/adjustment/fetchStockAdjustment');?>" class="btn btn-circle btn-info btnactionsubmit">
								<i class="fa fa-download"></i>
								<span class="hidden-xs">Fetch Inventory Advice </span>
							</a>
							<a href="<?php echo base_url('stock/adjustment/postStockAdjustment');?>" class="btn btn-circle green-meadow btnactionsubmit hide">
								<i class="fa fa-upload"></i>
								<span class="hidden-xs"> Post Inventory Advice </span>
							</a>						
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<div class="table-actions-wrapper">
								<span></span>
								<select class="table-group-action-input form-control input-inline input-small input-sm">
									<option value="">Select...</option>
									<option value="0">Pending</option>
									<option value="1">Sent</option>								
								</select>
								<button class="btn btn-sm btn-success table-group-action-submit"><i class="fa fa-check"></i> Submit</button>
							</div>
							<table class="table table-striped table-bordered table-hover table-checkable" id="datatable_products">
								<thead>
									<tr role="row" class="heading">
										<th width="1%"><input type="checkbox" class="group-checkable" /></th>
										<th width="10%"><?php echo $this->globalConfig['account1Name'];?>&nbsp;Account</th>
										<th width="5%"><?php echo $this->globalConfig['account2Name'];?>&nbsp;Account</th>
										<th width="9%">GoodsMovementId</th>
										<th width="8%">StockTransferId</th>									
										<th width="8%">WarehouseId</th>
										<th width="8%">ProductId</th>
										<th width="10%">Quantity updated</th>
										<th width="6%">Price</th>
										<th width="9%">Type </th>
										<th width="10%">Created</th>
										<th width="6%">Status</th>
										<th width="10%">Actions</th>
									</tr>
									<tr role="row" class="filter">
										<td></td>
										<td></td>
										<td></td>
										<td><input type="text" class="form-control form-filter input-sm" name="orderId" /></td>
										<td><input type="text" class="form-control form-filter input-sm" name="ActivityId" /></td>
										<td><input type="text" class="form-control form-filter input-sm" name="warehouseId" /></td>
										<td><input type="text" class="form-control form-filter input-sm" name="productId" /></td>
										<td><input type="text" class="form-control form-filter input-sm" name="qty" /></td>
										<td><input type="text" class="form-control form-filter input-sm" name="price" /></td>
										<td><input type="text" class="form-control form-filter input-sm" name="GoodNotetype" /></td>
										<td>
											<div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">	
												<input type="text" class="form-control form-filter input-sm" readonly name="updated_from" placeholder="From" />
												<span class="input-group-btn"><button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button></span>
											</div>
											<div class="input-group date date-picker" data-date-format="yyyy-mm-dd">	
												<input type="text" class="form-control form-filter input-sm" readonly name="updated_to " placeholder="To" />	
												<span class="input-group-btn"><button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button></span>
											</div>
										</td>
										<td>
											<select name="status" class="form-control form-filter input-sm">
												<option value="">Select...</option>	
												<option value="0">Pending</option>	
												<option value="1">Sent</option>	
												<option value="2">Updated</option>	
												<option value="3">Error</option>	
												<option value="4">Archive</option>
											</select>
										</td>
										<td>
											<div class="margin-bottom-5">	
												<button class="btn btn-sm btn-success filter-submit margin-bottom">		<i class="fa fa-search"></i> Search</button>
											</div>
											<button class="btn btn-sm btn-default filter-cancel">	<i class="fa fa-times"></i> Reset</button>
										</td>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>	
<?php 
	if(@!$this->session->userdata($this->router->directory.$this->router->class)[0]){
		echo '<script type="text/javascript"> jsOrder =  [ 1, "desc" ];</script>';
	}
?>
<script>
	loadUrl	=  '<?php echo base_url('stock/adjustment/getAdjustment');?>';
</script>