<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="index.html">Home</a><i class="fa fa-circle"></i> </li>
				<li><span>Stock Adjustment Details</span> </li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet ">		
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-shopping-cart"></i><?php echo ucwords($this->globalConfig['fetchSalesOrder']);?> Stock Adjustment Data</div
						</div>		
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<?php
								echo "<pre>";
								print_r(json_decode($salesInfo['rowData'],true));
								echo "</pre>";
							?>
						</div>
					</div>
				</div>
				<div class="portlet ">		
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-shopping-cart"></i><?php echo ucwords($this->globalConfig['postSalesOrder']);?> Stock Adjustment Data</div
						</div>		
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<?php
								echo "<pre>";
								print_r(json_decode($salesInfo['createdRowData'],true));
								echo "</pre>";
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>