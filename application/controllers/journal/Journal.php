<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Journal extends MY_Controller {
	function __construct(){
		parent::__construct();			
		$this->load->model('journal/journal_model','',TRUE);
	}
	public function index(){
		$data	= array();
		$this->template->load_template("journal/journal",$data);
	}
	public function getJournal(){
		$records	= $this->journal_model->getJournal();
		echo json_encode($records);
	}
	public function fetchJournal($sku = ''){
		$this->journal_model->fetchJournal($sku);
	}
	public function postJournal($sku = ''){
		$this->journal_model->postJournal($sku);
	}
	public function journalinfo($orderId = ''){
		$data['journalinfo']	= $this->db->get_where('amazon_ledger',array('orderId' => $orderId))->row_array();
		$this->template->load_template("journal/journalinfo",$data,$this->session_data);
	}
}