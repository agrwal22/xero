<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Globallog extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$model	= $this->load->model('logs/globallog_model');	
		$this->load->library('mypagination');
	} 
	public function index(){
		$data			= array();	
		$baseurl		= base_url() . "globallog/index/";
		$config			= $this->mypagination->getPaginationConfig($baseurl);
		$total_row		= $this->globallog_model->record_count();
		$config["total_rows"]	= $total_row;	
		$this->mypagination->initialize($config);
		$data			= array();
		$data["data"]	= $this->globallog_model->getBpLogDetails($config["per_page"], $config["page"]);
		$str_links		= $this->mypagination->create_links();
		$data["links"]	= explode('&nbsp;',$str_links ); 
		$this->template->load_template("logs/globallog",$data,$this->session_data);			
	}
	public function getLogDetails($logId){ 
		$data['loddetails']	= $this->globallog_model->getLogDetails($logId);
		$this->template->load_template("logs/details",$data,$this->session_data); 
	}
}