<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Bppagesetup extends MY_Controller {
	public function __construct(){
		parent::__construct();
	}
	public function index(){
		$data	= array();
		$this->template->load_template("bppagesetup/",array("data"=>$data));		
	}
	public function save(){
		$data	= $this->input->post('data');		
		$res	= $this->shipping_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->shipping_model->delete($id);
		}
	}
}
?>