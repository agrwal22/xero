<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Webhooks extends CI_Controller {
	public $file_path;
	public function __construct(){
		parent::__construct();
	}
	public function refreshToken($id = ''){
		$this->xero->refreshToken($id);
	}
	
	/* automation for NLG Xero	- bridge23 */
	public function  processXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();		
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
	}
	
	/* automation for S&G Xero	- bridge35 */
	public function  processSandG(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();		
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
	}
	
	/* automation for InventoryControl Xero	- bridge23 */
	public function  processICM(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();		
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
	}
	
	/* automation for GoodStartPackaging Xero	- bridge25 */
	public function  processGoodstartpackaging(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();		
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
	}
	
	/* automation for GBICS Xero	- bridge39 */
	public function  processGbics(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();		
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
	}
	
	/* automation for GrandPrixRaceWear Xero	- bridge25 */
	public function  processGrandprix(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();		
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		/* $this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment(); */
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
	}
	
	/* automation for GreenWayHerbalProductsLtd Xero	- bridge44 */
	public function  processGreenway(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();		
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		/* $this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment(); */
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
	}
	
	/* automation for Arte-N Xero	- bridge25 */
	public function  processArten(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();		
		/* $this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase(); */
		/* $this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment(); */
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		/* $this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();	 */	
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
	}
	
	/* automation for Innovation1st Xero	- bridge45 */
	public function  processInnovation1st(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();		
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
	}
	
	/* automation for CabbagesAndRoses Xero	- bridge46 */
	public function  processCabbagesAndRosesXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();		
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
	}
	
	public function processSync(){
		$this->load->model('stock/sync_model','',TRUE);
		$this->sync_model->fetchSync();
		$this->sync_model->postSync();
	}
	public function runTask($taskName = ''){
		if($taskName){
			$isRunning = $this->getRunningTask($taskName);
			if(!$isRunning){
				$isSellRUn = shell_exec('/opt/plesk/php/7.0/bin/php /var/www/vhosts/bsitc-bridge23.com/httpdocs/nlg/index.php webhooks '.$taskName);
			}
		}
		die();
	}
	public function runTaskIcm($taskName = ''){
		if($taskName){
			$isRunning = $this->getRunningTask($taskName);
			if(!$isRunning){
				$isSellRUn = shell_exec('/opt/plesk/php/7.0/bin/php /var/www/vhosts/bsitc-bridge23.com/httpdocs/inventorycontrol/index.php webhooks '.$taskName);
			}
		}
		die();
	}
	public function runTaskPlanks($taskName = ''){
		if($taskName){
			$isRunning = $this->getRunningTask($taskName);
			if(!$isRunning){
				$isSellRUn = shell_exec('/opt/plesk/php/7.0/bin/php /var/www/vhosts/bsitc-bridge35.com/httpdocs/planksxero/index.php webhooks '.$taskName);
			}
		}
		die();
	}
	public function runTaskSandg($taskName = ''){
		if($taskName){
			$isRunning = $this->getRunningTask($taskName);
			if(!$isRunning){
				$isSellRUn = shell_exec('/opt/plesk/php/7.0/bin/php /var/www/vhosts/bsitc-bridge35.com/httpdocs/sandgxero/index.php webhooks '.$taskName);
			}
		}
		die();
	}	
	public function runTaskGsp($taskName = ''){
		if($taskName){
			$isRunning = $this->getRunningTask($taskName);
			if(!$isRunning){
				$isSellRUn = shell_exec('/opt/plesk/php/7.0/bin/php /var/www/vhosts/bsitc-bridge25.com/httpdocs/goodstartpackaging/index.php webhooks '.$taskName);
			}
		}
		die();
	}
	public function runTaskGbics($taskName = ''){
		if($taskName){
			$isRunning = $this->getRunningTask($taskName);
			if(!$isRunning){
				$isSellRUn = shell_exec('/opt/plesk/php/7.0/bin/php /var/www/vhosts/bsitc-bridge39.com/httpdocs/gbics/index.php webhooks '.$taskName);
			}
		}
		die();
	}
	public function runTaskGrandprix($taskName = ''){
		if($taskName){
			$isRunning = $this->getRunningTask($taskName);
			if(!$isRunning){
				$isSellRUn = shell_exec('/opt/plesk/php/7.0/bin/php /var/www/vhosts/bsitc-bridge25.com/httpdocs/grandprix/index.php webhooks '.$taskName);
			}
		}
		die();
	}
	public function runTaskInnovation1st($taskName = ''){
		if($taskName){
			$isRunning = $this->getRunningTask($taskName);
			if(!$isRunning){
				$isSellRUn = shell_exec('/opt/plesk/php/7.1/bin/php /var/www/vhosts/bsitc-bridge45.com/httpdocs/innovation1st/index.php webhooks '.$taskName);
			}
		}
		die();
	}
	public function runTaskCabbagesAndRosesXero($taskName = ''){
		if($taskName){
			$isRunning = $this->getRunningTask($taskName);
			if(!$isRunning){
				$isSellRUn = shell_exec('/opt/plesk/php/7.0/bin/php /var/www/vhosts/bsitc-bridge46.com/httpdocs/cabbagesandrosesxero/index.php webhooks '.$taskName);
			}
		}
		die();
	}
	
	public function getRunningTask($checkTask = 'processNlg'){
		/* die(); */ 
		$return = false;
		$checkTask = trim(strtolower($checkTask));
		if($checkTask){
			exec('ps aux | grep php', $outputs);
			foreach($outputs as $output){
				$output = strtolower($output);
				if(substr_count($output,$checkTask)){
					if(!substr_count($output,'runtask')){ 
						$return = true;
					}
				}
			}
		}
		return $return;
	}
	public function closeRunner(){
		date_default_timezone_set('GMT');
		$cpid = posix_getpid(); 
		exec('ps aux | grep php', $outputs);
		$currentTime = gmdate('YmdHis',strtotime('-300 min')); 
		foreach($outputs as $output){
			$ps = preg_split('/ +/', $output);
			$pid = $ps[1];				
			$cronStartTimeTimeStamp = strtotime($ps['8']);
			if(substr_count($output,'runTask/')){
				shell_exec("kill $pid");	
			}
			else if(substr_count($output,'/index.php')){
				if(gmdate('Y',$cronStartTimeTimeStamp) == gmdate('Y')){
					$cronStartTime = date('YmdHis',$cronStartTimeTimeStamp);					
					if($cronStartTime < $currentTime){ 
						shell_exec("kill $pid");
					}
				}
			}
		}
	}
	public function salesPaymnetStatus(){
		$orderIdsDatas = $this->db->select('orderId')->get_where('sales_order',array('isPaymentCreated' => '1'))->result_array();
		$orderIds = array_column($orderIdsDatas,'orderId');		
		if($orderIds){
			$this->brightpearl->reInitialize($accountId);
			$returns = array();
			foreach ($this->brightpearl->accountDetails as $account1Id => $accountDetails) {
				$notPaid = array();
				$orderDatas = $this->brightpearl->getResultById($orderIds,'/order-service/order/',$account1Id,200,0);	
				foreach($orderDatas as $orderData){
					if($orderData['orderPaymentStatus'] != 'PAID'){
						$notPaid[] = $orderData['id'];
					}
				}
			}
			if($notPaid){
				$notPaid = array_filter($notPaid);
				$this->db->where_in('orderId',$notPaid)->update('sales_order',array('isPaymentCreated' => '0'));
			}
		}
	}
	
	
}