<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');
class PaymentCloser extends CI_Controller {
	public $file_path;
	public function __construct(){
		parent::__construct();
	}
	public function closePayment(){
		$paymentDataTemp	= array();
		$peakPaymentDate	= date('Y-m-d H:i:s',strtotime('-180 days'));
		$allpayments		= $this->db->select('orderId,createOrderId,paymentDetails,created,account1Id,account2Id')->where("date(`created`) > ","date('".$peakPaymentDate."')",false)->get_where('sales_order',array('createOrderId <>' => '', 'paymentDetails <>' => ''))->result_array();
		$this->xero->reInitialize();
		$this->brightpearl->reInitialize();
		foreach($allpayments as $allpaymentsdata){
			$xeroID			= $allpaymentsdata['createOrderId'];
			$BrightpearlID	= $allpaymentsdata['orderId'];
			$account1Id		= $allpaymentsdata['account1Id'];
			$account2Id		= $allpaymentsdata['account2Id'];
			$paymentDetails	= json_decode($allpaymentsdata['paymentDetails'],true);
			if(!$paymentDetails){
				continue;
			}
			$checkOrderStatus	= 0;
			foreach(@$paymentDetails as $pkey => $paymentDetail){
				if(($paymentDetail['status'] == '0') AND ($paymentDetail['amount'] > '0')){
					$checkOrderStatus	= 1;
					break;
				}
			}
			if($checkOrderStatus){
				if($allpaymentsdata['lastpaymentcheck']){
					if($allpaymentsdata['lastpaymentcheck'] < date('Ymd',strtotime('-5 days'))){
						continue;		
					}
				}
				$Xerourl	= '2.0/Invoices/'.$xeroID;
				$this->xero->initializeConfig($account2Id, 'GET', $Xerourl);
				$XeroResult	= $this->xero->getCurl($Xerourl, "GET", '', 'json', $account2Id)[$account2Id];
				if($XeroResult['Invoices']['Invoice']['InvoiceID']){
					if(abs($XeroResult['Invoices']['Invoice']['AmountDue']) > 0){
						continue;
					}
					else{
						$BPurl		= '/order-service/order/'.$BrightpearlID;
						$BPresponse	= $this->brightpearl->getCurl($BPurl, "GET", '', 'json', $account1Id)[$account1Id];
						if($BPresponse[0]['orderPaymentStatus'] == 'PAID'){
							foreach($paymentDetails as $pkey => $paymentDetail){
								$paymentDetails[$pkey]['status']	= 1;
								if($paymentDetail['amount'] == 0){
									$paymentDetails[$pkey]['amount']	= 1;
								}
							}
							$updateArray['paymentDetails']		= json_encode($paymentDetails);
							$updateArray['isPaymentCreated']	= 1;
							$updateArray['status']				= 3;
							$updateArray['lastpaymentcheck']	= date('Ymd');
							$this->db->where(array('orderId' => $BrightpearlID))->update('sales_order',$updateArray);
						}
						else{
							continue;
						}
					}
				}
			}
		}
	}
}