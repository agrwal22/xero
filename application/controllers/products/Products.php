<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Products extends MY_Controller{
	function __construct(){
		parent::__construct();			
		$this->load->model('products/products_model','',TRUE);
	}
	public function index(){
		$data	= array();
		$this->template->load_template("products/products",$data);
	}
	public function getProduct(){
		$records	= $this->products_model->getProduct();
		echo json_encode($records);
	}
	public function fetchProducts($productId = '',$fetchedFromBp = '1',$fetchedFromVend = '0'){ 
		$this->products_model->fetchProducts($productId,$fetchedFromBp,$fetchedFromVend);  
	}
	public function postProducts($productId = ''){
		$this->products_model->postProducts($productId);
	}
	public function productInfo($productId = ''){
		$data['productInfo']	= $this->db->get_where('products', array('productId' => $productId))->row_array();
		$this->template->load_template("products/productInfo",$data);
	}
	public function loadProduct(){
		$this->template->load_template("products/loadProduct");  
	}	
	public function getLoadProduct(){
		$records	= $this->products_model->getLoadProduct();
		echo json_encode($records);
	}
	public function postLoadedProduct(){
		$records	= $this->products_model->postLoadedProduct();
		$this->getLoadProduct();
	}
	public function clearProducts(){
		$this->db->where('status < ',2)->delete('product_load');
		$this->getLoadProduct();
	}
	public function getVarient(){
		$newSku	= $this->input->post('newSku');
		$color	= $this->input->post('color');
		$datas	= $this->db->select('sku,name,ean,upc,productId,color,size')->get_where('products',array('newSku' => $newSku,'color' => $color))->result_array();
		$str	= '<table class ="table" ><thead> <tr><th>Product Id</th><th>Name</th><th>SKU</th><th>Color</th><th>Size</th></tr></thead><tbody>';
		foreach($datas as $data){
			$str	.=	'<tr><td>'.$data['productId'].'</td><td>'.$data['name'].'</td><td>'.$data['sku'].'</td><td>'.$data['color'].'</td><td>'.$data['size'].'</td></tr>';
		}
		$str	.=	'</tbody></table>';
		echo $str;		
	}
	public function uploadProduct(){
		$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postProduct'].'_account')->result_array();
		$account2Id				= @$_REQUEST['account2Id'];
		$account1Id				= @$_REQUEST['account1Id'];
        error_reporting('0');
		$batchUpdate	= array();
        if ($_FILES['uploadprefile']['tmp_name']) {
            $file	= fopen($_FILES['uploadprefile']['tmp_name'], "r");
            $count	= 0;
            while (!feof($file)) {
				$count++;
                $row	= fgetcsv($file);               
				if($count <= 1){
					continue;
				} 
                if($row['1']){
					$batchUpdate[strtolower($row['0'])]	= array(
						'sku' 			=> $row['0'],
						'qty' 			=> $row['1'],
						'unitPrice' 	=> $row['2'],
						'availableDate'	=> $row['3'],
						'accountCode'   => $row['4'],
						'account1Id' 	=> $account1Id,
						'account2Id' 	=> $account2Id,
						'status' 		=> '0',
					);
                }
            }
            fclose($file);
        }
		if($batchUpdate){
			$skus	= array_keys($batchUpdate);
			$skus	= array_filter($skus);
			sort($skus);
			if($skus){
				$this->db->where_in('sku',$skus)->delete('product_load');
			}
			$batchUpdates	= array_chunk($batchUpdate,200);
			foreach($batchUpdates as $batchUpdate){
				if($batchUpdate['0']){
					$this->db->insert_batch('product_load',$batchUpdate);
				}
			}
		}
        redirect($_SERVER['HTTP_REFERER'] , 'refresh');
    }
}