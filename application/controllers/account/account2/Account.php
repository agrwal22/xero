<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Account extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->library('form_validation');	
		$this->globalConfig['account2Liberary'];	
		$this->load->model('account/'. $this->globalConfig['account2Liberary'] .'/account_model');	
	}
	public function index(){
		$data			= array();
		$data			= $this->account_model->get();
		$data['type']	= 'account2';
		$this->template->load_template("account/". $this->globalConfig['account2Liberary'] ."/account",array("data"=>$data));		
	}
	public function save(){
		$data	= $this->input->post('data');		
		$res	= $this->account_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->account_model->delete($id);
		}
	}
}
?>