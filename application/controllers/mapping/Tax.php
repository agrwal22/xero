<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Tax extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/tax_model');	
	}
	public function index(){
		$data	= array();
		$data	= $this->tax_model->get();
		$data['account1TaxId']	= $this->{$this->globalConfig['account1Liberary']}->getAllTax();
		$data['account2TaxId']	= $this->{$this->globalConfig['account2Liberary']}->getAllTax();
		$this->template->load_template("mapping/tax",array("data"=>$data));		
	}
	public function save(){
		$data	= $this->input->post('data');		
		$res	= $this->tax_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->tax_model->delete($id);
		}
	}
}
?>