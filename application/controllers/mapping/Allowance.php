<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Allowance extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/allowance_model');	
	}
	public function index(){
		$data	= array();
		$data	= $this->allowance_model->get();
		$data['account1AllowanceId']	= $this->{$this->globalConfig['account1Liberary']}->getAllAllowance();
		$data['account2AllowanceId']	= $this->{$this->globalConfig['account2Liberary']}->getAllAllowance();
		$data['tradingPartnerId']		= $this->{$this->globalConfig['account2Liberary']}->getTradingPartnerId();
		$this->template->load_template("mapping/allowance",array("data"=>$data));		
	}
	public function save(){
		$data	= $this->input->post('data');		
		$res	= $this->allowance_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->allowance_model->delete($id);
		}
	}
}
?>