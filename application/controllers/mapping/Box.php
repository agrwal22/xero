<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Box extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/box_model');	
	}
	public function index(){
		$data	= array();
		$data	= $this->box_model->get();
		$this->template->load_template("mapping/box",array("data"=>$data));		
	}
	public function save(){
		$data	= $this->input->post('data');		
		$res	= $this->box_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->box_model->delete($id);
		}
	}
}
?>