<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Trading extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/trading_model');	
	}
	public function index(){
		$data	= array();
		$data	= $this->trading_model->get();
		$data['account1TradingId']	= $this->{$this->globalConfig['account1Liberary']}->getAllAllowance();
		$data['account2TradingId']	= $this->{$this->globalConfig['account2Liberary']}->getAllAllowance();
		$data['pricelist']			= $this->{$this->globalConfig['account1Liberary']}->getAllPriceList();
		$data['channel']			= $this->{$this->globalConfig['account1Liberary']}->getAllChannel();
		$data['shippingmethods']	= $this->{$this->globalConfig['account1Liberary']}->getAllShippingMethod();
		$data['tax']				= $this->{$this->globalConfig['account1Liberary']}->getAllTax();
		$this->template->load_template("mapping/trading",array("data"=>$data));		
	}
	public function save(){
		$data	= $this->input->post('data');		
		$res	= $this->trading_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->trading_model->delete($id);
		}
	}
}
?>