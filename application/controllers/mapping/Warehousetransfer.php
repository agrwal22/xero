<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Warehousetransfer extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/warehousetransfer_model');	
	}
	public function index(){
		$data	= array();
		$data	= $this->warehousetransfer_model->get();
		$data['account1FromWarehouseId']	= $this->{$this->globalConfig['account1Liberary']}->getAllLocation();
		$data['account2FromWarehouseId']	= $this->{$this->globalConfig['account2Liberary']}->getAllLocation();
		$this->template->load_template("mapping/warehousetransfer",array("data"=>$data));		
	}
	public function save(){
		$data	= $this->input->post('data');		
		$res	= $this->warehousetransfer_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->warehousetransfer_model->delete($id);
		}
	}
}
?>