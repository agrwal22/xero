<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Payment extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/payment_model');	
	}
	public function index(){
		$data	= array();
		$data	= $this->payment_model->get();
		$data['account1PaymentId']	= $this->{$this->globalConfig['account1Liberary']}->getAllPaymentMethod();
		$data['account2PaymentId']	= $this->{$this->globalConfig['account2Liberary']}->getAllPaymentMethod();
		$this->template->load_template("mapping/payment",array("data"=>$data));		
	}
	public function save(){
		$data	= $this->input->post('data');		
		$res	= $this->payment_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->payment_model->delete($id);
		}
	}
}
?>