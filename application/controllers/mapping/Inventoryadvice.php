<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Inventoryadvice extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/inventoryadvice_model');	
	}
	public function index(){
		$data = array();
		$data = $this->inventoryadvice_model->get();
		$data['warehouseId']   = $this->{$this->globalConfig['account1Liberary']}->getAllLocation();
		$data['pricelist']   = $this->{$this->globalConfig['account1Liberary']}->getAllPriceList();
		$this->template->load_template("mapping/inventoryadvice",array("data"=>$data));		
	}
	public function save(){
		$data = $this->input->post('data');		
		$res = $this->inventoryadvice_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->inventoryadvice_model->delete($id);
		}
	}
}
?>