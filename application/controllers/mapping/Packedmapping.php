<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Packedmapping extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/packedmapping_model');	
	}
	public function index(){
		$data	= array();
		$data	= $this->packedmapping_model->get();
		$data['warehouseId']	= $this->{$this->globalConfig['account1Liberary']}->getAllLocation();
		$data['box']			= $this->db->get('mapping_box')->result_array();
		$this->template->load_template("mapping/packedmapping",array("data"=>$data));		
	}
	public function save(){
		$data	= $this->input->post('data');		
		$res	= $this->packedmapping_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->packedmapping_model->delete($id);
		}
	}
}
?>