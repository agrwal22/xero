<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pricelist extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/pricelist_model');	
	}
	public function index(){
		$data	= array();
		$data	= $this->pricelist_model->get();
		$data['account1PricelistId']	= $this->{$this->globalConfig['account1Liberary']}->getAllPriceList();
		$data['account2PricelistId']	= $this->{$this->globalConfig['account2Liberary']}->getAllPriceList();
		$this->template->load_template("mapping/pricelist",array("data"=>$data));		
	}
	public function save(){
		$data	= $this->input->post('data');		
		$res	= $this->pricelist_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->pricelist_model->delete($id);
		}
	}
}
?>