<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Channel extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/channel_model');	
	}
	public function index(){
		$data	= array();
		$data	= $this->channel_model->get();
		$data['account1ChannelId']		= $this->{$this->globalConfig['account1Liberary']}->getAllChannelMethod();
		$data['account1WarehouseId']	= $this->{$this->globalConfig['account1Liberary']}->getAllLocation();
		$data['account2ChannelId']		= $this->{$this->globalConfig['account2Liberary']}->getAllChannelMethod();
		$this->template->load_template("mapping/channel",array("data"=>$data));		
	}
	public function save(){
		$data	= $this->input->post('data');		
		$res	= $this->channel_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->channel_model->delete($id);
		}
	}
}
?>