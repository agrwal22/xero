<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Warehousestock extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/warehousestock_model');	
	}
	public function index(){
		$data	= array();
		$data	= $this->warehousestock_model->get();
		$data['account1WarehouseId']	= $this->{$this->globalConfig['account1Liberary']}->getAllLocation();
		$data['account2WarehouseId']	= $this->{$this->globalConfig['account2Liberary']}->getAllLocation();
		$this->template->load_template("mapping/warehousestock",array("data"=>$data));		
	}
	public function save(){
		$data	= $this->input->post('data');		
		$res	= $this->warehousestock_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->warehousestock_model->delete($id);
		}
	}
}
?>