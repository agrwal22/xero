<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Category extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/category_model');	
	}
	public function index(){
		$data	= array();
		$data	= $this->category_model->get();
		$data['account1CategoryId']	= $this->{$this->globalConfig['account1Liberary']}->getAllCategoryMethod();
		$data['account2Department']	= $this->{$this->globalConfig['account2Liberary']}->getAllCategoryMethod();
		$this->template->load_template("mapping/category",array("data"=>$data));		
	}
	public function save(){
		$data	= $this->input->post('data');		
		$res	= $this->category_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->category_model->delete($id);
		}
	}
	public function autoSave($data = ''){
		$res	= $this->category_model->autoSave($data);
	}
}
?>