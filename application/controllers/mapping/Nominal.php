<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Nominal extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/nominal_model');	
	}
	public function index(){
		$data	= array();
		$data	= $this->nominal_model->get();
		$data['account1NominalId']	= $this->{$this->globalConfig['account1Liberary']}->nominalCode();
		$data['account1ChannelId']	= $this->{$this->globalConfig['account1Liberary']}->getAllChannelMethod();
		$data['account2NominalId']	= $this->{$this->globalConfig['account2Liberary']}->getAccountDetails();
		$this->template->load_template("mapping/nominal",array("data"=>$data));		
	}
	public function save(){
		$data	= $this->input->post('data');		
		$res	= $this->nominal_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->nominal_model->delete($id);
		}
	}
}
?>