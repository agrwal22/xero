<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Genericcustomer extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/genericcustomer_model');	
	}
	public function index(){
		$data	= array();
		$data	= $this->genericcustomer_model->get();
		$data['account1ChannelId']	= $this->{$this->globalConfig['account1Liberary']}->getAllChannelMethod();
		$data['account2ChannelId']	= array();
		$this->template->load_template("mapping/genericcustomer",array("data"=>$data));		
	}
	public function save(){
		$data	= $this->input->post('data');		
		$res	= $this->genericcustomer_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->genericcustomer_model->delete($id);
		}
	}
}
?>