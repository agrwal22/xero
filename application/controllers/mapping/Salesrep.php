<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Salesrep extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/salesrep_model');	
	}
	public function index(){
		$data	= array();
		$data	= $this->salesrep_model->get();
		$data['account1SalesrepId']	= $this->{$this->globalConfig['account1Liberary']}->getAllSalesrep();
		$data['account2SalesrepId']	= $this->{$this->globalConfig['account2Liberary']}->getAllSalesrep();
		$this->template->load_template("mapping/salesrep",array("data"=>$data));		
	}
	public function save(){
		$data	= $this->input->post('data');		
		$res	= $this->salesrep_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->salesrep_model->delete($id);
		}
	}
}
?>