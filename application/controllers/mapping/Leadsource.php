<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Leadsource extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/leadsource_model');	
	}
	public function index(){
		$data	= array();
		$data	= $this->leadsource_model->get();
		$data['account1LeadsourceId']	= $this->{$this->globalConfig['account1Liberary']}->getAllLeadsource();
		$data['account2RegistersId']	= $this->{$this->globalConfig['account2Liberary']}->getAllRegisters();
		$data['account2LeadsourceId']	= $this->{$this->globalConfig['account2Liberary']}->getAllLocation();
		$this->template->load_template("mapping/leadsource",array("data"=>$data));		
	}
	public function save(){
		$data	= $this->input->post('data');		
		$res	= $this->leadsource_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->leadsource_model->delete($id);
		}
	}
}
?>