<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Brand extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/brand_model');	
	}
	public function index(){
		$data	= array();
		$data	= $this->brand_model->get();
		$data['account1BrandId']	= $this->{$this->globalConfig['account1Liberary']}->getAllBrand();
		$data['account2BrandId']	= $this->{$this->globalConfig['account2Liberary']}->getAllBrand();
		$data['account2SupplierId'] = $this->{$this->globalConfig['account2Liberary']}->getAllSupplier();
		$this->template->load_template("mapping/brand",array("data"=>$data));		
	}
	public function save(){
		$data	= $this->input->post('data');		
		$res	= $this->brand_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->brand_model->delete($id);
		}
	}
}
?>