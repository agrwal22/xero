<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Paymentpurchase extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/paymentpurchase_model');	
	}
	public function index(){
		$data	= array();
		$data	= $this->paymentpurchase_model->get();
		$data['account1PaymentId']	= $this->{$this->globalConfig['account1Liberary']}->getAllPaymentMethod();
		$data['account2PaymentId']	= $this->{$this->globalConfig['account2Liberary']}->getAccountDetails();
		$this->template->load_template("mapping/paymentpurchase",array("data"=>$data));		
	}
	public function save(){
		$data	= $this->input->post('data');		
		$res	= $this->paymentpurchase_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->paymentpurchase_model->delete($id);
		}
	}
}
?>