<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Taximacro extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/Taximacro_model');	
	}
	public function index(){
		$data	= array();
		$data	= $this->Taximacro_model->get();
		$data['account1TaxId']	= $this->{$this->globalConfig['account1Liberary']}->getAllTax();
		$data['account2TaxId']	= $this->{$this->globalConfig['account2Liberary']}->getAllTax();
		$this->template->load_template("mapping/taximacro",array("data"=>$data));		
	}
	public function save(){
		$data	= $this->input->post('data');		
		$res	= $this->Taximacro_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->Taximacro_model->delete($id);
		}
	}
}
?>