<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Credit extends MY_Controller {
	function __construct(){
		parent::__construct();			
		$this->load->model('sales/credit_model','',TRUE);
	}
	public function index(){
		$data = array();
		$this->template->load_template("sales/credit",$data,$this->session_data);
	}
	public function getCredit(){ 
		$records = $this->credit_model->getCredit();
		echo json_encode($records);
	}
	public function fetchSalesCredit($orderId = ''){
		$this->credit_model->fetchSalesCredit($orderId);
	}
	public function fetchSalesCreditConfirmation($orderId = ''){
		$this->credit_model->fetchSalesCreditConfirmation($orderId);
	}
	public function postSalesCreditConfimation($orderId = ''){
		$this->credit_model->postSalesCreditConfimation($orderId);
	}
	public function postSalesCredit($orderId = ''){
		$this->credit_model->postSalesCredit($orderId);
	}
	public function creditInfo($orderId = ''){
		$data['salesInfo'] = $this->db->get_where('sales_credit_order',array('orderId' => $orderId))->row_array();
		$this->template->load_template("sales/creditInfo",$data,$this->session_data);
	}	
	public function creditItem($orderId){
		$data = array();
		$data['orderInfo'] = $this->db->get_where('sales_credit_order',array('orderId' => $orderId))->row_array();
		$data['address'] = $this->db->get_where('sales_credit_address',array('orderId' => $data['orderInfo']['orderId']))->result_array();
		$data['items'] = $this->credit_model->getCreditItem($orderId);
		if(!$data['address']){
			$data['address']['0'] = $this->account2Config[$data['orderInfo']['account2Id']];
			$data['address']['1'] = $this->account2Config[$data['orderInfo']['account2Id']];
			$data['address']['0']['type'] = 'ST';$data['address']['1']['type'] = 'BY';
		}
		$this->template->load_template("sales/creditItem",$data,@$this->session_data); 
	} 

	
}