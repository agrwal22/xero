<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sales extends MY_Controller {
	function __construct(){
		parent::__construct();			
		$this->load->model('sales/sales_model','',TRUE);
	}
	public function index(){
		$data = array();
		$this->template->load_template("sales/sales",$data,$this->session_data);
	}
	public function getSales(){
 		
		$records = $this->sales_model->getSales();
		echo json_encode($records);
	}
	public function fetchSales($orderId = ''){
		$this->sales_model->fetchSales($orderId);
	}	
	public function postSales($orderId = ''){
		$this->sales_model->postSales($orderId);
	}
	public function fetchAcknowledgement($orderId = ''){
		$this->sales_model->fetchAcknowledgement($orderId);
	}
	public function postAcknowledgement($orderId = ''){
		$this->sales_model->postAcknowledgement($orderId);
	}	
	public function salesInfo($orderId = ''){
		$data['salesInfo'] = $this->db->get_where('sales_order',array('orderId' => $orderId))->row_array();
		$this->template->load_template("sales/salesInfo",$data,$this->session_data);
	}	
	public function salesItem($orderId){
		$data = array();
		$data['orderInfo'] = $this->db->get_where('sales_order',array('orderId' => $orderId))->row_array();
		$data['address'] = $this->db->order_by('id','desc')->limit(2,0)->get_where('sales_address',array('orderId' => $orderId))->result_array();
		$data['items'] = $this->sales_model->getSalesItem($orderId);
		if(!$data['address']){
			$data['address']['0'] = $this->account2Config[$data['orderInfo']['account2Id']];
			$data['address']['1'] = $this->account2Config[$data['orderInfo']['account2Id']];
			$data['address']['0']['type'] = 'ST';$data['address']['1']['type'] = 'BY';
		}
		$this->template->load_template("sales/salesItem",$data,@$this->session_data); 
	}
}