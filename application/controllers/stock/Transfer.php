<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transfer extends MY_Controller {
	function __construct(){
		parent::__construct();			
		$this->load->model('stock/transfer_model','',TRUE);
	}
	public function index(){
		$data = array();
		$this->template->load_template("stock/transfer",$data,$this->session_data);
	}
	public function getTransfer(){
		$records = $this->transfer_model->getTransfer();
		echo json_encode($records);
	}
	public function fetchStockTransfer($orderId = ''){
		$this->transfer_model->fetchStockTransfer($orderId);
	}
	public function postStockTransfer($orderId = ''){
		$this->transfer_model->postStockTransfer($orderId);
	}
	public function transerInfo($orderId = ''){
		$data['transerInfo'] = $this->db->get_where('stock_transfer',array('orderId' => $orderId))->row_array();
		$this->template->load_template("stock/transferInfo",$data,$this->session_data);
	}
	
	public function transferItem($orderId){
		$data = array();
		$data['orderInfo'] = $this->db->get_where('stock_transfer',array('orderId' => $orderId))->row_array();
		$data['address'] = $this->db->get_where('customer_address',array('customerId' => $data['orderInfo']['customerId']))->row_array();
		$data['customerInfo'] = $this->db->get_where('customers',array('customerId' => $data['orderInfo']['customerId']))->row_array();
		$data['items'] = $this->transfer_model->getTransferItem($orderId);
		if(!$data['address']){
			$data['address']['0'] = $this->account2Config[$data['orderInfo']['account2Id']];
			$data['address']['1'] = $this->account2Config[$data['orderInfo']['account2Id']];
			$data['address']['0']['type'] = 'ST';$data['address']['1']['type'] = 'BY';
		}
		$this->template->load_template("stock/stockItem",$data,@$this->session_data); 
	} 

	
}