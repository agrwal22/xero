<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Inventoryadvice extends MY_Controller {
	function __construct(){
		parent::__construct();			
		$this->load->model('stock/inventoryadvice_model','',TRUE);
	}
	public function index(){
		$data = array();
		$this->template->load_template("stock/inventoryadvice",$data);
	}
	public function getInventoradvice(){
		$records = $this->inventoryadvice_model->getInventoradvice();
		echo json_encode($records);
	}
	public function fetchInventoradvice($sku = ''){
		$this->inventoryadvice_model->fetchInventoradvice($sku);
	}
	public function postInventoradvice($sku = ''){
		$this->inventoryadvice_model->postInventoradvice($sku);
	}
}
