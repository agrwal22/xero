<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Adjustment extends MY_Controller {
	function __construct(){
		parent::__construct();			
		$this->load->model('stock/adjustment_model','',TRUE);
	}
	public function index(){
		$data = array();
		$this->template->load_template("stock/adjustment",$data);
	}
	public function getAdjustment(){
		$records = $this->adjustment_model->getAdjustment();
		echo json_encode($records);
	}
	public function fetchStockAdjustment($sku = ''){
		$this->adjustment_model->fetchStockAdjustment($sku);
	}
	public function postStockAdjustment($sku = ''){
		$this->adjustment_model->postStockAdjustment($sku);
	}
	public function adjustmentInfo($recordId = ''){
		$data['salesInfo'] = $this->db->get_where('stock_adjustment',array('id' => $recordId))->row_array();
		$this->template->load_template("stock/adjustmentinfo",$data,$this->session_data);
	}
}
