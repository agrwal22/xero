<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Credit extends MY_Controller {
	function __construct(){
		parent::__construct();			
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
	}
	public function index(){
		$data = array();
		$this->template->load_template("purchase/credit",$data,$this->session_data);
	}
	public function getCredit(){ 
		$records = $this->purchasecredit_overridemodel->getCredit();
		echo json_encode($records);
	}
	public function fetchPurchaseCredit($orderId = ''){
		$this->purchasecredit_overridemodel->fetchPurchaseCredit($orderId);
	}
	public function postPurchaseCredit($orderId = ''){
		$this->purchasecredit_overridemodel->postPurchaseCredit($orderId);
	}	
	public function purchaseInfo($orderId = ''){
		$data['purchaseInfo'] = $this->db->get_where('purchase_credit_order',array('orderId' => $orderId))->row_array();
		$this->template->load_template("purchase/creditInfo",$data,$this->session_data);
	}	
	public function creditItem($orderId){
		$data = array();
		$data['orderInfo'] = $this->db->get_where('purchase_credit_order',array('orderId' => $orderId))->row_array();
		$data['address'] = $this->db->get_where('purchase_credit_address',array('orderId' => $orderId))->row_array();
		$data['customerInfo'] = $this->db->get_where('customers',array('email' => $data['orderInfo']['customerEmail']))->row_array();
		$data['receipt'] = $this->db->get_where('purchase_dispatch',array('orderId' => $orderId))->result_array();
		$data['items'] = $this->purchasecredit_overridemodel->getCreditItem($orderId);
		$this->template->load_template("purchase/creditItem",$data,@$this->session_data); 
	} 

	
}