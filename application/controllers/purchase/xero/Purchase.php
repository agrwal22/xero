<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Purchase extends MY_Controller {
	function __construct(){
		parent::__construct();			
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
	}
	public function index(){
		$data = array();
		$this->template->load_template("purchase/purchase",$data,$this->session_data);
	}
	public function getPurchase(){
		$records = $this->purchase_overridemodel->getPurchase();
		echo json_encode($records);
	}
	public function fetchPurchase($orderId = ''){
		$this->purchase_overridemodel->fetchPurchase($orderId);
	}
	public function postPurchase($orderId = ''){
		$this->purchase_overridemodel->postPurchase($orderId);
	}	
	public function purchaseInfo($orderId = ''){
		$data['purchaseInfo'] = $this->db->get_where('purchase_order',array('orderId' => $orderId))->row_array();
		$this->template->load_template("purchase/purchaseInfo",$data,$this->session_data);
	}	
	public function purchaseItem($orderId){
		$data = array();
		$data['orderInfo'] = $this->db->get_where('purchase_order',array('orderId' => $orderId))->row_array();
		$data['address'] = $this->db->get_where('purchase_address',array('orderId' => $orderId))->row_array();
		$data['customerInfo'] = $this->db->get_where('customers',array('email' => $data['orderInfo']['customerEmail']))->row_array();
		$data['receipt'] = $this->db->get_where('purchase_dispatch',array('orderId' => $orderId))->result_array();
		$data['items'] = $this->purchase_overridemodel->getPurchaseItem($orderId);
		$this->template->load_template("purchase/purchaseItem",$data,@$this->session_data); 
	} 

	
}