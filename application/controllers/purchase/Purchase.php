<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Purchase extends MY_Controller {
	function __construct(){
		parent::__construct();			
		$this->load->model('purchase/purchase_model','',TRUE);
	}
	public function index(){
		$data		= array();
		$this->template->load_template("purchase/purchase",$data,$this->session_data);
	}
	public function getPurchase(){
		$records	= $this->purchase_model->getPurchase();
		echo json_encode($records);
	}
	public function fetchPurchase($orderId = ''){
		$this->purchase_model->fetchPurchase($orderId);
	}
	public function postPurchase($orderId = ''){
		$this->purchase_model->postPurchase($orderId);
	}
	public function fetchAcknowledgement(){
		$this->purchase_model->fetchAcknowledgement();
	}
	public function fetchReceipt(){
		$this->purchase_model->fetchReceipt();
	}
	public function postReceipt(){
		$this->purchase_model->postReceipt();
	}
	public function fetchInvoice(){
		$this->purchase_model->fetchInvoice();
	}
	public function postInvoice(){
		$this->purchase_model->postInvoice();
	}
	public function postAcknowledgement(){
		$this->purchase_model->postAcknowledgement();
	}
	public function purchaseInfo($orderId = ''){
		$data['purchaseInfo'] = $this->db->get_where('purchase_order',array('orderId' => $orderId))->row_array();
		$this->template->load_template("purchase/purchaseInfo",$data,$this->session_data);
	}
	public function purchaseItem($orderId){
		$data					= array();
		$data['orderInfo']		= $this->db->get_where('purchase_order',array('orderId' => $orderId))->row_array();
		$data['address']		= $this->db->get_where('purchase_address',array('orderId' => $orderId))->row_array();
		$data['customerInfo']	= $this->db->get_where('customers',array('email' => $data['orderInfo']['customerEmail']))->row_array();
		$data['receipt']		= $this->db->get_where('purchase_dispatch',array('orderId' => $orderId))->result_array();
		$data['items']			= $this->purchase_model->getPurchaseItem($orderId);
		$this->template->load_template("purchase/purchaseItem",$data,@$this->session_data); 
	}
}