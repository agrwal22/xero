<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Credit extends MY_Controller {
	function __construct(){
		parent::__construct();			
		$this->load->model('purchase/purchasecredit_model','',TRUE);
	}
	public function index(){
		$data		= array();
		$this->template->load_template("purchase/credit",$data,$this->session_data);
	}
	public function getCredit(){ 
		$records	= $this->credit_model->getCredit();
		echo json_encode($records);
	}
	public function fetchPurchaseCredit($orderId = ''){
		$this->purchasecredit_model->fetchPurchaseCredit($orderId);
	}
	public function postPurchaseCredit($orderId = ''){
		$this->purchasecredit_model->postPurchaseCredit($orderId);
	}
	public function fetchAcknowledgement(){
		$this->purchasecredit_model->fetchAcknowledgement();
	}
	public function fetchReceipt(){
		$this->purchasecredit_model->fetchReceipt();
	}
	public function postReceipt(){
		$this->purchasecredit_model->postReceipt();
	}
	public function fetchInvoice(){
		$this->purchasecredit_model->fetchInvoice();
	}
	public function postInvoice(){
		$this->purchasecredit_model->postInvoice();
	}
	public function postAcknowledgement(){
		$this->purchasecredit_model->postAcknowledgement();
	}
	public function purchaseInfo($orderId = ''){
		$data['purchaseInfo']	= $this->db->get_where('purchase_credit_order',array('orderId' => $orderId))->row_array();
		$this->template->load_template("purchase/creditInfo",$data,$this->session_data);
	}
	public function creditItem($orderId){
		$data					= array();
		$data['orderInfo']		= $this->db->get_where('purchase_credit_order',array('orderId' => $orderId))->row_array();
		$data['address']		= $this->db->get_where('purchase_credit_address',array('orderId' => $orderId))->row_array();
		$data['customerInfo']	= $this->db->get_where('customers',array('email' => $data['orderInfo']['customerEmail']))->row_array();
		$data['receipt']		= $this->db->get_where('purchase_dispatch',array('orderId' => $orderId))->result_array();
		$data['items']			= $this->purchasecredit_model->getCreditItem($orderId);
		$this->template->load_template("purchase/creditItem",$data,@$this->session_data); 
	}
}