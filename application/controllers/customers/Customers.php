<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Customers extends MY_Controller {
	function __construct(){
		parent::__construct();			
		$this->load->model('customers/customers_model','',TRUE);
	}
	public function index(){
		$data	= array();
		$this->template->load_template("customers/customers",$data,$this->session_data);
	}
	public function getCustomers(){
		$records	= $this->customers_model->getCustomers();
		echo json_encode($records);
	}
	public function fetchCustomers($customerId = ''){
		$this->customers_model->fetchCustomers($customerId);
	}
	public function customerInfo($customerId = ''){
		$data['customerInfo']	= $this->db->get_where('customers', array('id' => $customerId))->row_array();
		$this->template->load_template("customers/customerInfo",$data);
	}
	public function postCustomers($productId = ''){
		$this->customers_model->postCustomers($productId);
	}
}