<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sales extends MY_Controller {
	function __construct(){
		parent::__construct();			
		$this->load->model('report/sales_model','',TRUE);
	}
	public function index(){
		$data		= array();
		$this->template->load_template("report/sales",$data,$this->session_data);
	}
	public function getSales(){
		$records	= $this->sales_model->getSales();
		echo json_encode($records);
	}
	public function fetchSales($orderId = ''){
		$this->sales_model->fetchSales($orderId);
	}	
	public function postSales($orderId = ''){
		$this->sales_model->postSales($orderId);
	}		
}